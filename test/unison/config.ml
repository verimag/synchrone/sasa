

let fault = Some(fun _ _ _ -> Random.int State.m)

let legitimate pidl from_pid =
  (* a legitimate configuration is reached when all states have the same values *)
  let v = fst(from_pid (List.hd pidl)) in
  List.fold_left
    (fun acc pid -> acc && fst(from_pid pid) = v)
    true
    (List.tl pidl)

let legitimate = Some legitimate
let init_search_utils = None
let potential = None
