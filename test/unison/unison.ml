(* Time-stamp: <modified the 24/07/2024 (at 11:05) by Erwan Jahier> *)
(* Synchronous unison.

   This  is  algo  4.2  in the  book  ``Introduction  to  Distributed
   Self-Stabilizing  Algorithms'' By  Altisen, Devismes,  Dubois, and
   Petit

   nb: The daemon ougth to be synchronous.  *)

open Algo

let m = State.m

let (init_state: int -> string -> 'v) =
  fun _ _ ->
    (*     Printf.eprintf "unison.ml: tossing!\n";flush stderr;  *)
    Random.int m

let new_clock_value nl clock =
  let cl = List.map (fun n -> state n) nl in
  let min_clock = List.fold_left min clock cl in
  (min_clock + 1) mod m

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun clock nl ->
    if (new_clock_value nl clock) <> clock then ["g"] else []


(*@
  function min (nl: 'v neighbor list): 'v =
  List.fold_left (fun a b -> state a <= state b)
*)
(*@
  nc = step_f c nl a
  ensures forall q. q in nl -> nc = (state q)
*)
let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun clock nl _ ->
    new_clock_value nl clock
