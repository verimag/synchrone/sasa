
type t = int
let to_string = (fun s -> Printf.sprintf "c=%i" s)

(* let m=10 (* max 2 (1+2*diameter ()) *) *)
let diameter = Algo.diameter ()
let m = max 2 (2*diameter-1)

let (of_string: (string -> t) option) =
  Some (fun s ->
      let res = Scanf.sscanf s "c=%d" (fun i -> i ) in
      res
    )


let copy x = x
let actions = ["g"]
let potential = None

let legitimate pidl from_pid =
  (* a legitimate configuration is reached when all states have the same values *)
  let v = fst(from_pid (List.hd pidl)) in
  List.fold_left
    (fun acc pid -> acc && fst(from_pid pid) = v)
    true
    (List.tl pidl)


let legitimate = Some legitimate
