open Algo

let legitimate = None
let fault = None
let potential = Some Potential.f


let n2s nl _s =
  match nl with
  | [I(_, i, _)] ->  i
  | _ -> assert false
let s2vl i = [I (min 0 (i-1), i, i+1)]
let init_search_utils = Some(s2vl, n2s)
