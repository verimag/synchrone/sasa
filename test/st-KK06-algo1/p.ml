(* Time-stamp: <modified the 27/02/2023 (at 10:45) by Erwan Jahier> *)

(*
 A Self-stabilizing Algorithm for Finding a Spanning Tree in a Polynomial Number of Moves
 Adrian Kosowski and Lukasz Kuszner
 PPAM 2005, LNCS 3911, pp. 75–82, 2006.

Hyp:
- distributed daemon
- connected un-directed graph

 *)

open Algo

let (init_state: int -> string -> 'st) =
  fun _nl _ -> (Random.int 10)


let min_n nl = (* returns the min of the neigbhors *)
  List.fold_left
    (fun acc n -> if acc < state n then acc else state n)
    (state (List.hd nl))
    (List.tl nl)

let max_n nl = (* returns the max of the neigbhors *)
  List.fold_left
    (fun acc n -> if acc > state n then acc else state n)
    (state (List.hd nl))
    (List.tl nl)

let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun e nl ->
  if (*  R: v <> root ∧ f (v) ≤ min_u∈N(v) f (u)
             --->  f (v) = max_u∈N(v) f (u) + 1 *)
    e <= min_n nl
  then  ["R"]
  else  []


let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun e nl ->
    function
    | "R" -> 1+ max_n nl
    | _ -> e
