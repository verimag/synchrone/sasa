(*
nb: page 3 of the paper, there are 2 errors in the definition of S_T:

    S_T(v) = Somme((w,w') \in E([r,v]_T) , max (f(w')-f(w'), 0)

should be

    S_T(v) = Somme((w,w') \in E([r,v]_T) , max (min(1,f(w')-f(w)), 0)

to match the numbers provided in Fig 1

Anyway, so that I have a decreasing and positive potential, I use

   pot = Sum_v d_T(root,v) - Sum_v S_T(v)
 *)

(* the  first time  d_T is  called, a  spanning tree  (bfs ?)  of the
   current topology is computed *)

module StrMap = Map.Make(String)
type sp_tree =  string list StrMap.t

(* To mark visited nodes *)
module StrSet = Set.Make(String)

let (mark : StrSet.t -> string -> StrSet.t) =
  fun s x -> (* Printf.printf "%s is visited\n%!" x; *)
  StrSet.add x s

let refT : sp_tree ref = ref StrMap.empty

let compute_sp_tree root get =
  let get_pidl pid = get pid |> snd |> List.split |> snd in
  let rec (f: sp_tree ->  string -> (string * string list) list -> StrSet.t ->
           sp_tree * StrSet.t) =
    fun res pid tovisit visited ->
    match tovisit with
      | [] -> res, visited
      | (_,[])::[] -> res, visited
      | (_,[])::((npid,nl)::nll) -> (
        (*  let nl = List.filter (fun n -> not (StrSet.mem n visited)) nl in *)
        let res = StrMap.add npid nl res in
        f res npid ((npid,nl)::nll) visited
      )
      | (npid, n::nl)::nll ->
        assert (pid = npid);
        let children = get_pidl n |> List.filter (fun n -> not (StrSet.mem n visited)) in
        let visited = List.fold_left mark visited children in

        f res npid ((npid, nl)::nll @ [(n, children)]) visited
  in
  let root_children = get_pidl root in
  let visited =  List.fold_left mark (mark StrSet.empty root) root_children in
  let res = fst (f StrMap.empty root ((root,[])::[root, root_children]) visited) in
  (*   StrMap.iter (fun n cl -> Printf.printf "%s->[%s]\n%!" n (String.concat "," cl)) res; *)
  res

(* test *)
let grid3x3 = function
  | "n1" -> 0, [0,"n2"; 0, "n4"]
  | "n2" -> 0, [0,"n1"; 0, "n3"; 0, "n5"]
  | "n3" -> 0, [0,"n2"; 0, "n5"; 0, "n6"]
  | "n4" -> 0, [0,"n1"; 0, "n5"; 0, "n7"]
  | "n5" -> 0, [0,"n4"; 0, "n2"; 0, "n6"; 0, "n8"]
  | "n6" -> 0, [0,"n3"; 0, "n5"; 0, "n9"]
  | "n7" -> 0, [0,"n4"; 0, "n8"]
  | "n8" -> 0, [0,"n7"; 0, "n5"; 0, "n9"]
  | "n9" -> 0, [0,"n8"; 0, "n6"]
  | _ -> assert false

let b =
  let t = compute_sp_tree "n1" grid3x3 in
  StrMap.bindings t


let get_sp_tree root get =
  if StrMap.is_empty !refT then
    let t = compute_sp_tree root get in
    refT := t; t
  else
    !refT

let d_T t n1 n2 =
  let rec f cpt top =
    if top = n2 then Some cpt else
      match StrMap.find_opt top t with
      | None -> None
      | Some cl ->
        List.fold_left
          (fun acc c ->
             match acc with
             | Some res -> Some res
             | None -> f (cpt+1) c
          )
          None
          cl
  in
  match f 0 n1 with
  | None -> assert false
  | Some res -> res

let sum = List.fold_left (+) 0

let s_T get t root v =
  let rec enumerate_path acc top =
    if top = v then Some acc else
      match StrMap.find_opt top t with
      | None -> None
      | Some cl ->
        List.fold_left
          (fun res c ->
             match res with
             | Some res -> Some res
             | None -> enumerate_path ((top,c)::acc) c
          )
          None
          cl
  in
  let f w = fst (get w) in
  let paths_opt : (string * string) list option = enumerate_path [] root in
  match paths_opt with
  | None -> assert false
  | Some paths ->
    List.map (fun (w, w') -> max 0 (min 1 (f w' - f w))) paths
    |>
    (* (fun l -> Printf.printf "s_T(%s)=%s\n%!" v (String.concat "+" (List.map string_of_int l)); sum l) *)
    sum

let f pidl get =
  let root, pidl =
    match pidl with
    | [] -> failwith "no root found"
    | pid::pidl ->  pid, pidl (* ZZZ Convention: the first one is the root *)
  in
  let t:sp_tree = get_sp_tree root get in
  let d_T = d_T t in
  let s_T = s_T get t root in
  List.map (fun v ->
      (* Printf.printf "d_T(%s, %s)=%d ; s_T(%s) = %d   \n%!" root v  (d_T root v) v (s_T v); *)
      d_T root v - s_T v
    ) pidl |> sum |> float_of_int
