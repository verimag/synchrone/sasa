# Time-stamp: <modified the 15/03/2024 (at 16:36) by Erwan Jahier>
#
# Define some default rules that ought to work most of the time
#

sasa=sasa

LIB=-package algo

ifndef DAEMON
# some rules uses this one
DAEMON=
endif



# obselete: use 'dune build' instead
#%.ml: %.dot
# 	sasa -reg $<

# ifndef OCAMLOPT
# 	OCAMLOPT=ocamlfind ocamlopt -bin-annot
# endif
#%.cmxs: %.ml
#	$(OCAMLOPT) $(LIB) -shared state.ml $(shell sasa -algo $*.dot) config.ml $< -o $@
# ifndef OCAMLC
# 	OCAMLC=ocamlfind ocamlc -bin-annot
# endif
# %.cma: %.ml
# 	$(OCAMLC) $(LIB) -a state.ml $(shell sasa -algo $*.dot) $< -o $@

%.cmxs:
	[ -f $*.ml ] && rm $*.ml || true
	topology=$* dune build
%.cma:
	[ -f $*.ml ] && rm $*.ml || true
	topology=$* dune build
%.cmxa:
	[ -f $*.ml ] && rm $*.ml || true
	topology=$* dune build
%.a:
	[ -f $*.ml ] && rm $*.ml || true
	topology=$* dune build


genclean:
	rm -f *.cmxs sasa *.cm* *.o *.pdf *.rif *.gp *.log *.dro *.seed *.c *.h sasa-*.dot *.cmt *.annot
	rm -rf .rdbg-session*.ml rdbg-session*.ml luretteSession* a.out *.cov .read_dot.ml
	rm -f *.exec  grid*.ml  rdbg-history .rdbg-cmds.ml rdbg-cmds.ml my-rdbg-tuning.ml
	dune clean

clean: genclean
.PRECIOUS: %.dot

##################################################################################
#

g:gnuplot
s:sim2chrogtk


%.lut: %.dot %.cmxs
	sasa -gld $< || echo "==> ok, I'll use the existing $@ file"

%_oracle.lus: %.dot %.cmxs
	sasa -glos $< || echo "==> ok, I'll use the existing $@ file"

%.rif: %.cmxs %.dot
	$(sasa) $(sasaopt) $*.dot > $*.rif

%.pdf:
	dot -Tpdf $*.dot -o $*.pdf
	xpdf $*.pdf

%.ocd: %.ml
	rdbg -camldebug -sut "sasa $.dot" -l 1000

%.test: %.dot %.cmxs
	sasa  $<

############################################################################################
# Golden master tests
# compare the generated rif file with a previously generated reference
EXPDIR=`rdbg --ocaml-version`
$(EXPDIR):
	[ -d $(EXPDIR) ] || (mkdir -p $(EXPDIR) ; make utest)

%.gm_test: %.cmxs $(EXPDIR)
	sasa  $*.dot -seed 42 -l 1000 > $*.rif && \
	diff -I "# on" -I " version " -B -u -i -w $(EXPDIR)/$*.rif.exp $*.rif > $*.res
	[ ! -s $*.res ]  && echo "\n\n==> $*.gm_test went fine"

# update the reference
%.ugm_test: %.cmxs $(EXPDIR)
	sasa  $*.dot -seed 42 -l 1000 > $*.rif && \
	cp $*.rif $(EXPDIR)/$*.rif.exp

# fix the seed (used, e.g., in Makefile.dot) for the non-regression tests
ifndef SEED
  SEED = --seed 42
endif
############################################################################################

%.rdbg: %.cma
	ledit -h rdbg-history -x rdbg --sasa -sut "sasa $* $(DAEMON)"  -l 10000

%.rdbg-test: %.cma
	rdbg --sasa -sut "sasa $*.dot $(DAEMON)" -go --input some_session -l 10000 \
		| grep Error > $*.rdbg-test.log && echo "Error: 'make $@' failed" \
		  && cat $*.rdbg-test.log && exit 2  || echo "'make $@' suceeds"

%.rdbgui: %.cma
	ledit -h rdbg-history -x rdbgui4sasa -sut "sasa $*.dot"  -l 10000

%.simu:
	make $*.rdbgui
