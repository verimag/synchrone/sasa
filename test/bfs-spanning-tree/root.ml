(* Time-stamp: <modified the 16/01/2023 (at 17:07) by Erwan Jahier> *)

(* This is algo 5.3 in the book *)

open Algo
open State

let (init_state: int -> string -> State.t) =
  fun _i _ ->
    {
      d = Random.int d;
      par = -1;
    }

let (enable_f: State.t -> State.t neighbor list -> action list) =
  fun st _nl ->
    if st.d <> 0 then ["CD"] else []

let (step_f : State.t -> State.t neighbor list -> action -> State.t) =
  fun st _nl ->
    function  | _ -> { st with d = 0 }
