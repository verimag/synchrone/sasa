
let d=Algo.diameter()

type t = {
  d:int;
  par:int;
}

let (to_string: (t -> string)) =
  fun s ->
    Printf.sprintf "d=%d par=%d" s.d s.par

let (of_string: (string -> t) option) =
  Some (fun s ->
    Scanf.sscanf s "d=%d par=%d" (fun d par -> {d = d; par = par}))

let (copy : ('v -> 'v)) = fun x -> x

let actions = ["CD";"CP"]
let potential = None
let legitimate = None
let fault = None 
