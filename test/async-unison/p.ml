(* Time-stamp: <modified the 26/05/2023 (at 16:58) by Erwan Jahier> *)

(* Bouded unison, cf Section 5 in:

     Couvreur,  J., Francez, N.,  and Gouda, M. G.  (1992) Asynchronous
     unison (extended abstract). Proceedings  of the 12th International
     Conference on Distributed Computing Systems, Yokohama, Japan, June
     9-12, pp.  486–493. IEEE Computer Society.

*)
open Algo

let n = Algo.card()
let k = n * n + 1

let (init_state: int state_init_fun) = fun _n _ -> (Random.int k)

(* Make sure modulo returns positive numbers (the ocaml mod does not) *)
let modulo x n = x mod n |> fun x -> if x < 0 then n+x mod n else x

let behind pc qc = (modulo (qc-pc) k) <= n
let far pc qc = (not (behind pc qc)) && (not (behind qc pc))

let (enable_f: int enable_fun) =
  fun c nl ->
    if List.for_all (fun q -> behind c (state q)) nl then ["I(p)"] else
    if List.exists  (fun q -> far c (state q)) nl && c <> 0 then ["R(p)"] else []

let (step_f: int step_fun) =
  fun c _nl a ->
    match a with
    | "I(p)" -> modulo (c + 1) k
    | "R(p)" -> 0
    | _      -> assert false
