open Algo

let is_close s1 s2 = not (P.far s1 s2)

(* A configuration is legitimate when all neigbhors are close *)
let all_true = List.fold_left (&&) true

let legitimate pidl get =
  let all_close (s, nl) = List.for_all (fun (n,_) -> is_close s (state n)) nl in
  List.map (fun pid -> all_close (get pid)) pidl |> all_true

let legitimate = Some legitimate

let fault = None
let n2s nl _s =
  match nl with
  | [I(_, i, _)] ->  i
  | _ -> assert false
let s2vl i = [I (min 0 (i-1), i, max P.k (i+1))]

let init_search_utils = Some(s2vl, n2s)

let sumi l = List.fold_left (+) 0 l
let sumf l = List.fold_left (+.) 0. l
let count b = if b then 1 else 0

(* It's actually only a  pseudo-potential function (i.e., it measures
   a distance  to the set of  stable configurations, but that  is not
   strictly decreasing), which is ok for simulation purposes.  *)
let _pseudo_pot pidl get =
  let count_behind  (clock, nl) =
    let nl = fst (List.split nl) in
    List.map (fun n -> P.behind clock (state n)) nl |> List.map count |> sumi
  in
  List.map (fun pid -> count_behind(get pid)) pidl |> sumi |> float_of_int

let count b = if b then 0. else 1.


let count_far (s, nl) =
  List.fold_left (fun acc (n,_) -> acc +. count (is_close s (state n))) 0. nl

let count_edges pidl get =
  (* count the number of edges that are out of sync *)
  List.map (fun pid -> count_far (get pid)) pidl |> sumf

let count_nodes pidl get =
  (* count the number of nodes that are out of sync *)
  List.map (fun pid -> count_far (get pid)) pidl |> List.map (min 1.0) |> sumf


(*  *)

(* let potential = Some count_edges *)
let potential = Some count_nodes
