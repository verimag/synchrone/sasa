
type t = int
let to_string = (fun s -> Printf.sprintf "c=%i" s)
let copy x = x
let actions = ["I(p)"; "R(p)"]

let (of_string: (string -> t) option) =
  Some (fun s ->
      try
        Scanf.sscanf s "c=%i"
          (fun c -> c)
      with _ -> assert false
    )
