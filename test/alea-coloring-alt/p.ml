(* Time-stamp: <modified the 17/01/2023 (at 14:41) by Erwan Jahier> *)

(* A variant of test/alea-coloring:

   Algo 3.3.1 (page 16) of Self-stabilizing Vertex Coloring of Arbitrary Graphs
   by Maria Gradinariu and Sebastien Tixeuil

   http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.38.8441&rep=rep1&type=pdf
*)

open Algo
let k=max_degree () + 1

let (init_state: int -> string -> 'v) = fun _i _ -> 0
(* Random.int k *)

(* if free is sorted (from max to min), so is the result *)
let rec insert i free =
  match free with
    [] -> [i]
  | x::t -> if i > x then i::free else x::(insert i t)

(* Returns the free colors is descending order (n.log(n)) *)
let (free : 'v neighbor list -> 'v list) = fun nl ->
  let n_colors = List.map (fun n -> state n) nl in (* neighbor colors *)
  let n_colors = (* no duplicate, in ascending order *)
    List.sort_uniq compare n_colors in
  let rec aux free n_colors i =
    (* for i=0 to k-1, put i in free if not in n_colors *)
    if i > k then free else
      (match n_colors with
       | x::tail ->
         if x = i then aux free tail (i+1) else aux (insert i free) n_colors (i+1)
       | [] -> aux (insert i free) n_colors (i+1)
      )
  in
  let res = aux [] n_colors 0 in
  List.rev res

let agree_i ri nl =
  ri = List.hd (free nl)

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun ri nl ->
    if not (agree_i ri nl) then ["conflict"] else []

let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun e nl _ ->
  if (Random.bool ()) then e else (List.hd (free nl))
