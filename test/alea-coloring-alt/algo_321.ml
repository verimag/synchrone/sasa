(* Time-stamp: <modified the 16/04/2020 (at 17:16) by Erwan Jahier> *)

(* A variant of test/alea-coloring:

   Algo 3.2.1 (page 14) of Self-stabilizing Vertex Coloring of Arbitrary Graphs
   by Maria Gradinariu and Sebastien Tixeuil

   http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.38.8441&rep=rep1&type=pdf
*)

open Algo

let b=max_degree ()
  
open State
    
let (init_state: int -> string -> 'v) =
  fun k id -> { id = id ; r = Random.int k }

let color s = (state s).r

let free_color cl = (* returns the max color that is not in cl (which is sorted) *)
  let rec f c cl =
    match cl with
      [] -> c
    | x::t ->
      if c > x then c (* x is the max, so c is free *)
      else if c = x then f (c-1) t (* c is not free *)
      else assert false (* should not occur *)
  in
  f b cl

(* returns the list of used colors *)
let (used_colors : 'v neighbor list -> int list) = fun nl -> 
  let cl = List.map (fun n -> color n) nl in
 
  List.sort_uniq (fun x y -> compare y x) cl

(* cl is sorted, so the max is at the head *)
let agree i cl = i = free_color cl

let max_list = function
  | [] -> assert false (* should not occur *)
  | h :: t -> List.fold_left max h t  

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun c nl ->
  let cl = used_colors nl in
  let agree_i = agree c.r cl in
  let nl_same_color = List.filter (fun n -> c.r = color n) nl in
  if not agree_i && nl_same_color <> [] && c.id > (state (max_list nl_same_color)).id 
  then ["C1"] else if
    not agree_i && nl_same_color = []
  then ["C2"] else []

  
let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun e nl ->
  function
  | "C1" | "C2" -> { e with r = free_color (used_colors nl) }
  | _ -> e
           



