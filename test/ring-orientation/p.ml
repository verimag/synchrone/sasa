(* Time-stamp: <modified the 21/03/2024 (at 17:12) by Erwan Jahier> *)

(* Uniform deterministic self-stabilizing ring-orientation on odd-length rings
  Hoepman, Jaap-Henk
  WDAG'1994
  https://core.ac.uk/download/pdf/301654088.pdf

  hyp: central daemon
 *)

open Algo
open State


let at_the_edge pid = (* relies on the dot naming convention (we take the ones of gg) *)
  pid = "p0" || pid = (Printf.sprintf "p%d" (card() -1))

let (init_state: int -> string -> 'st) =
  fun _nl pid ->
  { color_0 = Random.bool () ; phase_pos = Random.bool () ; orient_left =  Random.bool () ;
    rank_p_0 = not (at_the_edge pid) }

let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun q nl ->
  assert (card() mod 2 = 1); (* Odd rings only *)
    match List.map state nl, q.rank_p_0 with
    |  [r; p], false
    |  [p; r], true ->
      (match p,q,r with
        | { color_0 = true  ; _ }, { color_0 = true  ; _ },                    { color_0 = true  ; _ } -> ["a"]
        | { color_0 = true  ; _ }, { color_0 = false ; phase_pos = true ; _ }, { color_0 = true  ; _ } -> ["b"]
        | { color_0 = false ; _ }, { color_0 = false ; _ },                    { color_0 = false ; _ } -> ["c"]
        | { color_0 = false ; _ }, { color_0 = true  ; phase_pos = true; _  }, { color_0 = false ; _ } -> ["d"]

        | { color_0 = true  ; phase_pos = true ; _ },
          { color_0 = true  ; phase_pos = false ; _ },
          { color_0 = false ; phase_pos = false ; _ }  -> ["e"]

        | { color_0 = false ; phase_pos = false ; _ },
          { color_0 = true  ; phase_pos = false ; _ },
          { color_0 = true  ; phase_pos = true ; _ } -> ["e"]

        | { color_0 = false ; phase_pos = true ; _ },
          { color_0 = false ; phase_pos = false ; _ },
          { color_0 = true  ; phase_pos = false ; _ } -> ["f"]

        | { color_0 = true  ; phase_pos = false ; _ },
          { color_0 = false ; phase_pos = false ; _ },
           { color_0 = false ; phase_pos = true ; _ } -> ["f"]

        | { color_0 = true  ; phase_pos = false ; _ },
          { color_0 = true  ; phase_pos = false ; _ },
          { color_0 = false ;  _ }
        | { color_0 = false ;  _ },
          { color_0 = true  ; phase_pos = false ; _ },
          { color_0 = true  ; phase_pos = false ; _ } -> ["g"]

        | { color_0 = true  ; phase_pos = true ; _ },
          { color_0 = true  ; phase_pos = true ; _ },
          { color_0 = false ;  _ }
        | { color_0 = false ;  _ },
          { color_0 = true  ; phase_pos = true ; _ },
          { color_0 = true  ; phase_pos = true ; _ } -> ["h"]

        | { color_0 = false  ; phase_pos = false ; _ },
          { color_0 = false  ; phase_pos = false ; _ },
          { color_0 = true ; _ }
        | { color_0 = true ; _ },
          { color_0 = false  ; phase_pos = false ; _ },
          { color_0 = false  ; phase_pos = false ; _ } -> ["i"]

        | { color_0 = false  ; phase_pos = true ; _ },
          { color_0 = false  ; phase_pos = true ; _ },
          { color_0 = true ; _ }
        | { color_0 = true ; _ },
          { color_0 = false  ; phase_pos = true ; _ },
          { color_0 = false  ; phase_pos = true ; _ } -> ["j"]

        | _ -> []
      )
    | _ -> failwith "only works on rings"

(*        | { color_0 = cp ; phase_pos = pp ; _ },
          { color_0 = cq ; phase_pos = pq ; _ },
          { color_0 = cr ; phase_pos = pr ; _ } ->
           let c2s c = if c then "0" else "1" in
          let p2s c = if c then "+" else "-" in
          failwith (Printf.sprintf "Missing case: %s%s %s%s %s%s "
                      (c2s cp) (p2s pp) (c2s cq) (p2s pq) (c2s cr) (p2s pr) )

 *)

let orientation = function
  | [  { phase_pos = true ; _} ; { phase_pos = false; _ }  ] -> true
  | [  { phase_pos = false ; _} ; { phase_pos = true ; _ } ] -> false
  | [  { phase_pos = true ; _} ; { phase_pos = true; _ } ] -> assert false
  | [  { phase_pos = false ; _} ; { phase_pos = false; _ } ] -> assert false
  | _ -> assert false

let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun e nl ->
  let may_rev l = if e.rank_p_0 then l else List.rev l in

  function
  | "a" -> { e with color_0 = false ; phase_pos = false }
  | "b" -> { e with color_0 = false ; phase_pos = false }
  | "c" -> { e with color_0 = true ; phase_pos = false }
  | "d" -> { e with color_0 = true ; phase_pos = false }

  | "e" -> { e with color_0 = false ; phase_pos = true ; orient_left = orientation (List.map state nl |> may_rev) }
  | "f" -> { e with color_0 = true ; phase_pos = true  ; orient_left = orientation (List.map state nl |> may_rev) }

  | "g" -> { e with color_0 = true ; phase_pos = true }
  | "h" -> { e with color_0 = true ; phase_pos = false }
  | "i" -> { e with color_0 = false ; phase_pos = true }
  | "j" -> { e with color_0 = false ; phase_pos = false }
  | _ -> e
