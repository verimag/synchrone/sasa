(* Time-stamp: <modified the 20/03/2024 (at 17:10) by Erwan Jahier> *)

type t = { color_0 : bool ; phase_pos : bool ; orient_left : bool ; rank_p_0 : bool}
(*  the rank of p is 0 in the neighbors array:  neighbors[0] is p and neighbors[1] is q *)
(*  the rank terminology comes from page 3 of the Hoepmann's TR *)


let to_string s =
  Printf.sprintf "color_0=%b phase_pos=%b orient_left=%b rank_p_0=%b" s.color_0 s.phase_pos s.orient_left s.rank_p_0

let (of_string: (string -> t) option) =
  Some (fun s ->
      Scanf.sscanf s "color_0=%B phase_pos=%B orient_left=%B rank_p_0=%B"
        (fun color_0 phase_pos orient_left rank_p_0 -> { color_0 ; phase_pos ; orient_left ; rank_p_0 }))

let copy x = x

let actions = ["a";"b";"c";"d";"e";"f";"g";"h";"i";"j"]
