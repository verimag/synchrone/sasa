(* Time-stamp: <modified the 17/01/2023 (at 14:41) by Erwan Jahier> *)

(* cf Collin-Dolex-94 *)

open Algo
open State

let (init_state: int -> string -> State.t) =
  fun i _ ->
    {
      path = Array.make delta (-1)  (* XXX put random values here too! *);
      par  = try Random.int i with _ -> assert false
    }

let str_of_array a =
  let l = List.map State.to_string (Array.to_list a) in
  "["^(String.concat "," l)^"]"

let min_path al =
      match al with
      | [] -> assert false
      | x::t -> List.fold_left min x t

let (get_paths: 'v neighbor list -> int array list) =
  fun nl ->
    List.mapi (fun _alpha_i n -> (state n).path) nl

(* The index of the first negative elt in [a] starting from 1. Returns
   the size of the [a] if none is found *)
let end_of_a a =
  let s = Array.length a in
  let rec aux a i =
    if i=s then s else
    if a.(i) < 0 then i else aux a (i+1)
  in
  let res = aux a 1 in
  res
let _ = (
  assert (end_of_a [|-1;-1;-1;-1|] = 1);
  assert (end_of_a [|1;1;1;-1;-1|] = 3);
)

(* concat or truncate:
   concat [i;j;k;-1;...] l = [i;j;k;l;-1;...]
   concat [i;j;k;] l = [j;k;l]
 *)
let (concat_path : int array -> int -> int array) =
  fun a alpha ->
    let a = Array.copy a in (* ouch! Bad things happen without this copy *)
    let s = Array.length a in
    let last = end_of_a a in
    if last = s then ( (* truncate *)
      for i = 1 to s-1 do
        a.(i-1) <- a.(i)
      done;
      a.(s-1) <- alpha
    )
    else (* concat *)
      a.(last) <- alpha;
    a
let _ = (
  assert (concat_path [|-1;-1|] 10 = [|-1;10|]);
  assert (concat_path [|-1;6;4;-1;-1|] 10 = [|-1;6;4;10;-1|]);
  assert (concat_path [|-1;6;4;11;12|] 10 = [|6;4;11;12;10|])
  )

let array_to_string a = String.concat ";" (List.map string_of_int (Array.to_list a))

let equals_up_to p1 p2 i =
  let res = ref true in
  for j = 0 to i do
    res := !res && p1.(j) = p2.(j) ;
  done;
  !res
let _ = (
  assert (     equals_up_to [|-1;6;4;11;12|] [|-1;6;4;110000;12|] 2);
  assert (not (equals_up_to [|-1;6;4;11;12|] [|-1;6;4;110000;12|] 3));
)

let (compute_parent : t neighbor list -> int array -> int) =
  fun nl p ->
    (* The parent of a process p is the neighbor which path is a
       subpath of p *)
    let last_p = end_of_a p in
    let paths = List.mapi (fun i n -> (state n).path, i) nl in
    let _,parent =
      match
        List.find_opt
          (fun (npath, _) ->
             let last_n = end_of_a npath in
             last_n = last_p-1 && equals_up_to npath p (last_n-1)
          )
          paths
      with
        Some p -> p
      | None -> [||],(-1) (* may happen before convergence *)
    in
    parent

let compute_path nl _e =
  let paths = get_paths nl in
  let paths = List.map2 (fun p n -> concat_path p (reply n)) paths nl in
  let path = min_path paths in
  path

let (enable_f: t -> t neighbor list -> action list) =
  fun v nl ->
    let path = compute_path nl v in
    if path <> v.path then ["update_path"] else
    if compute_parent nl path <> v.par then ["compute_parent"]
    else []

let (step_f : t -> t neighbor list -> action -> t) =
  fun v nl ->
    function
    | "update_path" ->
      let path = compute_path nl v in
      { v with path = path }
    | "compute_parent" ->
      let path = compute_path nl v in
      { v with par = compute_parent nl path }
    | _ -> assert false
