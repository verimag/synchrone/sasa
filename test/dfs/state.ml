
let delta=Algo.card()
            
type t = {
  path: int array;
  par: int
}

(* For big graphs, printing the path results in huge (size(g)^2) rif files *)
let (to_string_full: t -> string) =
  fun v -> 
  Printf.sprintf "%s par=%i"
    (snd (Array.fold_left
            (fun (i,acc) x -> i+1,Printf.sprintf "%s path%d=%d" acc i x) (0,"") v.path))
    v.par

(* hide the path *)
let (to_string_par: t -> string) =
  fun v -> 
  Printf.sprintf "par=%i" v.par

let (to_string: t -> string) =
  if Algo.card() > 1000 then to_string_par else to_string_full
  
let (copy : t -> t) =
  fun x -> { x with path = Array.copy x.path }

let of_string = None
let actions = ["update_path";"compute_parent"]
let potential = None
let legitimate = None
let fault = None 
