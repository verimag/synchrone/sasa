(* Time-stamp: <modified the 17/01/2023 (at 14:42) by Erwan Jahier> *)

(* cf Collin-Dolex-94 *)

open Algo
open State

let (init_state: int -> string -> State.t) =
  fun _i _ ->
    {
      path = Array.make delta (-1);
      par  = -10
    }

let (enable_f: t -> t neighbor list -> action list) =
  fun v _nl ->
    if v.path = (Array.make delta (-1)) then [] else ["update_path"]


let (step_f : t -> t neighbor list -> action -> t) =
  fun v _nl ->
    function
    | "update_path" ->  { v with path = Array.make delta (-1) }
    | _ -> assert false
