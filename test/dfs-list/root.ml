(* Time-stamp: <modified the 17/01/2023 (at 14:43) by Erwan Jahier> *)

(* cf Collin-Dolex-94 *)

open Algo
open State

let (init_state: int -> string -> 'v) =
  fun _i _ ->
    {
      path = [-1];
      par  = -10
    }

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun v _nl ->
    if v.path = [-1] then [] else ["update_path"]


let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun v _nl ->
    function
    | "update_path" ->  { v with path = [-1] }
    | _ -> assert false
