(* Time-stamp: <modified the 17/01/2023 (at 14:44) by Erwan Jahier> *)

(* cf Collin-Dolex-94 *)

open Algo
open State

let (init_state: int -> string -> 'v) =
  fun i _ ->
    {
      path = [-1]; (* XXX put random values here too! *)
      par  = try Random.int i with _ -> assert false
    }

let min_path al =
      match al with
      | [] -> assert false
      | x::t -> List.fold_left min x t

let (get_paths: 'v neighbor list -> int list list) =
  fun nl ->
    List.map (fun n -> (state n).path) nl

(* concat or truncate *)
let (concat_path : int list -> int -> int list) =
  fun l alpha ->
  let l = if List.length l = delta then (List.tl l) else l
  in
    l@[alpha]

let is_sub_list l1 l2 = (List.rev (List.tl (List.rev l2))) = l1

let (compute_parent : State.t neighbor list -> int list -> int) =
  fun nl p ->
    (* The parent of a process p is the neighbor which path is a
       subpath of p *)
    let paths = List.mapi (fun i n -> (state n).path, i) nl in
    let _,parent =
      match
        List.find_opt
          (fun (npath, _) ->
             is_sub_list npath p
          )
          paths
      with
        Some p -> p
      | None -> [],(-1) (* may happen before convergence *)
    in
    parent

let compute_path nl _e =
  let paths = get_paths nl in
  let paths = List.map2 (fun p n -> concat_path p (reply n)) paths nl in
  let path = min_path paths in
  path

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun v nl ->
    (*
    Printf.eprintf " compute_path %d \n" (List.length nl)  ;  flush stderr;
    let p_str = array_to_string v.path in
    Printf.eprintf " p=[%s] parent=%d \n" p_str v.par ;
    List.iter (fun n ->
        let n_str = array_to_string (state n).path in
        Printf.eprintf "    n=[%s] parent=%d\n" n_str (state n).par
      ) nl ; flush stderr;
   *)
  let path = compute_path nl v in
    if path <> v.path then ["update_path"] else
    if compute_parent nl path <> v.par then ["compute_parent"]
    else []

let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun v nl ->
    function
    | "update_path" ->
      let path = compute_path nl v in
      { v with path = path }
    | "compute_parent" ->
      { v with par = compute_parent nl v.path }
    | _ -> assert false
