
let delta=Algo.card()
type t = {
  path: int list;
  par: int
}

let string_of_int_list l =
  let n = List.length l in
  let rec f i acc =
    if i = delta then acc else
      (* for RIF tools, we need to always output the same number of values *) 
      f (i+1) (Printf.sprintf "%s path%d=%d" acc i (if i<n then List.nth l i else -1))
  in
  f 0 ""

let to_string_full v = Printf.sprintf "%s par=%i" (string_of_int_list v.path) v.par
let to_string_par v = Printf.sprintf "par=%i" v.par
let to_string = if Algo.card() > 1000 then to_string_par else to_string_full
let of_string = None
let copy x = x
let actions = ["update_path";"compute_parent"]
let potential = None
let legitimate = None
let fault = None 
