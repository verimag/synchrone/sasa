```sh
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa.git
cd sasa/test
```

The [test](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/tree/master/test) directory contains various examples of self-stabilizing distributed programs taken from the book \`\`Introduction to Distributed Self-Stabilizing Algorithms'' By Altisen, Devismes, Dubois, and Petit.

1.  `test/skeleton/`: a fake algorithm meant to be used as a skeleton
2.  `test/dijkstra-ring/`: Dijkstra token ring
3.  `test/unison/`: Synchronous unison
4.  `test/coloring/`: a graph coloring algorithm
5.  `test/alea-coloring/`: a randomized variant of the previous one
6.  `test/bfs-spanning-tree/`: a Breadth First Search Spanning tree construction

It also contains implementations of algorithms found in the literature:

1.  `test/async-unison/`: Asynchronous unison ("Asynchronous unison" by Couvreur, J., Francez, N., and Gouda, M. G. in 1992)
2.  `test/st-CYH91`: another Spanning tree construction ("A self-stabilizing algorithm for constructing spanning trees" by Chen, Yu, and Huang in 1991)
3.  `test/bfs-st-HC92`: another BFS Spanning tree construction ("A self-stabilizing algorithm for constructing breadth-first trees" by Huang and Chen in 1992)
4.  `test/st-KK06_algo1` and `test/st-KK06_algo2`: another Spanning tree construction ("A Self-stabilizing Algorithm for Finding a Spanning Tree in a Polynomial Number of Moves" by Kosowski and Kuszner, 2006)
5.  `test/dfs/`: a Depth First Search using arrays (the \`\`atomic state model'' version of a [Depth First Search algorithm proposed by Collin and Dolev in 1994](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.57.1100&rep=rep1&type=pdf))
6.  `test/dfs-list/`: the same Depth First Search, but using lists instead or arrays
7.  `test/rsp-tree/`: The Algorithm 1 of "Self-Stabilizing Disconnected Components Detection and Rooted Shortest-Path Tree Maintenance in Polynomial Steps" by Stéphane Devismes, David Ilcinkas, Colette Johnen.

Each directory contains working examples, which are checked using the Continuous Integration facilities of Gitlab (cf the [.gitlab-ci.yml](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/tree/master/.gitlab-ci.yml) script and the [CI pipelines](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/pipelines) results).

If you want to reproduce or understand what those non-regression tests do, uo can have a look at the `test/*/Makefile` files, present in each directory (which all include the [test/Makefile.inc](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/tree/master/test/Makefile.inc)and the [test/Makefile.dot](https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/tree/master/test/Makefile.dot) ones).

The `test` directory also contains sub-directories which gather programs shared by all examples:

-   `test/lustre/`: contains Lustre programs used as (`test/*/*.lus`) oracles
-   `test/rdbg-utils/`: contains `ocaml` functions that can be used from `rdbg`