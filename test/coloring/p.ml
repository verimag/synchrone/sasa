(* Time-stamp: <modified the 24/07/2024 (at 11:02) by Erwan Jahier> *)
(*  This  is algo  3.1  in  the  book ``Introduction  to  Distributed
   Self-Stabilizing  Algorithms'' By  Altisen, Devismes,  Dubois, and
   Petit *)

open Algo
let k = if is_directed () then
          failwith "this algo is for non-directed graphs!"
        else
          max_degree () + 1

let (init_state: int -> string -> 'v) = fun i _ -> Random.int i

let (colors : 'v neighbor list -> 'v list) = fun nl ->
  List.map (fun n -> state n) nl

(* Returns the free colors is ascending order (n.log(n))
nb: suppose that all states are in [0;k-1]
*)
let (free : 'v neighbor list -> 'v list) = fun nl ->
  assert (List.for_all (fun n -> state n < k) nl); (* all colors should be < k *)
  let comp_neg x y = - (compare x y) in
  let n_colors = List.map (fun n -> state n) nl in (* neighbor colors *)
  let n_colors = (* neighbor colors, no duplicate, in descending order *)
    List.sort_uniq comp_neg n_colors in
  let rec aux free n_colors i =
    (* for i=k-1 to 0, put i in free if not in n_colors *)
    if i < 0 then free else
      (match n_colors with
       | x::tail ->
         if x = i then aux free tail (i-1) else aux (i::free) n_colors (i-1)
       | [] -> aux (i::free) n_colors (i-1)
      )
  in
  aux [] n_colors (k-1)

let (enable_f: int -> int neighbor list -> action list) =
  fun c nl ->
  if List.exists (fun n -> state n = c) nl then ["conflict"] else []


(*@ nc = step c nl a
  ensures forall n. n in nl -> (state n) <> nc /\ (0 <= c < nc -> c in colors nl)
*)
let (step_f : int -> int neighbor list -> action -> int) =
  fun _ nl _ ->
  List.hd (free nl) (* Returns the smallest possible color *)
