open Algo

(* *)
let clash_number pidl get =
  let clash = ref 0 in
  let color pid = fst (get pid) in
  List.iter (fun pid ->
      List.iter (fun (n,_) -> if state n = color pid then incr clash) (snd (get pid)))
    pidl;
  float_of_int !clash

let potential = Some clash_number
(* let potential = None *)
let legitimate = None
let fault = Some (fun d _pid _st -> Random.int d)


let md = max_degree ()
let s2n s = [I (min 0 (s-1), s, max (md+1) (s+1))]
let n2s nl _s =
  match nl with
  | [I(_, i, _)] ->  i
  | _ -> assert false

let init_search_utils = Some (s2n, n2s)
