(* Time-stamp: <modified the 06/07/2024 (at 17:00) by Erwan Jahier> *)

(*  Elementary Cellular Automata (Wolfram)

https://mathworld.wolfram.com/ElementaryCellularAutomaton.html
*)

(*
   126 : Sierpinski
   30  : Choatic
   110 : Turing-complete
 *)
let rule_nb = 126

let _ = assert( 0 <= rule_nb  && rule_nb <256 ) (* array of size 8 *)

(**********************************************************************************)
let to_a8 (n: int) : bool array =
  let a = Array.make 8 false in
  let ref_n = ref n in
  for i = 0 to 7 do
    a.(i) <- !ref_n mod 2 = 1;
    ref_n := !ref_n / 2;
  done;
  a

let (rule_nb_array: bool array) = to_a8 rule_nb

(* The next value (of cell) depends its value and the ones of its 2 neighbors *)
let b2i b = if b then 1 else 0
let next_i x1 cell x2 = rule_nb_array.(7 - (4*x1 + 2*cell + x2))
let next x1 cell x2 = next_i (b2i x1) (b2i cell) (b2i x2)

(**********************************************************************************)
(* Initialisation: only one cell is true, in the middle *)
let n = Algo.card()
let cpt = ref 0
let init_state _ _ = cpt := (!cpt+1) mod n; !cpt = n / 2

let enable_f cell nl =
  match List.map Algo.state nl with
  | [x1; x2] -> if next x1 cell x2 = cell then [] else ["flip"]
  | _ -> []

let step_f x _ _ = not x
