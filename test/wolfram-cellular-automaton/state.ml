
type t = bool
let to_string = (fun s -> Printf.sprintf "%B" s)
let of_string = Some bool_of_string
let copy x = x
let actions = ["flip"]
