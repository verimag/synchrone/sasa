(* Time-stamp: <modified the 22/04/2020 (at 10:23) by Erwan Jahier> *)
(* Randomized version of algo 3.1 in the book *)

open Algo
let k=max_degree () + 1 

let (init_state: int -> string -> 'v) = fun _i _ -> 0
(* Random.int i *)

(* Returns the free colors is ascending order (n.log(n)) *)
let (free : 'v neighbor list -> 'v list) = fun nl ->
  let comp_neg x y = - (compare x y) in
  let n_colors = List.map (fun n -> state n) nl in (* neighbor colors *)
  let n_colors = (* neighbor colors, no duplicate, in descending order *)
    List.sort_uniq comp_neg n_colors in
  let rec aux free n_colors i =
    (* for i=k-1 to 0, put i in free if not in n_colors *)
    if i < 0 then free else
      (match n_colors with
       | x::tail ->
         if x = i then aux free tail (i-1) else aux (i::free) n_colors (i-1)
       | [] -> aux (i::free) n_colors (i-1)
      )
  in
  aux [] n_colors (k-1)

let (enable_f: 'v -> 'v neighbor list -> action list) =
  fun c nl ->
    if List.exists (fun n -> state n = c) nl then ["conflict"] else []

let (step_f : 'v -> 'v neighbor list -> action -> 'v) =
  fun e nl _ ->
  if (Random.bool ()) then e else
    (* Returns the smallest possible color *)
    (List.hd (free nl))
    
