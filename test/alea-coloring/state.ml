type t = int
let to_string = string_of_int
let of_string = Some int_of_string
let copy = fun x -> x
let actions = ["conflict"]
let potential = None
let legitimate = None
let fault = None
