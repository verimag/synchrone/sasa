(* Time-stamp: <modified the 06/03/2020 (at 10:12) by Erwan Jahier> *)

(* A self-stabilizing algorithm for constructing breadth-first trees 
 Shing-Tsaan Huang, Nian-Shing Chen 
Information   Processing Letters 41(1992) 109-117 14 February 1992

Hyp:
- distributed daemon
- connected un-directed graph
- anonymous
 *)

open Algo
open State

let (init_state: int -> string -> 'st) =
  fun i _ ->
    {
      level  = 2+Random.int (n-1);
      par    = Random.int i;
    }

let par_level (nl:State.t neighbor list) (e:State.t) : int = 
  let p = List.nth nl e.par in
  let l_p = (state p).level in
  l_p

let level n = (state n).level

let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun e nl ->
  let l_i = e.level in (* level of the current node *)
  let l_p = par_level nl e in (* level of its parent in the tree *)
  if
    (* (RO)  l_i <> l_p+1 ^ l_p<n  --->  l_i := l_p+1.  *)
    l_i <> l_p+1 && l_p < n
  then ["R0"] else if
    (* (Rl) Let k be a neighbor of i such that L(k) = min({L(j) s.t. j \in Nj),
       l_p>L(k) ---> Li := L(k)+1 ;  pi:=k.
       . *)
    let k = List.fold_left
        (fun acc n -> if level acc < level n then acc else n)
        (List.hd nl)
        (List.tl nl)
    in
    l_p > level k
  then ["R1"] else 
    []

let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun e nl a ->
  match a with 
  | "R0" -> { e with level = 1 + par_level nl e }
  | "R1" ->
    let k, rank, _ = List.fold_left
        (fun (k,rank,i) n -> if level k < level n then k,rank,i+1 else n,i,i+1)
        (List.hd nl, 0, 1)
        (List.tl nl)
    in
    { level = 1 + level k ; par = rank }
  | _ -> e
 

