(* Time-stamp: <modified the 06/03/2020 (at 09:51) by Erwan Jahier> *)

let (init_state: int -> string ->  'st) =
  fun _i _ ->
    {
      State.level  = 0;
      State.par = -1;
    }

(* The root is never enabled... *)
let enable_f = fun _ _ -> []

(* ... nor activated! *)
let step_f = fun e _nl _a -> e
let actions = Some [];


