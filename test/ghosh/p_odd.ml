(* Time-stamp: <modified the 23/02/2024 (at 16:09) by Erwan Jahier> *)

(* The Ghosh mutual exclusion algo: odd nodes (a.k.a., x-machines)

Sukumar Ghosh, Binary self-stabilization in distributed systems,
Information Processing Letters, Volume 40, Issue 3, 1991,Pages 153-159,
ISSN 0020-0190, https://doi.org/10.1016/0020-0190(91)90172-E.  *)

open Algo
open State

let (init_state: int -> string -> t) =
  fun _ _ -> { s = (Random.bool ()) ; kind = Odd }


let (enable_f: t -> t neighbor list -> action list) =
  fun s1 nl ->
  let state n = (Algo.state n).s in
  let s1 = s1.s in
    match List.map state nl with
    | [s0; s2; s3] | [_;s0; s2; s3] -> if s0 = s1 && s1 = s2 && s3 = not s0  then ["a"] else []
    | _ ->  failwith "invalid topology"


let (step_f : t -> t neighbor list -> action -> t ) =
  fun s0 _nl _ ->
  { s0 with s = not s0.s }
