(* Time-stamp: <modified the 23/02/2024 (at 16:09) by Erwan Jahier> *)

(* The Ghosh mutual exclusion algo: even nodes (a.k.a., y-machines)

Sukumar Ghosh, Binary self-stabilization in distributed systems,
Information Processing Letters, Volume 40, Issue 3, 1991,Pages 153-159,
ISSN 0020-0190, https://doi.org/10.1016/0020-0190(91)90172-E.  *)

open Algo
open State

let (init_state: int -> string -> t) =
  fun _ _ -> { s = (Random.bool ()) ; kind = Even }

let (enable_f: t -> t neighbor list -> action list) =
  fun s2 nl ->
  let s2 = s2.s in
  let state n = (Algo.state n).s in
    match List.map state nl with
    | [s0; s1; s3] | [s0; s1; s3; _] -> if s0=s1 && s1 = s3 && s2 = not s0  then ["a"] else []
    | _ ->  failwith "invalid topology"

let (step_f : t -> t neighbor list -> action -> t ) =
  fun s0 _nl _ ->
  { s0 with s = not s0.s }
