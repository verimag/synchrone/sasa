open Algo
open State

let (legitimate: pid list -> (pid -> t * (t neighbor * pid) list) -> bool) =
  fun  pidl get ->
  (* increment the token number i if pid is enabled *)
  let incr_token i pid =
    let s, nl = get pid in
    let nl = List.map fst  nl in
    let have_token =
      match s.kind with
      | Bottom -> P0.enable_f s nl <> []
      | Odd -> P_odd.enable_f s nl <> []
      | Even -> P_even.enable_f s nl <> []
      | Top -> P_last.enable_f s nl <> []
    in
    if have_token then i+1 else i
  in
  (* Now let's iterate on all the nodes starting from |tokens|=0 *)
  let token_nb = List.fold_left incr_token 0 pidl in
  (* if there is 1 token, returns true *)
  token_nb = 1
let legitimate = Some legitimate

let potential = None
let fault = Some(fun _ _ st -> { st with s = (Random.bool ()) })

open Algo

let n2s nl n =
  match nl with
  | [B(b)] -> { n with s = b }
  | _ -> assert false

let s2vl b = [B(b.s)]

let init_search_utils = Some(s2vl, n2s)
