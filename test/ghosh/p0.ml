(* Time-stamp: <modified the 31/05/2023 (at 16:51) by Erwan Jahier> *)

(* The Ghosh mutual exclusion algo: first node (a.k.a., bottom machine)

Sukumar Ghosh, Binary self-stabilization in distributed systems,
Information Processing Letters, Volume 40, Issue 3, 1991,Pages 153-159,
ISSN 0020-0190, https://doi.org/10.1016/0020-0190(91)90172-E.  *)

open Algo
open State

let (init_state: int -> string -> t) =
  fun _ _ -> { s = (Random.bool ()) ; kind = Bottom }

let (enable_f: t -> t neighbor list -> action list) =
  fun s0 nl ->
  let state n = (Algo.state n).s in
  let s0 = s0.s in
    match nl with
    | [s1] | [s1; _]  -> if s0 = not (state s1)  then ["a"] else []
    | _ ->  failwith "invalid topology"

let (step_f : t -> t neighbor list -> action -> t ) =
  fun s0 _nl _ ->
  { s0 with s = not s0.s }
