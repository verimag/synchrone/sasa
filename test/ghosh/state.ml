
(* It  is necessary to be  able to distinguish between  node kinds to
   define the legitimate predicate.
*)
type node_kind = Bottom | Odd | Even | Top
type t = { s : bool; kind : node_kind }

let node_kind_to_string = function
    Bottom -> "Bottom"
  | Odd -> "Odd"
  | Even -> "Even"
  | Top -> "Top"

(*  this one  prevents  the  comparison between  sasa  and salut  via
   lurette to work, because in the  lustre version, the state is made
   of  a bool  only (which  could be  changed; the  thing is  that in
   lustre, we  can compute the legitimate  configuration without this
   information).

   However, this one is necessary to be able to start from a specific
   initial configuration *)
let _to_string = (fun s -> Printf.sprintf "%b %s" s.s (node_kind_to_string s.kind))
let to_string = (fun s -> Printf.sprintf "%b" s.s )

let of_string = Some (fun s ->
    match s with
    | "true Bottom" -> { s = true ;kind = Bottom }
    | "true Odd"    -> { s = true ;kind = Odd }
    | "true Even"   -> { s = true ;kind = Even }
    | "true Top"    -> { s = true ;kind = Top }
    | "false Bottom"-> { s = false ;kind = Bottom }
    | "false Odd"   -> { s = false ;kind = Odd }
    | "false Even"  -> { s = false ;kind = Even }
    | "false Top"   -> { s = false ;kind = Top }
    | _ -> failwith "in ghosh/state.ml: fail to parse init config"
  )
let copy x = x
let actions = ["a"]
