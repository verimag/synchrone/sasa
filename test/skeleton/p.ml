(* Time-stamp: <modified the 29/06/2023 (at 16:04) by Erwan Jahier> *)

(* a dumb algo that compiles *)

open Algo

let (init_state: int -> string -> State.t) =
  fun _nl _ -> (Random.int 10)


let (enable_f: State.t -> State.t neighbor list -> action list) =
  fun _ nl ->
    match state (List.hd nl) with
    | 0 ->  ["action2"]
    | 1 ->  []
    | _ ->  ["action1"]


let (step_f : State.t -> State.t neighbor list -> action -> State.t ) =
  fun e _ ->
    function
    | "action1" -> 0
    | "action2" -> 1
    | _ -> e
