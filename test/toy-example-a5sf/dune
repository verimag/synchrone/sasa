;; dune file to build the cmxs (resp the cma) needed by sasa (resp rdbgui4sasa)

(library
 (name user_algo_files) ;; a fake name because "%{env:topology=ring4}" is rejected
 (libraries algo)
 ; (wrapped false) ; so that user_algo_files is not used in the .cm*
 (library_flags -linkall))

;; copy the <topology>.dot to the build dir if it already exists
;; or create it via make otherwise

(rule
 (mode
  (promote (until-clean)))
 (target %{env:topology=rtree4}.dot)
 (deps
  (:makefile Makefile)
  (:makefiledot Makefile.dot))
 (action
  (bash
   "[ -f ../../%{env:topology=rtree4}.dot ] && cp ../../%{env:topology=rtree4}.dot . || make %{env:topology=rtree4}.dot")))

;; generate the registration file (out of the dot file)

(rule
 (target %{env:topology=rtree4}.ml)
 (deps
  (:dotfile ./%{env:topology=rtree4}.dot))
 (action
  (bash "sasa -reg %{dotfile}")))

(rule
 (mode
  (promote (until-clean)))
 (action
  (progn
   ;; give them back the rigth name
   (copy user_algo_files.cmxs %{env:topology=rtree4}.cmxs)
   (copy user_algo_files.cmxa %{env:topology=rtree4}.cmxa)
   (copy user_algo_files.cma %{env:topology=rtree4}.cma)
   (copy user_algo_files.a %{env:topology=rtree4}.a)
   ;; we can have only one registering file in the build dir
   (bash "rm %{env:topology=rtree4}.ml")))
 (deps
  (:src ./%{env:topology=rtree4}.dot)))

;; The glue lustre code between the oracle and the topology

(rule
 (target %{env:topology=rtree4}_oracle.lus)
 (mode
  (promote (until-clean)))
 (deps
  (:dotfile ./%{env:topology=rtree4}.dot)
  (:cmxs ./%{env:topology=rtree4}.cmxs))
 (action
  (bash "sasa -glos %{dotfile} -o $(basename $(realpath $PWD/../..))")))
