open Algo
open P
open State

let (isEnabledS: pid -> (pid -> ('a * 'a neighbor list)) -> bool) =
  fun id get -> ((fst (get id))).sub <> (((fst (get id))).input + (sum_sub (snd (get id))))

let (isEnabledR: pid -> (pid -> ('a * 'a neighbor list)) -> bool) =
  fun id get ->
  let id_v, id_nl = get id in
  if id_v.par = -1 then (id_v.res <> id_v.sub) else
    let par = state (List.nth id_nl id_v.par) in
    let p_par_res = (fst (get par.pid)).res in
    (id_v.res <> max p_par_res id_v.sub)

let int_of_pid pid =
  if pid = "Root" then 0 else
    int_of_string (String.sub pid 1 ((String.length pid) - 1))

let (loc_pot: pid -> pid list -> (pid -> ('a * 'a neighbor list)) -> int) =
  fun p _pidl get ->
  if isEnabledS p get then
    1 + vn*(vn-1) + int_of_pid p
  else if isEnabledR p get then
    1 + vn - int_of_pid p
  else 0

let rec (sum_loc_pots: pid list -> pid list -> (pid -> ('a * 'a neighbor list)) -> int) =
  fun pidl pidl_mem get ->
  match pidl with
    [] -> 0
  | p::listp -> (loc_pot p pidl_mem get) + (sum_loc_pots listp pidl_mem get)

let (pf: pid list -> (pid -> ('a * ('a neighbor * pid) list)) -> float) =
  fun pidl get ->
  let get pid =
    let v, l = get pid in
    v, fst (List.split l)
  in
  float_of_int (sum_loc_pots pidl pidl get)

let potential = Some pf



let legitimate = None (* None => only silent configuration are legitimate *)
let fault = None

let mini= 0
let maxi = max_degree ()

(* the  par field  is an input  of the algorithm  that should  not be
   modified *)

let s2n s = [I(min mini (s.input-1), s.input, s.input+1);
             I(min mini (s.sub-1), s.sub, s.sub+1); I(min mini (s.res-1), s.res, s.res+1)]
let n2s nl s =
  match nl with
  | [I(_, input, _); I(_, sub, _); I(_, res, _)] ->
    { s with input; sub; res }
  | _ -> assert false

let init_search_utils = Some (s2n, n2s)
