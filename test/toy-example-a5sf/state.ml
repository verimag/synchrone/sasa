
open Algo

let d = max_degree()
let vn = card ()

type t = {
  pid: string;
  input:int;
  par:int;
  sub:int;
  res:int
}

let (to_string: (t -> string)) =
  fun s ->
    Printf.sprintf "pid=%s input=%d par=%d sub=%d res=%d"
      s.pid s.input s.par s.sub s.res

let (of_string: (string -> t) option) =
  Some (fun s ->
      try
        Scanf.sscanf s "pid=%s input=%d par=%d sub=%d res=%d"
          (fun pid input par sub res -> {pid; input; par; sub; res})
      with _ -> assert false
    )

let (copy : ('v -> 'v)) = fun x -> x
let actions = ["S";"RR";"RP"]
