(* From  "Acyclic Strategy  for Silent Self-Stabilization  in Spanning
   Forests" Karine Altisen, Stéphane Devismes,  and Anaı̈s Durand,
   SSS 2018

https://hal.archives-ouvertes.fr/hal-01938671
*)

open Algo
open State

let (init_state: int -> string -> 'st) =
  fun _n pid ->
  if (not (is_rooted_tree())) then failwith "The topology should be a rooted tree";
  {
    pid = pid;
    (* input and par are inputs of the algo *)
    input = Random.int 100; (* input of the algo *)
    par = if pid="Root" then -1 else 0; (* the input tree is considered to be sorted
                                           alphabetically (wrt a bf traversal) *)
    (* sub and res are computed by the algo *)
    sub = Random.int 100;
    res = Random.int 100
    }

let rec (children: 'st neighbor list -> 'st list) =
  fun nl ->
  match nl with
    [] -> []  (*if the current node p is parent of his neighbor n then n is a child of p*)
  | n::liste -> if ((reply n) = (state n).par) then (state n)::(children liste) else children liste

let (sum: int list -> int) = fun l -> List.fold_left (+) 0 l

let (sum_sub: 'st neighbor list -> int) =
  fun nl ->
  sum (List.map (fun st -> st.sub) (children nl))


let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun p nl ->
  assert (p.par >= -1);
  assert (p.par<List.length nl);
  if p.sub <> (p.input + (sum_sub nl)) then ["S"]
  else if ((p.par =  -1) && (p.res <> p.sub)) then ["RR"]
  else if ((p.par <> -1) && (p.res <> max (state (List.nth nl p.par)).res p.sub)) then ["RP"]
  else []

let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun p nl a ->
  let sumsub = sum_sub nl in
  match a with
  | "S"  -> {p with sub = (p.input + sumsub)}
  | "RR" -> {p with res = p.sub}
  | "RP" ->
    assert (p.par>=0);
    assert (p.par<List.length nl);
    {p with res = max (state (List.nth nl p.par)).res p.sub}
  | _ -> assert false
