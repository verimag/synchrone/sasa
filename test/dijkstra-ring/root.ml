(* Time-stamp: <modified the 16/01/2025 (at 10:30) by Erwan Jahier> *)

open Algo

open State

let (init_state: int -> string -> 's) =
  fun _n _ ->
    (*     let k = (card() - 1) in *)
    (*     let _ = assert (k > 0) in *)
    { root = true ; v = Random.int k }

let (enable_f: 's -> 's neighbor list -> action list) =
  fun e nl ->
    let pred = List.hd nl in
    if e.v = (state pred).v then ["T"] else []

let (step_f :  's -> 's neighbor list -> action -> 's) =
  fun e _nl a ->
    (*     let k = (card() - 1) in *)
    match a with
    | _ -> { e with v = (e.v + 1) mod k }
