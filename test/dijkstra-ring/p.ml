(* Time-stamp: <modified the 16/01/2025 (at 10:30) by Erwan Jahier> *)

open Algo

open State
let (init_state: int -> string -> 's) =
  fun _ _ ->
  (* This algo is meant to work on directed rings *)
  assert(is_directed());
  assert(is_cyclic());
  assert(is_connected());
  assert(mean_degree() = 2.0);
  assert(min_degree() = 2);
    (*     let k = (card() - 1) in *)
    (*     let _ = assert (k > 0) in *)
    { root = false ; v = Random.int k }

let (enable_f: 's -> 's neighbor list -> action list) =
  fun e nl ->
  let pred = match nl with [n] -> n | _ ->
    failwith "Error: the topology should be a directed ring!\n%!"
  in
    if e.v <> (state pred).v then ["T"] else []

let (step_f : 's -> 's neighbor list -> action -> 's) =
  fun e nl a ->
    let pred = match nl with [n] -> n | _ -> assert false in
    match a with
    | _ ->  { e with v = (state pred).v }
