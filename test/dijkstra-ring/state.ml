
(* semi-anonymous network: each node knows if its neighbors is the root!
Actually only necessary to define the legitimate predicate
*)
let k = Algo.card()
type t = { root: bool ; v : int }
let to_string s = Printf.sprintf "c=%i" s.v
let first = ref true
let (of_string: (string -> t) option) =
    Some (fun s ->
      let res =
        try Scanf.sscanf s "{root=%d;c=%d}" (fun i1 i2 -> { root = i1<>0; v = i2 } )
        with
          e1  ->
          try (* if the root node is not explicitly set in the dot file, we
                 consider the first one to be the root *)
            let res = Scanf.sscanf s "c=%d" (fun i -> { root = !first; v = i } ) in
            first := false;
            res
          with
            e2  ->
              Printf.printf
                "state.ml: can't parse the initial state in the .dot: '%s'\n%s\n%s\n%!" s
                (Printexc.to_string e1) (Printexc.to_string_default e2) ;
              assert false
      in
      (* Printf.printf "state.ml: { root = %b ; c = %d }\n%!" res.root res.v; *)
      res

    )

let copy x = x
let actions = ["T"]
