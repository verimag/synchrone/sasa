open Algo
open State

(** Computes the value Z of the book, that is 0 if the values are convex,
  and the minimum number of incrementations the root has to do so that its value
  is different to every other value of the ring.

  A configuration is convex if there is no value that is the same than the root
  seperated from the root with another value.
  2 2 2 3 0 1 3 -> convex, z=0
  2 4 5 3 0 1 3 -> convex, z=0
  2 2 2 3 0 2 3 -> not convex, z=2
*)
module IntSet = Set.Make(Int)

let verb = false

let compute_Z root root_st (get: Algo.pid -> State.t * (State.t Algo.neighbor * Algo.pid) list) =
  let v = root_st.v in
  let used = ref IntSet.empty in
  let rec convex pid encountered res =
    (* Printf.eprintf (if encountered then "<" else "|"); *)
    (* Printf.eprintf (if res then ">" else "|"); *)
    let next_st, next =
      match get pid with
        (_, [s,n]) -> state s, n | _ ->
        failwith "Can't compute the cost of a topology that is not a directed ring"
    in
    if next = root then res
    else
    let next_v = next_st.v in
    (* Printf.eprintf "%s %d" next next_v; *)
    used := IntSet.add next_v !used;
    convex next (encountered || next_v = v) (res && (not encountered || next_v = v))
  in
  if convex root false true
  then 0
  else
    let rec get_min_free cur_val dist =
      if cur_val = v then assert false;
      if not (IntSet.mem cur_val !used) then dist
      else
        get_min_free ((cur_val + 1) mod (card () + 1)) (dist + 1)
    in
    get_min_free (v+1) 1
;;

(* Computes the sum_dist as described in the book. It is the sum of the distance
   from each token to the root *)
let compute_sd (root: pid)
    (get: Algo.pid -> State.t * (State.t Algo.neighbor * Algo.pid) list) =
  let rec compute pid total rang =
    let st, ((n_state, neighbor): 's * pid) =
      match get pid with
        (st, [n]) -> st, n
      | _ -> failwith "Can't compute the cost of a topology that is not a directed ring"
    in
    if pid = root then (
      if verb then Printf.printf "\t%s=%d enabled=%b rang=%d total=%d\n%!"
        pid st.v ([]<>Root.enable_f st [n_state]) rang total;
      total
    )
    else
    let total = if (P.enable_f st [n_state]) <> [] then total + rang else total in
    if verb then Printf.printf "\t%s=%d enabled=%b rang=%d total=%d\n%!"
      pid st.v ([]<>P.enable_f st [n_state]) rang total;
    compute neighbor total (rang+1)
  in
  let succ_root: pid =
    match get root with
    | (_, [_, n]) -> n
    | _ -> failwith "Can't compute the cost of a topology that is not a directed ring"
  in
  if verb then Printf.printf "  - Computing the sum_dist: \n" ;
  compute succ_root 0 1
;;

(* Computes the cost (it's the regular cost not the tighter_cost) *)
let cost : pid list -> (pid -> t * (t neighbor * pid) list) -> float =
  fun pidl get ->
  let root = match List.find_opt (fun p -> (fst (get p)).root) pidl with
    | Some res -> res
    | None -> assert false
  in
    if verb then Printf.printf "- Computing the potential: \n" ;
    let root_st = fst (get root) in
    let n = card() in
    let z = compute_Z root root_st get in
    let c = if z = (n - 1)
      then (3 * n * (n - 1) / 2) - n - 1
      else
        let sum_dist = compute_sd root get in
        let res = z * n + sum_dist - 2 in
        if verb then Printf.printf "    sum_dist=%d \n\n" sum_dist;
        if res < 0 then 0 else res
    in
    if verb then Printf.printf "  potential=%d z=%d \n%!" c z;
    float_of_int c
;;

let potential = Some cost

let fault = Some (fun _d _pid st -> { st with v = Random.int (card())} )

(* For the Dijkstra ring, a configuration is legitimate iff there is
 exactly one token.

   nb: there are as many tokens as enabled nodes. So let's count enable nodes!
 *)
let (legitimate: pid list -> (pid -> t * (t neighbor * pid) list) -> bool) =
  fun  pidl get ->
  (* increment the token number i if pid is enabled *)
  let incr_token i pid =
    let s, nl = get pid in
    let nl = List.map fst nl in
    let have_token = (if s.root then Root.enable_f s nl else P.enable_f s nl) <> [] in
    if have_token then i+1 else i
  in
  (* Now let's iterate on all the nodes starting from |tokens|=0 *)
  let token_nb = List.fold_left incr_token 0 pidl in
  (* if there is 1 token, returns true *)
  token_nb = 1

let legitimate = Some legitimate

(* init search *)
let maxi = card () + 1

let _s2n s = [I (0, s.v, maxi)]
let s2n s = [I (min 0 (s.v - 1), s.v, max maxi (s.v + 1))]

let n2s nl s =
  match nl with
  | [I(_, i, _)] ->  { s with v = i }
  | _ -> assert false

let init_search_utils = Some (s2n, n2s)
