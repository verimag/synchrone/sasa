(* Time-stamp: <modified the 17/01/2023 (at 14:58) by Erwan Jahier> *)

(* "A self-stabilizing algoritm for constructing spanning trees"
 Nian-Shing Chen,  Wey-Pyng Yu and Shing-Tsaan Huang
 Information Processing Letters 39 (1991) 147-151

Hyp:
- central daemon
- connected unidirected graph

*)

let (init_state: int -> string -> 'st) =
  fun _i _ ->
    {
      State.level  = 0;
      State.par = -1
    }

(* The root is never enabled... *)
let enable_f = fun _ _ -> []

(* ... nor activated! *)
let step_f = fun e _nl _a -> e
