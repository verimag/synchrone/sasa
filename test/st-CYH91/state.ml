
let n = Algo.card()

type t = { level:int; par:int }
let to_string = (fun s -> Printf.sprintf "level=%i par=%i" s.level s.par)
let of_string = Some
    (fun s ->
       Scanf.sscanf s "{level=%d;par=%d}"
         (fun d par -> { level = d; par = par }))

let copy x = x


let print fmt s =
  Format.pp_print_string fmt  (to_string s)

let print_neighbor =
  Algo.fmt_print_neighbor

let actions = ["R0";"R1";"R2"]
let potential = None
let legitimate = None
let fault = None 
