(* Time-stamp: <modified the 06/03/2020 (at 09:45) by Erwan Jahier> *)

(* "A self-stabilizing algorithm for constructing spanning trees"
 Nian-Shing Chen,  Wey-Pyng Yu and Shing-Tsaan Huang
 Information Processing Letters 39 (1991) 147-151 1991

Hyp:
- central daemon
- connected un-directed graph

*)

open Algo
open State

let (init_state: int -> string -> 'st) =
  fun i _ ->
    {
      level  = Random.int n;
      par    = Random.int i;
    }

let par_level (nl:State.t neighbor list) (e:State.t) : int = 
  let p = List.nth nl e.par in
  let l_p = (state p).level in
  l_p
    
let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun e nl ->
  let l_i = e.level in
  let l_p = par_level nl e in
  if
  (* (RO) L(i)<>n ^ L(i)<>L(p)+1 ^ L(p)<>n --> L(i) := L(p) + 1 *)
    l_i <> n && l_i <> l_p+1 && l_p <> n
  then ["R0"] else if
    (* (Rl) L(i)<>n ^ L(p)=n  --> L(i):=n. *)
    l_i <> n && l_p = n 
  then ["R1"] else if
  (* (R2) Let k be some neighbor of i,
     L(i)=n ^ L(k)<n-1      --> L(i) := L(k) + 1; P(i) := k.
  *)
    List.exists (fun k -> l_i=n && (state k).level<n-1) nl
  then ["R2"] else
    []
 

let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun e nl a ->
  (*   Printf.printf "step_f [%s]\n%!" a;   *)
    match a with 
    | "R0" -> { e with level = 1 + par_level nl e }
    | "R1" -> { e with level = n }
    | "R2" -> 
      let l_i = e.level in
      let cond k = l_i=n && (state k).level<n-1 in
      let rec find_neighbor i = function
        (* returns the first neighbor satisfying cond (and its rank) *)
        | [] -> assert false
        | k::t -> if cond k then i,k else find_neighbor (i+1) t
      in
      let parent,k = find_neighbor 0 nl in
      { level = (state k).level+1; par = parent }
    | _ -> e
 


  
