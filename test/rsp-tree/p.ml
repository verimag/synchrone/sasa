(* Time-stamp: <modified the 17/06/2022 (at 15:49) by Erwan Jahier> *)

(*  The  Algorithm  1  of  "Self-Stabilizing  Disconnected  Components
   Detection and  Rooted Shortest-Path Tree Maintenance  in Polynomial
   Steps" by Stéphane Devismes, David Ilcinkas, Colette Johnen

    https://hal.archives-ouvertes.fr/hal-01485652v4 

    Hyp: 
    - semi-anonymous network (there is a root + no pid)
    - indirect naming (Algo.reply is available)
    - weigthed network (positive integers)
    - distributed  daemon

*)

open Algo
open State

let (init_state: int -> string -> 'st) =
  fun s _ ->
  if s = 0 then { st=C; par=(-1);d=0 } else
  {
    st = (match Random.int 4 with
        | 0 -> I
        | 1 -> C 
        | 2 -> EB
        | _ -> EF);
    par = Random.int s;
    d   = Random.int s
  }
  
(***************************************************************)
(* Predicates and macros *)

let parent u nl = List.nth nl u.par

(* children(u) = {v ∈ Γ(u) | stu != I ∧ stv != I ∧ parv = u ∧ 
                              dv ≥ du + ω(v, u) ∧ (stv = stu ∨ stu = EB)}  *)

let (children: t -> t Algo.neighbor list -> t Algo.neighbor list) =
  fun u nl -> 
  List.filter
    (fun v -> u.st <> I && (state v).st <> I &&
              (state v).par = reply v &&
              (state v).d >= u.d + weight v &&
              ((state v).st = u.st || u.st = EB))
    nl

(*  abRoot(u) ≡ stu != I ∧ 
         [paru not_in Γ(u) ∨ stparu = I ∨ 
          du < dparu + ω(u, paru ) ∨ (stu != stparu ∧ stparu != EB) ]
*)
let abRoot u nl =
  let paru = parent u nl in
  let stparu = (state paru).st in
  let dparu = (state paru).d in
  u.st <> I && (
    (u.par > List.length nl)  ||
    stparu = I ||
    u.d < dparu + weight paru ||
    (u.st <> stparu && stparu <> EB)
  )
 
(*  p_reset(u) ≡  stu = EF ∧ abRoot(u) *)
let p_reset u nl = u.st = EF && abRoot u nl
  
(*  P_correction(u) ≡  (∃v ∈ Γ(u) | stv = C ∧ dv + ω(u, v) < du ) *)
let p_correction u nl =
  List.exists (fun v -> (state v).st = C && (state v).d + weight v < u.d) nl 


let min1 (d1,u) (d2,v) = if d1 < d2 then (d1,u) else (d2,v)
let minimum l =
  match l with
  | [] -> assert false
  | x::t -> List.fold_left min1 x t

(*  computePath(u) : paru := argmin_{v∈Γ(u)∧stv =C} (dv + ω(u, v)); 
                     du := dparu + ω(u, paru ); 
                     stu := C *)
let computePath u nl =
  let paru = parent u nl in
  let nli = List.mapi (fun i x -> i,x) nl in
  let nli_C = List.filter (fun (_,v) -> (state v).st = C) nli in
  let d_n_l = List.map (fun (i,v) -> (state v).d + weight v, i) nli_C in
  {
    st = C ;
    par = snd (minimum d_n_l);
    d = (state paru).d + weight paru;
  }
  
(***************************************************************)
(* Rules
  
 RR (u)  : (P_reset(u) ∨ stu = I) ∧ (∃v ∈ Γ(u) | stv = C) → computePath(u)
 *)
let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun u nl ->
  if nl=[] then [] else
    let al = if
      (*  RC (u)  : stu = C ∧ P_correction(u)             → computePath(u) *)
      u.st = C && p_correction u nl then ["Rc(u)"] else []
    in
    let al = if
      (* REB (u) : stu = C ∧ ¬P_correction(u)∧          → stu := EB
                  (abRoot(u) ∨ stparu = EB) *)
      u.st = C && not (p_correction u nl) &&
      (abRoot u nl || (state (parent u nl)).st = EB)
    then "Reb(u)"::al else al
  in
  let al = if
    (* REF (u) : stu = EB ∧ (∀v ∈ children(u) | stv = EF )       → stu := EF *)
    u.st = EB && List.for_all (fun v -> (state v).st = EF) (children u nl)
    then "Ref(u)"::al else al
  in
  let al = if
    (* RI (u)  : P_reset(u) ∧ (∀v ∈ Γ(u) | stv != C)             → stu := I *)
    p_reset u nl && List.for_all (fun v -> (state v).st <> C) nl
    then "Ri(u)"::al else al
  in
  let al = if
    (* RR (u)  : (P_reset(u) ∨ stu = I) ∧ (∃v ∈ Γ(u) | stv = C) → computePath(u)*)
    (p_reset u nl || u.st = I) && List.exists (fun v -> (state v).st = C) nl
    then "Rr(u)"::al else al
  in
  al

  
let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun u nl ->
    function
    | "Rc(u)"  -> computePath u nl
    | "Reb(u)" -> { u with st = EB }
    | "Ref(u)" -> { u with st = EF } 
    | "Ri(u)"  -> { u with st = I  } 
    | "Rr(u)"  -> computePath u nl
    | _ -> u
