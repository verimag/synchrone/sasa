
type status = I | C | EB | EF 
let status2string = function 
  | I  -> "I"
  | C  -> "C"
  | EB -> "EB"
  | EF -> "EF"
let status2int = function 
  | I  -> 0
  | C  -> 1 
  | EB -> 2
  | EF -> 3
    
type t = {
  st : status;
  par : int;
  d  : int
}
let to_string s =
  Printf.sprintf "st=%d par=%d d=%d" (status2int s.st) s.par s.d
let of_string = None
let copy x = x
let actions = ["Rc(u)"; "Reb(u)"; "Ref(u)"; "Ri(u)";  "Rr(u)"]
let potential = None
let legitimate = None
let fault = None 
