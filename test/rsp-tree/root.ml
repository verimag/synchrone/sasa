(* Time-stamp: <modified the 06/03/2020 (at 11:13) by Erwan Jahier> *)

open Algo
open State
let (init_state: int -> string -> 'st) =
  fun _ _ ->
  {
    st = C;
    par = -1;
    d  = 0
  }

let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun _e _nl -> []
        
  
let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun s _nl _a -> s
