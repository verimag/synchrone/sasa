
open Algo
open State

let debug = false

let exit2 msg =
  Printf.printf "Error in config.ml: %s\n%!" msg;
  exit 2

let remove_nth n l =
  assert (n>=0);
  let rec f n l =
    if n=0 then List.tl l else
      match l with
      | [] ->  assert false
      | x::tl -> x::(f (n-1) tl)
  in
  if (n >= List.length l) then (
    exit2 (Printf.sprintf "cannot get the %dth element of a list of size %d"
                n (List.length l))
  )
  else
    f n l

let rec (pot : pid -> (pid -> ('a * ('a neighbor*pid) list)) -> int -> int -> int) =
  fun pid get level acc ->
  (* From a pid  and its level, adds to acc the  potential of the tree
     rooted in pid *)
  if debug then Printf.printf "pot %s\n%!" pid;
  let s, nl = get pid in
  let nl = if s.isRoot then nl else remove_nth s.par nl in
  let nl2 = List.map fst nl in
  let acc = if P.enable_f s nl2 <> [] then (
      if debug then Printf.printf "%s -> acc=%d+%d\n%!" pid acc level;
      acc+level
    )
    else (
      acc
    )
  in
  List.fold_left (fun acc (_, pid) -> pot pid get (level+1) acc) acc nl



(* Do this work only once, we store the root name in a string option ref *)
let root = ref None
let get_root pidl get =
  match !root with
  | None ->
    ( match List.filter (fun pid -> (fst (get pid)).isRoot) pidl with
        [ pid ] -> root := Some pid; pid
      | [] -> exit2 "at least one node should be the root"
      | _::_ -> exit2 "at most one node should be the root"
    )
  | Some pid -> pid

(* The  potential is defined as  the sum of enabled  nodes levels (the
   level is 1 for root, 2 for its children, and so on *)
let (pf: pid list -> (pid -> ('a * ('a neighbor * pid) list)) -> float) =
  fun pidl get ->
  let root = get_root pidl get in
  if debug then Printf.printf "=================> %s is the root \n%!" root;
  let root_pot = pot root get 1 0 in
  if debug then (
    let enab pid =
      let v,nl = get pid in
      if P.enable_f v (List.map fst nl)  = [] then
        ""
      else
        pid
    in
    let enab_list = List.map enab pidl in
    Printf.printf "=================> potential(%s) = %d\n%!" (String.concat "," enab_list) root_pot
  );
  (*     (String.concat "," (List.map (fun pid -> Printf.sprintf "%s=%b" get pid) pidl) root_pot ; *)
  float_of_int root_pot

let potential = Some pf

let legitimate = None (* None => only silent configuration are legitimate *)
let fault = Some(fun _ pid _ ->
  {
    isRoot = pid = "Root"; (* ZZZ: The root of the tree should be named "Root"! *)
    alpha = Random.int (2*k+1);
    par = if pid="Root" then -1 else 0 (* the input tree should be sorted alphabetically (wrt a bf traversal) *)
  })

open State
let maxi = 2*k+1

let _s2n s = [I (0, s.alpha, maxi)]
let s2n s = [I (min 0 (s.alpha - 1), s.alpha, max maxi (s.alpha + 1))]

let n2s nl s =
  match nl with
  | [I(_, i, _)] ->  { s with alpha = i }
  | _ -> assert false

let init_search_utils = Some (s2n, n2s)
