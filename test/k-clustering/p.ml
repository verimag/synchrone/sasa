(*
Algorithm 1 of:
  A Framework for Certified Self-Stabilization
  Case Study: Silent Self-Stabilizing k-Dominating Set on a Tree
https://hal.archives-ouvertes.fr/hal-01272158/document

Encoded by Hugo Hannot
*)
(* nb:  it is meant to  start with a spanning  trees. The rtree%i.dot
   rules in ../Makefile.dot generates rooted tree. We can use them as
   spanning trees by taking advantage of  the fact that the nodes are
   sorted alphabetically if we perform a bread-first traversal of the
   tree : we then the just need  to consider the first neigbor as the
   parent in the tree.  *)

open Algo
open State

type s = State.t
type sl = s list
type n = State.t neighbor
type nl = n list

let isRoot p = (state p).isRoot

let (isShort: n -> bool) =
  fun p -> (state p).alpha < k

let (isTall: n -> bool) =
  fun p -> (state p).alpha >= k

(* Actually unused *)
let (_kDominator: 'v -> bool) =
  fun p -> ((state p).alpha = k) || ((isShort p) && (isRoot p))

let (children: s -> nl -> nl) =
  fun _p nl ->
  List.filter (fun q -> (state q).par = reply q) nl

let (shortChildren: s -> nl -> nl) =
  fun p nl ->
  List.filter isShort (children p nl)

let (tallChildren: s -> nl -> nl) =
  fun p nl ->
  List.filter isTall (children p nl)

let rec (max: nl -> int -> int) =
  fun sl cur ->
  match sl with
  | [] -> cur
  | n::tail ->
    let s = state n in
    if (s.alpha) > cur then max tail (s.alpha) else max tail cur

let rec (min: nl -> int -> int) =
  fun sl cur ->
  match sl with
  | [] -> cur
  | n::tail ->
    let s = state n in
    if (s.alpha) < cur then min tail (s.alpha) else min tail cur

let (maxAShort: s -> nl -> int) =
  fun p nl -> max (shortChildren p nl) (-1)

let (minATall: s -> nl -> int) =
  fun p nl -> min (tallChildren p nl) (2*k+1)

let (newAlpha: s -> nl -> int) =
  fun p nl ->
  let mas = (maxAShort p nl) in
  let mit = (minATall p nl) in
  let res = if (mas + mit) <= (2*k - 2) then (mit + 1) else (mas + 1) in
  (*   Printf.printf "newAlpha -> %d\n%!" res;  *)
  res
(*end macros*)

let (init_state: int -> string -> s) =
  fun _ pid ->
  assert(is_rooted_tree());
  {
    isRoot = pid = "Root"; (* ZZZ: The root of the tree should be named "Root"! *)
    alpha = Random.int (2*k+1);
    par = if pid="Root" then -1 else 0 (* the input tree should be sorted alphabetically (wrt a bf traversal) *)
  }

let (enable_f: s -> nl -> action list) =
  fun p nl -> if (p.alpha <> (newAlpha p nl)) then ["change_alpha"] else []

let (step_f : s -> nl -> action -> s) =
  fun p nl a ->
  if a = "change_alpha" then {p with alpha = (newAlpha p nl)} else assert false
