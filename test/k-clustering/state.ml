(* Time-stamp: <modified the 12/02/2023 (at 22:18) by Erwan Jahier> *)

open Algo

let d = max_degree()

type t = {
  isRoot:bool;
  alpha:int;
  par:int
  }

let (to_string: (t -> string)) =
  fun s ->
  Printf.sprintf "isRoot=%b alpha=%d par=%d" s.isRoot s.alpha s.par

let (of_string: (string -> t) option) =
  Some (fun s ->
      Scanf.sscanf s "isRoot=%B alpha=%d par=%d"
        (fun b alpha p -> {isRoot = b ; alpha = alpha ; par = p }))

let (copy : ('v -> 'v)) = fun x -> x
let actions = ["change_alpha"]

let k = 2
