
type t = int
let to_string = (fun s -> Printf.sprintf "%i" s)
let of_string = Some int_of_string
let copy x = x
let actions = ["R";"R1"]
let potential = None
let legitimate = None
let fault = None
