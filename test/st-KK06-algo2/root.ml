(* Time-stamp: <modified the 05/03/2020 (at 21:49) by Erwan Jahier> *)

let (init_state: int -> string -> 'st) =
  fun _ _ -> 0

(* The root is never enabled... *)
let enable_f = fun _ _ -> []

(* ... nor activated! *)
let step_f = fun e _nl _a -> e


