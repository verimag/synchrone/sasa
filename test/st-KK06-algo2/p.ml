(* Time-stamp: <modified the 05/03/2020 (at 21:48) by Erwan Jahier> *)

(* 
 A Self-stabilizing Algorithm for Finding a Spanning Tree in a Polynomial Number of Moves
 Adrian Kosowski and Lukasz Kuszner
 PPAM 2005, LNCS 3911, pp. 75–82, 2006.

Hyp:
- distributed daemon
- connected un-directed graph

 *)

open Algo

let n = Algo.card ()

(* for some known constant N>=n *)
let bigN = 2*n 
             
let (init_state: int -> string -> 'st) =
  fun _nl _ -> (Random.int bigN)


let min_n nl = (* returns the min of the neigbhors *) 
  List.fold_left
    (fun acc n -> if acc < state n then acc else state n)
    (state (List.hd nl))
    (List.tl nl)
    
let max_n nl = (* returns the max of the neigbhors *) 
  List.fold_left
    (fun acc n -> if acc > state n then acc else state n)
    (state (List.hd nl))
    (List.tl nl)

let max_n_alt f_v nl = (* returns the max of the neigbhors that are < f_v *) 
  List.fold_left
    (fun acc n -> if acc > state n || state n >= f_v then acc else state n)
    (state (List.hd nl))
    (List.tl nl)

let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun f_v nl ->
  if (*  R: v <> root ∧ f (v) ≤ min_{u∈N(v) f(u)} ∧ max_{u∈N(v) f (u)} < N
             --->  f (v) = max_u∈N(v) f (u) + 1 *)
    f_v <= min_n nl && max_n nl < bigN
  then  ["R"] else if
    (* R1: if v <> root ∧ f (v) > max_{u∈N (v) : f (u)<f(v)} (f (u) + 1)
          --->  f (v) = max_{u∈N(v) : f(u)<f(v) (f(u) + 1)}
    *)
    f_v > 1+ max_n_alt f_v nl
  then  ["R1"] else 
    []
              
  
let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun f_v nl ->
    function
    | "R"  -> 1 + max_n nl
    | "R1" -> 1 + max_n_alt f_v nl
    | _ -> f_v
      


