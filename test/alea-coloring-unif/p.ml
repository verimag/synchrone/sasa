(* Time-stamp: <modified the 22/04/2020 (at 10:57) by Erwan Jahier> *)

(* "Probabilistic  Self-stabilizing Vertex Coloring  in Unidirectional
   Anonymous  Networks" by  Samuel  Bernard,  Stéphane Devismes,  Katy
   Paroux, Maria Potop-Butucaru, and Sébastien Tixeuil *)

open Algo

let k = max_degree () + 1 
    
let (init_state: int -> string -> 'v) =  fun _i _ -> 0
(* Random.int k *)

let (enable_f: 'st -> 'st neighbor list -> action list) =
  fun c nl -> (* enabled if one neighbor has color c *)
  if List.exists (fun n -> state n = c) nl then ["a"] else []         

(* Returns the free colors is ascending order (n.log(n)) *)
let free nl =
  let comp_neg x y = - (compare x y) in
  let n_colors = List.map (fun n -> state n) nl in (* neighbor colors *)
  let n_colors = (* no duplicate, in descending order *)
    List.sort_uniq comp_neg n_colors
  in
  let rec aux free n_colors i =
    (* for i=k-1 to 0, put i in free if not in n_colors *)
    if i < 0 then free else
      (match n_colors with
       | x::tail ->
         if x = i then aux free tail (i-1) else aux (i::free) n_colors (i-1)
       | [] -> aux (i::free) n_colors (i-1)
      )
  in
  aux [] n_colors (k-1)
  
let (step_f : 'st -> 'st neighbor list -> action -> 'st ) =
  fun c nl _ -> 
  let free_colors = c::(free nl) in
  let i = Random.int (List.length free_colors) in
  List.nth free_colors i

(* A variant (quadratic instead of n.log(n)) *)
let all_colors = List.init k (fun i -> i) (* [0;1;...;k-1] *) 
let _step_f_v1 c nl _ = 
  let cl = List.map state nl in 
  let free_colors = List.filter (fun c -> not(List.mem c cl)) all_colors in 
  let free_colors = c::free_colors in 
  let i = Random.int (List.length free_colors) in
  List.nth free_colors i
