#!/bin/bash
# The same as run-kind2, with an extra argument (for generating the k.lus file)

set -x

k=$1
shift

echo "const k = $k ;" > k.lus

../run-kind2.sh $@
