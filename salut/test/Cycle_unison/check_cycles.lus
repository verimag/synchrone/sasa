-- include "../../lib/daemon.lus"

include "daemon_hyp.lus"

node check_cycles(activations : bool^actions_number^card; inits : state^card)
returns (ok : bool; enables : bool^actions_number^card);
var config : state^card;

  --  i2 : state^card;
var legitimate, noloop : bool;
    lustre_round : bool;
    lustre_round_nb : int;
let
  -- forbid legitimate configurations for the initial one
--  assert(not (legitimate<<actions_number, card>>(enables, inits)));

-- Bad idea in the end
--  assert(not(exists_a_smaller_rot_mir<<card>>(inits)));

  -- not necessary, saves a few percents
--  assert( --rangei_basic<<card-1>>(inits) and
--          inits[0]=0);

--  assert(rangei<<card-1>>(inits));

  assert(rangei_alt(inits));
  assert(true -> daemon<<actions_number,card>>(activations, pre enables));
  assert(true -> inits = pre inits);
--  inits = inits0 -> pre(inits);

  config, enables, lustre_round, lustre_round_nb = topology(activations, inits);

  legitimate = legitimate<<actions_number, card>>(enables, config);
-- When a no cycle exists, it is better (for verification time) to use rot_mir. Otherwise, it isn't.
  noloop = not (rot_mir<<card>>(inits, config));
--  noloop = not (inits = config);
  ok = true -> noloop or legitimate;
tel;

-----------------------------------------------------------------------------------
-- c1 and c2 are equals up to rotation and mirroring
-- this version uses mirror, which uses concat (which is kind2 un-friendly)
function rot_mir_concat<<const card:int>>(c1, c2:state^card) returns (res:bool);
let
  res = rot<<card>>(c1,c2) or rot<<card>>(mirror<<card>>(c1), c2);
tel

-- the same without concat: more involved (and more efficient?)
function rot_mir<<const card:int>>(c1, c2:state^card) returns (res:bool);
let
  res = rot<<card>>(c1,c2) or rot_mir_i<<card,card>>(c1,c2);
tel
node rot_mir5 = rot_mir<<5>>

-- it is the same code as rot, but checking that all possible rot of c2 is a mirror of c1
function rot_mir_i<< const card:int; const i:int >>(c1, c2:state^card) returns (res:bool);
let
  res = with (i=0) then false else
        rot_mir_i<<card,i-1>>(c1,c2) or rot_mir_i_j<<card, i, card>>(c1,c2);
tel
--
function rot_mir_i_j<< const card:int; const i:int ; const j:int >>
                     (c1, c2:state^card) returns (res:bool);
let
  res = with (j=0) then true else
          ((c1[(j-1) mod card] = c2[(card+card-j-i) mod card]) -- any rot is a mirror
      and rot_mir_i_j<<card, i, j-1>>(c1,c2));
tel
node check_rot_mir<<const card:int>>(c1, c2:state^card) returns (res:bool);
let
  res = (rot_mir_concat<<card>>(c1,c2) = rot_mir<<card>>(c1,c2));
tel
node check_rot_mir2 = check_rot_mir<<2>>
node check_rot_mir3 = check_rot_mir<<3>>
node check_rot_mir4 = check_rot_mir<<4>>
node check_rot_mir5 = check_rot_mir<<5>>
node check_rot_mir6 = check_rot_mir<<6>>
node check_rot_mir7 = check_rot_mir<<7>>
node check_rot_mir8 = check_rot_mir<<8>>

-- correct for arrays of size 2 to n
node check_rot_mir_2<<const n:int>>(c1, c2:state^n) returns (res:bool);
let
  res = with n=1 then true else check_rot_mir<<n>>(c1,c2)
                            and check_rot_mir_2<<n-1>>(c1[0..n-2],c2[0..n-2]);
tel
node check_rot_mir_2_10 = check_rot_mir_2<<10>>

(* To verify with kind2 that the 2 rot_mir are equivalent for arrays of size 2 to 10:

 lv6 state.lus check_cycles.lus -n check_rot_mir_2_10 -ec | sed "s/tel/--%MAIN ;\n--%PROPERTY res;\ntel\n/" > checkme.ec
 kind2 checkme.ec
*)

-----------------------------------------------------------------------------------

function rot<<const card:int>>(c1, c2:state^card) returns (res:bool);
let
  res = rot_i<<card,card>>(c1,c2);
tel
function rot_i<< const card:int; const i:int >>(c1, c2:state^card) returns (res:bool);
let
  res =
    with (i=0) then (false)
               else (rot_i<<card,i-1>>(c1,c2) or rot_i_j<<card, i, card>>(c1,c2));
tel
function rot_i_j<< const card:int; const i:int ; const j:int >>(c1, c2:state^card) returns (res:bool);
let
  res =
    with (j=0) then true
               else ( (c1[j-1] = c2[(j-1+i) mod card]) and rot_i_j<<card, i, j-1>>(c1,c2));
tel
node rot3=rot<<3>>;
node rot4=rot<<4>>;

(* verify rot is correct
 lv6 state.lus check_cycles.lus -n check_rot5 -ec | sed "s/tel/--%MAIN ;\n--%PROPERTY res;\ntel\n/" > checkme.ec
 kind2 checkme.ec
*)
node check_rot<<const card:int>>(c1,c2:state^card) returns (res:bool);
let
  res = (rot<<card>>(c1,c2) = rot<<card>>(c2,c1));
tel
node check_rot5 = check_rot<<5>>;


-----------------------------------------------------------------------------------
function mirror<<const card:int >>(config: state^card) returns (res: state^card);
let
  res = with (card=1) then [config[0]]
        else mirror<<card-1>>(config[1..card-1]) | [config[0]];
tel


-- nb: is_mirror doesn't use concat, but is more complicated to write
function is_mirror<<const N:int >>(config, config2: state^N) returns (res: bool);
let
  res = is_mirror_do<<N; 0>>(config, config2);
tel
function is_mirror_do<<const N:int; const i:int>>(c1, c2: state^N) returns (res: bool);
let
  res =
    with (i=N/2) then (if N mod 2 = 0 then true else c1[N/2]=c2[N/2])
                 else (c1[i] = c2[N-i-1] and c2[i] = c1[N-i-1] and is_mirror_do<<N, i+1>>(c1,c2));

tel
node is_mirror4=is_mirror<<4>>;
node is_mirror5=is_mirror<<5>>;

node check_mirror<<const card:int>>(c1:state^card) returns (res:bool);
let
  res = (is_mirror<<5>>(c1,mirror<<card>>(c1)));
tel
node check_mirror5 = check_mirror<<5>>;

(* verify that is_mirror is correct (wrt mirror)
 lv6 state.lus check_cycles.lus -n check_mirror5 -ec | sed "s/tel/--%MAIN ;\n--%PROPERTY res;\ntel\n/" > checkme.ec
 kind2 checkme.ec
*)
-----------------------------------------------------------------------------------
node rangei4<<const i:int; const k:int>>(c:state^card) returns (res:bool);
let
  res = with i = 0  then c[0] = 0 else
--        with i <= k then
                      0 <= c[i] and c[i] <= i and rangei4<<i-1,k>>(c);
--                    else 0 <= c[i] and c[i] <= k and rangei<<i-1,k>>(c);
tel

function rangei<<const i:int>>(c:state^i+1) returns (res:bool);
let
  res =  0 <= c[i] and
        with i = 0 then c[0]=0
   else with i < k then rangei<<i-1>>(c[0..i-1]) and c[i] <= i
                   else rangei<<i-1>>(c[0..i-1]) and c[i] <= k ;
tel


type acc = { i:int; res:bool };
function range_0_k(acc: acc; ci:state) returns (res:acc);
let
   res = acc { i = acc.i+1; res = acc.res and 0 <= ci  and ci < k};
tel
function rangei_alt(c:state^card) returns (res:bool);
let
  res = fillred<<range_0_k, card>>(acc {i=0;res=true}, c).res;
tel



function rangei_basic<<const i:int>>(c:state^card) returns (res:bool);
let
  res = with i < 0  then true
                    else 0 <= c[i] and c[i] <= k and rangei_basic<<i-1>>(c);
tel


-- all states are initially in [0; card|[
function rangeii<<const low:int; const k:int>>(c:state^card) returns (res:bool);
var
  ranges_min, ranges_max : bool^card;
let
   ranges_min = map<< <= , card>>(low^card, c);
   ranges_max = map<< <= , card>>(c, k^card);
   res = boolall<<card>>(ranges_min) and boolall<<card>>(ranges_max);
tel


-----------------------------------------------------------------------------------
-- unused


function smaller<< const N : int >>(c1,c2:state^N) returns (res:bool);
let
  res =  with (N=1) then (c1[0] < c2[0])
                    else (c1[0] < c2[0]) or (c1[0]=c2[0] and smaller<<N-1>>(c1[1..N-1], c2[1..N-1]));

tel

function exists_a_smaller_rot_mir(c1:state^card) returns (res:bool);
var
  c2 : state^card;
let
  c2 = mirror<<card>>(c1);
  res = smaller<<card>>(c2, c1) or exists_a_smaller_rot(c1, c1)
                                or exists_a_smaller_rot(c1, c2);
tel

-- there exists a rotation of c2 that is smaller than c1
function exists_a_smaller_rot(c1, c2:state^card) returns (res:bool);
let
  res = exists_a_smaller_rot_rec<<card-1>>(c1, c2);
tel
-- there exists a rotation (among N) of c2 that is smaller than c1
function exists_a_smaller_rot_rec<< const N:int>>(c1, c2:state^card) returns (res:bool);
var c2_rot:state^card;
let
  c2_rot = c2[1..card-1] | [c2[0]];
  res =
    with (N=1) then smaller<<card>>(c2_rot, c1)
               else smaller<<card>>(c2_rot, c1) or exists_a_smaller_rot_rec<<N-1>>(c1,c2_rot);
tel
