#!/bin/bash
 set -x

MY_DIR=$(dirname $(readlink -f $0))



source $MY_DIR/gen-salut-batch.sh

help=" Generate a (standalone) lustre program that runs salut with a lustre daemon
usage:
  $0 daemon topology topo_size action_number step_number
"
if [[ $# < 6 ]]
    then
    printf "5 arguments are needed!\n$help"
fi

STEP_NUMBER=$5

################################################################
lv6 -2c -cc $outfile -n ${main_node} ${LIB_LUS} -dir ${main_node} > lv6_${main_node}.log

start=`date +%s.%N`

[ -f ./${main_node}.exec ] && \
    (for (( c=1; c<=$STEP_NUMBER; c++ )) ; do echo $((((1<<15)| RANDOM))); done ; echo q) | \
    time ./${main_node}.exec   | tail

end=`date +%s.%N`

runtime_salut=$( echo "$end - $start" | bc -l )

################################################################
case $daemon in
    synchronous)
	sasad="-sd"
	;;
    central)
	sasad="-cd"
	;;
    *)
	sasad=""
	;;
esac


# set -x
start=`date +%s.%N`
time sasa -restart ${sasad} ${topology}${n}.dot -l ${STEP_NUMBER}  > sasa_${main_node}.log
end=`time date +%s.%N`

runtime_sasa=$( echo "$end - $start" | bc -l )

################################################################

ratio1=$( echo "${runtime_salut}  / ${runtime_sasa} * 100 " | bc -l )
ratio2=$( echo "$runtime_sasa  / $runtime_salut * 100 " | bc -l )

LC_NUMERIC="en_US.UTF-8"

printf "   ===> speedup = %.2f (%.2f)\n" $ratio1 $ratio2

echo "* $0 $* " `date +"%d/%m/%Y- %H:%M:%S"` >> ../result.org
echo "   ===> salut a mis $runtime_salut secondes"  >> ../result.org
echo "   ===> sasa a mis $runtime_sasa secondes"  >> ../result.org
printf "   ===> speedup = %.2f (%.2f)\n" $ratio1 $ratio2 >>  ../result.org

DATE=`date +"%d/%m/%Y-%H:%M:%S"`
algodir=$(dirname $PWD)
algo=$(basename $algodir)
printf "| %s | %s | %s | %s | %s | %.2f | %.2f | %.2f | %s | %s  |\n"  \
    ${topology} ${size} $algo  $daemon \
    ${STEP_NUMBER} $runtime_salut $runtime_sasa $ratio1  "$DATE"  `uname -n` >>  ../../result-tab.org
