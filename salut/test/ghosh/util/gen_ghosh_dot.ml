
(* usage: ocaml gen_ghosh_dot.ml 22  *)

open Printf

let f n =
  let fn = Printf.sprintf "ghosh%d.dot" n in
  let oc = open_out fn in
  let pn = fprintf oc "%s\n" in
  pn "graph g {";
  pn"  p0 [algo=\"p0.ml\"]";
  for i=1 to n-2 do
    if i mod 2 = 1 then
      fprintf oc "  p%d [algo=\"p_odd.ml\"]\n" i
    else
      fprintf oc "  p%d [algo=\"p_even.ml\"]\n" i ;
  done;
  fprintf oc "  p%d [algo=\"p_last.ml\"]\n" (n-1);
  pn "\n# transitions";
  for i=1 to n-1 do
    fprintf oc "  p%d -- p%d \n" i (i-1);
  done;
  for i=0 to n-3 do
    fprintf oc "  p%d -- p%d \n" (i) (i+2)
  done;
  pn "}";

  close_out oc;
  Printf.printf "File %s has been generated\n" fn

let _ = f (int_of_string Sys.argv.(1))
