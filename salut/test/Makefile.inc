#
# To use this file, define the following variables and `include` it:
#   SASA_ALGOS   # the of files (blank separated) defining the enable and step functions
#   DECO_PATTERN # specift how to decorate (gg generated) .dot files gg-deco
# and optionnaly
#   DOT2LUSFLAGS # flags for the dot to lustre translater
#   SASAFLAGS    # flags for sasa
#   TOPOLOGY     # the topology used by defaukt
#

TOPOLOGY=grid4

.PHONY = build all clean test verify

DOT2LUS := salut

build: $(TOPOLOGY).lus

verif: $(TOPOLOGY).lus $(TOPOLOGY).cmxs $(TOPOLOGY)_verify.lv4

genclean:
	rm -f *.cmxs *.cmx *.cmi *.o
	rm -f $(TOPOLOGY).ml
	rm -f *.rif *.seed *.cov .rdbg-cmds.ml
	rm -f $(TOPOLOGY).lus
	rm -f *_verify.lv4 sasa-*.dot *.gp
	rm -rf *.c *.h *_const.lus *.cmt *.exec *.sh luretteSession* .rdbg-session* *.log *.pdf
	rm -f daemon_hyp.lus
	dune clean

# generating lv6 files of out dot files
%_const.lus: %.dot
	$(DOT2LUS) $(DOT2LUSFLAGS) $<
%.lus: %_const.lus

LIB=-package algo
OCAMLOPT=ocamlfind ocamlopt -bin-annot


ROOTDIR=$(shell git rev-parse --show-toplevel || echo $(CI_PROJECT_DIR))
LIB_LUS=$(ROOTDIR)/test/lustre/round.lus $(ROOTDIR)/salut/lib/daemon.lus
RUN_KIND2=$(ROOTDIR)/salut/test/run-kind2.sh


##########################################################################################"

ifneq ("$(wildcard /usr/bin/time)","")
    TIME=/usr/bin/time
else
    TIME=
endif

##########################################################################################"
# Simulates the Lustre algo with rdbgui4sasa

%.simu:
	make $*.rdbgui4sasa
%.rdbgui4sasa: %.cma
	make $*_const.lus
	rdbgui4sasa -l 10000 -sut-nd "lv6 $(LIB_LUS) $*_const.lus $*.lus  -n $* -exec"

##########################################################################################"
# Use lurette to compare the lustre and the ocaml version of the algorithm
# nb : the <dirname>/<dirname>_oracle.lus should exist and contain a valid oracle

%.lurette:
	make $*.dot $*_const.lus $*.cmxs $*_oracle.lus
	lurette -sut "sasa  $*.dot $(SASAFLAGS)" -oracle "lv6  $(LIB_LUS)  $*.lus $*_oracle.lus -n oracle -2c-exec"

# ditto, but using nodes where all locals are outputs
# provided that <xxx>_oracle.lus define a <xxx>_oracle_debug node
# (cf, e.g., salut/test/ring-orientation/ring_orientation_oracle.lus)
%.lurette.debug:
	make $*.dot $*_const.lus $*.cmxs $*_oracle.lus
	lurette -sut "sasa -seed 172317971 $*.dot $(SASAFLAGS)" -oracle "lv6  $(LIB_LUS)  $*.lus $*_oracle.lus -n oracle_debug -exec"

# ditto, without a seed
%.lurette_no_seed:
	make $*.dot $*_const.lus $*.cmxs $*_oracle.lus
	lurette -sut "sasa $*.dot $(SASAFLAGS)" -oracle "lv6  $(LIB_LUS) $*.lus $*_oracle.lus -n oracle -2c-exec"


%.cmxs: %.dot
	topology=$* dune build
%.cma: %.dot
	topology=$* dune build


%_oracle.lus: %.cmxs
	sasa -glos $*.dot || echo ""


##########################################################################################"
# model-checking with lesar


# Do not expand nodes
%_verify.noexpand.lv4:  verify.lus
	make $*_const.lus
	lv6 -eei $(LIB_LUS) $*.lus $^ $*_const.lus -n $(prop) -rnc --lustre-v4 -o $@
	cat $@ | tr '\n' '@' |sed "s/tel@/tel/g" | sed "s/@/\\n/g" | sed "s/tel-- end of node verify/--%MAIN ;\n--%PROPERTY ok;\ntel\n/"  > $@.tmp
	mv $@.tmp $@

##########################################################################################"
# model-checking with lesar
%_verify4pascal.ec: %.dot verify.lus
	make $*_const.lus
	lv6  $(LIB_LUS) $*.lus $*_const.lus verify.lus -n $(prop) -ec -o $@

# to use lesar, perform bit-blasting via ec2ec -usrint
%_verify.ec: %.dot verify.lus
	make $*_const.lus
	lv6  $(LIB_LUS) $*.lus $*_const.lus verify.lus -n $(prop) -ec -o $@.tmp
	echo "include \"$(ROOTDIR)/salut/lib/bit-blast/signed_binary6.lus\"" > $@.tmp2
	echo "include \"$(ROOTDIR)/salut/lib/bit-blast/binary.lus\"" >> $@.tmp2
	ec2ec -usrint $@.tmp >> $@.tmp2
	cat $@.tmp2 | tr '\n' '@' |sed "s/tel@/tel/g" | sed "s/@/\\n/g" | sed "s/tel-- end of node verify/--%MAIN ;\n--%PROPERTY ok;\ntel\n/"  > $@.tmp
	mv $@.tmp $@ ; rm  $@.tmp2


%.lesar:%_verify.ec
	$(TIME) lesar $< $(prop) -forward -states 10000000

##########################################################################################"
# Compare sasa and salut simulation perf
# cf salut-vs-sasa.sh and gen-salut-batch.sh

-include Makefile.local
