include "cost.lus"
include "daemon_hyp.lus"
const n = card;  -- processes nb: automatically generated
-- Theorem 6.30 p76 in [zebook]
const worst_case = n*(n-1) + (n-4)*(n+1) div 2 + 1;-- in steps
node verify(active : bool^1^n; init_config : state^n)
returns (enabled : bool^1^n; ok : bool);
var
  config : state^n;
--  enabled : bool^1^n; -- 1 because that algo has only 1 rule
  enabled1: bool^n;   -- enabled projection
  legitimate, round : bool;
  closure, converge_cost, converge_wc, converge, wc_is_tigth : bool;
  steps, cost, round_nb : int;
let
  assert(true -> daemon<<1,n>>(active, pre enabled));
  assert(rangei<<0,n>>(init_config)); -- to speed-up the proof
  config, enabled, round, round_nb = topology(active, init_config);
  -- stability = mutual exclusion: exactly 1 node is enabled at a time
  enabled1 = map<<nary_or<<1>>,n>> (enabled); -- projection
  legitimate = nary_xor<<card>>(enabled1);
  closure = true -> (pre(legitimate) => legitimate);
  cost = cost(enabled, config);
  converge_cost = (true -> legitimate or pre(cost)>cost);
  steps = 0 -> (pre(steps) + 1); -- 0, 1, 2, ...
  converge_wc = (steps >= worst_case) => legitimate;
  wc_is_tigth = (steps >= worst_case - 1) => legitimate;
  assert(true -> init_config = pre init_config);
  converge = true -> ((init_config = config) => legitimate);
  ok = closure and converge_cost and converge_wc and converge;
tel;

function rangei<<const low:int; const n:int>>(c:state^n)
returns (res:bool);
var
  ranges_min, ranges_max : bool^n;
let
   ranges_min = map<< <= , n>>(low^n, c);
   ranges_max = map<< < , n>>(c, n^n);
   res = boolall<<n>>(ranges_min) and boolall<<n>>(ranges_max);
tel;
