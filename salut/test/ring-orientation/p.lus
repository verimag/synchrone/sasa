-- Time-stamp: <modified the 20/03/2024 (at 16:05) by Erwan Jahier>

-----------------------------------------------------------
--Step and Enable functions:
function p_enable<<const degree:int>>(q : state; neighbors : neigh^degree)
returns (enabled : bool^actions_number);
var
  na : state^degree;
  p, r : state;
let
  na = map<<state,degree>>(neighbors);
  p,r = if q.rank_p_0
        then (na[0],na[1])
        else (na[1],na[0]);-- if p is at the 'edge' of the pseuso-ring (first or last)

  enabled = [
  -- A (0)
      p.color_0 and     q.color_0 and     r.color_0,
  -- B (1)
      p.color_0 and not q.color_0  and    r.color_0
     and  q.phase_pos, -- not in Figure 3, but necessary to converge!
  -- C (2)
  not p.color_0 and not q.color_0 and not r.color_0,
  --D (3)
  not p.color_0 and     q.color_0 and not r.color_0
  and q.phase_pos , -- ditto

  -- E (4)
   ((      p.color_0 and     p.phase_pos and
          q.color_0 and not q.phase_pos and
      not r.color_0 and not r.phase_pos)
  or
  (  not p.color_0 and not p.phase_pos and
          q.color_0 and not q.phase_pos and
          r.color_0 and     r.phase_pos)),

  -- F (5)
    (( not p.color_0 and     p.phase_pos and
      not q.color_0 and not q.phase_pos and
          r.color_0 and not r.phase_pos)
     or
     (      p.color_0 and not p.phase_pos and
      not q.color_0 and not q.phase_pos and
      not r.color_0 and     r.phase_pos)),

  -- G (6)
  ( (     p.color_0 and not p.phase_pos and
          q.color_0 and not q.phase_pos and
      not r.color_0       )
 or ((not p.color_0 and
          q.color_0 and not q.phase_pos and
          r.color_0 and not r.phase_pos))),
  -- H (7)
  ( (     p.color_0 and     p.phase_pos and
          q.color_0 and     q.phase_pos and
      not r.color_0       )
 or ((not p.color_0 and
          q.color_0 and     q.phase_pos and
          r.color_0 and     r.phase_pos))),
  -- I (8)
  ( ( not p.color_0 and not p.phase_pos and
      not q.color_0 and not q.phase_pos and
          r.color_0       )
 or ((    p.color_0 and
      not q.color_0 and not q.phase_pos and
      not r.color_0 and not r.phase_pos))),
  -- J (9)
  ( ( not p.color_0 and     p.phase_pos and
      not q.color_0 and     q.phase_pos and
          r.color_0       )
 or ((    p.color_0 and
      not q.color_0 and     q.phase_pos and
      not r.color_0 and     r.phase_pos)))
];
tel;

function p_step<<const d:int>>(
  q : state;
  neighbors : neigh^d;
  a : action)
returns (new_q : state);
var
  na : state^d;
  p, r : state;
let
  na = map<<state,d>>(neighbors);
  p,r = if q.rank_p_0
        then (na[0],na[1])
        else (na[1],na[0]);-- if p is at the 'edge' of the pseuso-ring (first or last)
  new_q=if a=A then
          state { color_0=false;phase_pos=false;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }
     else if a=B then
            state { color_0=false;phase_pos=false;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }
     else if a=C then
             state { color_0=true ;phase_pos=false;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }
     else if a=D then
             state { color_0=true ;phase_pos=false;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }

     else if a=E then
             state { color_0=false;phase_pos=true ;orient_left=orient(p,r); rank_p_0=q.rank_p_0 }
--     else if a=Epr then
--             state { color_0=false;phase_pos=true ;orient_left=orient(p,r) }
--     else if a=Erp then
--             state { color_0=false;phase_pos=true ;orient_left=orient(r,p) }
     else if a=F then
             state { color_0=true ;phase_pos=true ;orient_left=orient(p,r); rank_p_0=q.rank_p_0 }
--     else if a=Fpr then state { color_0=true ;phase_pos=true ;orient_left=orient(p,r) }
--     else if a=Frp then state { color_0=true ;phase_pos=true ;orient_left=orient(r,p) }

     else if a=G then
             state { color_0=true ;phase_pos=true ;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }
     else if a=H then
             state { color_0=true ;phase_pos=false;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }
     else if a=I then
             state { color_0=false;phase_pos=true ;orient_left=q.orient_left; rank_p_0=q.rank_p_0 }
     else (* a=J *)
             state { color_0=false;phase_pos=false;orient_left=q.orient_left; rank_p_0=q.rank_p_0 };
tel;

function orient(p,r : state) returns (res : bool);
let
  res = (p.phase_pos and not r.phase_pos);
tel;

(* config_0_color_0              false  1    true 0
    config_0_phase_pos            false -    true +
    config_0_orient_left          false      true

    config_1_color_0               true 0   true  0
    config_1_phase_pos            false -   false -
    config_1_orient_left          false     false

    config_2_color_0              false  1  false 1
    config_2_phase_pos             true  +  true  +
    config_2_orient_left          false     false
    == Locals ==
                                        1-g        2-e
    config_0_color_0              false 1    false  1   false 1
    config_0_phase_pos            false -    false  -   false -
    config_0_orient_left          false      false     false <-

    config_1_color_0               true 0    true  0    true   0
    config_1_phase_pos            false -    true  +    true   +
    config_1_orient_left          false      false      false <-

    config_2_color_0               true 0    true  0    false 1
    config_2_phase_pos            false -    false -    true  +
    config_2_orient_left          false      false      true ->

    legitimate                    false      true!!     false

    enabled_1_1                   t                                                                       t
    enabled_2_4                         t
    enabled_0_3                               t
    enabled_1_5                                     t
    enabled_2_1                                           t
    enabled_0_4                                                 t
    enabled_1_3                                                       t
    enabled_2_5                                                             t
    enabled_0_1                                                                   t
    enabled_1_4                                                                         t
    enabled_2_3                                                                               t
    enabled_0_5                                                                                     t

    config_0_color_0              0     0     0     0     0     0     1     1     1     1     1     1     0
    config_0_phase_pos            +     +     +     -     -     -     +     +     +     -     -     -     +

    config_1_color_0              1     1     1     1     0     0     0     0     0     0     1     1     1
    config_1_phase_pos            +     -     -     -     +     +     +     -     -     -     +     +     +

    config_2_color_0              0     0     1     1     1     1     1     1     0     0     0     0     0
    config_2_phase_pos            -     -     +     +     +     -     -     -     +     +     +     -     -



*)