#!/bin/bash
#set -x

if [ -z ${ROOTDIR+x} ]; then ROOTDIR=$(git rev-parse --show-toplevel); fi
LIB_LUS="${ROOTDIR}/test/lustre/round.lus ${ROOTDIR}/salut/lib/daemon.lus"
daemon_lustre_file=${ROOTDIR}/salut/lib/daemon.lus

help=" Generate a (standalone) lustre program that runs salut with a lustre daemon

It is useful to perform batch simulations.

usage:
  $0 daemon topology n type [an]

where:
  - daemon is the name of one the node in ${daemon_lustre_file}
  - topology is one of graph supported by gg (ring, diring, grid, etc.)
  - n is the size of the topology [*]
  - type is int or bool (used to generate initial states) or bfs or hoepman (ad-hoc)
  - an is the action number of the algorithm (1 by default)

nb: only work for algorithms which node state is made of an integer or a boolean

[*] except for grids, where the size of the topology would be n*n

"

# 3 or 4 arguments only
  if [[ $# < 4 ]]
  then
      printf "$help"
      exit 2
  elif [[ $# = 3 ]]
  then
      an=1
  else
      an=$5
fi
daemon=$1
topology=$2
n=$3
type=$4

  if [[ $topology == "grid" ]]
  then
      size=$(($n * $n))
  else
      size=$n
  fi

make ${topology}${n}.dot
make ${topology}${n}.cmxs


TMP=.
# work in a tmp dir to ease the cleaning
#  TMP=.tmp
#  [ -d ${TMP} ] || mkdir ${TMP}
#  cp Makefile ${TMP}
#  cp ${ROOTDIR}/salut/test/Makefile.inc  ${TMP}
#  cp ${ROOTDIR}/salut/test/Makefile.dot  ${TMP}
#  cp *.ml *.lus ${TMP}
#  cp -f ${topology}*  ${TMP}

# to measure the perf, we don't want the algo to stop too soon.
echo "let legitimate = None" >> ${TMP}/config.ml
cd  ${TMP}

main_node=${daemon}_${topology}${n}
outfile=${main_node}.lus
dotfile=${topology}${n}.dot
lusfile=${topology}${n}.lus
lusconstfile=${topology}${n}_const.lus

make ${dotfile} || (echo "Beware: Makefile.dot should be in the scope of make!"; exit 2)

salut ${dotfile}

# XXX this work only if the state is made of an integer or a boolean!
  if [[ $type == "bool" ]]
      then
      config_init=`printf "["; for ((a=1; a < $size ; a++)) do printf "%s," $(printf "t\nf" | shuf -n 1); done; printf "%s]" $(printf "t\nf" | shuf -n 1)`
  elif [[ $type == "int" ]]
      then
      config_init=`printf "["; for ((a=1; a < $size ; a++)) do printf "%d," $RANDOM; done;  printf "%d]" $RANDOM`
  elif [[ $type == "bfs" ]]
      then
      config_init=`printf "["; for ((a=1; a < $size ; a++)) do printf "state { d=%d ; par=%d }," $RANDOM $RANDOM; done;  printf "state { d=%d ; par=%d }]" $RANDOM $RANDOM`
  elif [[ $type == "hoepman" ]]
      then
      config_init=`printf "["; for ((a=1; a < $size ; a++)) do printf "state { color_0=%s ; phase_pos=%s ; orient_left=%s }," $(printf "t\nf" | shuf -n 1) $(printf "t\nf" | shuf -n 1) $(printf "t\nf" | shuf -n 1); done;  printf "state { color_0=%s ; phase_pos=%s ; orient_left=%s }]" $(printf "t\nf" | shuf -n 1) $(printf "t\nf" | shuf -n 1) $(printf "t\nf" | shuf -n 1)`
  elif [[ $type == "kclust" ]]
      then
      config_init=`printf "["; for ((a=1; a < $size ; a++)) do printf "state { isRoot=%s; alpha=%d ; par=%d }," $(printf "t\nf" | shuf -n 1) $RANDOM $RANDOM; done;  printf "state { isRoot=%s; alpha=%d ; par=%d }]" $(printf "t\nf" | shuf -n 1) $RANDOM $RANDOM`
  fi


#
if [[ $an -eq 2 ]]
    then
    enab_init="false,false"
elif [[ $an -eq 1 ]]
    then
    enab_init="false"
elif [[ $an -eq 12 ]]
    then
    enab_init="false,false,false,false,false,false,false,false,false,false,false,false"
else
    echo "$0 : only works if the action number is 1, 2, or 12 ...):"
    exit 1;
fi

timestamp=`date +"%d/%m/%Y- %H:%M:%S"`
printf "(* Automatically generated ($timestamp) by '$0 $*' *)

include \"${daemon_lustre_file}\"
include \"${lusfile}\"
include \"${lusconstfile}\"

node ${main_node}(any:int)\n\
    returns (p_c : state^${size}; Enab_p : bool^${an}^${size});\n\
var\n\
  round:bool; round_nb:int;\n\
  initials : state^${size};\n\
let\n\
    initials = ${config_init};\n\
    p_c, Enab_p, round, round_nb = ${topology}${n}([${enab_init}] ^ ${size} -> ${daemon}<<$an,$size>>(any, pre Enab_p), initials);\n\
tel"   > $outfile

echo "
The file '$outfile' has been generated.
To run simulations, you can generate an executable:

   lv6 -2c -cc $outfile -n ${main_node} ${LIB_LUS} -dir ${main_node}

and run it:

  ./${main_node}.exec

or, to avoid the need to provide the dummy input:

   (for i in {1..10}; do echo $((((1<<15)| RANDOM))); done ; echo q) | ./${main_node}.exec
   (for i in {1..10}; do echo $((((1<<15)| RANDOM))); done ; echo q) | time ./${main_node}.exec  > /dev/null
"
