-----------------------------------------------------------------
--Get parent function:
function parent<<const n:int; const count:int>>(this : state; neighbors : neigh^n)
returns (res : neigh);
let
  res = if (n<=this.par) then neighbors[n-1]
        else with (count = n ) then neighbors[n-1]
        else if (count = this.par) then neighbors[count] else parent<<n,(count+1)>>(this,neighbors);
tel;

------------------------------------------------------------------
--Predicates:
function children<<const n:int>>(u:state; neighbors:neigh^n)
returns ( res : bool^n );
var v:state;
let
  v = state (neighbors[0]);
  res = with (n=1) then [ ((u.st <> I) and
                                   (v.st <> I) and
                                   (reply(neighbors[0]) = v.par) and
                                   (v.d >= u.d + weight(neighbors[0])) and
                                   ((v.st = u.st) or (u.st = EB))) ]
        else [ ((u.st <> I) and
                (v.st <> I) and
                (reply(neighbors[0]) = v.par) and
                (v.d >= u.d + weight(neighbors[0])) and
                ((v.st = u.st) or (u.st = EB))) ] | children<<n-1>>(u,neighbors[1..n-1]);
tel;

function abRoot<<const n:int>>(this : state; neighbors : neigh^n)
returns(res : bool);
var
  par_u: state;
  par_u_neigh:neigh;
let
  par_u_neigh= parent<<n,0>>(this, neighbors);
  par_u = state(par_u_neigh);
  res = (this.st <> I) and ( (this.par > n) or
                             (par_u.st = I) or
                             (this.d < (par_u.d + weight(par_u_neigh) )) or
                             (this.st <> par_u.st and par_u.st <> EB));
tel;

function p_reset<<const n:int>>(this : state; neighbors: neigh^n)
returns (res : bool);
let
  res = ( (this.st = EF) and (abRoot<<n>>(this,neighbors)) );
tel;

function condition(elem:neigh; this:state) returns(res : bool);
var
  e:state;
let
  e = state(elem);
  res = (e.st = C) and (e.d + weight(elem) < this.d);
tel;

function p_correction<<const n:int>>(this : state; neighbors : neigh^n)
returns(res : bool);
let
  res = nary_or<<n>>(map<<condition,n>>(neighbors,(this^n)));
tel;

---------------------------------------------------------------------
--Macro:
function argmin<<const n:int; const count:int; const indice:int>>(min:state; w:int; nl : neigh^n)
returns(res : int);
var
  elem:state;
let
  elem=state(nl[0]);
  res = with ( n = 1) then (if elem.st = C and min.st = C then
                             if (elem.d + weight(nl[0])) <= (min.d + w) then count else indice
                            else if elem.st = C then count else indice)
        else (if elem.st = C then
                if min.st = C then
                  if (elem.d + weight(nl[0])) <= (min.d + w) then argmin<<n-1,count+1,count>>(elem,weight(nl[0]),nl[1..n-1])
                  else argmin<<n-1,count+1,indice>>(min,w,nl[1..n-1])
                else
                  argmin<<n-1,count+1,count>>(elem,weight(nl[0]),nl[1..n-1])
              else
                argmin<<n-1,count+1,indice>>(min,w,nl[1..n-1])
              );
tel;

function computePath<<const n:int>>(this : state ; neighbors : neigh^n)
returns(res : state);
var par_u:neigh;
let
  par_u = parent<<n,0>>(this,neighbors);
  res = state { st = C;
                par = argmin<<n,0,0>>((state { st=EB; par=-1; d=-1 }), 1, neighbors);
                d = (state(par_u).d + weight(par_u)); };
tel;

---------------------------------------------------------------------
--Enable Macro function:
function for_all<<const n:int>>(neigh_list: neigh^n; bool_list: bool^n)
returns (res : bool);
var
  elem:state;
let
  elem = state(neigh_list[0]);
  res = with (n = 1) then (if bool_list[0] = true
                           then elem.st = EF
                           else true)
        else (if bool_list[0] = true
              then (elem.st = EF) and (for_all<<n-1>>(neigh_list[1 .. n-1], bool_list[1 .. n-1]))
              else (for_all<<n-1>>(neigh_list[1 .. n-1], bool_list[1 .. n-1])));
tel;

function not_equal_c (elem:neigh)
returns (res : bool);
let
  res = (state(elem).st<>C);
tel;

function equal_c (elem:neigh)
returns (res : bool);
let
  res = (state(elem).st=C);
tel;
--------------------------------------------------------------------
--Step and Enable functions:
function p_enable<<const degree:int>>(this : state; neighbors : neigh^degree)
returns (enabled : bool^actions_number);
var
  par_u:state;
  bool_list:bool^degree;
let
  par_u = state(parent<<degree,0>>(this, neighbors));
  bool_list = children<<degree>>(this, neighbors);
  enabled = [ ((this.st = C) and p_correction<<degree>>(this,neighbors)),
              ((this.st = C) and (not (p_correction<<degree>>(this,neighbors))) and ((abRoot<<degree>>(this,neighbors)) or (par_u.st = EB))),
              ((this.st = EB) and (for_all<<degree>>(neighbors, bool_list))),
              ((p_reset<<degree>>(this, neighbors)) and boolred<<degree,degree,degree>>(map<< not_equal_c , degree>>(neighbors))),
              (((p_reset<<degree>>(this,neighbors)) or this.st = I) and (boolred<<1,degree,degree>>(map<< equal_c , degree>>(neighbors))))
            ];
tel;

function p_step<<const degree:int>>(
  this : state;
  neighbors : neigh^degree;
  action : action)
returns (new : state);
let
  new = if (action = Rc) then computePath<<degree>>(this, neighbors)
        else if (action = Reb) then state { st = EB; par = this.par; d = this.d }
        else if (action = Ref) then state { st = EF; par = this.par; d = this.d }
        else if (action = Ri) then state { st = I; par = this.par; d = this.d }
        else if (action = Rr) then computePath<<degree>>(this, neighbors)
        else this;
tel;
