#include "utils_random.h"
#include <assert.h>
void utils_c_rand_step(_integer x,_integer *res){
  assert (0 < x );
  *res = (rand() % x);
}
