/* This file was generated by lv6 version v6.106.1. */
/*  lv6 utils.lus -n random -2c -exec -dir Random */
/* on PC-Travail the 18/07/2022 at 15:00:26 */
#include "utils_random.h"
//// Defining step functions
// Step function(s) for utils_c_rand_ctx
// Step function(s) for utils_random_ctx
void utils_random_step(_integer x,_integer *res){

  utils_c_rand_step(x,res); 

} // End of utils_random_step

