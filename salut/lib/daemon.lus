-- Defines various daemons (recognizers and generators)

include "utils.lus"
include "bitset.lus"

--------------------------------------------------------------------------------------------
-- recognizers version, for test oracles or model-checking purposes

-- All enabled nodes are activated.
function daemon_is_synchronous<<const an:int; const pn:int>>
(acti, enab : bool^an^pn) returns (ok:bool);
let
  ok = (acti = enab);
tel;

function daemon_is_valid<<const an:int; const pn:int>>
(acti, enab : bool^an^pn) returns (ok : bool);
let
  ok = all_true<<pn>>(map<<activation_is_valid<<an>>, pn>>(acti, enab));
tel;

function daemon_is_distributed<<const an:int; const pn:int>>
(acti, enab : bool^an^pn) returns (ok : bool);
let
  ok = daemon_is_valid<<an, pn>>(acti, enab) and
    (   silent<<an,pn>>(enab)
           or boolred<<1,pn,pn>>(map<<nary_or<<an>>, pn>>(acti)));
tel;

-- Exactly one node is activated.
function daemon_is_central<<const an:int; const pn:int>>
(acti, enab : bool^an^pn) returns (ok : bool);
let
  ok =  daemon_is_valid<<an, pn>>(acti, enab)  and
      (   silent<<an,pn>>(enab)
           or boolred<<1,1,pn>>(map<<nary_or<<an>>, pn>>(acti)));
tel;

-- Two neighboring nodes can not be both actived.
function daemon_is_locally_central<<const an:int; const pn:int>>(
	acti, enab : bool^an^pn
) returns (ok : bool);
var
  active : bool ^ pn;
  active_adjacencies : bool ^ pn ^ pn;
  no_active_adjacencies : bool ^ pn;
  locally_central : bool ^ pn;
let
  active = map<<nary_or<<an>>, pn>>(acti);
  active_adjacencies = map<<inter<<pn>>, pn>>(active^pn, adjacency);
  no_active_adjacencies = map<<all_false<<pn>>, pn>>(active_adjacencies);
  locally_central = map<<=>,pn>>(active, no_active_adjacencies);
  ok = daemon_is_distributed<<an,pn>>(acti, enab)
      and all_true<<pn>>(locally_central);
tel;


-------------------------------------------------------------------------------------------
-- generators (for simulation)

-- the i  in input  is used  to get random  numbers from  daemons; it
-- should not be  necessary for the synchronous daemon,  but it makes
-- it more homogeneous to have the same interface for all daemons.
function synchronous<<const an:int; const pn:int>>(i:int; enab : bool^an^pn)
returns (acti: bool^an^pn);
let
  acti = enab;
tel;

function central<<const an:int; const pn:int>>(i:int;enab : bool^an^pn)
returns (acti : bool^an^pn);
var
  rng, temp, nb_enab:int;
let
  nb_enab = red<<if_true_add_1<<an>>,pn>>(0,enab);
  rng = i mod (if nb_enab = 0 then pn else nb_enab);
  temp,acti = fillred<<make_activ_tab<<an>>,pn>>(rng,enab);
tel;
function test_central = central<<1,7>>

function make_activ_tab<<const an:int>>(ain:int; elem:bool^an)
returns(aout:int; res:bool^an);
var
  elem_is_true:bool;
let
  elem_is_true = nary_or<<an>>(elem);
  res= if elem_is_true and (ain=0) then elem else false^an; -- hyp: at most 1 action/process is enab
  aout = if elem_is_true then ain-1 else ain;
tel;

function if_true_add_1<<const an:int>>(ain: int; elem: bool^an)
returns(aout : int);
let
  aout= if nary_or<<an>>(elem) then ain+1 else ain;
tel;

-- function distributed<<const an:int; const pn:int>>(enab : bool^an^pn)
-- returns (acti : bool^an^pn);
-- var
--  nb_enab,rng:int;
-- XXX todo


--------------------------------------------------------------------------------------------
-- utils

-- No node is enabled: the algorithm is silent.
function silent<<const an:int; const pn:int>>(enab: bool^an^pn)
returns (ok : bool);
let
	ok = all_false<<pn>>(map<<nary_or<<an>>, pn>>(enab));
tel;

-- An activation acti[i] is possible only if enab[i] is true.
function activation_is_valid<<const an:int>>(acti, enab : bool^an)
 returns (ok : bool);
let
  ok = all_true<<an>>(map<< =>,an>>(acti, enab));
tel;

-- All nodes enabled.
function all_active<<const an:int; const pn:int>>(enab: bool^an^pn)
returns (ok : bool);
let
        ok = all_true<<pn>>(map<<nary_or<<an>>, pn>>(enab));
tel;

-- Measures time complexity in moves.
node move_count<<const an:int; const pn:int>>(acti : bool^an^pn)
returns (count:int);
let
  count = 0 -> (pre(count) + pop_count<<pn>>(map<<nary_or<<an>>, pn>>(acti)));
tel;
