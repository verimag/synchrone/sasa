(* Time-stamp: <modified the 11/10/2021 (at 14:45) by Erwan Jahier> *)

open Sasacore
(* Process programmer API *)
type action = string (* just a label *)

type 's neighbor = {
  pid:  string;
  spid: string;
  state:  's ;
  reply: unit -> int;
  weight: unit -> int;
}

let (_compare_neighbor: 's neighbor -> 's neighbor -> int) =
  fun x y ->
  compare x.pid y.pid

let (print_neighbor: 's neighbor -> unit) =
  fun n -> Format.print_string n.pid

let (fmt_print_neighbor: Format.formatter -> 's neighbor -> unit) =
  fun fmt n ->
  Format.pp_print_string fmt n.pid

(** processes local state (user defined) *)
let (state : 's neighbor -> 's) = fun s -> s.state

(** Returns the channel number that let this neighbor access to the
   content of the process, if it neighbor can access it.  Returns -1
   if the neigbor can not access to the process, which may happen in
   directed graphs only.  This info is not available in all
   simulation modes. *)
let (reply : 's neighbor -> int) = fun s -> s.reply ()

(** Makes sense in directed graphs only *)
let (weight : 's neighbor -> int) = fun s -> s.weight ()

module PidMap = Map.Make(String)

type pid = string
type 's enable_fun = 's -> 's neighbor list -> action list
type 's step_fun   = 's -> 's neighbor list -> action -> 's
type 's state_init_fun = int -> string -> 's
type 's fault_fun = int -> string -> 's -> 's
type 's legitimate_fun = pid list -> (pid -> 's *  ('s neighbor * pid) list) -> bool

type 's potential_fun = pid list -> (pid -> 's *  ('s neighbor * pid) list) -> float
type value = F of float * float * float | I of int * int * int | B of bool
type 's state_to_values_fun =  ('s -> value list) * (value list -> 's -> 's)

type 's algo_to_register = {
  algo_id: string;
  init_state: 's state_init_fun;
  enab: 's enable_fun;
  step: 's step_fun
}
type 's to_register = {
  algo : 's algo_to_register list;
  state_to_string: 's -> string;
  state_of_string: (string -> 's) option;
  copy_state: 's -> 's;
  actions: action list;
  legitimate_function : 's legitimate_fun option;
  fault_function : 's fault_fun option;
  potential_function: 's potential_fun option;
  init_search_utils : 's state_to_values_fun option (** for sasa --init-search *)

}

let (to_reg_neigbor : 's Register.neighbor -> 's neighbor) =
  fun n ->
    {
      state = n.Register.state ;
      pid = n.Register.pid;
      spid = n.Register.spid;
      reply = n.Register.reply;
      weight = n.Register.weight;
    }

let (to_reg_info : 's * ('s Register.neighbor * pid) list -> 's * ('s neighbor *pid) list) =
  fun (s, nl) ->
  s, List.map (fun (n,pid) -> to_reg_neigbor n, pid) nl

let (to_reg_enable_fun : 's enable_fun ->
     's Register.neighbor list -> 's -> action list) =
  fun f nl s ->
    f s (List.map to_reg_neigbor nl)

let (to_reg_step_fun : 's step_fun ->
     's Register.neighbor list -> 's -> action -> 's) =
  fun f nl s a ->
    f s (List.map to_reg_neigbor nl) a

let (to_reg_potential_fun :
       's potential_fun -> pid list -> (pid -> 's * ('s Register.neighbor * pid) list) -> float) =
  fun pf pidl f ->
  let nf pid = to_reg_info (f pid) in
  pf pidl nf

let (to_reg_legitimate_fun :
       's legitimate_fun -> pid list -> (pid -> 's * ('s Register.neighbor * pid) list) -> bool) =
  fun lf pidl from_pid ->
  let n_from_pid pid =
    let s, nl = from_pid pid in
    s, List.map (fun (n,pid) -> to_reg_neigbor n, pid) nl
  in
    lf pidl n_from_pid

let (to_reg_s2n: ('s -> value list) -> 's -> Register.value list) =
  fun f s ->
  List.map
    (function
      | F(minx,x,maxx) -> Register.F (minx,x,maxx)
      | I(minx,x,maxx) -> Register.I  (minx,x,maxx)
      | B x -> Register.B x
    )
    (f s)

let (to_reg_n2s: (value list -> 's -> 's) -> Register.value list -> 's -> 's) =
  fun f nl s ->
  f (List.map
       (function
         | Register.F(minx,x,maxx) -> (F (minx,x,maxx))
         | Register.I(minx,x,maxx) -> (I (minx,x,maxx))
         | Register.B x -> (B x)
       )
       nl
    )
    s

let (register1 : 's algo_to_register -> unit) =
  fun s ->
    Register.reg_enable     s.algo_id (to_reg_enable_fun s.enab);
    Register.reg_step       s.algo_id (to_reg_step_fun s.step);
    Register.reg_init_state s.algo_id s.init_state;
    ()

let registered = ref false
(* exported *)
let (register : 's to_register -> unit) =
  fun s ->
    if !registered then failwith "Register can only be called once!";
    registered := true;
    Register.reg_value_to_string s.state_to_string;
    Register.reg_copy_value s.copy_state;
    List.iter register1 s.algo;
    (match s.state_of_string with None -> () | Some f -> Register.reg_value_of_string f);
    Register.reg_actions s.actions;
    (match s.potential_function with
     | None -> ()
     | Some pf -> Register.reg_potential (Some (to_reg_potential_fun pf))
    );
    (match s.legitimate_function with
     | None -> ()
     | Some ff -> Register.reg_legitimate (Some (to_reg_legitimate_fun ff))
    );
    (match s.fault_function with
     | None -> ()
     | Some ff -> Register.reg_fault (Some ff)
    );
    (match s.init_search_utils with
     | None -> ()
     | Some(s2n,n2s) -> Register.reg_init_search_utils (Some (to_reg_s2n s2n, to_reg_n2s n2s))
    );
    ()

let card = Register.card
let get_graph_attribute = Register.get_graph_attribute
let get_graph_attribute_opt = Register.get_graph_attribute_opt
let min_degree = Register.min_degree
let mean_degree = Register.mean_degree
let max_degree = Register.max_degree
let is_cyclic = Register.is_cyclic
let is_connected = Register.is_connected
let is_directed = Register.is_directed
let links_number = Register.links_number
let diameter = Register.diameter

let is_tree = Register.is_tree
let is_in_tree = Register.is_in_tree
let is_out_tree = Register.is_out_tree
let is_rooted_tree = Register.is_rooted_tree
let height = Register.height
let level = Register.level
let sub_tree_size = Register.sub_tree_size
let parent = Register.parent

(*
let pid n = n.pid
let spid n = n.spid
*)
