open Sasacore
(* open SasArg *)

let (get_action_value : (string * Data.v) list -> string -> string -> bool) =
  fun sl pid a ->
    let vn = Printf.sprintf "%s_%s" pid (StringOf.action a) in
    match List.assoc_opt vn sl with
    | Some (Data.B b) -> b
    | None
    | Some _ ->
      Printf.printf "Error: can not find %s in [%s]\n" vn
        (String.concat "," (fst (List.split sl)));
      flush stdout;
      assert false

open Sasacore
open Process

let (from_conf : 'v Process.t list -> 'v Conf.t -> RdbgPlugin.sl) =
  fun pl conf ->
    List.fold_left
      (fun acc p ->
         let state = Conf.get conf p.pid in
         let sl = SasaState.to_rdbg_subst p.pid state in
         acc@sl
      )
      []
      pl

let (from_sasa_env : 'v SimuState.t -> RdbgPlugin.sl) =
  fun st ->
  from_conf st.network st.config

let (get_sl_out: bool -> 'v Process.t list -> bool list list -> RdbgPlugin.sl) =
  fun enab pl ll ->
    let str = if enab then "Enab_" else "" in
    List.flatten (
      assert (List.length pl = List.length  ll);
      List.map2 (fun p enab_l ->
          assert (List.length p.actions = List.length enab_l);
          List.map2 (fun a enab ->
              Printf.sprintf "%s%s_%s" str p.pid (StringOf.action a), Data.B enab)
            p.actions enab_l)
        pl ll
    )

open SimuState

module StringMap = Map.Make(String)
let (compute_potentiel: 'v SimuState.t -> RdbgPlugin.sl) =
  fun st ->
  if Register.get_potential () = None then []
  else
    [("potential", Data.F (SimuState.compute_potentiel st))]

let (compute_legitimate: bool -> 'v SimuState.t -> bool) =
  fun silent st ->
  silent ||
  match Register.get_legitimate () with
  | None -> silent
  | Some f ->
    let pidl = List.map (fun p -> p.Process.pid) st.network in
    f pidl (SimuState.neigbors_of_pid st)

let (make_do: string array -> 'v SimuState.t -> RdbgPlugin.t) =
  fun argv st ->
  let pl = st.network in
  let prog_id = Printf.sprintf "%s"
      (String.concat " " (Array.to_list argv))
  in
  let vntl_i =
    List.map (fun (vn,vt) -> vn, Data.type_of_string vt)
      (Sasacore.SimuState.get_inputs_rif_decl st.sasarg pl)
  in
  let vntl_o =
    List.map (fun (vn,vt) -> vn, Data.type_of_string vt)
      (Sasacore.SimuState.get_outputs_rif_decl st.sasarg pl)
  in
  let vntl_o =
    if Register.get_potential () = None then vntl_o else
      ("potential", Data.Real)::vntl_o in
  let vntl_o = ("silent", Data.Bool)::("legitimate", Data.Bool)::
               ("round", Data.Bool)::("round_nb", Data.Int)::vntl_o in
  let pre_enable_processes_opt = ref None in
  let sasa_config = ref st.config in
  let reset () =
    pre_enable_processes_opt := None;
    sasa_config :=
      (Sasacore.SimuState.make false argv).config (* to take seed changes into account *)
  in
  (*  Do the same job as SasaMain.simustep *)
  let (step_custom: RdbgPlugin.sl -> RdbgPlugin.sl) =
    fun sl_in ->
      let st = Sasacore.SimuState.update_config !sasa_config st in
      match !pre_enable_processes_opt with
      | None  -> ( (* the first step *)
          (* 1: Get enable processes *)
          let pnall, enab_ll = Sasacore.SimuState.get_enable_processes st in
          let sasa_nenv = from_sasa_env st in
          let pot_sl = compute_potentiel st in
          let silent = List.for_all (fun b -> not b) (List.flatten enab_ll) in
          let legit = compute_legitimate silent st in
          Round.update legit false enab_ll enab_ll;
          pre_enable_processes_opt := Some(pnall, enab_ll);
          ("silent", Data.B silent)::("legitimate", Data.B legit)::
          ("round", Data.B Round.s.is_round)::("round_nb", Data.I Round.s.cpt)::pot_sl
          @ sasa_nenv @ (get_sl_out true pl enab_ll)
        )
      | Some (pre_pnall, pre_enab_ll) ->
        (* let was_silent = List.for_all (fun b -> not b) (List.flatten pre_enab_ll) in *)
        (* if was_silent then failwith "Silent"; *)
        (* 2: read the actions from the outside process, i.e., from sl_in *)
        let activate_val, pnal = Daemon.f st.sasarg.dummy_input
            (st.sasarg.verbose > 0) st.sasarg.daemon st.network
            SimuState.neigbors_of_pid st pre_pnall pre_enab_ll
            (get_action_value sl_in) Step.f
        in
        (* 3: Do the steps *)
        let st = Sasacore.Step.f pnal st in
        let nst = update_config st.config st in
        let sasa_nenv = from_sasa_env nst in
        (* 1': Get enable processes *)
        let pnall, enab_ll = Sasacore.SimuState.get_enable_processes nst in
        let pot_sl = compute_potentiel nst in
        let silent = List.for_all (fun b -> not b) (List.flatten enab_ll) in
        let legit = compute_legitimate silent nst in
        Round.update legit false activate_val enab_ll;
        pre_enable_processes_opt := Some(pnall, enab_ll);
        sasa_config := st.config;
        ("silent", Data.B silent)::("legitimate", Data.B legit)::
        ("round", Data.B Round.s.is_round)::("round_nb", Data.I Round.s.cpt)::pot_sl @
        sasa_nenv @ (get_sl_out true pl enab_ll)
  in
  let (step_internal_daemon: RdbgPlugin.sl -> RdbgPlugin.sl) =
    fun sl_in ->
      (* in this mode, sasa does not play first *)
      let st = update_config !sasa_config st in
      (* 1: Get enable processes *)
      let pnall, enab_ll = Sasacore.SimuState.get_enable_processes st in
      let pot_sl = compute_potentiel st in
      let silent = List.for_all (fun b -> not b) (List.flatten enab_ll) in
      let legit = compute_legitimate silent st in
      if silent then (
        let enab = get_sl_out true pl enab_ll in
        let trigger = (* set all trig var to false *)
          List.map (fun (n,v) -> String.sub n 5 ((String.length n) -5),v) enab
        in
        ("silent", Data.B silent)::("legitimate", Data.B legit)::
        ("round", Data.B Round.s.is_round)::("round_nb", Data.I Round.s.cpt)::pot_sl@
        (from_sasa_env st) @ enab @ trigger
      )
      else
        (* 2: read the actions from the outside process, i.e., from sl_in *)
        let activate_val, pnal =
          Daemon.f st.sasarg.dummy_input
            (st.sasarg.verbose > 0) st.sasarg.daemon st.network
            SimuState.neigbors_of_pid st pnall enab_ll
            (get_action_value sl_in) Step.f
        in
        Round.update legit true activate_val enab_ll;
        (* 3: Do the steps *)
        let nst = Sasacore.Step.f pnal st in
        sasa_config := nst.config;
        ("silent", Data.B silent)::("legitimate", Data.B legit)::
        ("round", Data.B Round.s.is_round)::("round_nb", Data.I Round.s.cpt)::pot_sl @
        (from_sasa_env st) @ (get_sl_out true pl enab_ll) @
        (get_sl_out false pl activate_val)
  in
  let exaustive_search_res = ref None in
  let rec (step_exhaustive_search : RdbgPlugin.sl -> RdbgPlugin.sl) =
    fun x ->
      match !exaustive_search_res with
      | None ->
        let log = open_out (st.sasarg.topo ^ ".log") in
        let path = ExhaustSearch.f log (st.sasarg.daemon=ExhaustCentralSearch) st in
        exaustive_search_res := Some path;
        step_exhaustive_search x
      | Some [] -> []
      | Some ((enab_ll, trig_ll, legit, pot, conf)::path) ->
        exaustive_search_res := Some path;
        let trig_ll = if List.flatten trig_ll = [] then enab_ll else trig_ll in
        let silent = List.for_all (fun b -> not b) (List.flatten enab_ll) in
        let conf_sl = from_conf st.network conf in
        let enab = get_sl_out true pl enab_ll in
        let trig = get_sl_out false pl trig_ll in
        ("silent", Data.B silent)::("legitimate", Data.B legit)::
        ("potential", Data.F pot)::
        ("round", Data.B Round.s.is_round)::("round_nb", Data.I Round.s.cpt)::
        conf_sl @ enab @ trig
  in
  let es_ss_table = Hashtbl.create 10 in
  let exhaustive_search_save_state i =
    Hashtbl.replace es_ss_table i !exaustive_search_res
  in
  let exhaustive_search_restore_state i =
    match Hashtbl.find_opt es_ss_table i with
    | Some esr -> exaustive_search_res := esr
    | None ->
      Printf.eprintf "Cannot restore state %i from sasa\n" i;
      flush stderr
  in
  let exhaustive_search_reset ()  =
    match !exaustive_search_res with
    | None -> ()
    | Some _ ->
      let log = open_out (st.sasarg.topo ^ ".log") in
      ExhaustSearch.reset ();
      let path = ExhaustSearch.f log (st.sasarg.daemon=ExhaustCentralSearch) st in
      exaustive_search_res := Some path
  in
  let _exhaustive_search_reset () = () in
  let step =
    match st.sasarg.daemon with
    | DaemonType.Custom -> step_custom
    | ExhaustCentralSearch
    | ExhaustSearch -> step_exhaustive_search
    | _ -> step_internal_daemon
  in
  let ss_table = Hashtbl.create 10 in
  let step_dbg sl_in ctx cont =
    let sl_out = step sl_in in
    { ctx with
      (* RdbgEvent.nb = 0; *)
      (* RdbgEvent.step = 0; (* we are actually in the middle of the first step! *) *)
      RdbgEvent.depth = ctx.RdbgEvent.depth + 1;
      RdbgEvent.kind = RdbgEvent.Exit;
      RdbgEvent.lang = "sasa";
      RdbgEvent.sinfo = None;
      RdbgEvent.name = "sasa";
      RdbgEvent.inputs = vntl_i;
      RdbgEvent.outputs = vntl_o;
      RdbgEvent.locals = [];
      RdbgEvent.data = sl_in@sl_out;
      RdbgEvent.next = (
        fun () ->
          let ctx = { ctx with RdbgEvent.nb = 1 + ctx.RdbgEvent.nb } in
          cont sl_out ctx );
    }
  in
  let (mems_in  : Data.subst list) = [] in
  let (mems_out : Data.subst list) = [] in
  let sasa_save_state i =
    let prgs = Random.get_state () in
    (* Printf.eprintf "Save state %i from sasa\n%!" i; *)
    Hashtbl.replace ss_table i
      (prgs, !sasa_config, !pre_enable_processes_opt, Round.get())
  in
  let sasa_restore_state i =
    if i = min_int then (* Is it a good idea to use this for fault injection? Not sure. *)
      (match Register.get_fault () with
       | None -> Printf.eprintf "Cannot inject faults: no fault function has been provided\n%!";
       | Some f ->
         Printf.eprintf "Perform a fault injection in sasa\n%!";
         pre_enable_processes_opt := None;
         sasa_config := SimuState.inject_fault_in_conf f st !sasa_config;
         Round.reinit()
      )
    else
      match Hashtbl.find_opt ss_table i with
      | Some (prgs, e, pepo, r) ->
        Random.set_state prgs;
        (* Printf.eprintf "Restore state %i from sasa\n%!" i; *)
        sasa_config := e; pre_enable_processes_opt := pepo;
        Round.set r;
      | None ->
        Printf.eprintf "Cannot restore state %i from sasa\n" i;
        flush stderr
  in
  Round.reinit_mask();
  {
    id = prog_id;
    inputs = vntl_i;
    outputs= vntl_o;
    reset= (match st.sasarg.daemon with
        | ExhaustCentralSearch | ExhaustSearch -> exhaustive_search_reset
        | _ -> reset
      );
    kill=(fun _ -> flush stdout; flush stderr);
    init_inputs=mems_in;
    init_outputs=mems_out;
    step=step;
    step_dbg = step_dbg;
    save_state = (match st.sasarg.daemon with
        | ExhaustCentralSearch | ExhaustSearch -> exhaustive_search_save_state
        | _ -> sasa_save_state
      );
    restore_state = (match st.sasarg.daemon with
        | ExhaustCentralSearch | ExhaustSearch -> exhaustive_search_restore_state
        | _ -> sasa_restore_state
      );
  }


let (make: string array -> RdbgPlugin.t) =
  fun argv ->
    try
      make_do argv (Sasacore.SimuState.make false argv)
    with
    | Dynlink.Error e ->
      Printf.printf "Error (SasaRun.make): %s\n" (Dynlink.error_message e);
      flush stdout;
      exit 2
    | Failure msg ->
      Printf.printf "Error (SasaRun.make): %s\n" msg;
      flush stdout;
      exit 2

    | Assert_failure (file, line, col)  ->
      prerr_string (
        "\nError: oops, sasa internal error\n\tFile \""^ file ^
        "\", line " ^ (string_of_int line) ^ ", column " ^
        (string_of_int col) ^ "\n") ;
      flush stderr;
      exit  2
