(* Time-stamp: <modified the 15/04/2021 (at 11:28) by Erwan Jahier> *)

type pid = string

(* Try to guess the type of values held in a string  *)
type data_or_name = Data of Data.v * Data.t | Name of string
let (string_to_data : string -> data_or_name) =
  fun str  -> 
  match int_of_string_opt str with
  | Some i -> Data (Data.I i, Data.Int)
  | None -> (
      match float_of_string_opt str with
      | Some f -> Data (Data.F f, Data.Real)
      | None -> (
          match str with
          | "true"  | "True"  | "t" | "T" -> Data (Data.B true, Data.Bool)
          | "false" | "False" | "f" | "F" -> Data (Data.B false, Data.Bool)
          | _   -> Data (Data.Str(str), Data.String)
        )
    )

(* All the functions below are based on Algo.state_to_string *)
let (to_list : 'v -> data_or_name list) =
  fun v -> 
    let str = Register.to_string v in
    let lstr = Str.split  (Str.regexp "[ \t]+") str in
    let lstr = List.map
        (fun str ->
           match Str.split  (Str.regexp "[=]") str with
           | [v] -> [string_to_data v]
           | [n;v] -> [Name n; string_to_data v]
           |  _ ->
             let msg = Printf.sprintf "Bad format '%s' for printing state values" str in 
             failwith msg
        )
        lstr
    in
    List.flatten lstr

(* remove var name hints *)
let (to_rif_data : 'v -> string) =
  fun v -> 
    let lopt = to_list v in
    let lstr = List.fold_left
        (fun acc opt ->
           match opt with
           | Data(v,_) -> (Data.val_to_string string_of_float v)::acc
           | Name _ -> acc
        )
        [] (List.rev lopt)
    in
    String.concat " " lstr

let (to_data: pid -> 'v -> (string * Data.v * Data.t) list) =
  fun pid v ->
    let lopt = to_list v in
    let _, _, res = List.fold_left
        (fun (i,nopt,acc) opt ->
           match opt,nopt with
           | Data (v,t), Some n -> i  ,None,(Printf.sprintf "%s_%s" pid n, v, t)::acc
           | Data (v,t), None   -> i+1,None,(Printf.sprintf "%s_v%d" pid i, v, t)::acc
           | Name n, Some _n -> i, Some n, acc (* SNO *)
           | Name n, None -> i, Some n, acc
        )
        (0,None,[])
        lopt
    in
    List.rev res
      
let (to_rif_decl : pid -> 'v -> (string * Data.t) list) =
  fun pid v ->
    let dl = to_data pid v in
    List.map (fun (n,_v,t) -> n,t) dl

let (to_var_names : pid -> 'v -> string list) =
  fun pid v ->
    let dl = to_data pid v in
    List.map (fun (n,_v,_t) -> n) dl

let (to_rdbg_subst : pid -> 'v ->  (string * Data.v) list) =
  fun pid v ->
    let dl = to_data pid v in
    List.map (fun (n,v,_) -> n,v) dl
