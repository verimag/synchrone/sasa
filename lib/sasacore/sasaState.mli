(* Time-stamp: <modified the 15/04/2021 (at 11:37) by Erwan Jahier> *)

(** 'v is the type of Algo local state, and is defined by users.

   Instead  of requiring  functions  that convert  'v  into Data.t,  I
   choose to  implement them on top  of Algo.state_to_string (although
   it is probably less robust).

   The reasons for doing that are:
   - Data.v are  only required for interacting  with synchronous tools
   in RIF
   - it is boring to do
   - it  is  not  so  easy,  as all  function  definitions  should  be
   consistent

   Moreover, requiring a state_to_string also provides an easy mean to
   hide information  in the RIF.   Indeed, if  the user only  wants to
   show  part of  the local  state,  he just need  to hide it via  the
   state_to_string function.  *)

type pid = string

(** Returns a list variable names used in the RIF outputs. Those names are
    made of the pid and the type 'v using the state_to_string function *)
val to_rif_decl   : pid -> 'v -> (string * Data.t) list
val to_rif_data : 'v -> string

val to_var_names : pid -> 'v -> string list 

(** used to interact with rdbg *)
val to_rdbg_subst : pid -> 'v -> (string * Data.v) list
