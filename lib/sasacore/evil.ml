(* Time-stamp: <modified the 15/03/2023 (at 16:01) by Erwan Jahier> *)

type 'v pna = 'v Process.t * 'v Register.neighbor list * Register.action
type 'v enabled = 'v pna list list
type 'v triggered = 'v pna list
type 'v step = 'v triggered -> 'v SimuState.t -> 'v SimuState.t


module StringMap = Map.Make(String)
module StringSet = Set.Make(String)

(* Among the enabled nodes, groups the ones that are connected.

   Indeed,
*)
let (connected_components_gen : bool -> 'v pna list list -> 'v pna list list list) =
  fun only_one all ->
  let pid2pnal =
    List.fold_left
      (fun acc al ->
        match al with
        | [] -> acc
        | (p,_,_)::_ -> StringMap.add p.Process.pid al acc)
      StringMap.empty
      all
  in
  let get_neighors = function
    | [] -> assert false
    | (_,nl,_)::_ -> nl
  in
  let rec f marked acc all =
    match all with
    | [] -> acc
    | []::tl -> f marked acc tl
    | ((p,nl,_a)::_)::tl ->
       if StringSet.mem p.Process.pid marked then
         f marked acc tl
       else
         let marked = StringSet.add p.Process.pid marked in
         let marked, component = g marked nl [p.Process.pid] in
         let component = List.map (fun pid -> StringMap.find pid pid2pnal) component in
         if only_one then
           [component]
         else
           f marked (component::acc) tl
  and g marked nl acc =
    match nl with
    | [] -> marked, acc
    | n::nl ->
       if StringSet.mem n.Register.pid marked then
         g marked nl acc
       else
         let marked = StringSet.add n.Register.pid marked in
         if StringMap.mem n.Register.pid pid2pnal then
           let n_nl = get_neighors (StringMap.find n.Register.pid pid2pnal) in
           g marked (List.rev_append n_nl nl) (n.Register.pid::acc)
         else
           (* not all neighbors are enabled !*)
           g marked nl acc
  in
  f StringSet.empty [] all

(* Returns all the connected components *)
let (_connected_components : 'v pna list list -> 'v pna list list list) =
  fun all ->
  connected_components_gen true all

(* Returns 1  connected component (the first its finds) *)
let (_connected_component : 'v pna list list -> 'v pna list list) =
  fun all ->
    List.hd (connected_components_gen false all)

let time verb lbl f x =
  let t = Sys.time() in
  let fx = f x in
  if verb then Printf.eprintf " [%s] Execution time: %fs\n" lbl (Sys.time() -. t);
  fx

let _time2 verb lbl f x y =
  let t = Sys.time() in
  let fxy = f x y in
  if verb then Printf.eprintf " [%s] Execution time: %fs\n" lbl (Sys.time() -. t);
  fxy

let time3 verb lbl f x y z =
  let t = Sys.time() in
  let fxy = f x y z in
  if verb then Printf.eprintf " [%s] Execution time: %fs\n" lbl (Sys.time() -. t);
  fxy

let (greedy: bool -> 'v SimuState.t -> 'v Process.t list ->
     ('v SimuState.t -> string -> 'v * ('v Register.neighbor * string) list) ->
     'v step -> 'v pna list list -> 'v pna list) =
  fun verb st pl neigbors_of_pid step all ->
  assert (all<>[]);
  match Register.get_potential () with
  | None -> failwith "No potential function has been provided"
  | Some user_pf ->
    let pf pnal = (* pnal contains a list of activated processes *)
      let pidl = List.map (fun p -> p.Process.pid) pl in
      let nst = step pnal st in
      let get_info pid =
        let _, nl = neigbors_of_pid nst pid in
        Conf.get nst.config pid, nl
      in
      user_pf pidl get_info
    in
    let cpt = ref 0 in
    let (get_max :'v pna list list -> 'v pna list * float) = fun all ->
      let pnal1, p1, shedules =
        match time verb "Evil.greedy enumerate" Enumerate.all all with
        | Enumerate.NoMore -> assert false
        | Enumerate.Elt(pnal, c) -> cpt:=1; pnal, pf pnal, c
      in
      let rec search_max acc (pnal_acc, v_acc) shedules =
        match shedules with (* returns more than one max in case of equality *)
        | Enumerate.NoMore -> (pnal_acc, v_acc)::acc
        | Enumerate.Elt(pnal, c) ->
          incr cpt;
          let v = pf pnal in
          if v = v_acc then search_max ((pnal_acc, v_acc)::acc) (pnal, v)  (c())
          else if v < v_acc
          then search_max [] (pnal_acc, v_acc) (c())
          else search_max [] (pnal, v)  (c())
      in
      let maxl =
        time3 verb "Evil.greedy search" search_max [] (pnal1, p1) (shedules())
      in
      if verb then
        Printf.eprintf "[Evil.greedy]: %d choices have the same potentials\n%!"
          (List.length maxl) ;
      List.nth maxl (Random.int (List.length maxl))
    in
    let res = fst (get_max all) in
    if verb then Printf.eprintf " [Evil.greedy] Number of trials: %i\n%!" !cpt;
    res

(* val greedy_central: bool -> 'v Conf.t -> ('v Process.t * 'v Register.neighbor list) list -> *)
let (greedy_central: bool -> bool -> 'v SimuState.t -> 'v Process.t list ->
     ('v SimuState.t -> string -> 'v * ('v Register.neighbor * string) list) ->
     'v step -> 'v pna list list -> 'v pna list) =
  fun verb det_greedy st pl neigbors_of_pid step all ->
  assert (all<>[]);
  match Register.get_potential () with
  | None -> failwith "No potential function has been provided"
  | Some user_pf ->
    let pf pna =
      let pidl = List.map (fun p -> p.Process.pid) pl in
      let nst = step [pna] st in
      let get_info pid =
        let _, nl = neigbors_of_pid nst pid in
        Conf.get nst.config pid, nl
      in
      user_pf pidl get_info
    in
    (*     let all = time verb "Evil.greedy_central connected component" *)
    (*         connected_component all) *)
    (*     in *)
    let cpt = ref 0 in
    let (get_max :'v pna list list -> 'v pna * float) = fun all ->
      let pnal1, p1, shedules =
        match time verb "Evil.greedy_central enumerate" Enumerate.central all with
        | Enumerate.NoMore -> assert false
        | Enumerate.Elt(pna, c) -> cpt :=1; pna, pf pna, c
      in
      let rec search_max acc (pnal_acc, v_acc) shedules =
        match shedules with
        | Enumerate.NoMore -> (pnal_acc, v_acc)::acc
        | Enumerate.Elt(pnal, c) ->
          incr cpt;
          let v = pf pnal in
          if v = v_acc then search_max ((pnal_acc, v_acc)::acc) (pnal, v)  (c())
          else if v < v_acc
          then search_max [] (pnal_acc, v_acc) (c())
          else search_max [] (pnal, v)  (c())
      in
      let maxl = time3 verb "Evil.greedy_central search"
          search_max [] (pnal1, p1) (shedules())
      in
      let maxl_s = List.length maxl in
      if verb && maxl_s >1 then
        Printf.eprintf "[Evil.greedy]: %d choices have the same potentials\n%!" maxl_s;
      List.nth maxl (if det_greedy then 0 else Random.int maxl_s)
    in
    let res = fst (get_max all) in
    if verb then (
      Printf.eprintf " [Evil.greedy] Number of trials: %i\n%!"  !cpt;
    );
    [res]

(* exported *)
let (bad: int -> 'v SimuState.t -> 'v pna list list -> 'v pna list) =
  fun _max_size _e _all ->
  assert false (* todo *)

(* exported *)
let (worst4convex: 'v SimuState.t -> 'v pna list list -> 'v pna list) =
  fun _e _all ->
  assert false (* todo *)
