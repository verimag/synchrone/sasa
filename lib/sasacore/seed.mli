(* Time-stamp: <modified the 08/11/2021 (at 10:53) by Erwan Jahier> *)

(** [set verbose seed] *)
val set : ?verb:bool -> int -> unit 
val reset : unit -> unit 

(** The string is used to create  a file name to save/restore the seed
   when the --replay option is used *)
val get : ?verb:bool -> string -> int

val replay_seed : bool ref
