(* Time-stamp: <modified the 24/03/2023 (at 23:02) by Erwan Jahier> *)

open Register

type 's search_node = { st : 's ; d : int ; cost : int ; cpt : int }
type point = value Array.t

let debug = false

type distance = Far | Close
let mutate_value distance = function
  | F (minf,f,maxf) ->
    let nf = match distance with
      | Far -> Random.float (maxf-.minf) +. minf
      | Close ->
        let delta = Random.float (maxf-.minf)/.100. -. (maxf-.minf)/.200. in
        if f+.delta<minf || f+.delta>maxf then f-.delta else f+.delta
    in
    F(minf, nf, maxf)

  | I (mini,i,maxi) ->
    assert (mini<maxi);
    let ni = match distance with
      | Far ->
        let ni = Random.int(maxi-mini) + mini in
        if ni = i then maxi else ni
      | Close ->
        if i=mini then i+1 else if i=maxi then i-1 else
        if Random.bool () then i+1 else i-1
    in
    I (mini,ni,maxi)
  | B b -> B (not b)
(*
in order to test the distribution of the mutants

   let a = Array.make 10 0 in
   let p = ref (I (3, 4, 6)) in
   for i=1 to 100000 do
       p:=mutate_value Far !p;
       match !p with I(_,i,_) -> a.(i) <-a.(i)+1;
   done;
   a;;

   let a = Array.make 10 0 in
   let p = ref (F (3., 4., 6.)) in
   for i=1 to 100000 do
       p:=mutate_value Far !p;
       match !p with F(_,f,_) -> a.(int_of_float f) <-a.(int_of_float f)+1;
   done;
   a;;
*)

let one_dim_succ d n =
  let p = Array.copy n in
  let j = Random.int (Array.length p) in
  p.(j) <- mutate_value d p.(j);
  p


let ran_dim_succ d n =
  let p = Array.copy n in
  for j=0 to Array.length p - 1 do
    if Random.bool () then p.(j) <- mutate_value d p.(j)
  done;
  p

let all_dim_succ d n =
  let p = Array.copy n in
  for j=0 to Array.length p - 1 do
    p.(j) <- mutate_value d p.(j)
  done;
  p


let tf = float_of_int
let ti = int_of_float

let (choose : int -> point -> (point -> point) -> point list) =
  fun n p heuristic ->
  (*choose n successors of p using heuristic *)
  assert (n>=0);
  let rec f acc i = if i <= 0 then acc else f ((heuristic p)::acc)  (i-1) in
  f [] n

(*$T choose
  List.length (choose 10 [| F(10.), F(42.) |] (one_dim_succ Close)) = 10
*)

(*****************************************************************************)
(* XXX a ranger ailleurs !!! *)

open Process

let (point_to_ss : point -> 'v SimuState.t -> 'v SimuState.t) =
  fun point ss ->
  let (state_to_values, values_to_state :
       ('v -> Register.value list) * (Register.value list -> 'v -> 'v )) =
    match Register.get_init_search_utils () with
    | None -> assert false
    | Some (f, g) -> f, g
  in
  let state_size =
    assert (ss.network <> []);
    let p0 = List.hd ss.network in
    let s0 = state_to_values p0.init in
    List.length s0
  in
  let make_value l i j =
    if debug then
      Printf.printf "make_value  i=%d j=%d size=%dx%d \n%!"
        i j (Array.length point) state_size;
    let rec f l i j =
      if i=0 then l else f (point.(i+j-1)::l) (i-1) j
    in
    f l i j
  in
  let new_config, _ =
    List.fold_left
    (fun (e,j) p ->
       let value = make_value [] state_size (j*state_size) in
       let st = values_to_state value (Conf.get e p.pid) in
       let e = Conf.set e p.pid st in
       e, j+1
    )
    (ss.config, 0)
    ss.network
  in
  if debug then Printf.printf "point_to_ss  ok\n%!";
  { ss with config = new_config }


let (ss_to_point : 'v SimuState.t -> point) =
  fun ss ->
  let (state_to_values : ('v -> Register.value list) ) =
    match Register.get_init_search_utils () with
      | None ->
        failwith ("the Algo.init_search_utils registration field should provide "^
                  "state_to_values functions")
      | Some (f, _) -> f
  in
  let size =
    assert (ss.network <> []);
    let p0 = List.hd ss.network in
    let s0 = state_to_values p0.init in
    (List.length s0) * (List.length ss.network)
  in
  let point = Array.make size (I (0,0,0) (* fake unused value *) ) in
    let i = ref 0 in
    List.iter
    (fun p ->
       let values = state_to_values p.init in
       List.iter (fun value ->
           assert (!i<Array.length point);
           point.(!i) <- value; incr i) values
    )
    ss.network;
  point
(*****************************************************************************)

open LocalSearch


module ValueArrayNode = struct
  type t = value array search_node
  let compare n1 n2 = compare n2.cost n1.cost
end

(* open Functory.Sequential *)
open Functory.Cores
module Q = Psq.Make (Int) (ValueArrayNode)

let value2str = function
    Register.F(_, f, _) -> string_of_float f
  | Register.I(_, i, _) -> string_of_int i
  | Register.B true -> "t"
  | Register.B false -> "f"

let _point2str p =
  Array.fold_right (fun value acc -> (value2str value)::acc)  p []
  |> String.concat ";"

open SimuState

(* mix local and global search, a la simulating annealing, with one round *)
let _succ_far_and_close dmax log cpt step_cpt percent_done cost ss_init n =
  let beam_size = max 50 ss_init.sasarg.cores_nb in
  let percent_close = ((tf !cpt) /. (tf dmax)) ** 2.0 in
  let percent_far = 1.0 -. percent_close in

  let far_nb = max 1 (ti ((tf beam_size) *. percent_far) / 6) in
  let close_nb = max 1 (ti ((tf beam_size) *. percent_close) / 6) in
  incr step_cpt;
  let pl = (choose far_nb n.st (one_dim_succ Close)) @
           (choose far_nb n.st (ran_dim_succ Close)) @
           (choose far_nb n.st (all_dim_succ Close)) @
           (choose close_nb n.st (one_dim_succ Far)) @
           (choose close_nb n.st (ran_dim_succ Far)) @
           (choose close_nb n.st (all_dim_succ Far))
  in
  let new_cpt, res =
    map_local_fold
      ~f: (fun p -> cost p, p)
      ~fold:(fun (cpt,nl) (c,p) ->
          assert(dmax <> 0);
          let n_percent_done = if dmax < 100 then 1 else cpt / (dmax / 100) in
          if n_percent_done <> !percent_done  && not (ss_init.sasarg.quiet) then (
            percent_done := n_percent_done;
            Printf.printf "%d%% of the %d simulations have been tryied so far...\r%!"
              n_percent_done dmax
          );
          Printf.fprintf log "At depth %d, cost=%d\n%!" (n.d+1) c;
          cpt+1,{ d=n.d+1; cost=c; st=p ; cpt = cpt}::nl
        )
      (!cpt, [])
      pl
  in
  Printf.fprintf log "fchc: cpt=%d->%d cost=[%s]\n%!" !cpt new_cpt
    (String.concat "," (List.map (fun n -> string_of_int n.cost) res));
  cpt:=new_cpt;
  res

(* purely local search *)
let succ_close dmax log cpt step_cpt percent_done cost ss_init n =
  let beam_size = max 50 ss_init.sasarg.cores_nb in
  let close_nb = max 1 (ti ((tf beam_size)) / 3) in
  incr step_cpt;
  let pl = (choose close_nb n.st (one_dim_succ Far)) @
           (choose close_nb n.st (ran_dim_succ Far)) @
           (choose close_nb n.st (all_dim_succ Far))
  in
  let new_cpt, res =
    map_local_fold
      ~f: (fun p -> cost p, p)
      ~fold:(fun (cpt,nl) (c,p) ->
          assert(dmax <> 0);
          let n_percent_done = if dmax < 100 then 1 else cpt / (dmax / 100) in
          if n_percent_done <> !percent_done && not (ss_init.sasarg.quiet) then (
            percent_done := n_percent_done;
            Printf.printf "%d%% of the %d simulations have been tryied so far...\r%!"
              n_percent_done dmax
          );
          Printf.fprintf log "At depth %d, cost=%d\n%!" (n.d+1) c;
          cpt+1,{ d=n.d+1; cost=c; st=p ; cpt = cpt}::nl
        )
      (!cpt, [])
      pl
  in
  Printf.fprintf log "fchc: cpt=%d->%d cost=[%s]\n%!" !cpt new_cpt
    (String.concat "," (List.map (fun n -> string_of_int n.cost) res));
  cpt:=new_cpt;
  res


(**********************************************************************)
(* Use hashed configuration + int set to implement Tabu list.

nb 1 : the evaluation of each initial config is (generally) costy, so
   this set should remain small => no need to drop elements

nb 2 : using a hash function is not safe, but clashes are
- unlikely (cf nb 1)
- not critical  : configurations  are created at  random, and  only a
   very small fraction of the  set of possible configurations will be
   tried anyway. So if a config is wrongly rejected, it is not a big deal
 *)

module IntSet = Set.Make(Int)
let (tabu_add : 's search_node -> IntSet.t -> IntSet.t) =
  fun n tabu ->
  IntSet.add (Hashtbl.hash n) tabu

let (tabu_mem : 's search_node -> IntSet.t -> bool) =
  fun n tabu ->
  IntSet.mem (Hashtbl.hash n) tabu

let empty_tabu_list = IntSet.empty

(**********************************************************************)


(* First Choice Hill Climbing: a  successor is chosen at random (using
   some  heuristics), and  became  the  current state  if  its cost  is
   better.

   The heuristic to choose the succ is chosen at random in an array of
   heuristics.   The probability  of  each heuristic  evolves, but  is
   never null.  *)
let (fchc : out_channel -> ('v SimuState.t -> int) -> 'v SimuState.t -> int
     -> 'v SimuState.t) =
  fun log run ss_init dmax ->
  let cpt = ref 0 in
  let step_cpt = ref 1 in
  let cost p = run (point_to_ss p ss_init) in
  let pinit = ss_to_point ss_init in
  let percent_done = ref 0 in
  Functory.Cores.set_number_of_cores ss_init.sasarg.cores_nb;
  let visiting, visited =
    match ss_init.sasarg.is_tabu_mode with
    | false -> (fun _ x -> x), (fun _ _ -> false)
    | true -> tabu_add, tabu_mem
  in
  let g =
    {
      init = ({ st = pinit ; d = 0 ; cost = cost pinit ; cpt = 0 }, Q.empty, empty_tabu_list);
      succ = succ_close dmax log cpt step_cpt percent_done cost ss_init ;
      stop = (fun _ _n -> !cpt >= dmax);
      is_goal = (fun _n -> true);
      push = (fun tv n ->
          Printf.fprintf log "Pushing a point of cost %d (queue size=%d)\n%!"
            (n.cost) (Q.size tv);
          Q.add n.cpt n tv);
      pop = (fun tv -> match Q.pop tv with None -> None | Some((i,x),t) ->
          Printf.fprintf log "Poping a point of cost %d (simu #%d)\n%!" x.cost i;
          Some(x, t));
      visiting;
      visited;
      cut  = (fun pn n -> if pn.cost > n.cost then (
          Printf.fprintf log "%d: cut a point at depth %d of cost %d < %d\n%!"
            !cpt n.d n.cost pn.cost;
          true
        )
          else false
        );
    }
  in

  let rec run_more psol more =
    ( match more (Some psol) with
      | LocalSearch.NoMore ->
        (* occurs if all successors are cut *)
        run_more psol more
      | LocalSearch.Stopped ->
        Printf.printf "\nThe worst initial configuration costs %d :" psol.cost;
        point_to_ss psol.st ss_init
      | LocalSearch.Sol (nsol, more) ->
        if nsol.cost > psol.cost then (
          if ss_init.sasarg.quiet then
            Printf.printf "data: %d %d %d\n%!" nsol.cost nsol.cpt nsol.d
          else
            Printf.printf "Hey, I've found a conf of cost %d! (simu #%d, depth %d)\n%!"
              nsol.cost nsol.cpt nsol.d;
          run_more nsol more
        ) else (
          run_more psol more
        )
    )

  in
  match LocalSearch.run g None with
  | LocalSearch.Stopped -> assert false (* SNO *)
  | LocalSearch.NoMore-> assert false (* SNO *)
  | LocalSearch.Sol (sol, more) ->
    if ss_init.sasarg.quiet then
      Printf.printf "data: %d %d %d\n%!" sol.cost sol.cpt sol.d
      else
        Printf.printf "Hey, I've found a conf of cost %d! (simu #%d, depth %d)\n%!"
          sol.cost sol.cpt sol.d;
    run_more sol more

open Topology
open SimuState
open Process

(* generate a new random configuration using the user init functions *)
let reinit_simu g ss =
  let pl = List.map2
      (fun n p ->
         { p with
           init = let algo_id = Filename.remove_extension n.Topology.file in
             Register.get_init_state algo_id (List.length (g.succ p.pid)) p.pid
         })
      g.nodes
      ss.network
  in
  let e = Conf.init () in
  let e = SimuState.update_env_with_init e pl in

  update_config e ss

(*****************************************************************************)
(* Global search : use no heuristic, the init wtate is chosen at random  *)
let (global : out_channel -> ('v SimuState.t -> int) -> 'v SimuState.t -> int
     -> 'v SimuState.t) =
  fun log run ss_init dmax ->
  let dot_file = ss_init.sasarg.topo in
  let g = Topology.read dot_file in
  let percent_done = ref 0 in
  let rec loop cpt (ss_worst, worst) =
    let ss = reinit_simu g ss_init in
    let ss_worst, worst =
      let res = run ss in
      Printf.fprintf log "simu %d, cost=%d\n%!" cpt res;
      if res > worst then (
        if ss_init.sasarg.quiet then
          Printf.printf "data: %d %d\n%!" res cpt
        else
          Printf.printf "Hey, I've found a conf of cost %d! (simu #%d)\n%!" res cpt;
        ss, res
      )
      else
        ss_worst, worst
    in
    let n_percent_done = if dmax < 100 then 1 else cpt / (dmax / 100) in
    if n_percent_done <> !percent_done && not (ss_init.sasarg.quiet) then (
      percent_done := n_percent_done;
      Printf.printf "%d%% of the %d simulations have been tryied so far...\r%!"
        n_percent_done dmax
    );
    if cpt > dmax then (
      Printf.printf "\nThe worst initial configuration costs %d :" worst;
      ss_worst
    )
    else loop (cpt+1) (ss_worst, worst)
  in
  loop 1 (ss_init, run ss_init)
