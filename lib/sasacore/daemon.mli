(* Time-stamp: <modified the 25/01/2023 (at 16:07) by Erwan Jahier> *)

type 'v pna = 'v Process.t * 'v Register.neighbor list * Register.action
type 'v enabled = 'v pna list list
type 'v triggered = 'v pna list

(** f dummy_input_flag verbose_mode daemon p_nl_l actions_ll enab

inputs:
- dummy_input_flag: true when used with --ignore-first-inputs
- verbose_mode: true when the verbose level is > 0
- daemon:
- p_nl_l: list of all processes, and their neighbors
- actions_ll: list of list of existing actions
- enab: list of list saying which actions are enabled

    At the inner list level, exactly one action ought to be chosen. At the
    outer list level, the number of chosen actions depends on the kind
    of daemons.

    In custom mode, as a side-effect, read on stdin which actions should be activated.

returns:
   - a string containing the values (in RIF) of activating variables
   - the list of activated actions

nb: it is possible that we read on stdin that an action should be
   activated even if it is not enabled (which would be a daemon
   "error").  For the time being, we ignore the daemon "error" and
   inhibit the activation.
*)

type 'v step = 'v triggered -> 'v SimuState.t -> 'v SimuState.t

val f : bool -> bool -> DaemonType.t -> 'v Process.t list ->
  ('v SimuState.t -> string -> 'v * ('v Register.neighbor * string) list) ->
  'v SimuState.t -> 'v enabled -> bool list list ->
  (string -> string -> bool) -> 'v step -> bool list list * 'v triggered

val get_activate_val: 'v triggered -> 'v Process.t list -> bool list list

(** Used in gtkgui.ml *)
val central: 'a list list -> 'a list
val distributed: 'a list list -> 'a list

(* pid + its neighbors in input *)
val locally_central: ('v * 'v list) list list -> 'v list
val map3 : ('a -> 'b -> 'c -> 'd) -> 'a list -> 'b list -> 'c list  -> 'd list
