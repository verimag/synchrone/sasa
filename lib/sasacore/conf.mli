(* Time-stamp: <modified the 14/10/2021 (at 15:35) by Erwan Jahier> *)

(** Storing process variables values.

    nb: this type is opaque iff 'v is opaque.
*)
type 'v t

val init: unit -> 'v t

(** [set env process_id var_value] *)
val set: 'v t -> string -> 'v -> 'v t

(** [get env process_id] *)
val get: 'v t -> string -> 'v

(** Use registered copy function to  return an hopefully (if the user
   provided copy state function is correct) fresh copy *)
val get_copy: 'v t -> string -> 'v
