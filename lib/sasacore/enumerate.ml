

(* Enumerate all schedules using continuations *)
type 'a cont = NoMore | Elt of 'a * (unit -> 'a cont)

(* compose continuations *)
let rec (comp : 'a cont -> 'a cont -> 'a cont) =
  fun c1 c2 ->
  match c1 with
  | NoMore -> c2
  | Elt(x, c1) -> Elt(x, fun () -> comp (c1()) c2)

(* Enumerate all possible schedules (with one action per process at most)
   nb: it can be a lot!
*)
let (all : 'a list list -> 'a list cont) = fun all ->
  let rec f acc all =
    let res = match all with
      | [] ->
        if acc = [] then NoMore else Elt(acc, fun () -> NoMore)
      | []::tl -> f acc tl
      | al::tl ->
         List.fold_left
           (fun cont_acc a ->
             let cont_a = f (a::acc) tl in
             comp cont_a cont_acc
           )
           (f acc tl)
           al
    in
    res
  in
  assert(List.exists (fun l -> l<>[]) all);
  f [] all

(* Enumerate all possible schedules for central daemons) *)
let (central : 'a list list -> 'a cont) = fun all ->
  let al = List.flatten all in
  List.fold_left (fun acc a -> Elt(a, fun () -> acc)) NoMore al



let (all_list : 'a list list -> 'a list list) = fun ll ->
  let rec f acc c =
    match c with
    | NoMore -> acc
    | Elt(x,c) -> f (x::acc) (c())
  in
  f [] (all ll)

let sort_ll ll =
  ll |> List.map (List.sort compare) |> List.sort compare

(*$T all_list
   sort_ll (all_list [ [1] ;[2]; [3]; [] ]) = sort_ll [ [1]; [2]; [3]; [1;2]; [1;3]; [2;3]; [1;2;3] ];
   sort_ll (all_list [ [1;2]; [3]; [] ])    = sort_ll [ [1]; [2]; [3]; [1;3]; [2;3] ];
 *)

let (central_list : 'a list list -> 'a list list) = fun all ->
  let al = List.flatten all in
  List.map (fun x -> [x]) al


let string_of_int_ll ll =
  "[" ^ (String.concat "," (List.map (fun l -> "[" ^ (String.concat "," l ^ "]")) ll)) ^ "]"

(*$T central_list
    List.sort compare (central_list [ [1;2]; [3]; [4;5] ]) = [ [1]; [2]; [3]; [4]; [5] ];
 *)
