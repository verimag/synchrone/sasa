(* Time-stamp: <modified the 15/03/2023 (at 15:59) by Erwan Jahier> *)

type t =
  | Synchronous (* select all actions *)
  | Central (* select 1 action *)
  | LocallyCentral (* never activates two neighbors actions in the same step [1] *)
  | Distributed (* select at least one action *)
  | Custom (* enable/actions are communicated via stdin/stdout in RIF *)
  | Greedy (* always choose the set that maximize the potential function *)
  | GreedyCentral (* Ditto, but chooses one action only *)
  | GreedyDetCentral (* Ditto, but with randomness *)
  | ExhaustSearch (* Explore all possible paths *)
  | ExhaustCentralSearch (* Explore all possible paths of central daemons *)

  (* not yet implemented *)
  | Bad of int (* try  to choose the  set actions that  maximize the
                   potential  function but  looking  at sub-graphs  of
                   size N at max *)

(* [1] nb: the current implementation of locally central daemon is
   biased by the degree of nodes. *)
