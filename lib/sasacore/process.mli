(* Time-stamp: <modified the 15/04/2021 (at 11:55) by Erwan Jahier> *)

(** There is such a Process.t per node in the dot file. *)
type 'v t = {
  pid : string; (* unique *)
  actions: Register.action list;
  init : 'v;
  enable : 'v Register.enable_fun;
  step : 'v Register.step_fun;
}

(** [make custom_mode_flag node init_state]  builds a process out of a
   node. To do that, it  retrieves the registered functions by Dynamic
   linking of the  cmxs file specified in the "algo"  field of the dot
   node.

    The custom_mode_flag is set in the sasa command line.

    nb: the provided  initial values comes from the dot  file if  the
    init field; otherwise it comes from in the Algo.init_state function *)
val make: bool -> Topology.node -> 'v -> 'v t
