(* Time-stamp: <modified the 13/05/2019 (at 10:17) by Erwan Jahier> *)

(** Reads on stdin a bool *)

val bool: bool -> string -> string -> bool
