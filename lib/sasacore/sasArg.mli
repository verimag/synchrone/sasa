(* Time-stamp: <modified the 09/02/2024 (at 10:17) by Erwan Jahier> *)

type init_search =
   No_init_search | Local of int * int | Global of int  | Annealing of int

type t = {
  mutable topo: string;
  mutable length: int;
  mutable cores_nb: int;
  mutable verbose: int;
  mutable daemon: DaemonType.t;
  mutable es_stop_if_no_progress: int option;
  mutable es_dfs: bool;
  mutable es_bfs: bool;
  mutable es_tabu_mode: bool;
  mutable is_tabu_mode: bool;
  mutable es_continue_after_best: bool;
  mutable es_dont_cut: bool;
  mutable restart: bool;
  mutable rif: bool;
  mutable output_file_name: string option;
  mutable no_data_file: bool;
  mutable quiet: bool;
  mutable ifi: bool;
  mutable gen_dot_at_legit: bool;
  mutable gen_lutin: bool;
  mutable gen_oracle: bool;
  mutable dummy_input: bool;
  mutable output_algos: bool;
  mutable gen_register: bool;
  mutable init_search: init_search;

  mutable _args : (string * Arg.spec * string) list;
  mutable _user_man  : (string * string list) list;
  mutable _hidden_man: (string * string list) list;

  mutable _others : string list;
  mutable _margin : int;
}

val usage_msg : string -> string

val parse : string array -> t
