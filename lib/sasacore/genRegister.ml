(*
takes as input some ocaml files, and generates an ocaml file
that (Algo.)register(s) the corresponding algorithms.

More precisely, the input files ougth to define a module inplementing 
the following interface:

 val name: string
 val actions : string list;
 type state
 val init_state: int -> string -> state
 val enable_f: state Algo.neighbor list -> state -> Algo.action list
 val step_f : state Algo.neighbor list -> state -> Algo.action -> state
 val state_to_string: state -> string
 val state_copy : state -> dfs_value

  *)

let ml_filename_to_module fn =
  let str = Filename.remove_extension fn in
  String.capitalize_ascii str

let from_ml ml =
  let m = ml_filename_to_module ml in
    Printf.sprintf "
      {
        algo_id = \"%s\";
        init_state = %s.init_state;
        enab = %s.enable_f;
        step = %s.step_f;          
      }" (String.uncapitalize_ascii m) m m m  


let (f: string list -> string * string * string -> unit) =
  fun ml_ins (state_file, config_file, register_file) ->
    let state_module = ml_filename_to_module state_file in
    let config_module = ml_filename_to_module config_file in
    if Sys.file_exists register_file then (
      Printf.eprintf " [sasa] Warning: %s already exist.\n%!" register_file
    ) else (
      let oc = open_out register_file in
      let entete = Mypervasives.entete2 "(*" "*)" SasaVersion.str SasaVersion.sha in
      let l = List.map from_ml ml_ins in
      Printf.fprintf oc "%s" entete ;
      Printf.fprintf oc "let () =
  Algo.register {
    algo = [ %s ];
    state_to_string = %s.to_string;
    state_of_string = %s.of_string;
    copy_state = %s.copy;
    actions = %s.actions;
    potential_function = %s.potential;
    legitimate_function = %s.legitimate;
    fault_function = %s.fault;
    init_search_utils = %s.init_search_utils;    
  }
"
        (String.concat ";" l) 
        state_module state_module state_module state_module config_module config_module
        config_module config_module;
      flush oc;
      close_out oc;
      Printf.eprintf " [sasa] The file %s has been generated\n" register_file;
      flush stderr
    );
    if Sys.file_exists state_file then (
      Printf.eprintf " [sasa] Warning: %s already exist.\n%!" state_file
    ) else (
      let oc = open_out state_file in
      let entete = Mypervasives.entete2 "(*" "*)" SasaVersion.str SasaVersion.sha in
      Printf.fprintf oc "%s" entete ;
      Printf.fprintf oc "type t = define_me
let to_string _ = \"define_me\"
let of_string = None
let copy x = x
let actions = [\"a\"]
";
      flush oc;
      close_out oc;
      Printf.eprintf " [sasa] The file %s has been generated\n" state_file;
      flush stderr
    );
    if Sys.file_exists config_file then (
      Printf.eprintf " [sasa] Warning: %s already exist.\n%!" config_file
    ) else (
      let oc = open_out config_file in
      let entete = Mypervasives.entete2 "(*" "*)" SasaVersion.str SasaVersion.sha in
      Printf.fprintf oc "%s" entete ;
      Printf.fprintf oc "
let potential = None (* None => only -sd, -cd, -lcd, -dd, or -custd are possible *)
let legitimate = None (* None => only silent configuration are legitimate *)
let fault = None (* None => the simulation stop once a legitimate configuration is reached *)
let init_search_utils = None (* To provide to use --init-search *)
";
      flush oc;
      close_out oc;
      Printf.eprintf " [sasa] The file %s has been generated\n" config_file;
      flush stderr
    )

