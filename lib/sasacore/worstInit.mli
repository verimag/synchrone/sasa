(* Time-stamp: <modified the 06/04/2022 (at 14:55) by Erwan Jahier> *)

(** First Choice Hill Climbing: a  successor is chosen at random (using
   some  heuristics), and  became  the  current state  if  its cost  is
   better.

   The heuristic to choose the succ is chosen at random using various 
   heuristics. *)
val fchc : out_channel -> ('v SimuState.t -> int) -> 'v SimuState.t -> int ->
  'v SimuState.t

(** Global  search : use  no heuristic, the  init wtate is  chosen at
   random using the user init functionstype 's node = { st : 's; d : int; cost : int; cpt : int; }
val debug : bool
type distance = Far | Close
val mutate_value : 'a -> 'b -> 'c
val one_dim_succ : 'a -> 'b array -> 'b array
val ran_dim_succ : 'a -> 'b array -> 'b array
val all_dim_succ : 'a -> 'b array -> 'b array
val tf : int -> float
val ti : float -> int
module ValueArrayNode : sig val compare : 'a node -> 'b node -> int end
module Q : sig end
val value2str : 'a -> 'b
val point2str : 'a array -> string
val reinit_simu : 'a -> 'b -> 'ca*)    
val global : out_channel -> ('v SimuState.t -> int) -> 'v SimuState.t -> int ->
  'v SimuState.t
