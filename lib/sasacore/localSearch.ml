
type ('n, 'tv, 'v) t = {
  init : 'n * 'tv * 'v;
  succ : 'n -> 'n list; (* returns (all or some) neighbors *)

  is_goal : 'n -> bool; (* is the node a solution of the problem *)
  stop : 'n -> 'n -> bool; (* [stop pre_sol n] to stop the search before all nodes are visited *)
  cut: 'n -> 'n -> bool; (* if [cut pre_sol n] is true, stop exploring n *)

  push : 'tv -> 'n -> 'tv; (* add the node in the set of nodes to visit *)
  pop  : 'tv -> ('n * 'tv) option; (* pick a node to visit *)

  visiting : 'n -> 'v -> 'v; (* mark a node as visited *)
  visited  : 'n -> 'v -> bool; (* check if a node has been visited *)
}

type 'n sol = Stopped | NoMore | Sol of 'n * 'n moresol
and 'n moresol = 'n option -> 'n sol


let debug = false

let cut_nb = ref 0
let tabu_nb = ref 0
let sol_nb = ref 0

let (run : ('n, 'tv, 'v) t -> 'n option -> 'n sol) =
 fun g pre_sol ->
  let cut sol_opt n = match sol_opt with
    | None -> false
    | Some sol ->
      let res = g.cut sol n in
      if res then incr cut_nb;
      res
  in
  let visited n v = if g.visited n v then (incr tabu_nb; true) else (false) in
  let pre_process sol_opt (v, tv) n =
    if visited n v || cut sol_opt n then (v, tv) else (g.visiting n v, g.push tv n)
  in
  Sys.catch_break true;
  let rec loop ps n tv v psol =
   let do_succ_cont s =
     if g.stop ps n then Stopped else loop2 n tv v s
   in (* to avoid code duplication *)
   if not (g.is_goal n) then
     do_succ_cont psol
   else
     (incr sol_nb; Sol(n, do_succ_cont))
  and loop2 n tv v psol = (* look at the n successors *)
    if debug then Printf.printf "look at successors\n%!";
    let v, tv = List.fold_left (pre_process psol) (v, tv) (g.succ n) in
    (match g.pop tv with
     | None -> NoMore
     | Some (next_n, tv) -> loop n next_n tv v psol
    )
  in
  let n, tv, v = g.init in
  try loop n n tv v pre_sol
  with Stdlib.Sys.Break ->
    Printf.printf  "The search has been interrupted by CTRL-C\n%!";
    Stopped

let stat log =
  Printf.fprintf log "
- local search statistics:
  - | cutted branches | = %d
  - | tabu list hits  | = %d
  - | solutions | = %d

%!" !cut_nb !tabu_nb !sol_nb
