
(** [f ml_ins (state_file, config_file, register_file)] 
    Takes as input some ocaml files and 3 output file names, and it
    generates those 3 files if they do not  already exist:
    - 1 that defines a skeleton ocaml program for dealing with algo states
    - 1 that defines a skeleton ocaml program for dealing with algo config 
       (fault,potential,legitimate)
    - 1 that (Algo.)register(s) the (input) algorithms.

 Existing output file won't be overrided (a warning is printed).

 The input files ought to define a module implementing the following interface:

    val name: string
    val actions : string list option;
    val init_state: int -> state
    val enable_f: state Algo.neighbor list -> state -> Algo.action list
    val step_f  : state Algo.neighbor list -> state -> Algo.action -> state
    val state_to_string: state -> string

  *)

val f: string list -> string * string * string -> unit
