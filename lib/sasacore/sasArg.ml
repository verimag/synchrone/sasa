(* Time-stamp: <modified the 09/02/2024 (at 10:16) by Erwan Jahier> *)

type init_search =
   No_init_search | Local of int * int | Global of int  | Annealing of int

type t = {
  mutable topo: string;
  mutable length: int;
  mutable cores_nb: int;
  mutable verbose: int;
  mutable daemon: DaemonType.t;

  (* for exhaustive daemon only *)
  mutable es_stop_if_no_progress: int option;
  mutable es_dfs: bool;
  mutable es_bfs: bool;
  mutable es_tabu_mode: bool;
  mutable is_tabu_mode: bool;
  mutable es_continue_after_best: bool;
  mutable es_dont_cut: bool;

  mutable restart : bool;
  mutable rif: bool;
  mutable output_file_name: string option;
  mutable no_data_file: bool;
  mutable quiet: bool;
  mutable ifi: bool;
  mutable gen_dot_at_legit: bool;
  mutable gen_lutin: bool;
  mutable gen_oracle: bool;
  mutable dummy_input: bool;
  mutable output_algos: bool;
  mutable gen_register: bool;
  mutable init_search: init_search;

  mutable _args : (string * Arg.spec * string) list;
  mutable _user_man  : (string * string list) list;
  mutable _hidden_man: (string * string list) list;

  mutable _others : string list;
  mutable _margin : int;
}

let usage_msg tool = ("usage: " ^ tool ^ " [<option>]* <topology>.dot
use -h to see the available options.
" )
let print_usage tool = Printf.printf "%s\n" (usage_msg tool); flush stdout


let (make_args : unit -> t) =
  fun () ->
    {
      topo = "";
      length = max_int;
      cores_nb = 1;
      verbose = 0;
      daemon = DaemonType.Distributed;
      es_stop_if_no_progress = None;
      es_dfs = false;
      es_bfs = false;
      es_tabu_mode = true;
      is_tabu_mode = true;
      es_continue_after_best = false;
      es_dont_cut= false;
      restart = false;
      rif = false;
      output_file_name = None;
      no_data_file = false;
      quiet = false;
      ifi = false;
      gen_dot_at_legit = false;
      gen_lutin = false;
      gen_oracle = false;
      dummy_input = false;
      output_algos = false;
      gen_register = false;
      init_search = No_init_search;
      _args = [];
      _user_man  = [];
      _hidden_man  = [];
      _others = [];
      _margin =12;
    }


let pspec args os  (c, ml) = (
  let (m1, oth) = match ml with
	 |	h::t -> (h,t)
	 |	_ -> ("",[])
  in
  let t2 = String.make args._margin ' ' in
  let cl = String.length c in
  let t1 = if (cl < args._margin ) then
	 String.make (args._margin - cl) ' '
  else
	 "\n"^t2
  in
	 Printf.fprintf os "%s%s%s" c t1 m1;
	 List.iter (function x -> Printf.fprintf os "\n%s%s" t2 x) oth ;
	 Printf.fprintf os "\n" ;
)

let options args oc = (
	let l = List.rev args._user_man in
	List.iter (pspec args oc) l
)
let more_options args oc = (
	let l = List.rev (args._hidden_man) in
	List.iter (pspec args oc) l
)
let (mkopt : t -> string list -> ?hide:bool -> ?arg:string -> Arg.spec ->
     string list -> unit) =
  fun opt ol ?(hide=false) ?(arg="") se ml ->
    let treto o = opt._args <- (o, se, "")::opt._args in
	   List.iter treto ol ;
	   let col1 = (String.concat ", " ol)^arg in
	     if hide
        then opt._hidden_man <- (col1, ml)::opt._hidden_man
	     else opt._user_man   <- (col1, ml)::opt._user_man

let myexit i = exit i
(*******************************************************************************)

(*** User Options Tab **)
let (mkoptab : string array -> t -> unit) =
  fun argv args ->
    (
    mkopt args ["--length";"-l"] ~arg:" <int>"
      (Arg.Int (fun i -> args.length <- i))
      ["Maximum number of steps to be done (" ^
       (string_of_int args.length) ^ " by default).\n"];

    mkopt args  ["--synchronous-daemon";"-sd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.Synchronous))
      ["Use a Synchronous daemon"];

    mkopt args  ["--central-daemon";"-cd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.Central))
      ["Use a Central daemon (selects exactly one action)"];

    mkopt args  ["--locally-central-daemon";"-lcd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.LocallyCentral))
      ["Use a Locally Central daemon";
       "(i.e., never activates two neighbors actions in the same step)"];
    mkopt args  ["--distributed-daemon";"-dd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.Distributed))
      ["Use a Distributed daemon (which select at least one action).";
       "This is the default daemon."];

    mkopt args  ["--custom-daemon";"-custd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.Custom;args.rif <- true))
      ["Use a Custom daemon (forces --rif)"];

    mkopt args  ["--greedy-daemon";"-gd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.Greedy))
      ["Use the daemon that maximizes the potential function at each step.";
       "Performs 2^|enabled| trials (per step). "];

    mkopt args  ["--greedy-central-daemon";"-gcd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.GreedyCentral))
      ["Ditto, but restricted to central daemons."];

    mkopt args  ["--greedy-deterministic-central-daemon";"-gdcd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.GreedyDetCentral))
      ["Ditto, but always return the same solution."];

    mkopt args  ["--exhaustive-daemon";"-ed"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.ExhaustSearch))
      ["Use the daemon that maximizes the number of steps. ";
       "The search is stopped when the maximum number of steps has been reached ";
       "(which is controlled by the -l/--length option)" ];

    mkopt args  ["--exhaustive-central-daemon";"-ecd"]
      (Arg.Unit(fun () -> args.daemon <- DaemonType.ExhaustCentralSearch))
      ["Ditto, but for central daemons" ];

    mkopt args  ["--es-abort-if-not-progressing"]
      (Arg.Int(fun i -> args.es_stop_if_no_progress <- Some i))
      ["Abort the exhaustive search if not progressing (i.e., when #step>step(last sol)x<int>)." ] ~arg:" <int>";

    mkopt args  ["--es-dfs"]
      (Arg.Unit(fun () -> args.es_dfs <- true))
      ["Use a depth first search to perform the exploration." ];

    mkopt args  ~hide:true ["--es-bfs"]
      (Arg.Unit(fun () -> args.es_bfs <- true))
      ["Use a breadth first search to perform the exploration." ];

    mkopt args  ["--es-no-tabu"]
      (Arg.Unit(fun () -> args.es_tabu_mode <- false))
      ["Do not use Tabu list during the exhaustive search." ];

    mkopt args  ["--is-no-tabu"]
      (Arg.Unit(fun () -> args.is_tabu_mode <- false))
      ["Do not use Tabu list during the initial configuration search." ];

    mkopt args  ~hide:true ["--es-continue-when-best-sol-found"]
      (Arg.Unit(fun () -> args.es_continue_after_best <- true))
      ["Do not stop when |path(sol)|=pot(init): this is necessary when E.x phi'(c)>0 (pseudo pot)" ];

    mkopt args  ~hide:true ["--es-dont-cut"]
      (Arg.Unit(fun () -> args.es_dont_cut <- true))
      ["Do not cut path when n.cost < prev_sol.cost : this is useful when E.x phi'(c)>0 (pseudo pot)" ];


    (*     mkopt args  ["--bad-daemon";"-bd"] ~arg:" <int>" *)
    (*       (Arg.Int (fun i -> args.daemon <- DaemonType.Bad i)) *)
    (*       ["Use a  daemon that tries  to maximize the  potential function, "; *)
    (*         "considering sub-graphs of a given maximal size"]; *)
    mkopt args  ["--local-init-search";"-is"]
      (Arg.Int(fun i ->
           match args.init_search with
           | Global g -> args.init_search <- Local (g,i)
           | _ ->  args.init_search <- Local (0,i)))
      ["Use local search algorithms to find an initial configuration that pessimize ";
       "the step number. The argument is the maximum number of trials to do the search. ";
       "Require the state_to_nums Algo.to_register field to be defined."]  ~arg:" <int>";

    mkopt args  ["--global-init-search";"-gis"]
      (Arg.Int(fun i ->
           match args.init_search with
           | Local (_,l) -> args.init_search <- Local (i,l)
           | _ ->  args.init_search <- Global i))
      ["Use global (i.e., completely random)  search to find an initial configuration ";
       "that pessimize the step number. The argument is the maximum number of trials";
       " to do the search. "]  ~arg:" <int>";

(*    mkopt args  ["--init-search-simulated-annealing";"-issa"]
      (Arg.Int(fun i -> args.init_search <- Annealing i))
      ["ditto + simulated annealing. XXX NOT YET IMPLEMENTED"]  ~arg:" <int>";
*)
    mkopt args  ["--cores-nb";"-cn"]
      (Arg.Int(fun i -> args.cores_nb <- i))
      ["Number of cores to use during --init-search simulations (default is 1)"];

   mkopt args ~hide:true ["--rif";"-rif"]
      (Arg.Unit(fun () -> args.rif <- true))
      ["Print only outputs (i.e., behave as a rif input file)"];

   mkopt args  ~hide:true  ["--no-data-file";"-nd"]
      (Arg.Unit(fun () -> args.no_data_file <- true))
      ["Do not print any data"];

    mkopt args  ~hide:false  ["--outfile";"-o"]
      (Arg.String(fun fn -> args.output_file_name <- Some fn))
      ["Generate simulation data in a file (use stdout otherwise)"];

   mkopt args ~hide:true ["--gen-dot-at-legit";"-gdal"]
      (Arg.Unit(fun () -> args.gen_dot_at_legit <- true))
      ["Generate a dot file initialised with the reached legitimate config"];

    mkopt args  ["--seed";"-seed"]
      (Arg.Int(fun i -> Seed.set ~verb:(args.verbose>0) i)) ~arg:" <int>"
      ["Set the pseudo-random generator seed of build-in daemons (wins over --replay)"];

    mkopt args  ["--replay";"-replay"]
      (Arg.Unit(fun () -> Seed.replay_seed := true))
      ["Use the last generated seed to replay the last run"];

    mkopt args  ["--restart";"-restart"]
      (Arg.Unit(fun () -> args.restart <- true))
      ["If a fault function has been provided, use it to restart when a legitimate configuration is reached"];

   mkopt args ~hide:true ["--gen-lutin-daemon";"-gld"]
      (Arg.Unit(fun () -> args.gen_lutin <- true))
      ["Generate Lutin daemons and exit (not finished)"];

   mkopt args ~hide:true ["--gen-lustre-oracle-skeleton";"-glos"]
      (Arg.Unit(fun () -> args.gen_oracle <- true))
      ["Generate a Lustre oracle skeleton"];

   mkopt args ~hide:true ["--list-algos";"-algo"]
     (Arg.Unit(fun () -> args.output_algos <- true))
      ["Output the algo files used in the dot file and exit. "];

   mkopt args ~hide:true ["--gen-register";"-reg"]
     (Arg.Unit(fun () -> args.gen_register <- true))
      ["Generates the registering files and exit. "];

   mkopt args ~hide:true ["--dummy-input"]
      (Arg.Unit(fun () -> args.dummy_input <- true))
      ["Add a dummy input"];

    mkopt args ~hide:true ["--ignore-first-inputs"; "-ifi"]
      (Arg.Unit(fun () -> args.ifi <- true))
      ["[Deprecated] make sasa ignore its first input vector"];

    mkopt args ["--version";"-version";"-v"]
      (Arg.Unit (fun _ ->
           (print_string (SasaVersion.str^"-"^SasaVersion.sha^"\n");
            flush stdout;exit 0)))
      ["Display the sasa version and exit."];

    mkopt args ~hide:true ["--ocaml-version"]
      (Arg.Unit (fun _ -> (print_string (Sys.ocaml_version^"\n"); flush stdout; exit 0)))
      ["Display the version ocaml version sasa was compiled with and exit."];

    mkopt args  ["--quiet";"-q"]
      (Arg.Unit (fun () -> args.quiet <- true))   ["Set the quiet mode (for batch)"];

    mkopt args  ["--verbose";"-vl"] ~arg:" <int>"
      (Arg.Int (fun i -> args.verbose <- i))   ["Set the verbose level"];

    mkopt args ["--help";"-help"; "-h"]
      (Arg.Unit (fun _ -> print_usage (argv.(0)); options args stdout; exit 0))
      ["Display main options"];

    mkopt args ["--more";"-m"] (Arg.Unit (fun () -> more_options args stdout; exit 0))
      ["Display more options"]

  )

(* all unrecognized options are accumulated *)
let (add_other : t -> string -> unit) =
  fun opt s ->
    opt._others <- s::opt._others

let current = ref 0;;
let first_line b = (
	try (
		let f = String.index b '\n' in
		String.sub b 0 f
	) with Not_found -> b
)
let file_notfound f = (
	prerr_string ("File not found: \""^f^"\"");
	prerr_newline ();
	myexit 1
)
let unexpected s = (
	prerr_string ("unexpected argument \""^s^"\"");
	prerr_newline ();
	myexit 1
)

let parse argv = (
  let save_current = !current in
  try (
    let args = make_args () in
    mkoptab argv args;
    Arg.parse_argv ~current:current argv args._args (add_other args) (usage_msg argv.(0));
    Functory.Cores.set_number_of_cores args.cores_nb;
    (List.iter
       (fun f ->
          if (String.sub f 0 1 = "-") then
            unexpected f
          else if not (Sys.file_exists f) then
            file_notfound f
          else ()
       )
       args._others);
    current := save_current;
    args.topo <- (match args._others with
          [] ->
          Printf.fprintf stderr "*** The topology file is missing in '%s'\n%s\n"
            (argv.(0)) (usage_msg argv.(0));
          exit 2;
        | x::_ -> x
      );
    args
  )
  with
  | Arg.Bad msg ->
    Printf.fprintf stderr " [sasa] Error when calling '%s': %s\n%s\n%!" (argv.(0))
      (first_line msg) (usage_msg argv.(0)); exit 2;
  | Arg.Help msg ->
    Printf.fprintf stdout "%s\n%s\n" msg (usage_msg argv.(0));
    exit 0
)
