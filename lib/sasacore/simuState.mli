(* Time-stamp: <modified the 09/02/2024 (at 10:22) by Erwan Jahier> *)

(** The module is used by
    - the main sasa simulation loop (in ../../src/sasaMain.ml)
    - the RdbgPlugin.t maker (in ../sasa/sasaRun.ml)

*)



(* type 'v t = SasArg.t * 'v layout * 'v Conf.t *)
type 'v t = {
  config: 'v Conf.t;
  sasarg: SasArg.t;
  network: 'v Process.t list;
  neighbors: ('v Register.neighbor list) Map.Make(String).t; (* pid's neighbors *)
}

(* [make dynlink_flag argv] *)
val make : bool -> string array -> 'v t

type 'v enable_processes =
  ('v Process.t * 'v Register.neighbor list * Register.action) list list * bool list list

val get_enable_processes: 'v t -> 'v enable_processes

(** [update_config e c] updates c using e *)
val update_config: 'v Conf.t -> 'v t -> 'v t

val update_env_with_init : 'v Conf.t -> 'v Process.t list -> 'v Conf.t

(** inject a fault in a configuration using the registered (user) fault function

nb : inject_fault_in_conf uses only the static part of SimuState.t
*)
val inject_fault : (int -> string -> 'v -> 'v) -> 'v t -> 'v t


val inject_fault_in_conf : (int -> string -> 'v -> 'v) -> 'v t -> 'v Conf.t -> 'v Conf.t

(** Get pid's state and neighbors *)
val neigbors_of_pid : 'v t -> string -> 'v * ('v Register.neighbor * string) list

val compute_potentiel: 'v t -> float
val legitimate: 'v t -> bool


(* For SasaRun *)
val get_inputs_rif_decl : SasArg.t -> 'v Process.t list -> (string * string) list
val get_outputs_rif_decl: SasArg.t -> 'v Process.t list -> (string * string) list

val to_dot : 'v t -> string
