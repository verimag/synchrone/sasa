
(** {1 Some printers } *)

open Register
let (algo_neighbor : 'v Register.neighbor -> string) = fun n ->
  Printf.sprintf "%s (%s)" n.pid (Register.to_string n.state)

open Process
let (env: 'v Conf.t -> 'v Process.t list -> string) =
  fun env pl ->
    let value_to_string = Register.get_value_to_string () in
    let l = List.map
        (fun p ->
           Printf.sprintf "%s: %s" p.pid
             (value_to_string (Conf.get env p.pid)))
        pl
    in
    String.concat ", " l


let (env_rif: 'v Conf.t -> 'v Process.t list -> string) =
  fun env pl ->
    let l = List.map
        (fun p ->
           Printf.sprintf "%s" (SasaState.to_rif_data (Conf.get env p.pid)))
        pl
    in
    String.concat " " l

(* Remove from action names char that cannot appear as ident in Lustre, Lutin, etc. *)
let action str =
  let is_int = function
    | '0'..'9'  -> true
    | _ -> false
  in
  let str = if String.length str>0 && is_int str.[0] then "A_"^str else str in
  let str = Str.global_replace (Str.regexp "[-,\\.'`]") "_" str in
  let str = Str.global_replace (Str.regexp "[(){}]") "" str in
  let str = Str.global_replace (Str.regexp "[[]") "" str in
  let str = Str.global_replace (Str.regexp "[]]") "" str in
  str

let (array_lv6 : ('a -> string) -> 'a array -> string) =
  fun tostr a ->
  let l = Array.fold_right (fun b acc -> (tostr b)::acc) a [] in
  "["^(String.concat "," l)^"]"

let (matrix_lv6 : ('a -> string) -> 'a array array -> string) =
  fun tostr m ->
  let l = Array.fold_right (fun a acc -> (array_lv6 tostr a)::acc) m [] in
  "[\n\t"^(String.concat ",\n\t" l)^"]"
