(* Time-stamp: <modified the 09/02/2024 (at 13:55) by Erwan Jahier> *)

open Process

let b2s b = if b then "t" else "f"

let already_defined = function (* some attributes may already be defined *)
  | "links_number"
  | "max_degree"
  | "min_degree"
  | "mean_degree"
  | "diameter"
  | "card"
  | "is_cyclic"
  | "is_connected"
  | "is_a_tree"
  | "f"
  | "t"
  | "adjacency"  -> true
  | _ -> false

let graph_attributes_to_string () =

  let al = Register.graph_attribute_list () in
  let l = List.map
      (fun (n,v) -> if not (already_defined n) then
          Printf.sprintf "const %s=%s;\n" n v else "")
      al
  in
  String.concat "" l


let (f: Topology.t -> string option -> 'v Process.t list -> string) =
  fun g output_file_name pl ->
  let actions_nb = List.map (fun p -> List.length p.actions) pl in
  let m = List.fold_left max (List.hd actions_nb) (List.tl actions_nb) in
  let n = List.length pl in

  let al = List.map (fun p -> List.map (fun a -> p.pid^"_"^a) p.actions) pl in
  let al = List.flatten al in
  let al = List.map StringOf.action al in
  let enabl = List.map (fun a -> "Enab_"^a) al in
  let vars =
    List.fold_left
      (fun acc p ->
         let l = SasaState.to_rif_decl p.pid p.init in
         List.fold_left (fun acc (n,t) -> (n, Data.type_to_string t)::acc) acc (List.rev l)
      )
      []
      (List.rev pl)
  in
  let vars_nb = List.length vars in
  let vars_decl = List.map (fun (v,t)-> v^":"^t) vars in
  let input_state = "\n  "^(String.concat ";" vars_decl)  in
  let input_trig = ";\n  "^(String.concat "," al) ^ ":bool" in
  let input_enab = ";\n  "^(String.concat "," enabl) ^ ":bool" in
  let input_decl = input_state^input_enab^input_trig^ ";round:bool; round_nb:int" in
  let array_decl =
    Printf.sprintf "\tActi:bool^an^pn;\n\tEnab:bool^an^pn;\n\tConfig:state^card;\n"
  in
  let acti_name p a = Printf.sprintf "%s_%s" p.pid (StringOf.action a) in
  let enab_name p a = Printf.sprintf "Enab_%s_%s" p.pid (StringOf.action a) in
  let array_def_acti =
    Printf.sprintf "\tActi = [%s];\n"
      (String.concat ","
         (List.map (fun p ->
              "["^(String.concat "," (List.map (acti_name p) p.actions))^"]") pl))
  in
  let array_def_enab =
    Printf.sprintf "\tEnab = [%s];\n"
      (String.concat ","
         (List.map (fun p ->
              "["^(String.concat "," (List.map (enab_name p) p.actions))^"]") pl))
  in
  let array_def_config =
    if vars_nb <> n then
      let struct_nb_var = vars_nb/n in
      let rec struct_string vars_list count =
        match vars_list with
        | []->""
        | e::[]->(Printf.sprintf "%s) " e)
        | e::tail-> (
            if (count mod struct_nb_var) = 0 then
              (Printf.sprintf "to_state(%s, " e ) ^ (struct_string tail (count-1))
            else if (count mod struct_nb_var) = 1 then
              (Printf.sprintf "%s), " e) ^ (struct_string tail (count-1))
            else
              (Printf.sprintf "%s, " e) ^ (struct_string tail (count-1))
          )
      in
      Printf.sprintf "\tConfig = [%s];\n" (struct_string (List.map fst vars) vars_nb)
    else
      Printf.sprintf "\tConfig = [%s];\n" (String.concat "," (List.map fst vars))
  in
  let algo = match output_file_name with
    | None -> Filename.basename (Sys.getcwd())
    | Some str -> str
  in
  let algo = StringOf.action algo in
  Printf.sprintf "%sinclude \"%s_oracle.lus\"
const an=%d; -- actions number
const pn=%d; -- processes number
const max_degree=%d;
const min_degree=%d;
const mean_degree=%f;
const diameter=%d;
const card=%d;
const links_number=%d;
const is_cyclic=%b;
const is_connected=%b;
const is_a_tree=%b;
const f=false;
const t=true;
const adjacency=%s;
const distance=%s;
%s
node oracle(legitimate:bool;%s%s) \nreturns (ok:bool);
var
  %slet
  %s  %s  %s
  ok = %s_oracle<<an,pn>>(legitimate,%s Enab, Acti, Config, round, round_nb);
tel


node oracle_debug(legitimate:bool;%s%s) \nreturns (ok:bool;
  lustre_config : state^card;
  lustre_enabled : bool^actions_number^card;
--  lustre_cost:int;
  lustre_round:bool;
  lustre_round_nb:int;
%s
);
let
  %s  %s  %s
  ok, lustre_config, lustre_enabled,lustre_round,lustre_round_nb = %s_oracle_debug<<an,pn>>(legitimate,%s Enab, Acti, Config, round, round_nb);
tel
"
    (Mypervasives.entete "--"  SasaVersion.str SasaVersion.sha)
    algo
    m n
    (Register.max_degree ())
    (Register.min_degree ())
    (Register.mean_degree ())
    (Register.diameter ())
    (Register.card ())
    (Register.links_number ())
    (Register.is_cyclic ())
    (Register.is_connected ())
    (Register.is_tree ())
    (StringOf.matrix_lv6 b2s (Topology.to_adjacency g))
    (StringOf.matrix_lv6 string_of_int (Topology.to_distance  g))
    (graph_attributes_to_string ())

    (if Register.get_potential () = None then "" else  "potential:real; ")
    input_decl
    array_decl
    array_def_acti
    array_def_enab
    array_def_config
    algo
    (if Register.get_potential () = None then "" else  "potential,")

    (if Register.get_potential () = None then "" else  "potential:real; ")
    input_decl
    array_decl
    array_def_acti
    array_def_enab
    array_def_config
    algo
    (if Register.get_potential () = None then "" else  "potential,")
