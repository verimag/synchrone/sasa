(* Time-stamp: <modified the 16/01/2023 (at 16:14) by Erwan Jahier> *)

(** generates oracle skeletons *)
val f: Topology.t -> string option -> 'v Process.t list -> string
