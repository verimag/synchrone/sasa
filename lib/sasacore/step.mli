
(* [f pnal e] performs a step (according to the actions in pnal) and returns a new env *)

val f : ('v Process.t * 'v Register.neighbor list * Register.action) list ->
         'v SimuState.t -> 'v SimuState.t
