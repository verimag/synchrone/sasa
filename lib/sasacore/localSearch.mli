(* Time-stamp: <modified the 27/10/2021 (at 12:08) by Erwan Jahier> *)

(** A generic module to implement local searches
    https://en.wikipedia.org/wiki/Local_search_(optimization)
*)

(** Parameterized by node, nodes to visit later, already visited (+ to visit) nodes

- 'tv: nodes to visit late can be implemented by lists, or priority queues
- 'v:  visited nodes can be implemented by lists, or sets
*)
type ('n, 'tv, 'v) t = {
  init : 'n * 'tv * 'v;
  succ : 'n -> 'n list; (* returns (all or some) neighbors *)     

  is_goal : 'n -> bool; (* is the node a solution of the problem *)
  stop : 'n -> 'n -> bool; (* if [stop pre_sol n], stop the search *)
  cut: 'n -> 'n -> bool;   (* if [cut pre_sol n], don't explore n *)

  push : 'tv -> 'n -> 'tv; (* add the node in the set of nodes to visit *)
  pop  : 'tv -> ('n * 'tv) option; (* pick a node to visit *)

  visiting : 'n -> 'v -> 'v; (* mark a node as visited *)
  visited  : 'n -> 'v -> bool; (* check if a node has been visited *)
}



type 'n sol = Stopped | NoMore | Sol of 'n * 'n moresol
and 'n moresol = 'n option -> 'n sol
  
 (** [explore g] the graph induced by [g.succ] until either
    - [pop tv]~>None, then it returns NoMore 
    - [stop pre_s s]~> true and is_goal s~>false, then it returns [Stopped]
    - [is_goal s]~>true, then it returns [Sol(sol, cont)]

   When a valid node (a.k.a., a solution) is found, [run] returns it plus
   a continuation to carry on the search.

   The optional argument of ['n moresol] ought to contain a previously obtained
   solution (i.e., a node n for which [is_goal n=true]) that can be used by [cut]
   to cut branches.

     nb: no cost function is required. But of course, the [push] or the [cut]
     functions might use one.
*)
val run : ('n, 'tv, 'v) t -> 'n moresol
val stat : out_channel -> unit
