(* Time-stamp: <modified the 15/03/2023 (at 16:01) by Erwan Jahier> *)


(** This module gathers daemons that tries to reach the worst case with
   a potential function using various heuristic. *)

type 'v pna = 'v Process.t * 'v Register.neighbor list * Register.action
type 'v enabled = 'v pna list list
type 'v triggered = 'v pna list
type 'v step = 'v triggered -> 'v SimuState.t -> 'v SimuState.t

(** [greedy verb st pl neighbors_of_pid  step all] take the worst case
   among the  combinations of length  1, i.e.,  O(2^n) where n  is the
   number of enabled processes (|all|) *)
val greedy: bool -> 'v SimuState.t -> 'v Process.t list ->
  ('v SimuState.t -> string -> 'v * ('v Register.neighbor * string) list) ->
  'v step -> 'v enabled -> 'v triggered

(** Ditto, but for central daemons (of a connected component) *)
val greedy_central:
  bool -> bool -> 'v SimuState.t -> 'v Process.t list ->
  ('v SimuState.t -> string -> 'v * ('v Register.neighbor * string) list) ->
  'v step -> 'v enabled -> 'v triggered

(** Returns  the worst  case among  the combinations  of length  1 for
   convex  potential functions,  and just  a bad  one otherwise  (O(n)
   where n is the number of enabled processes).  *)
val worst4convex: 'v SimuState.t -> 'v enabled -> 'v triggered

(** enumerate all the cases (of length 1) in sub-graphs of given size (O(2^size_max)) *)
val bad: int -> 'v SimuState.t -> 'v enabled -> 'v triggered
