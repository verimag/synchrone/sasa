(* Time-stamp: <modified the 19/01/2023 (at 15:55) by Erwan Jahier> *)

type t = {
  mutable first : bool;
  mutable is_round : bool;
  mutable moves : int;
  mutable cpt   : int;
  mutable round_mask : bool list list;
  mutable pre_acti_ll: bool list list
}
type t_save = bool * int * int * bool list list * bool list list

let s:t = {
  first =  true;
  is_round = false;
  moves = 0;
  cpt =  0;
  round_mask = [];
  pre_acti_ll= [];
}


let all_false ll = List.for_all (List.for_all (not)) ll

let bool2str b = if b then "t" else "f"
let bll2str bll =
  String.concat " " (List.map bool2str (List.flatten bll))
let bl2str bl =
  String.concat " " (List.map bool2str bl)

let debug_update lbl legit =
  Printf.printf "Round.update: %s mask=%s cpt=%d round=%b legit=%b\n%!" lbl (bll2str s.round_mask) s.cpt s.is_round
    legit

let (update : bool -> bool -> bool list list -> bool list list -> unit) =
  fun legit use_pre_trigger curr_acti_ll enab_ll ->
  (* Depending on who calls it (sasaMain, sasaRun), we use the previous value of the trigger or not *)
  (*   debug_update "debut" legit; *)
  if s.first then (
    (*     Printf.printf "First round update\n%!"; *)
    s.first <- false;
    s.is_round <- false;
    s.cpt <- 0;
    s.round_mask <- enab_ll;
    s.pre_acti_ll <- curr_acti_ll
  ) else (
    if s.cpt = 0 then (
      (*        Printf.printf "Round.update: the first round begins\n%!" ;  *)
      s.cpt <- 1
    );
    let acti_ll = if use_pre_trigger then s.pre_acti_ll else curr_acti_ll in
    List.iter (List.iter (fun b -> if b then s.moves <- s.moves+1)) acti_ll;
    if all_false s.round_mask && all_false enab_ll then (
      s.is_round <- false;
    )
    else
      try (
        let new_mask = List.map2 (List.map2 (&&)) s.round_mask enab_ll in (* neutralize *)
        let new_mask = List.map2 (List.map2 (fun m a -> m&&not a)) new_mask acti_ll in
        s.pre_acti_ll <- curr_acti_ll;
        let empty mask = all_false mask in
        if empty new_mask then
          (
            if not legit then s.cpt <- s.cpt+1;
            s.is_round <- true;
            s.round_mask <- enab_ll
          )
        else (
          s.is_round <- false;
          s.round_mask <- new_mask
        )
      )
      with _ -> assert false
  )
(*    ; debug_update "fin" legit *)


let (get : unit -> t_save) = fun () ->
  s.first, s.moves, s.cpt, s.round_mask, s.pre_acti_ll

let (set : t_save -> unit) = fun (first, moves, cpt, round_mask, pre_acti_ll) ->
 s.first <- first;
 s.moves <- moves;
 s.cpt <- cpt;
 s.round_mask  <- round_mask;
 s.pre_acti_ll <- pre_acti_ll

let reinit () =
 s.first <- true;
 s.is_round <- false;
 s.moves <- 0;
 s.cpt <- 0;
 s.round_mask <- [];
 s.pre_acti_ll <- []

let reinit_mask () =
 s.is_round <- false;
 s.round_mask <- [];
 s.pre_acti_ll <- []
