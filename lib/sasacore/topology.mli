(* Time-stamp: <modified the 21/02/2024 (at 09:40) by Erwan Jahier> *)

(** {1 Topology: internal representation of Graphs } *)

type node_id = string
type node = {
  id: node_id; (** The id of the node as stated in the dot file *)
  file: string; (** the content of the algo field (a ml file) *)
  init: string; (** store the content of the init field *)
}


type t = {
  nodes: node list; (** *)
  pred: node_id -> (int * node_id) list; (** get neighbors, with weight *)
  succ: node_id -> node_id list;
  of_id: node_id -> node; (** *)
  directed:bool; (** true if the graph is directed *)
  attributes:  (string * string) list (** (name, value) list of graph attributes *)
}

(** Parse a sasa dot file *)
val read: string -> t

(** {1 Various eponymous util functions } *)


val to_adjacency: t -> bool array array
(*@ a = to_adjacency t
  t.directed=false -> forall i, j. a.(i).(j) = a.(j).(i)
*)

val to_distance: t -> int array array
val diameter: t -> int

val get_nb_link: t -> int
val get_mean_degree : t -> float
val is_connected : t -> bool
val is_cyclic : t -> bool
val is_rooted_tree : t -> bool
val is_tree : t -> bool
val is_in_tree : t -> bool
val is_out_tree : t -> bool
val get_height : t -> string -> int
val get_level : t -> string -> int
val get_parent : t -> string -> int option
val get_subtree_size : t -> string -> int

val get_degree: t -> int * int
(*@ dmin, dmax = get_degree t
    ensures 0 <= dmin <= dmax
*)

(**  [reply  g p  p_neighbor]  returns  the  channel number  that  let
   [p_neighbor] access to the content of  [p], if [p] is a neighbor of
   [p_neighbor].  Returns -1 if [p] is not a neighbor of [p_neighbor],
   which can happen in directed graphs. *)
val reply: t -> string -> string -> int
