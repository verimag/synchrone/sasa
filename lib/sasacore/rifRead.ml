(* Time-stamp: <modified the 13/05/2019 (at 10:16) by Erwan Jahier> *)

(* xxx use RifIO.read instead ! *)
let bool verbose_mode pname a =
  if verbose_mode then ( 
    Printf.eprintf "Enter a bool [1,t,T|0,f,F] for process %s\n" pname;
    flush stderr
  );
  let x = input_char stdin in
  let rec aux x =
    match x with
    | '0' | 'f' | 'F' -> false
    | '1' | 't' | 'T' -> true
    | 'q' -> Printf.eprintf "bye\n"; flush stderr; exit 0
    | '#' -> skip_comment ()
    | x ->
      if verbose_mode then (Printf.eprintf "'%c'" x; flush stderr);
      aux (input_char stdin)
  and skip_comment () =
    match input_char stdin with
    | '\n'-> aux (input_char stdin)
    | 'q' -> Printf.eprintf "bye\n"; flush stderr; exit 0
    | c  -> (if verbose_mode then print_char c) ;skip_comment ()
  in
  let res = aux x in
  if verbose_mode then (
    flush stderr;
    Printf.eprintf "%s_%s<-%s\n" pname a (if res then "t" else "f");
    flush stderr
  );
    res
