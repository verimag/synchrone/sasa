(* Time-stamp: <modified the 17/03/2023 (at 22:14) by Erwan Jahier> *)

open LocalSearch

type 'v ss = 'v SimuState.t
let pot st = if SimuState.legitimate st then 0.0 else SimuState.compute_potentiel st

type node = {
  d : int;
  cost : float; (* pot+depth *)
  pot : float;
  cpt : int;  (* => no sharing between config *)
  path : (bool list list * bool list list * bool * float * string) list;
  st : string;
}

let delta = 1.0 (* the potential decreases of at least delta at each step *)

(* This priority is given to  the highest (depth, potential) couple at
   the beginning,  which induces a  greedy depth first  search.  Then,
   once a first solution has been found, the priority goes to the most
   promising node *)
let best = ref None
let pot_init = ref max_float

let dfs = ref false
let bfs = ref false

let priority n =
  let d = float_of_int n.d in
  match !best, !dfs, !bfs with
  | None, _, false | _, true, _ -> if n.pot <= 0.0 then d else d -. (1.0 /. (n.pot/.delta))
  | Some b, false, false -> d +. n.pot *. (b /. !pot_init)
  | _,_, true -> -. d

let _priority n =
  let d = float_of_int n.d in
    if n.pot <= 0.0 then d else d -. (1.0 /. (n.pot/.delta))

module M = struct type t = node
  let compare a b = compare (priority b) (priority a)
end
module Q = Psq.Make (Int) (M)
module StrMap = Map.Make(String)


let cumulated_Q_time = ref 0.0
let update_Q_now = ref false

let update_Q q =
  (* when  the priority changes, we  need to recompute the  queue from
     scratch (actually,  it does  not work  too bad  if we  don't, but
     well...) *)
  let t0 = Sys.time () in
  let q = Q.fold (fun k p acc -> Q.add k p acc) Q.empty q in
  cumulated_Q_time := !cumulated_Q_time +. (Sys.time () -. t0);
  update_Q_now := false;
  q


let pf = Printf.fprintf
let do_log args = not args.SasArg.no_data_file && args.SasArg.verbose > 1
let p log args n pot enab trig =
  if not args.SasArg.no_data_file && args.SasArg.verbose > 1 then
  pf log " {%d: d = %d cost=%.1f ; pot=%.1f; enab = %s; trig = %s\n%!"
     n.cpt n.d n.cost  pot enab trig

let (marshall_ss : 'v ss -> string) =
  fun ss ->
  Marshal.to_string ss.config []

let _all_false ll = List.for_all (fun b -> not b) (List.flatten ll)

let sob = fun b -> if b then "t" else "f"

open SimuState
let (bnb : out_channel -> bool -> 'v ss ->
     (bool list list * bool list list * bool * float * 'v Conf.t) list) =
  fun log central st0 ->
  dfs := st0.sasarg.es_dfs;
  bfs := st0.sasarg.es_bfs;
  let max_step = st0.sasarg.length in
  let cpt = ref 0 in
  let cpt_of_last_improvement = ref 0 in (* to measure the progress of the search*)
  let qsize = ref 0 in
  let args = st0.sasarg in
  let get_ss str = SimuState.update_config (Marshal.from_string str 0) st0 in
  let successors n =
    let st = get_ss n.st in
    let pot_st = n.cost -. (float_of_int n.d) in
    let all, enab_ll = SimuState.get_enable_processes st in
    let enable_val = String.concat " " (List.map sob (List.flatten enab_ll)) in
    if SimuState.legitimate st then [] else (
      let all = if central then Enumerate.central_list all else Enumerate.all_list all in
      List.map
        (fun al ->
           incr cpt;
           if !cpt mod (st0.sasarg.length / 100) = 0 && not st0.sasarg.quiet then
             Printf.printf "%d%% of steps have been tryied so far...\r%!"
               (!cpt / (st0.sasarg.length / 100));
           let nst = Step.f al st in
           let pot_nst = pot nst in
           let nst_str = marshall_ss nst in
           let trig_val = Daemon.get_activate_val al st.network in
           let trig_val_str = String.concat " " (List.map sob (List.flatten trig_val)) in
           let node = {
             st = nst_str;
             d = n.d+1;
             pot = pot_nst;
             cpt = !cpt;
             path=(enab_ll, trig_val, false, pot_st, n.st)::n.path;
             cost = (float_of_int (n.d+1)) +. pot_nst (* XXX * delta *)
           }
           in
           p log st.sasarg node pot_nst enable_val trig_val_str;
           node
        )
        all
    )
  in
  let visited_config_cpt = ref 0 in
  let visiting, visited =
    let visiting n v =
      StrMap.add n.st n.d v
    in
    let visited n v =
      match StrMap.find_opt n.st v with
      | None -> incr visited_config_cpt; false
      | Some d -> d >= n.d (* if the depth of n is higher, we revisit it *)
    in
    if st0.sasarg.es_tabu_mode then
      visiting, visited
    else
      visiting,
      (*         (fun _ v -> v), (fun _ _ -> false) *)
      (fun n v -> if not (StrMap.mem n.st v) then incr visited_config_cpt; false)
  in
  let pot0 = pot st0 in
  pot_init := pot0;
  let not_progressing es_stop_if_no_progress = (* should this be done in LocalSearch ? *)
    match es_stop_if_no_progress with
    | None -> false
    | Some factor ->
      !cpt_of_last_improvement > 0 &&
      !cpt > !cpt_of_last_improvement * factor (* should be a CLI parameter? *)
  in
  let pb =
    {
      init = { st=marshall_ss st0; d=0; pot=pot0 ; cpt=0; path=[]; cost=pot0 },
             Q.empty, StrMap.empty;
      succ = successors;
      stop = (fun _ _node ->
          if !cpt >= max_step then (
            pf log "W: Max number of step reached (%d). |Q|=%d; visited config=%d\n%!"
              !cpt !qsize !visited_config_cpt;
            pf stdout "W: Max number of step reached (%d); use -l to change it\n%!" !cpt;
            true
          ) else if not_progressing st0.sasarg.es_stop_if_no_progress then (
            pf log "W: The search is stuck for a long time (%d steps): Abort.\n%!"
              (!cpt - !cpt_of_last_improvement);
            pf stdout "W: The search is stuck for a long time (%d steps): Abort.\n%!"
              (!cpt - !cpt_of_last_improvement);
            true
          ) else (
            false
          )
        );
      is_goal = (fun n -> n.cost <= float_of_int n.d);
      push = (fun tv n ->
          qsize := Q.size tv;
          if do_log args then
            pf log " ==> Pushing a node (#%d) of cost %.1f (pot=%.1f; d=%d; |Q|=%d; priority=%.3f)\n%!"
              n.cpt n.cost n.pot n.d  !qsize (priority n);
          Q.add n.cpt n tv);
      pop = (fun tv ->
          let tv = if !update_Q_now then update_Q tv else tv in
          match Q.pop tv with None -> None | Some((i,x),t) ->
            if do_log args then
              pf log "<== Poping a node (#%d) of cost %.1f (pot %.1f, depth %d, priority=%.3f)\n%!"
                i x.cost x.pot x.d  (priority x);
            Some(x, t));
      visiting = visiting;
      visited  = visited;
      cut = (fun psol n -> if not args.es_dont_cut && psol.cost >= n.cost then (
          if do_log args then
            pf log "%d: cut at depth %d of cost %.1f<=%.1f\n%!" !cpt n.d n.cost psol.cost;
          true
        )
          else false
        );
    }
  in
  let format sol =
    let msg = Printf.sprintf "  - the worst path is of length %d
  - %f seconds have been spent in the priority queue
  - %d configurations have been (re-)visited
  - %d steps have been performed\n%!" sol.d !cumulated_Q_time !visited_config_cpt !cpt
    in
    print_string msg;
    output_string log msg;
    LocalSearch.stat stdout;
    LocalSearch.stat log;
    let path =
      let st = get_ss sol.st in
      let pot_st = sol.cost -. (float_of_int sol.d) in
      let _all, enab_ll = SimuState.get_enable_processes st in
      (enab_ll,[], true, pot_st, sol.st)::sol.path
    in
    List.map
      (fun (e, t, leg, pot, str) -> e, t, leg, pot, Marshal.from_string str 0)
      (List.rev path)
  in
  let rec run_more psol more =
    if psol.d = int_of_float !pot_init && not args.es_continue_after_best then (
      pf log "The length of this solution is equal to the initial potential (%.1f). %s\n"
        !pot_init
        "\n\tIt is therefore the best we can find. \n\tStop the search.\n%!";
      pf stdout "The length of this solution is equal to the initial potential (%.1f). %s\n" !pot_init
        "\n\tIt is therefore the best we can find. \n\tStop the search.\n%!";
      format psol
    )
    else
      let more_sol = more (Some psol) in
      match more_sol with
      | LocalSearch.Stopped
      | LocalSearch.NoMore ->
        pf log "longuest_path: %d %d %s\n" (psol.d) !cpt (if !dfs then "dfs" else "promising");
        if more_sol = LocalSearch.Stopped then (
          pf log "W: The search stopped before the end\n";
          pf stdout "W: The search stopped before the end\n"
        )
        else (
          pf log "All possible paths have been explored!\n";
          pf stdout "All possible paths have been explored!\n";
        );
        format psol
      | LocalSearch.Sol (nsol, more) ->
        if nsol.cost > psol.cost then (
          pf stdout "==> [New longuest path] I've found a path of length %d! (after #%d steps)\n%!" (nsol.d) !cpt;
          update_Q_now := true;
          pf log    "==> [New longuest path] I've found a path of length %d! (after #%d steps)\n%!" (nsol.d) !cpt;
          pf log    "longuest_path: %d %d %s\n"  (nsol.d) !cpt (if !dfs then "dfs" else "promising");
          best := Some (float_of_int nsol.d);
          cpt_of_last_improvement := nsol.cpt;
          run_more nsol more
        ) else (
          run_more psol more
        )
  in
  match LocalSearch.run pb None with
  | LocalSearch.Stopped ->
    failwith ("The exploration stopped after "^(string_of_int max_step)
              ^" step without finding solution")
  | LocalSearch.NoMore-> failwith "There is no solution!"
  | LocalSearch.Sol (sol, more) ->
    pf log "==> The first solution has a path of length %d (step #%d)\n%!" sol.d !cpt;
    pf log    "longuest_path: %d %d %s\n" (sol.d) !cpt (if !dfs then "dfs" else "promising");
    pf stdout "==> The first solution has a path of length %d (step #%d)\n%!" sol.d !cpt;
    update_Q_now := true;
    best := Some (float_of_int sol.d);
    run_more sol more


let f = bnb

let reset () =
 best := None;
 pot_init := max_float;
 cumulated_Q_time := 0.0;
 update_Q_now := false
