(* Time-stamp: <modified the 19/06/2019 (at 10:06) by Erwan Jahier> *)

module Dico = Map.Make(String)

type 'v t = 'v Dico.t 

let (get: 'v t -> string -> 'v) =
  fun e pid  ->
    (*     Printf.printf "<-- get pid %s\n" pid; flush stdout;  *)
    try ((Dico.find pid e))
    with e ->
      failwith (Printf.sprintf "Unknown pid: %s (%s)" pid  (Printexc.to_string e))


let (get_copy: 'v t -> string -> 'v) =
  fun e pid ->
    let copy_value = Register.get_copy_value () in
    copy_value (get e pid)


let (set: 'v t -> string -> 'v -> 'v t) =
  fun e pid st ->
    (*     Printf.printf "--> set pid %s\n" pid; flush stdout;  *)
    (*     let lenv = Algo.set lenv "pid" (Algo.S pid) in *)
    Dico.add pid st e

let (init:unit -> 'v t) = fun () -> Dico.empty

