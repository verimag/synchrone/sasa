(* Time-stamp: <modified the 09/02/2024 (at 10:22) by Erwan Jahier> *)

open Register
open Topology

let (update_env_with_init : 'v Conf.t -> 'v Process.t list -> 'v Conf.t) =
  fun e pl ->
    let (aux: 'v Conf.t -> 'v Process.t -> 'v Conf.t) =
      fun e p ->
        Conf.set e p.pid p.init
    in
    List.fold_left aux e pl


let (get_neighors: Topology.t -> Topology.node_id -> 'v -> 'v Register.neighbor list) =
  fun g source_id init ->
  let idl = g.pred source_id in
    List.map
      (fun (w, neighbor_id) ->
         let node = g.of_id neighbor_id in
         {
           state = init;
           pid = node.id;
           spid = source_id;
           reply = (fun () -> Topology.reply g source_id neighbor_id);
           weight = (fun () -> w)
          }
      )
      idl

let (dump_process: string -> 'v Process.t * 'v Register.neighbor list -> unit) =
  fun msg (p,nl) ->
    let pvars = String.concat "," (SasaState.to_var_names p.pid p.init) in
    let neighbors = List.map StringOf.algo_neighbor nl in
    Printf.eprintf "%s: process %s\n\tvars:%s\n\tneighors: \n\t\t%s\n%!" msg p.pid  pvars
      (String.concat "\n\t\t" neighbors)

open Process
open SasArg

module StringMap = Map.Make(String)

type 'v t = {
  config: 'v Conf.t;
  sasarg: SasArg.t;
  (*   network: ('v Process.t * 'v Register.neighbor list) list; *)
  network: 'v Process.t list;
  neighbors: ('v Register.neighbor list) Map.Make(String).t;
}

let (neigbors_of_pid : 'v t -> pid -> 's * ('s neighbor * pid) list) =
 fun st pid ->
 let nl =
   match StringMap.find_opt pid st.neighbors with
   | Some x -> x
   | None ->
     Printf.eprintf "no %s found in %s\n%!" pid
       (String.concat "," (List.map (fun p -> p.Process.pid) st.network));
     exit 2
 in
  Conf.get st.config pid,  List.map (fun n -> n, n.Register.pid) nl


let (update_neighbor_env:
       'v Conf.t -> 'v Register.neighbor list -> 'v Register.neighbor list) =
  fun e nl ->
  List.map (fun n -> { n with state = Conf.get e n.Register.pid }) nl

let update_neighbors config neighbors = StringMap.map
    (fun nl -> update_neighbor_env config nl)
    neighbors

let (update_config: 'v Conf.t -> 'v t -> 'v t) =
  fun e st ->
  let verb = !Register.verbose_level > 2 in
  if verb then Printf.eprintf " ===> update_neighbor_env\n%!";
  { st with neighbors = update_neighbors e st.neighbors ; config = e }

type 'v enable_processes =
  ('v Process.t * 'v Register.neighbor list * Register.action) list list * bool list list

let (get_enable_processes: 'v t -> 'v enable_processes) =
  fun st ->
  let pl_n = List.map (fun p -> p, StringMap.find p.pid st.neighbors) st.network in
  let e = st.config in
  assert (pl_n <> []);
  let all = List.fold_left
      (fun acc (p,nl) ->
         let lenv = Conf.get e p.pid in
         let al = p.enable nl lenv in
         let al =
           List.map (fun a ->
               if not (List.mem a p.actions) then
                 (Printf.printf
                    "[sasa] Error: The action label \"%s\" is not declared (i.e., not in [%s])\n%!"
                    a (String.concat "; " (List.map (fun s -> "\""^s^"\"") p.actions));
                  exit 2);

               p,nl,a)
             al
         in
         al::acc)
      [] pl_n
  in
  assert (List.length pl_n = List.length all);
  let all = List.rev all in
  let enab_ll =
    List.map2
      (fun (p,_) al ->
         let al = List.map (fun (_,_,a) -> a) al in
         List.map (fun a_static -> List.mem a_static al) p.actions)
      pl_n
      all
  in
  all, enab_ll

let (get_inputs_rif_decl: SasArg.t -> 'v Process.t list -> (string * string) list) =
  fun args pl ->
  if args.daemon <> Custom then
    if args.dummy_input then ["_dummy","bool"] else []
  else
    let f acc p =
      List.fold_left
        (fun acc a -> (p.pid ^(if a="" then "" else "_")^(StringOf.action a) ,"bool")::acc)
        acc
        (List.rev p.actions)
    in
    List.fold_left f [] (List.rev pl)

let (get_outputs_rif_decl: SasArg.t -> 'v Process.t list -> (string * string) list) =
  fun args pl ->
  (* This fonction  may be called on huge lists:  thus it must remain
     tail-recursive and linear! *)
  let pl = List.rev pl in
  let vars = [] in
  (* Adding action vars *)
  let vars =
    if args.daemon = Custom then vars else
      List.fold_left
        (fun acc p ->
          List.fold_left
            (fun acc a ->
               ((Printf.sprintf "%s_%s" p.pid (StringOf.action a)),"bool")::acc)
            acc
            (List.rev p.actions)
        )
        vars
        pl
  in
  (* Adding enable action vars *)
  let vars =
    List.fold_left
      (fun acc p ->
        List.fold_left
          (fun acc a ->
            ((Printf.sprintf "Enab_%s_%s" p.pid (StringOf.action a)),"bool")::acc)
          acc
          (List.rev p.actions)
      )
      vars
      pl
  in
  (* Adding algo vars *)
  let vars =
    List.fold_left
      (fun acc p ->
        let l = List.rev (SasaState.to_rif_decl p.pid p.init) in
        List.fold_left (fun acc (n,t) -> (n, Data.type_to_string t)::acc) acc l
      )
      vars
      pl
  in
  vars

let (env_rif_decl: SasArg.t -> 'v Process.t list -> string) =
  fun args pl ->
    let ssl = get_outputs_rif_decl args pl in
    String.concat " "
      (List.map (fun (base, tstr) -> Printf.sprintf "\"%s\":%s" base tstr) ssl)


let rec (make : bool -> string array -> 'v t) =
  fun dynlink argv ->
  let args =
    try SasArg.parse argv;
    with
      Failure(e) ->
      output_string stdout e;
      flush stdout ;
      exit 2
    | e ->
      output_string stdout (Printexc.to_string e);
      flush stdout;
      exit 2
  in
  let seed = Seed.get ~verb:(args.verbose>0) args.topo in
  try
    let dynlink = if args.output_algos then false else dynlink in
    let dot_file = args.topo in
    let g = Topology.read dot_file in
    let nl = g.nodes in
      let algo_files = List.map (fun n -> n.Topology.file) nl in
      let algo_files = List.sort_uniq compare algo_files in
      let algo_files =
        (* we should have ml files  here; even if the dot file refers
           to, e.g.,  lustre files. The justification  for doing that
           is that  only the  file name  matters, not  the extension.
           This is necessary if we want  to share dot files with sasa
           and  salut.   Probably  it  would be  better  to  have  no
           extension at all in the .dot files, and let the tools that
           are  using  it decide  what  the  real file  extension  is
           supposed to be (sasa => .ml, salut => .lus) *)
        List.map (fun f -> (Filename.remove_extension f)^".ml") algo_files
      in
    if args.output_algos then (
      Printf.printf "%s\n%!" (String.concat " " algo_files);
      exit 0
    );
    let cmxs = (Filename.remove_extension dot_file) ^ ".cmxs" in
    if args.gen_register then (
      let base = Filename.remove_extension dot_file in
      let base = Filename.basename base in
      let base = Str.global_replace (Str.regexp "\\.") "" base in
      let ml_register_file = base ^ ".ml" in
      let ml_state_file = "state.ml" in
      let ml_config_file = "config.ml" in
      GenRegister.f algo_files (ml_state_file, ml_config_file, ml_register_file);
      exit 0
    );

    let nidl = List.map (fun n -> n.Topology.id) nl in
    let nstr = String.concat "," nidl in
    Register.set_topology g;
    List.iter (fun (n,v) -> Register.set_graph_attribute n v) g.attributes;

    Register.verbose_level := args.verbose;

    if !Register.verbose_level > 1 then Printf.eprintf "==> nodes: %s\n" nstr;

    if dynlink then (
      (* Dynamically link the cmxs file (not possible from rdbg) *)
      let cmxs = Dynlink.adapt_filename cmxs in
      if !Register.verbose_level > 0 then Printf.eprintf " [sasa] Loading %s...\n" cmxs;
      Dynlink.loadfile_private cmxs;
    ) else ();

    let initl = List.map (fun n ->
        if n.Topology.file = "" then (
        Printf.eprintf " [sasa] Empty algo attribute in %s.\n%!" dot_file;
        exit 1
        );
        let algo_id = Filename.remove_extension n.Topology.file in
        let value_of_string_opt = Register.get_value_of_string () in
        if value_of_string_opt = None || n.Topology.init = "" then
          (* Use the init functions if initial values are not in the dot *)
          Register.get_init_state algo_id (List.length (g.succ n.id)) n.id
        else
          match value_of_string_opt with
          | None -> assert false (* sno *)
          | Some f -> f n.Topology.init
      )
        nl
    in

    if !Register.verbose_level > 0 then Printf.eprintf "==> get_neighors\n";
    let algo_neighors = List.map2 (get_neighors g) nidl initl in
    let pl = List.map2 (Process.make (args.daemon=Custom)) nl initl in
    let e = Conf.init () in
    let e = update_env_with_init e pl in
    let algo_neighors = List.map (update_neighbor_env e) algo_neighors in
    let pl_n = List.combine pl algo_neighors in
    let neighbors =
      List.fold_left (fun acc (p,nl) -> StringMap.add p.pid nl acc)
        StringMap.empty pl_n
    in
    if !Register.verbose_level > 1 then List.iter (dump_process "") pl_n;
    if args.gen_lutin then (
      let fn = (Filename.remove_extension args.topo) ^ ".lut" in
      if Sys.file_exists fn then (
        Printf.eprintf " [sasa] %s already exists: rename it to proceed.\n%!" fn;
        exit 1
      ) else
        let oc = open_out fn in
        Printf.fprintf oc "%s%!" (GenLutin.f pl);
        close_out oc;
        Printf.eprintf " [sasa] %s has been generated.\n%!" fn;
        exit 0);
    if args.gen_oracle then (
      let fn = (Filename.remove_extension args.topo) ^ "_oracle.lus" in
      if Sys.file_exists fn then (
        Printf.eprintf " [sasa] %s already exists: rename it to proceed.\n%!" fn; exit 1
      ) else
        let oc = open_out fn in
        Printf.fprintf oc "%s%!" (GenOracle.f g args.output_file_name pl);
        close_out oc;
        Printf.eprintf " [sasa] %s has been generated.\n%!" fn;
        exit 0);
    if args.no_data_file || args.quiet then () else (
      let oc = if args.rif then stderr else stdout in
      if !Register.verbose_level > 0 then Printf.eprintf "==> open rif file...\n%!";
      if not args.rif then
        Printf.fprintf oc "%s" (Mypervasives.entete "#" SasaVersion.str SasaVersion.sha);
      Printf.fprintf oc "#seed %i\n" seed;
      let inputs_decl = get_inputs_rif_decl args pl in
      Printf.printf "#inputs ";
      if !Register.verbose_level > 0 then
        Printf.eprintf "==> get input var names...\n%!";
      List.iter (fun (vn,vt) -> Printf.printf "\"%s\":%s " vn vt) inputs_decl;
      Printf.printf "\n%!";
      let pot =  match Register.get_potential () with
        | None -> ""
        | Some _ -> "potential:real"
      in
      if !Register.verbose_level > 0 then
        Printf.eprintf "==> get output var names...\n%!";
      Printf.printf "#outputs %s \"legitimate\":bool %s round:bool round_nb:int\n" (env_rif_decl args pl) pot;
      Printf.printf "\n%!";
      flush_all()
    );
    if args.ifi then (
      if !Register.verbose_level > 0 then Printf.eprintf "==> read bool...\n%!";
      List.iter
        (fun p -> List.iter
            (fun a -> ignore (RifRead.bool (args.verbose>1) p.pid (StringOf.action a)))
            p.actions)
        pl;
      Printf.eprintf "Ignoring the first vector of sasa inputs\n%!";
    );
    if !Register.verbose_level > 0 then Printf.eprintf "==> SimuState.make done !\n%!";
    {
      sasarg = args;
      network = pl;
      neighbors = neighbors;
      config = e
    }
  with
  | Dynlink.Error e ->
    let res = Sys.command (Printf.sprintf "make %s.cmxs"(Filename.remove_extension args.topo)) in
    if res = 0 then make dynlink argv else (
      Printf.eprintf " [sasa] %s\n%!"
        (Dynlink.error_message e);
      Printf.eprintf " [sasa] It looks like you didn't compile your Ocaml algorithm.\n%!";
      flush_all();
      exit res
    )
  | e ->
    Printf.eprintf " [sasa] Error (Sasacore.make): %s\n%!" (Printexc.to_string e);
    flush_all();
    exit 2



let (to_dot : 'v t -> string) =
  fun ss ->
  let dot_file = ss.sasarg.topo in
  let g = Topology.read dot_file in
  let nodes_decl =
    String.concat "\n"
      (List.map
         (fun node ->
            Printf.sprintf " %s [algo=\"%s\" init=\"%s\"]" node.id node.file
              (Register.to_string (Conf.get ss.config node.id))
         )
         g.nodes)
  in
  let trans =
    if g.directed then
      List.flatten
        (List.map
           (fun n ->
              List.map
                (fun t -> Printf.sprintf "%s -> %s" t n.id)
                (g.succ n.id)
           )
           g.nodes
        )
    else
      List.flatten
        (List.map
           (fun n ->
              let l = g.succ n.id in
              List.map (fun t ->
                  if n.id < t then
                    Printf.sprintf "%s -- %s" n.id t
                  else
                    Printf.sprintf "%s -- %s" t n.id
              )
                l
           )
           g.nodes
        )
  in
  let trans = List.sort_uniq compare trans in
  let trans_str = String.concat "\n" trans in
  let attributes = String.concat " " (List.map (fun (n,v) ->  n^"="^v) g.attributes) in
  Printf.sprintf
    "%sgraph %s {\n graph %s\n\n%s\n\n%s\n}\n"
    (if g.directed then "di" else "") "g"
    (if attributes="" then "" else "["^attributes^"]")  nodes_decl trans_str

let (compute_potentiel: 'v t -> float) =
  fun st ->
  match Register.get_potential () with
  | None -> failwith "potential function not registered"
  | Some user_pf ->
    let pidl = List.map (fun p -> p.Process.pid) st.network in
    let p = user_pf pidl (neigbors_of_pid st) in
    p

let (legitimate: 'v t -> bool) = fun st ->
  match Register.get_legitimate () with
  | None -> (* check if st is silent *)
    get_enable_processes st |> snd |> List.flatten |> List.for_all not
  | Some ulf ->
    let pidl = List.map (fun p -> p.Process.pid) st.network in
    ulf pidl  (neigbors_of_pid st)




let (inject_fault_in_conf : (int -> string -> 'v -> 'v) -> 'v t -> 'v Conf.t -> 'v Conf.t) =
  fun ff st c ->

  let update_nodes e p =
    let nl = StringMap.find p.Process.pid st.neighbors in
    let pid = p.Process.pid in
    let v = Conf.get e pid in
    let v = ff (List.length nl) pid v in
    Conf.set e pid v
  in
  let nc = List.fold_left update_nodes c st.network in
  nc

let (inject_fault : (int -> string -> 'v -> 'v) -> 'v t -> 'v t) =
  fun ff st ->
  let nc = inject_fault_in_conf ff st st.config in
  let st = { st with config = nc } in
  update_config nc st
