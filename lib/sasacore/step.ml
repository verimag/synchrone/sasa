
open Register
open Process



let (update_env: 'v Conf.t -> 'v Process.t * 'v -> 'v Conf.t) =
  fun e (p, st) ->
    Conf.set e p.pid st

  
let (f2 :
       ('v Process.t * 'v Register.neighbor list * action) list -> 'v Conf.t -> 'v Conf.t) =
  fun pnal e -> 
    let lenv_list =
      List.map (fun (p,nl,a) ->
          (*  I perform  a copy  of  the local  env to  make sure  the
             configuration update is atomic *)
          let lenv = Conf.get_copy e p.pid in
          p, p.step nl lenv a)
        pnal
    in
    (* 4: update the env *)
    let ne = List.fold_left update_env e lenv_list in
    ne

let (f : ('v Process.t * 'v Register.neighbor list * Register.action) list ->
          'v SimuState.t -> 'v SimuState.t) =
  fun pnal st ->
  let e = f2 pnal st.config in
  SimuState.update_config e st
