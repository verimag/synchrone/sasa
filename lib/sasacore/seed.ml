(* Time-stamp: <modified the 08/11/2021 (at 10:53) by Erwan Jahier> *)

let seed = ref None
let replay_seed = ref false      

let set ?verb:(verbose=true) s = 
  if verbose then
    Printf.fprintf stderr " [sasa] The sasa random engine seed is set to %i\n%!" s;
  Random.init s;
  seed := Some s

let seed_file_name label =
  Printf.sprintf "sasa-%s.seed" label

(* for --replay *)
let reset_the_seed_to_last ?verb:(verbose=true) label =
  let f = seed_file_name label in
  try
    let ic = open_in f in
    let seed = int_of_string (input_line ic) in
    set ~verb:verbose seed;
    Printf.eprintf " [sasa] Replay the sasa run using the seed in %s\n" f;
    flush stderr;
    true
  with _ ->
    Printf.eprintf " [sasa] W: cannot recover the seed in %s\n" f;
    flush stderr;
    false

let reset () = seed := None

let rec (get : ?verb:bool -> string -> int) = 
  fun ?verb:(verbose=true) label ->
  let label = Filename.basename label in
  match !seed with
  | Some i -> i
  | None ->
    (* No seed is set: 
       - in -replay mode, we first try to read the seed in the seed file
       - otherwise, we create a random seed and save if into args, and 
          into a file for -replay *)
    if !replay_seed && reset_the_seed_to_last ~verb:verbose label then (get label) else (
      let seed = Random.self_init (); Random.int 1073741823 in
      let seed_file = seed_file_name label in
      let oc = open_out seed_file in
      Printf.fprintf oc "%d\n%s\n" seed
        (Mypervasives.entete "#" SasaVersion.str SasaVersion.sha);
      flush oc;
      close_out oc;
      set ~verb:verbose seed;
      seed
    )
