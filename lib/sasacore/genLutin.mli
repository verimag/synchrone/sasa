(* Time-stamp: <modified the 22/01/2020 (at 10:01) by Erwan Jahier> *)

(** generates various Lutin daemons (distributed, synchronous, etc.) *)
val f: 'v Process.t list -> string
