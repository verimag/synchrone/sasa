(* Time-stamp: <modified the 15/03/2023 (at 16:02) by Erwan Jahier> *)

(* Enabled processes (with its enabling action + neighbors) *)
type 'v pna = 'v Process.t * 'v Register.neighbor list * Register.action
type 'v enabled = 'v pna list list
type 'v triggered = 'v pna list
type 'v step = 'v triggered -> 'v SimuState.t -> 'v SimuState.t


let (random_list : 'a list -> 'a) = fun l ->
  assert (l <> []);
  List.nth l (Random.int (List.length l))

(* returns a random element of a list as well as the rest of the list *)
let (random_list2 : 'a list -> 'a * 'a list) = fun l ->
  assert (l <> []);
  let rec split acc i = function
    | [] -> assert false (* sno *)
    | x::l ->
      if i=0 then x, List.rev_append acc l else split (x::acc) (i-1) l
  in
  let i = Random.int (List.length l) in
  split [] i l

let (central: 'a list list -> 'a list) =
  fun all ->
    if all = [] then [] else
      let al = List.map random_list all in
      let a = random_list al in
      [a]

let rec (distributed: 'a list list -> 'a list) =
  fun all ->
    if all = [] then [] else
      (*     assert (all <> []); *)
      let al = List.map random_list all in
      let al = List.filter (fun _ -> Random.bool ()) al in
      if al = [] then distributed all else al

let (synchrone: 'a list list -> 'a list) = fun all ->
  if all = [] then [] else
  let al = List.map random_list all in
  al

(* LC= 2 neighbors cannot be activated at the same step

XXX this daemon is not fair: it is biased by the degree of nodes.
*)
let (locally_central_pna: 'v enabled -> 'v triggered) =
  fun all ->
    let remove_one_conflict al =
      let _a, al = random_list2 al in
      al
    in
    let rec remove_conflicts al =
      let activated_pids = List.map (fun (p,_,_) -> p.Process.pid) al in
      let conflicts, ok = List.partition (fun (_p,nl,_a) ->
          List.exists (fun n -> List.mem (n.Register.pid) activated_pids) nl
        ) al
      in
      if conflicts = [] then ok else
        let conflicts = remove_one_conflict conflicts in
        ok @ (remove_conflicts conflicts)
    in
    if all = [] then [] else
      let al = distributed all in
      remove_conflicts al

(* Somewhat duplicate the previous one. Hard to avoid... *)
let (locally_central: ('v * 'v list) list list -> 'v list) =
  fun all ->
    let remove_one_conflict al =
      let _a, al = random_list2 al in
      al
    in
    let rec remove_conflicts al =
      let activated_pids = List.map (fun (pid,_) -> pid) al in
      let conflicts, ok = List.partition (fun (_p,nl) ->
          List.exists (fun n -> List.mem n activated_pids) nl
        ) al
      in
      if conflicts = [] then ok else
        let conflicts = remove_one_conflict conflicts in
        ok @ (remove_conflicts conflicts)
    in
    if all = [] then [] else
      let al = distributed all in
      fst (List.split (remove_conflicts al))

let rec map3 f l1 l2 l3 =
  match (l1, l2, l3) with
    ([], [], []) -> []
  | (a1::l1, a2::l2, a3::l3) -> let r = f a1 a2 a3 in r :: map3 f l1 l2 l3
  | ([], _, _) -> invalid_arg "map3 (1st arg too short)"
  | (_, [], _) -> invalid_arg "map3 (2nd arg too short)"
  | (_, _, []) -> invalid_arg "map3 (3rd arg too short)"

(*$T map3
  map3 (fun x y z -> x-y+z) [1;2;3] [1;2;3] [1;2;3] = [1;2;3]
  map3 (fun x y z -> x-y+z) [] [] [] = []
*)

let (custom: 'v enabled -> 'v Process.t list -> bool list list ->
     (string -> string -> bool) -> bool list list * 'v triggered) =
  fun pnall pl enab_ll get_action_value ->
    let f p pnal enab_l =
      let actions = p.Process.actions in
      let trigger_l = List.map (get_action_value p.Process.pid) actions in
      let acti_l_al =
        map3
          (fun trig enab a ->
             let acti = trig && enab in
             acti, if acti
             then List.filter (fun (_,_,a') -> a=a') pnal
             else []
          ) trigger_l enab_l actions
      in
      acti_l_al
    in
    let acti_l_all = map3 f pl pnall enab_ll in
    let acti_l_al = List.flatten acti_l_all in
    let al = snd (List.split acti_l_al) in
    let acti = List.map (List.map fst) acti_l_all in
    acti, List.flatten al



let (remove_empty_list: 'a list list -> 'a list list) =
  fun ll ->
    List.filter (fun l -> l<>[]) ll

let (get_activate_val: 'v triggered -> 'v Process.t list -> bool list list)=
  fun al pl ->
    let actions =
      List.map (fun p -> List.map (fun a -> p,a) p.Process.actions) pl
    in
    let al = List.map (fun (p,_,a) -> p,a) al in
    List.map  (List.map (fun a -> List.mem a al)) actions

let (f: bool -> bool -> DaemonType.t -> 'v Process.t list ->
     ('v SimuState.t -> string -> 'v * ('v Register.neighbor * string) list) ->
     'v SimuState.t ->
     'v enabled -> bool list list -> (string -> string -> bool) -> 'v step ->
     bool list list * 'v triggered) =
  fun dummy_in verbose_mode daemon pl neigbors_of_pid st all enab get_action_value step ->
  let nall = remove_empty_list all in
  if nall = [] then (
    Printf.printf "Warning: the algorithm is now Silent\n%!";
    get_activate_val [] pl, []
  )
  else (
    if daemon <> Custom && dummy_in then
      ignore (RifRead.bool verbose_mode ((List.hd pl).pid) "");
    match daemon with
    | Synchronous  ->
       let al = synchrone nall in
       get_activate_val al pl, al
    | Central ->
       let al = central nall in
       get_activate_val al pl, al
    | LocallyCentral ->
       let al = locally_central_pna nall in
       get_activate_val al pl, al
    | Distributed ->
       let al = distributed nall in
       get_activate_val al pl, al
    | Greedy ->
       let al = Evil.greedy verbose_mode st pl neigbors_of_pid step nall in
       get_activate_val al pl, al
    | (GreedyDetCentral|GreedyCentral) ->
       let al = Evil.greedy_central verbose_mode (daemon=GreedyDetCentral) st pl neigbors_of_pid step nall in
       get_activate_val al pl, al
    | Bad i ->
       let al = Evil.bad i st nall in
       get_activate_val al pl, al
    | Custom -> custom all pl enab get_action_value

    | ExhaustCentralSearch
    | ExhaustSearch -> assert false (* SNO *)
  )
