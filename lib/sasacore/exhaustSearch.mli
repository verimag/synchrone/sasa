(* Time-stamp: <modified the 07/02/2023 (at 15:37) by Erwan Jahier> *)

(*  [f  outc  is_central  simuState]  returns  the  longest  possible
   execution after exploring exhaustively the configuration state space *)
val f : out_channel ->  bool -> 'v SimuState.t ->
        (bool list list * bool list list * bool * float * 'v Conf.t) list

val reset : unit -> unit
