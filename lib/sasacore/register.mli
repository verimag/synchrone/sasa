(* Time-stamp: <modified the 11/10/2021 (at 14:22) by Erwan Jahier> *)

(**  This module  duplicates and  extends the  Algo module  with get_*
   functions.

    The rationale for defining such a module is to be able to hide the
   get_*  functions from  the Algo  interface. Indeed,  they are  only
   called  by the  sasa engine,  and it  is dangerous  and useless  to
   expose them to algorithms designers.  *)

type 's neighbor = {
  state:  's ;
  pid: string;
  spid: string;
  reply: unit -> int;
  weight: unit -> int;
}

type algo_id = string
type action  = string
type 's enable_fun = 's neighbor list -> 's -> action list
type 's step_fun   = 's neighbor list -> 's -> action -> 's
type 's fault_fun = int -> string -> 's -> 's

type pid = string

type value = F of float * float * float | I of int * int * int | B of bool
type 's state_to_values_fun = ('s -> value list) * (value list -> 's -> 's)

type 's potential_fun = pid list -> (pid -> 's * ('s neighbor * pid) list) -> float
type 's legitimate_fun = string list -> (string -> 's * ('s neighbor * pid) list) -> bool

val reg_init_state : algo_id -> (int -> string -> 's) -> unit
val reg_enable : algo_id -> 's enable_fun -> unit
val reg_step : algo_id -> 's step_fun -> unit
val reg_potential : 's potential_fun option -> unit
val reg_init_search_utils : 's state_to_values_fun option -> unit
val reg_legitimate : 's legitimate_fun option -> unit
val reg_fault : 's fault_fun option -> unit
val reg_actions : action list -> unit
val reg_value_to_string : ('s -> string) -> unit
val reg_value_of_string : (string -> 's) -> unit
val reg_copy_value : ('s -> 's) -> unit

val card : unit -> int
val min_degree : unit -> int
val mean_degree : unit -> float
val max_degree : unit -> int
val is_cyclic : unit -> bool
val is_connected : unit -> bool
val links_number : unit -> int
val diameter : unit -> int

val is_tree : unit -> bool
val is_in_tree : unit -> bool
val is_out_tree : unit -> bool
val is_rooted_tree : unit -> bool
val sub_tree_size : string -> int
val parent : string -> int option
val is_directed : unit -> bool
val level : string -> int
val height : string -> int
val verbose_level: int ref

val get_graph_attribute : string -> string
val get_graph_attribute_opt : string -> string option


(* All the remaining functions are used only by the sasa Engine, while
   the functions above are only used in the Algo module (i.e., by
   distributed algorithm designers. *)

val get_enable : algo_id -> 's enable_fun
val get_step   : algo_id -> 's step_fun
val get_init_state : algo_id -> int -> string -> 's
val get_actions   : unit -> action list
val get_potential   : unit -> 's potential_fun option
val get_init_search_utils  : unit -> 's state_to_values_fun option
val get_legitimate   : unit -> 's legitimate_fun option
val get_fault   : unit -> 's fault_fun option
val get_value_to_string : unit -> 's -> string
val get_value_of_string : unit -> (string -> 's) option
val get_copy_value : unit -> ('s -> 's)
val to_string : 's -> string

(** Those are called by sasa once the graph has been parsed  *)
val set_topology : Topology.t -> unit
type node_id = string (* cf topology.mli *)

val set_graph_attribute : string -> string -> unit
val graph_attribute_list: unit -> (string * string) list
