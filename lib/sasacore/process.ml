(* Time-stamp: <modified the 28/11/2022 (at 17:41) by Erwan Jahier> *)

type 'v t = {
  pid : string;
  actions: Register.action list;
  init : 'v;
  enable : 'v Register.enable_fun;
  step : 'v Register.step_fun ;
}

let (make: bool -> Topology.node -> 'v -> 'v t) =
  fun custom_mode n init ->
    let pid = n.Topology.id in
    let ml = n.Topology.file in
    let id = Filename.remove_extension ml in
    let actions = Register.get_actions () in
    if custom_mode && actions = [] then
      failwith
        "Registering actions is mandatory in algorithms when using custom daemon!";
    let process = {
      pid = pid;
      init = init;
      actions = actions;
      enable = Register.get_enable id; 
      step = Register.get_step id;
    }
    in
    process

