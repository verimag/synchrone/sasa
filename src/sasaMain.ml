open Sasacore

let bool2str b = if b then "t" else "f"
let bll2str bll =
  String.concat " " (List.map bool2str (List.flatten bll))

exception Silent of int
exception Legitimate of int


let (print_step : out_channel -> 'v SimuState.t -> int -> int -> string -> string ->
     SasArg.t -> 'v Conf.t -> 'v Process.t list -> string -> bool list list -> unit) =
  fun log st n i legitimate pot args e pl activate_val enab_ll ->
  let enable_val = bll2str enab_ll in
  if st.sasarg.init_search <> No_init_search then (
    (* Printf.fprintf log "\n#step %s\n%!" (string_of_int (n-i)); *)
    (* Printf.fprintf log "%s %s %s %s\n%!" (StringOf.env_rif e pl) enable_val legitimate pot; *)
  ) else
  if args.no_data_file then (
    if not args.quiet then Printf.printf "\n#step %s\n%!" (string_of_int (n-i))
  ) else (
    if args.daemon = DaemonType.Custom then (
      (* in custom mode, to be able to talk with lurette, this should not be
           printed on stdout
      *)
      if not args.rif && not args.quiet then (
        Printf.eprintf "\n#step %s\n" (string_of_int (n-i)) ;
        Printf.eprintf "%s #outs " activate_val; flush stderr
      );
      if not args.quiet then
        Printf.printf "%s %s %s %s %s %d\n%!" (StringOf.env_rif e pl) enable_val
          legitimate pot (bool2str Round.s.is_round) Round.s.cpt;
    ) else (
      (* rif mode, internal daemons *)
      if not args.quiet then (
        if args.rif then
          Printf.printf " %s %s %s %s %s %s %d\n%!"
            (StringOf.env_rif e pl) enable_val activate_val legitimate pot (bool2str Round.s.is_round) Round.s.cpt
        else (
          Printf.printf "\n#step %s\n" (string_of_int (n-i));
          Printf.printf "%s%s %s %s %s %s %s %d\n%!"
            (if args.rif then "" else "#outs ")
            (StringOf.env_rif e pl) enable_val activate_val legitimate pot (bool2str Round.s.is_round) Round.s.cpt
        ));
    );
    flush stderr;
    flush stdout
  )

open Sasacore.SimuState
module StringMap = Map.Make(String)


let plur i = if i>1 then "s" else ""

let (simustep: out_channel -> int -> int -> string -> 'v SimuState.t -> 'v SimuState.t * string) =
  fun log n i activate_val st ->
  (* 1: Get enable processes *)
  let verb = !Register.verbose_level > 0 in
  if verb then Printf.fprintf log "==> SasaSimuState.simustep :1: Get enable processes\n%!";
  let all, enab_ll = Sasacore.SimuState.get_enable_processes st in
  if verb then Printf.fprintf log "==> SasaSimuState.simustep: Get the potential\n%!";
  let pot = if Register.get_potential () = None then "" else
      string_of_float (SimuState.compute_potentiel st)
  in
  let pl = st.network in
  if verb then Printf.fprintf log "==> SasaSimuState.simustep: is it legitimate?\n%!";
  let silent = List.for_all (fun b -> not b) (List.flatten enab_ll) in
  let leg = legitimate st || silent in
  let gen_dot_legit () =
    if st.sasarg.gen_dot_at_legit then (
      let newdot_fn = (Filename.chop_extension st.sasarg.topo) ^ "_legitimate.dot" in
      let newdot = open_out newdot_fn in
      Printf.printf "%s has been generated\n" newdot_fn;
      Printf.fprintf newdot "%s\n" (SimuState.to_dot st);
      close_out newdot;
    );
  in
  let st, all, enab_ll =
    if
      (* not (args.rif) && *)
      silent
    then (
      match if st.sasarg.restart then Register.get_fault () else None with
      | None ->
        Round.update leg true enab_ll enab_ll;
        print_step log st n i "t" pot st.sasarg st.config pl (bll2str enab_ll) enab_ll;
        gen_dot_legit ();
        raise (Silent (n-i))
      | Some ff ->
        print_step log st n i "t" pot st.sasarg st.config pl (bll2str enab_ll) enab_ll;
        let str = if st.sasarg.rif then "#" else "" in
        Printf.fprintf log
          "\n%sThis algo is silent after %i move%s, %i step%s, %i round%s.\n"
          str Round.s.moves (plur Round.s.moves) (n-i) (plur (n-i))
          Round.s.cpt (plur Round.s.cpt);
        Printf.fprintf log "%s==> Inject a fault\n%!" str;
        let st = SimuState.inject_fault ff st in
        let all, enab_ll = Sasacore.SimuState.get_enable_processes st in
        st, all, enab_ll
    )
    else if leg then (
      match if st.sasarg.restart then Register.get_fault () else None with
      | None ->
        Round.update leg true enab_ll enab_ll;
        print_step log st n i "t" pot st.sasarg st.config pl activate_val enab_ll;
        gen_dot_legit ();
        raise (Legitimate (n-i))
      | Some ff ->
        print_step log st n i "t" pot st.sasarg st.config pl activate_val enab_ll;
        let str = if st.sasarg.rif then "#" else "#" in
        Printf.fprintf log
          "\n%sThis algo reached a legitimate configuration after %i move%s, %i step%s, %i round%s.\n"
          str Round.s.moves (plur Round.s.moves) (n-i) (plur (n-i)) (Round.s.cpt) (plur (Round.s.cpt));
        Printf.fprintf log "%s==> Inject a fault\n%!" str;
        let st = SimuState.inject_fault ff st in
        let all, enab_ll = Sasacore.SimuState.get_enable_processes st in
        st, all, enab_ll
    )
    else
      st, all, enab_ll
  in
  let leg_str = if leg then "t" else "f" in
  if st.sasarg.daemon = DaemonType.Custom then
    print_step log st n i leg_str pot st.sasarg st.config pl activate_val enab_ll;
  (* 2: read the actions *)
  if verb then Printf.fprintf log "==> SasaSimuState.simustep : 2: read the actions\n%!";
  let get_action_value = RifRead.bool (st.sasarg.verbose > 1) in
  let next_activate_val, pnal = Daemon.f st.sasarg.dummy_input
      (st.sasarg.verbose >= 1) st.sasarg.daemon st.network SimuState.neigbors_of_pid
      st all enab_ll get_action_value Step.f
  in
  Round.update leg true next_activate_val enab_ll;
  let next_activate_val = bll2str next_activate_val in
  (* 3: Do the steps *)
  if verb then Printf.fprintf log "==> SasaSimuState.simustep : 3: Do the steps\n%!";
  if st.sasarg.daemon <> DaemonType.Custom then
    print_step log st n i leg_str pot st.sasarg st.config pl next_activate_val enab_ll;
  let st = Sasacore.Step.f pnal st in
  st, next_activate_val

let rec (simuloop: out_channel -> int -> int -> string -> 'v SimuState.t -> int) =
  fun log n i activate_val st ->
  let rec loop i activate_val st =
    if !Register.verbose_level > 0 then
      Printf.fprintf log "==> SasaSimuState.simuloop %d/%d \n%!" (n-i) n;
    let st, next_activate_val = simustep log n i activate_val st in
    if i > 0 then loop (i-1) next_activate_val st else (
      print_string "#q\n"; flush_all ()
    )
  in
  try (loop i activate_val st); n
  with
  | Silent i ->
    let str = if st.sasarg.rif then "#" else "" in
    Printf.fprintf log
      "\n%sThis algo is silent after %i move%s, %i step%s, %i round%s.\n%!"
      str Round.s.moves (plur Round.s.moves) i (plur i) Round.s.cpt (plur Round.s.cpt);
    if st.sasarg.rif then
      print_string "\nq\n#quit\n%!"
    else
      Printf.fprintf log "\n#quit\n";
    flush_all();
    i

  | Legitimate i ->
    let str = if st.sasarg.rif then "#" else "" in
    Printf.fprintf log
      "\n%s%sThis algo reached a legitimate configuration after %i move%s, %i step%s, %i round%s.\n%!"
      (if st.sasarg.rif then "#" else "#")
      str Round.s.moves (plur Round.s.moves) i (plur i) (Round.s.cpt) (plur (Round.s.cpt));
    if st.sasarg.rif then
      print_string "\nq\n#quit\n%!"
    else
      Printf.fprintf log "\n#quit\n";
    flush_all();
    i

let sob = fun b -> if b then "t" else "f"


(* to give a fake value to activate_val at the very first step: every one is false *)
let gen_f n =
  let rec f n acc =
    if n<=0 then acc else f  (n-1) (acc ^ " f")
  in
  f n ""

let () =
  let st = Sasacore.SimuState.make true Sys.argv in
  let n = st.sasarg.length in
  let oc_rif = match st.sasarg.output_file_name with
      None -> stdout | Some fn -> open_out fn in
  try
    match st.sasarg.init_search, st.sasarg.daemon with
    | No_init_search, (ExhaustSearch|ExhaustCentralSearch) ->
      let log = open_out (st.sasarg.topo ^ ".log") in
      let path = ExhaustSearch.f log (st.sasarg.daemon=ExhaustCentralSearch) st in
      List.iteri
        (fun i (enab, trig, leg, pot, conf) ->
           if trig <> [] then
             Round.update leg true trig enab
           else (* Round.update requires list of the same size *)
             Round.update leg true
               (List.map (fun l -> List.map (fun _ -> false) l) enab) enab;
           Printf.fprintf oc_rif "#step %d\n#outs %s %s %s %s %s %s %d\n" (i)
             (StringOf.env_rif conf st.network) (bll2str enab) (bll2str trig)
             (sob leg) (string_of_float pot) (bool2str Round.s.is_round) Round.s.cpt;
        )
        path;
      let i = List.length path - 1 in
      Printf.printf
        "\n%sThis algo reached a null potential after %i move%s, %i step%s, %i round%s.\n%!"
        (if st.sasarg.rif then "#" else "#")
        Round.s.moves (plur Round.s.moves) i (plur i) Round.s.cpt (plur Round.s.cpt);

    | No_init_search, _ ->
      ignore (simuloop stdout n n (gen_f (List.length st.network)) st)

    | Annealing _, _ -> assert false (* TODO *)
    | (Local _ |Global _) , _ ->
      let log = open_out (st.sasarg.topo ^ ".log") in
      let newdot_fn = (Filename.chop_extension st.sasarg.topo) ^ "_wi.dot" in
      let newdot = open_out newdot_fn in
      let search_kind =
        match st.sasarg.init_search with Local _ -> "local" | _ -> "global" in
      let run s =
        Round.reinit();
        let s = SimuState.update_config s.config s in
        Printf.fprintf log "------------- New simu from %s\n%!"
          (StringOf.env_rif s.config s.network);
        let res =
          try if st.sasarg.daemon = ExhaustSearch ||
                 st.sasarg.daemon = ExhaustCentralSearch
            then
              List.length (ExhaustSearch.f log (st.sasarg.daemon=ExhaustCentralSearch) st)
            else
              simuloop log n n (gen_f (List.length st.network)) s
          with error ->
            (* We consider that error here mean that a meaningless configuration
               has been tried
            *)
            Printf.fprintf log "==> %s: certainly due to a meaningless configuration\n%!"
              (Printexc.to_string error);
            (-1)
        in
        if res = n then (
          Printf.printf  " (%s)\n%!" (StringOf.env_rif s.config st.network);
          Printf.fprintf newdot "%s\n" (SimuState.to_dot s);
          Printf.printf "%s and %s have been generated using a %s search\n"
            (s.sasarg.topo ^ ".log") newdot_fn search_kind;
          flush_all();
          close_out newdot;
          close_out log;
          failwith(Printf.sprintf
                     "Maximum simulation length reached. Something went wrong or %d %s"
                     n "is not long enough (use sasa -l to try longer simulation)"
                  )
        )
        else
          res
      in
      let st =
        match st.sasarg.init_search with
        | Local (maxg, maxl) ->
          let st = if maxg=0 then st else WorstInit.global log run st maxg in
          WorstInit.fchc log run st maxl
        | Global maxg ->
          WorstInit.global log  run st maxg
        | _ -> assert false
      in
      Printf.printf  " (%s)\n%!" (StringOf.env_rif st.config st.network);
      Printf.fprintf newdot "%s\n" (SimuState.to_dot st);
      Printf.printf "%s and %s have been generated\n" (st.sasarg.topo ^ ".log") newdot_fn;
      flush_all();
      close_out newdot;
      close_out log
  with
  | e ->
    Printf.printf "%s%s\n%!" (if st.sasarg.rif then "#" else "") (Printexc.to_string e);
    print_string "\nq\n#quit\n%!";
    flush_all();
    exit 2
