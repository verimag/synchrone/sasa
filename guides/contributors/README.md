- [Releases Notes](#orgc258f2f)
- [Compiling sasa sources](#orgf0202c6)
- [Parsing dot files](#orgac76d9a)
- [The Algo API](#org9a3aaee)
- [Demons](#orgb528205)
- [Dealing with Algorithm values](#org38fa2cc)
- [Processes](#org71c6dca)
- [CLI arguments (sasa options)](#org27ce8af)
- [The Main Simulation](#orgb196df4)
- [The sasa rdbg plugin](#org72bd883)
- [Generating Version Numbers](#org6deee97)
- [Pluging `sasa` on Synchronous tools](#orgf7dae34)
  - [RIF](#orga7e4647)
  - [The rdbg plugin](#orgc135b8f)

Self-stabilizing Algorithms SimulAtor (Simulation d'Algorithmes autoStAbilisants)

Contributors Guide


<a id="orgc258f2f"></a>

# Releases Notes

<https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/releases>


<a id="orgf0202c6"></a>

# Compiling sasa sources

You will need:

-   `make`
-   a set of tools installable via [opam](https://opam.ocaml.org/)
    -   `dune`
    -   `ocamlgraph`
    -   `lutin` (not for compiling actually, but for using sasa with custom demons)

cf the test job in the Gitlab CI script: [../../.gitlab-ci.yml](../../.gitlab-ci.yml) for a working installation process.


<a id="orgac76d9a"></a>

# Parsing dot files

The `dot` file parsing is based on the `ocamlgraph` library. It is done by the Topology module:

```ocaml
(* Time-stamp: <modified the 02/04/2019 (at 13:42) by Erwan Jahier> *)

type node_id = string
type node = {
  id: node_id; (* The id of the node as stated in the dot file *)
  file: string; (* the content of the algo field (a cxms file) *)
  init: (string * string) list; (* store the content of the init field *)
}

type edge = node_id * node_id list

type t = {
  nodes: node list;
  succ: node_id -> node_id list; 
  of_id: node_id -> node;
}

(** Parse a sasa dot file *)
val read: string -> t
```

-   [../../lib/sasacore/topology.mli](../../lib/sasacore/topology.mli)
-   [../../lib/sasacore/topology.ml](../../lib/sasacore/topology.ml)

This module is also used from `rdbg` ([../../test/rdbg-utils/dot.ml](../../test/rdbg-utils/dot.ml)) to display dynamic graphs, where local variable values, as well as enabled and triggered actions, are made visible at each step.


<a id="org9a3aaee"></a>

# The Algo API

Users only need to see the [../../lib/algo/algo.mli](../../lib/algo/algo.mli) file.

```ocaml
(* Time-stamp: <modified the 03/04/2019 (at 23:12) by Erwan Jahier> *)

(** Process programmer API *)
type varT = It | Ft | Bt | Et of int | St | Nt | At of varT * int
type vars = (string * varT) list 

type value = I of int | F of float | B of bool  | E of int | S of string
           | N of int  (* neighbor canal number *)
           | A of value array
type local_env = string -> value 
type action = string (* label *)

type neighbor = {
  lenv:  local_env;
  n_vars: vars;
  pid: unit -> string; (* this info is not available in all modes (e.g., anonymous) *)
  reply: unit -> int; (* ditto *)
}

type enable_fun = neighbor list -> local_env -> action list
type step_fun   = neighbor list -> local_env -> action -> local_env

(** Those 3 registering functions must be called! *)
type algo_id = string
val reg_vars : algo_id -> vars -> unit
val reg_enable : algo_id  -> enable_fun -> unit
val reg_step : algo_id  -> step_fun -> unit

(** raised by sasa if one of the function above is not called *)
exception Unregistred of string * string

(** This one is not mandatory.  The initialisation done in the dot
   file have priority over this one.  *)
val reg_init_vars : algo_id -> (neighbor list -> local_env) -> unit

(** Mandatory in custom mode only. *)
val reg_actions : algo_id -> action list -> unit

(** util(s) *)
val value_to_string : value -> string


(**/**)
(** functions below are not part of the API *)
val vart_to_rif_decl: varT -> string -> (string * string) list
val vart_to_rif_string: varT -> string -> string
val verbose_level: int ref


(** the following functions are used by sasa *)
val get_vars   : algo_id -> vars
val get_enable : algo_id -> enable_fun
val get_step   : algo_id -> step_fun 
val get_init_vars  : algo_id -> (string * varT) list -> (neighbor list -> local_env) 
val get_actions   : algo_id -> action list
```

-   [../../lib/algo/algo.mli](../../lib/algo/algo.mli)
-   [../../lib/algo/algo.ml](../../lib/algo/algo.ml)

Functions registration is based on the `Dynlink` module (in the ocaml standard library) and (Hash)tables.


<a id="orgb528205"></a>

# Demons

In the custom demon mode, the demon is executed outside `sasa`, by a process that communicates via the standard input/output. More precisely, `sasa` writes on `stdout` a Boolean for each action and each process, that states if the corresponding action is enabled for the current step. Then it reads on `stdin` a Boolean for each action and each process that tells which actions (among the enabled ones) should be activated.

The demon can thus be played by users that read and enter Boolean values. It can also by simulated by `lutin` programs via the use of `lurette` (for batch executions) or `rdbg` (for interactive sessions).

```ocaml
(* Time-stamp: <modified the 02/04/2019 (at 14:03) by Erwan Jahier> *)

type t =
  | Synchronous (* select all actions *) 
  | Central (* select 1 action *)
  | LocallyCentral (* never activates two neighbors actions in the same step *)
  | Distributed (* select at least one action *)
  | Custom (* enable/actions are communicated via stdin/stdout in RIF *)


type pna = Process.t * Algo.neighbor list * Algo.action

(** f dummy_input_flag verbose_mode demon pl actions_ll enab

inputs:
- dummy_input_flag: true when used with --ignore-first-inputs 
- verbose_mode: true when the verbose level is > 0
- demon: 
- pl:
- actions_ll: list of list of existing actions 
- enab_ll: list of list of enabled actions

    At the inner list level, exactly one action ougth to be chosen. At the
    outter list level, the number of chosen actions depends on the kind
    of demons.

    In custom mode, as a side-effect, read on stdin which actions should be activated.

returns:
   - a string containing the values (in RIF) of activating variables
   - the list of activated actions

nb: it is possible that we read on stdin that an action should be
   activated even if it is not enabled (which would be a demon
   "error").  For the time being, we ignore the demon "error" and
   inhibit the activation.
*)

val f : bool -> bool -> t -> Process.t list -> pna list list -> bool list list ->
  string * pna list
```

-   [../../lib/sasacore/demon.mli](../../lib/sasacore/demon.mli)
-   [../../lib/sasacore/demon.ml](../../lib/sasacore/demon.ml)

custom Lutin programs that mimick the built-in modes can be generated by:

```ocaml
(* Time-stamp: <modified the 01/04/2019 (at 17:21) by Erwan Jahier> *)

(** generated various Lutin demons (distributed, synchronous, etc.) *)
val f: Process.t list -> string
```

-   [../../lib/sasacore/genLutin.mli](../../lib/sasacore/genLutin.mli)
-   [../../lib/sasacore/genLutin.ml](../../lib/sasacore/genLutin.ml )


<a id="org38fa2cc"></a>

# Dealing with Algorithm values

```ocaml
(* Storing process variables values *)
type t

val init: unit -> t

(** [set env process_id var_name var_value] *)
val set: t -> string -> string -> Algo.value -> t

(** [get env process_id var_name] *)
val get: t -> string -> string -> Algo.value
```

The current implementation is based on

-   [map](https://caml.inria.fr/pub/docs/manual-ocaml/libref/Map.html) for the process level,
-   function for the variable level

[../../lib/sasacore/env.ml](../../lib/sasacore/env.ml)


<a id="org71c6dca"></a>

# Processes

```ocaml
(* Time-stamp: <modified the 30/04/2019 (at 16:02) by Erwan Jahier> *)

(** There is such a Process.t per node in the dot file. *)
type t = {
  pid : string; (* unique *)
  variables : Algo.vars;
  actions: Algo.action list;
  init : Algo.neighbor list -> Algo.local_env;
  enable : Algo.enable_fun;
  step : Algo.step_fun;
}

(** [make dynlink_flag custom_mode_flag node] builds a process out of a dot
   node. To do that, it retreives the registered functions by Dynamic
   linking of the cmxs file specified in the "algo" field of the dot
   node.

    dynlink_flag: link the algo.cmxs files (not possible from rdbg) 

    nb: it provides variable initial values if not done in the dot
   (via the init field) nor in the Algo (via Algo.reg_init_vars) *)
val make: bool -> bool -> Topology.node -> t
```

-   [../../lib/sasacore/process.mli](../../lib/sasacore/process.mli)
-   [../../lib/sasacore/process.ml](../../lib/sasacore/process.ml)


<a id="org27ce8af"></a>

# CLI arguments (sasa options)

-   [../../lib/sasacore/sasArg.mli](../../lib/sasacore/sasArg.mli)
-   [../../lib/sasacore/sasArg.ml](../../lib/sasacore/sasArg.ml)


<a id="orgb196df4"></a>

# The Main Simulation

It is splitted into 2 modules, so that the sasa rdbg plugin can reuse the step functions:

1.  [../../lib/sasacore/sasa.ml](../../lib/sasacore/sasa.ml)
2.  [../../src/sasaMain.ml](../../src/sasaMain.ml)


<a id="org72bd883"></a>

# The sasa rdbg plugin

In order to be able to use `sasa` from `rdbg`, we needed to implement an `rdbg` plugin. `rdbg` plugin must implement this interface:

```ocaml
(* Time-stamp: <modified the 05/04/2019 (at 11:23) by Erwan Jahier> *)

(* This module defines the data structure required by RDBG to run a Reactive Program. *)

type sl = ( string * Data .v) list (* substitutions *)
type e = Event .t
type t = {
  id: string;
  inputs  : (Data.ident * Data.t) list; (* name and type *)
  outputs : (Data.ident * Data.t) list; (* ditto *)
  reset: unit -> unit;
  kill: string -> unit;
  save_state: int -> unit;
  restore_state: int -> unit;
  init_inputs  : sl;
  init_outputs : sl;
  step     : (sl -> sl); (* Lurette step *) 
  step_dbg : (sl -> e -> ( sl -> e -> e) -> e); (* RDBG step *)
}

val dummy:t 
```

cf <https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/rdbg/blob/master/src/rdbgPlugin.mli>

This is done the SasaRun module:

```ocaml
(* Time-stamp: <modified the 02/04/2019 (at 15:50) by Erwan Jahier> *)

(* XXX finishme!

Actually, the I/O mode of rdbg already makes it possible to use sasa
   from rdbg and see all of its variable values after each step (exit
   event). This plugin is necessary when one need a finer-grained
   intrumentation, e.g., to stop at enable step, or to at each process
   call/exit.

*)
val make: string array -> RdbgPlugin.t
```

-   [../../lib/sasalib/sasaRun.mli](../../lib/sasalib/sasaRun.mli)
-   [../../lib/sasalib/sasaRun.ml](../../lib/sasalib/sasaRun.ml)

using [../../lib/sasacore/sasa.ml](../../lib/sasacore/sasa.ml)

Examples of use can be found in:

-   <../../test/rdbg-utils/dot.ml>
-   <../../test/my-rdbg-tuning.ml>


<a id="org6deee97"></a>

# Generating Version Numbers

The [../../lib/sasacore/sasaVersion.ml](../../lib/sasacore/sasaVersion.ml) is automatically generated

-   by [../../Makefile.version](../../Makefile.version)
-   using git tags that are automatically generated using the `release` job of the CI script, which is based on [semantic-release-gitlab](https://www.npmjs.com/package/semantic-release-gitlab)

In order to generate sensible version numbers, please follow in your commit messages the conventions described here: <https://www.npmjs.com/package/conventional-changelog-eslint>

This also permits to generate some Releases Notes: <https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/sasa/releases>


<a id="orgf7dae34"></a>

# Pluging `sasa` on Synchronous tools


<a id="orga7e4647"></a>

## RIF

<http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lustre-v6/#outline-container-sec-5>

```ocaml
(* Time-stamp: <modified the 13/03/2019 (at 17:45) by Erwan Jahier> *)

(** Reads on stdin a bool *)

val bool: bool -> Process.t -> string -> bool
```

[../../lib/sasacore/rifRead.mli](../../lib/sasacore/rifRead.mli) [../../lib/sasacore/rifRead.ml](../../lib/sasacore/rifRead.ml)


<a id="orgc135b8f"></a>

## The rdbg plugin

<https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/rdbg/blob/master/src/rdbgPlugin.mli> <https://gricad-gitlab.univ-grenoble-alpes.fr/verimag/synchrone/lutils/blob/master/src/data.mli>

<../../test/rdbg-utils/dot.ml>

<../../test/my-rdbg-tuning.ml>
