
;;; lutin.el  -  Major mode for editing lustre source in Emacs

;;; lutin-mode.el --- Major mode for the Lutin language

;;; Code:

(defvar lutin-mode-hook nil)

(defvar  lutin-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "|" 'tuareg-electric)
    map)
   "Keymap for `lutin-mode'.")



;;; Définir les groupes de mots-clés du langage Lutin
(defconst lutin-keywords-control
  '("loop" "fby" "raise" "try" "trap" "catch" "do")
  "Mots-clés de contrôle (loop, fby, raise, etc.).")

(defconst lutin-keywords-decl
  '("let" "const" "extern" "node" "run" "include" "returns" "type")
  "Mots-clés liés à la déclaration (const, extern, node, etc.).")

(defconst lutin-keywords-scope
  '("assert" "exist" "in" "if" "then" "else")
  "Mots-clés conditionnels et d'affectation (let, assert, if, etc.).")

(defconst lutin-keywords-logic-truth
  '("true" "and" "or" "not" "false")
  "Mots-clés logiques (true, false, and, or, etc.).")

(defconst lutin-keywords-types
  '("trace" "bool" "int" "ref" "real")
  "Types (trace, bool, int, etc.).")

;;; Définir la coloration syntaxique
(defconst lutin-font-lock-keywords
  (let* ((control-do-regexp (regexp-opt lutin-keywords-control 'words))
         (decl-regexp (regexp-opt lutin-keywords-decl 'words))
         (scope-regexp (regexp-opt lutin-keywords-scope 'words))
         (logic-truth-regexp (regexp-opt lutin-keywords-logic-truth 'words))
         (types-regexp (regexp-opt lutin-keywords-types 'words)))
    `((,control-do-regexp . font-lock-keyword-face)
      (,decl-regexp . font-lock-constant-face)
      (,scope-regexp . font-lock-variable-name-face)
      (,logic-truth-regexp . font-lock-builtin-face)                                ;; Couleur pour les mots-clés logiques
      (,types-regexp . font-lock-type-face)))                                        ;; Couleur pour les types
  "Définition des expressions régulières pour la coloration syntaxique de Lutin.")


(defvar lutin-mode-syntax-table (make-syntax-table)
  "Syntax table used in the Lutin mode.")

					; `_' is also in words.
(modify-syntax-entry ?\_	"w"	lutin-mode-syntax-table)
				; comment line finisher:
(modify-syntax-entry ?\n	">"	lutin-mode-syntax-table)
				; comment start if double,
				; punct. otherwise.
(modify-syntax-entry ?\-	". 12" 	lutin-mode-syntax-table)

;; multiline comments (is this the right syntax?):
				; beg of multiline comment starter
(modify-syntax-entry ?\(	"()1b"	lutin-mode-syntax-table)
					; beg of multiline comment finisher
(modify-syntax-entry ?\)	")(4b"	lutin-mode-syntax-table)
					; end & beg of multiline comment starter
					; & finisher.
(modify-syntax-entry ?\*	". 23b"	lutin-mode-syntax-table)


;;; Définir le mode majeur Lutin
(define-derived-mode lutin-mode prog-mode "lutin"
  "Major mode for editing Lutin code."
  :syntax-table nil
  (setq font-lock-defaults '(lutin-font-lock-keywords))
  (use-local-map lutin-mode-map)
  (setq-local comment-start "--")
  (setq-local comment-end "")
  )

;;; Associating lutin-mode with .lutin files
(add-to-list 'auto-mode-alist '("\\.lut\\'" . lutin-mode))

;;; Fournir le mode
(provide 'lutin-mode)
; (require 'lutin-mode)


; ;;; lutin .el ends here...
