
;;; rif.el  -  Major mode for editing lustre source in Emacs

;;; rif-mode.el --- Major mode for the Rif language

;;; Code:

(defvar rif-mode-hook nil)

(defvar  rif-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "|" 'tuareg-electric)
    map)
   "Keymap for `rif-mode'.")



;;; Définir les groupes de mots-clés du langage Rif
(defconst rif-keywords-pragma
  '("inputs" "outputs" "in" "outs" "step" "locs")
  "Pragmas")

(defconst rif-keywords-logic
  '("true" "t" "f" "FALSE" "TRUE" "T" "F" "0" "1" "false")
  "bool values")

(defconst rif-keywords-types
  '("bool" "int" "real")
  "Types")

;;; Définir la coloration syntaxique
(defconst rif-font-lock-keywords
  (let* ((pragma-do-regexp (regexp-opt rif-keywords-pragma 'words))
         (logic-regexp (regexp-opt rif-keywords-logic 'words))
         (types-regexp (regexp-opt rif-keywords-types 'words)))
    `((,pragma-do-regexp . font-lock-keyword-face)
      (,logic-regexp . font-lock-builtin-face)
      (,types-regexp . font-lock-type-face)))
  "Définition des expressions régulières pour la coloration syntaxique de Rif.")


(defvar rif-mode-syntax-table (make-syntax-table)
  "Syntax table used in the Rif mode.")

					; `_' is also in words.
(modify-syntax-entry ?\_	"w"	rif-mode-syntax-table)
				; comment line finisher:
(modify-syntax-entry ?\n	">"	rif-mode-syntax-table)
				; comment start if double,
				; punct. otherwise.
(modify-syntax-entry ?\-	". 12" 	rif-mode-syntax-table)

;; multiline comments (is this the right syntax?):
				; beg of multiline comment starter
(modify-syntax-entry ?\(	"()1b"	rif-mode-syntax-table)
					; beg of multiline comment finisher
(modify-syntax-entry ?\)	")(4b"	rif-mode-syntax-table)
					; end & beg of multiline comment starter
					; & finisher.
(modify-syntax-entry ?\*	". 23b"	rif-mode-syntax-table)


;;; Définir le mode majeur Rif
(define-derived-mode rif-mode prog-mode "rif"
  "Major mode for editing Rif code."
  :syntax-table nil
  (setq font-lock-defaults '(rif-font-lock-keywords))
  (use-local-map rif-mode-map)
  (setq-local comment-start "# ")
  (setq-local comment-end "")
  )

;;; Associating rif-mode with .rif files
(add-to-list 'auto-mode-alist '("\\.lut\\'" . rif-mode))

;;; Fournir le mode
(provide 'rif-mode)
; (require 'rif-mode)


; ;;; rif.el ends here...
