

build: lib/sasacore/sasaVersion.ml salut/src/version.ml
	dune build  @install @runtest

install:
	dune install

reinstall:install

uninstall:
	dune uninstall


-include Makefile.sasa
include Makefile.version
