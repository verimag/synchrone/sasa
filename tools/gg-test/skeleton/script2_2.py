#!/usr/bin/env python3

import sys
sys.path.insert(0, '../..')
from scriptEnv import *
from statistics import stdev
proj = Project("proj_s2", True, False)

f = proj.open("log", "log", "")
compileAlgos(["p.ml"])

step_nb = []
for x in range(1000):
	
	path = proj.add_file("dots", "tree", ".dot")
	call("gg tree -n 20 -o "+path, verb = 1)
	call('gg-deco 0-:p.ml '+path+" -o "+path, verb = 1)
	rif = call('sasa -rif '+path, verb = 1)
	
	proj.add_file("rifs", "tree", ".rif", content = rif)
	(_,vals) = parseRif(rif)
	step_nb.append(len(vals))
	sd = None
	if x > 1:sd = stdev(step_nb)
	print(x,"-",sd)
	f.write(str(len(vals)) + " -- " + str(sd) + "\n")

print(sum(step_nb)/1000)