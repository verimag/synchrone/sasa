#!/usr/bin/env python3

import sys
sys.path.insert(0, "../..")
from scriptEnv import *

genGraph("ring", "ring.dot", {"n":20}, True)
#call("gg ring -n 20 -o ring.dot")

decoGraph("ring.dot", "0-:p.ml", comp = True)
#call('gg-deco 0-:p.ml ring.dot -o ring.dot', verb = 1)
#compileAlgos(["p.ml"])

(_,vals) = parseRif(callSasa("ring.dot"))
#(_,vals) = parseRif(call("sasa -rif ring.dot"))

print(len(vals))