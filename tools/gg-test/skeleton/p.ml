(* Time-stamp: <modified the 27/05/2019 (at 16:52) by Erwan Jahier> *)

(* a dumb algo *)

open Algo

type memory = int

let (init_state: int -> memory) = 
  fun _i -> Random.int 10

(*let (state_to_string: memory -> string) = 
  fun m -> 
    string_of_int m*)
  

let (copy_state : memory -> memory) = 
  fun m -> m

let (enable_f:'v neighbor list -> 'v -> action list) =
  fun nl e -> 
    match (List.hd nl).state with
    | 0 ->  if e <> 1 then ["action2"] else []
    | 1 ->  []
    | _ ->  if e <> 0 then ["action1"] else []

let (step_f : 'v neighbor list -> 'v -> action -> 'v) =
  fun nl e ->
    function
    | "action1" -> 0
    | "action2" -> 1
    | _ -> e


let () =
  Algo.register {
    algo = [
      {
        algo_id = "p";
        init_state = init_state;
        actions = Some ["action1"; "action2"];
        enab = enable_f;
        step = step_f;         
      }
    ];
    state_to_string = string_of_int;
    state_of_string = Some int_of_string;
    copy_state = copy_state;
  }
