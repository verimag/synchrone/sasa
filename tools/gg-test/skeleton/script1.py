#!/usr/bin/env python3

import sys
sys.path.insert(0, "../..")
from scriptEnv import *

myProj = Project()
compileAlgos(["p.ml"])
totSteps = 0

for _ in range(10):
	path = myProj.add_file("dots", "ring", ".dot")
	genGraph("ring", path, {"n":20}, True)

	decoGraph(path, "0-:p.ml", comp = False)

	(_,vals) = parseRif(callSasa(path))

	totSteps += len(vals)

print(totSteps, totSteps/10)