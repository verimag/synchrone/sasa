import subprocess, os, datetime, math

def call(command, log = None, verb = 2, options = {}):
	"""Calls the shell command given in argument, and give its output in stdout as return value
	
	If the command fails (command return value other than 0), the command's stdout is printed, and 
	the error 'subprocess.CalledProcessError' is raised. 

	Optional Args : 
	    log (filename): a log file, in which the command's stdout will be appended
	    verb (int): the verbose level. It has three levels : 0,1 or 2. 
	        On level 0, nothing will be printed excepted the raised error
	        On level 1, if there's an error, the command's stdout will be printed.
	        On level 2 (default), a message will tell when the command is called, 
	        and when it has finished.
	        On level 3, the command's stdout will always be printed
	    options (dict): a way to add options in the command line. Each key of the dict will 
	        be added to the command line with an additional "-" in front, followed by the 
	        corresponding value in the dict. For example, {"n":3, "o":"toto.ml"} will add the 
	        string '-n "3" -o "toto.ml"' to the command line.

	Return (str) : the output of the command

	Note : the module subprocess is used to call the command

	"""

	if verb > 1 : 
		print("Calling '"+command.split()[0]+"' ...")

	for k in options:
		if options[k] == "" :
			command += " -"+k
		else :
			command += " -"+k+' "'+str(options[k])+'"'

	try:
	    output = subprocess.check_output(
	        command, shell=True, stderr=subprocess.STDOUT, encoding = "utf-8")
	except subprocess.CalledProcessError as exc:
		if log != None:
			f = open(log, "a+")
			f.write(exc.output)
		if verb > 2 : 
			print("------ output : ------")
		if verb > 0 : 
			print(exc.output)
		if verb > 2 : 
			print("----------------------")
		raise

	if log != None:
		f = open(log, "a+")
		f.write(output)
	if verb > 2 : 
		print("------ output : ------")
		print(output)
		print("----------------------")
	if verb > 1 : 
		print("Done.")
	return output

def genGraph(graphType, outputFile, options, is_silent = False, log = None, verb = 2):
	"""Generate a graph, using the 'call' function on the command 'gg'.
	
	It also removes any '.lut' file at the name of the outputFile

	Args:
	    graphType (str): The first argument given to 'gg', corresponding to the command in 'gg <command>'.
	                     Use 'gg -h' in console to view the available commands (corresponding to available graphTypes).
	    outputFile (str): The output file given to 'gg' with the option '-o'. 
	                      We strongly advice to give it a '.dot' extension
	    graphProperties (dict): options given to 'call' function.
	    is_silent (bool, optional): Indicates whether the option '-s' will be added to the command line or not.
	    log (str) : the log file given to 'call'
	    verb (int) : the verbose level given to 'call'

	Return (str): the output of the 'gg' command

	"""

	args = "gg " + graphType + ' -o "' + outputFile + '"'
	
	if is_silent:
		args += "-s"
	
	ret = call(args, log, verb, options)
	
	try :
		os.remove(outputFile[0:-3]+"lut")
	except FileNotFoundError as e:
		pass
	return ret 	

def compileAlgos(fileList):
	"""Compile an iterable object (like lists or sets) of .ml files into .cmxs files, 
	or compile the .ml files of a string in the algo-files syntax supported by gg-deco."""
	if type(fileList) == str : 
		tmp = fileList
		fileList = []
		for s in tmp.split():
			fileList.append(s.split(":")[1])
	ret = []
	for file in fileList:
		ret.append(subprocess.call(["ocamlfind", "ocamlopt", "-shared", "-package", "algo", file, "-o", file[0:-3]+".cmxs"]))
	return ret

def decoGraph(inputFile: str, decoration, outputFile:str = "", comp = True):
	"""Decorate a graph, using the 'call' function on the command 'gg-deco' 
	    
	Args : 
	    inputFile (str) : the DOT file to decorate
	    decoration : a string, list or a dictionary describing the files of each node.
	                 If it is a string, it will be given as it is to gg-deco
	    outputFile (str, optional) : The name for the decorated DOT file. 
	                                 Same as the inputFile by default.
	    comp (bool, optional) : If True, runs 'compileAlgo' on decoration

	Return : A list of the return values of each file

	"""

	if (outputFile == "") : 
		outputFile = inputFile
	spec = []
	files = []

	if type(decoration) == dict:
		for key in decoration:
			spec.append(str(key) + ":" + decoration[key])
			files.append(decoration[key])
	elif type(decoration) == str:
		files = decoration
		spec = decoration.split()
	else : # type list
		spec = decoration
		for s in decoration:
			s = s.split(":")
			files.append([1])

	ret = subprocess.call(["gg-deco"] + spec + [inputFile, "-o", outputFile])
	if comp :
		ret = (ret, compileAlgos(files))
	return ret

def callSasa(topologyFile, length = 200, seed = None, daemon = "dd", rif = True):
	"""Calls sasa on the specified topologyFile (a decorated DOT file)

	Needs to have the compiled versions of all .ml files used in the topology file
	in the same directory. May create a .lut file if the daemon "custd" is used.

 	Args : 
 	    topologyFile (string) : the DOT file on which sasa is executed
 	    length (int, optional) : the maximum number of steps
 	    seed (int, optional) : the pseudo-random number generator seed. Random by default
 	    daemon (string, optional) : Indicates the daemon to use. List of possible daemons below
	    rif (bool, optional) : if True, indicates to sasa to output in RIF syntax
	
	Return : the output of sasa (string)
	
	Possible daemons :
		"sd": Synchronous demon (selects as much actions as possible, up to one action per node)
		"cd": Central demon (selects exactly one action)
		"lcd" : Locally Central demon (never activates two neighbors' actions in the same step)
	    "dd" : Distributed demon (select at least one action, randomly)

	"""

	commandLine = ["sasa", "-l", str(length), topologyFile]
	
	if seed != None:
		commandLine.append("-seed")
		commandLine.append(str(seed))
	
	if rif: commandLine.append("-rif")
	
	commandLine.append("-"+daemon)
	
	#if (daemon == "custd") and os.path.isfile(topologyFile[0:-3]+"lut") :
	#	subprocess.call(["sasa", "-gld", topologyFile])

	return subprocess.check_output(commandLine, stderr=subprocess.STDOUT, encoding = "utf-8")

def parseRif(rif):
	""" Parse a rif document made by a sasa call (from sasa's stdout or output file) 

	Return : a tuple of two elements :
	    - A list of the names of the variable
	    
	    - A list containing the state of each step. 
	      (A state is the list of the values of the variables.)
	"""

	"""
	Example (not in the docstring) : 
	    >>> rif = '\n#outputs "p0_x":int "p1_x":int "Enab_p0_action":bool "Enab_p1_action":bool '
	    >>> rif += '"p0_action":bool "p1_action":bool\n\n#step 1\n #outs 4 2 t t t t\n\n#step 2\n '
	    >>> rif += '#outs 0 0 f t f t\n\n#step 3\n #outs 0 1 f f f f\n\n#quit\n' 
		>>> (names, states) = parseRif(rif)
		>>> names
		['p0_x', 'p1_x', 'Enab_p0_action', 'Enab_p1_action', 'p0_action', 'p1_action']
		>>> states
		[[4, 2, True, True, True, True], [0, 0, False, True, False, True], [0, 1, False, False, False, False]]

	"""

	rif = list(filter(None, rif.split("\n")))
	names = []
	types = []
	states = []
	possibleTypes = {"int":int,"real":float,"bool":bool}
	for line in rif:
		newStateLine = []
		line = [x.split() for x in line.split("#", 1)]
		
		for v in line[0]:
			newStateLine.append(v)
		pragma = line[1][0]
		if pragma in ["inputs", "outputs"]:
			for n in line[1][1:]: 
				n = n.split(":")
				names.append(n[0][1:-1])
				types.append(possibleTypes[n[1]])

		if pragma == "outs":
			for v in line[1][1:]: 
				newStateLine.append(v)

		tmp = []
		for v, t  in zip(newStateLine, types):
			if t != bool:
				tmp.append(t(v))
			else :
				tmp.append(v in ["t","T","1"])
			
		if tmp != []:
			states.append(tmp)

	return (names,states)

def column(i, matrix, names = None):
	"""Outputs a list with the elements of a column in the matrix.
	
	If names is not specified, outputs the column i of the matrix
	If names is specified, outputs the column at the index of i in names.
	(useful to get the states of a variable from parseRif though all the steps)

	"""
	
	if names != None : i = names.index(i)
	return [x[i] for x in matrix]

class FileManager:
	"""Manage files of a project, to keep traces of a test battery.
	Inside a project directory, there will be a directory for each version 
	(i.e. for each creation of a Project object). 

	Proprieties : 
	    path (str) : the path to the current version
	"""

	def __init__(self, projectName = "sasaProject", version_name = "", date = True, time = True, add_index = True):
		"""Initialize a version of the project. 

		Args :
		    projectName (str) : The name of the project, in which a new version will be created.
		    version_name (str, optional) : indicates the version's name base name
		    date (bool, optional) : indicates if the version's name will contain the date after the base name
		    time (bool, optional) : indicates if the version's name will contain the time after the date or base name
		    add_index (bool, optional) : indicates if the version's name will contain an index if a version of the same 
		                                 name already exists (to ensure not to mix an old version with the new one).

		"""

		self.name = projectName # the name of the project
		version = version_name
		now = str(datetime.datetime.now()).split()
		if version_name == "" and date:
			version += "_"
		if date:
			version += now[0]
		if date and time:
			version += "_"
		if time:
			version += now[1].split(".")[0]

		try:
			os.makedirs(projectName)
		except FileExistsError:
			pass
		
		if os.path.exists(projectName+"/"+version) and add_index :
			i = 0
			while os.path.exists(projectName+"/"+version+"_"+str(i)):i += 1
			version += "_"+str(i)

		try :
			os.makedirs(projectName+"/"+version)
		except FileExistsError:
			if add_index : raise

		self.version = version # the name of the version
		self.path = projectName+"/"+version+"/" # the path to the version

	def createDir (self, dir):
		"""Creates the directory dir in the current version if it doesn't exist. 

		Return value (bool) : True if the directory was created, False if it already existed

		Note : this method is principally created to be used by other methods in the object.

		"""

		try:
			os.makedirs(self.path + dir)
			return True
		except FileExistsError:
			return False

	def open(self, dir, name, ext, openType = 'w+', add_index = True):
		"""Use the built-in function open() to open a file in the directory 'dir' of the current version.

		The actual name of the file is 'name_i.ext', with name and ext being the arguments of the same name,
		and i being the lower number such as no other file has the same name.

		Args :
		    dir (str) : the name of the directory in which the file will be created. Creates it if it doesn't exists.
		    name (str) : the name of the file, without extension. 
		    ext (str) : the extension of the file. If it doesn't start by a point ('.'), it will be added.
		    openType (str, optional) : the second argument given to the built-in function open(). 
		                               Note that if add_index is set to False and the file already exists, 
		                               'w'/'w+' will overwrite the file, whereas 'a'/'a+' and 'r' will not.
		    add_index (bool, optional) : indicates if an index will be given to the file's name. 
		                                 Set it to True if you want to ensure that the opened file is a new file.


		Return value :
		    The opened file (same as "open(path)")

		"""

		self.createDir(dir)
		i = 0
		path = self.path + dir + "/" + name
		if ext != "" and ext[0] != ".": ext = "."+ext

		while os.path.exists(path + "_" + str(i) + ext):i += 1
		path += "_"+str(i)+ ext

		f = open(path, 'w+')

		return f

	def add_file(self, dir, name, ext, content = "", add_index = True):
		"""Creates a new file in the directory dir of the current version.

		The actual name of the file is 'name_i.ext', with name and ext being the arguments of the same name,
		and i being the lower number such as no other file has the same name.

		Args :
		    dir (str) : the name of the directory in which the file will be created. Creates it if it doesn't exists.
		    name (str) : the name of the file, without extension. 
		    ext (str) : the extension of the file.
		    content (str, optional) : a string that will be added automatically to the file on creation.
		    add_index (bool, optional) : indicates if an index will be given to the file's name. 
		                                 Set it to True if you want to ensure that no file of the same name 
		                                 is overwritten on creation.

		Return value :
		    The path to the created file

		"""
		self.createDir(dir)
		i = 0
		path = self.path + dir + "/" + name
		#if ext != "" and ext[0] != ".": ext = "."+ext

		while os.path.exists(path + "_" + str(i) + ext):i += 1
		path += "_"+str(i)+ ext

		f = open(path, 'w+')

		f.write(content)
		f.close()

		return path

	def open_i(self, i, dir, name, ext, openType = 'a'):
		"""Opens a file in dir with the name 'name_i.ext'. 
		(The point between 'i' and 'ext' is for readability, and is not added by the method.)

		Note : Use it to modify a file created previously, not to create a file at index i 
		unless you are sure of what you are doing

		Args :
		    dir (str) : the name of the directory in which the file will be created. Creates it if it doesn't exists.
		    name (str) : the name of the file, without extension. 
		    ext (str) : the extension of the file. If it doesn't start by a point ('.'), it will be added.

		"""

		self.createDir(dir)
		return open(self.path + dir + "/" + name + "_" + str(i) + ext, openType)

	def read_i(self, i, dir, name, ext):
		f = open_i(self, i, dir, name, ext, "r")
		return f.read()

	def exist_i(self, i, dir, name, ext):
		"""Checks if the file 'name_i.ext' exists in dir.
		(The point between 'i' and 'ext' is for readability, and is not added by the method.)"""

		return os.path.exists(self.path + dir + "/" + name + "_" + str(i) + ext)

	def get_last(self, dir, name, ext):
		"""Outputs the index just before the first free index. 

		The purpose of this method is to give the last used index, but it might be wrong if 
		files are deleted or created without using this object, or if a file is created using open_i.

		Args :
		    dir (str) : the name of the directory in which the file will be created. Creates it if it doesn't exists.
		    name (str) : the name of the file, without extension. 
		    ext (str) : the extension of the file. If it doesn't start by a point ('.'), it will be added.
	
		"""
		
		i = 0
		while os.path.exists(self.path + dir + "/" + name + "_" + str(i) + ext): i += 1
		return i - 1

class UDGtools(object):
	"""A set of tools to use UDG"""
	@property # so that UDGtools() gives an error
	def __init__(self):pass
	
	@staticmethod
	def getRadius(n, h, w, md):
		math.sqrt(h * w * md / (math.pi * n))
