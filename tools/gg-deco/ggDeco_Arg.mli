type file = string
type files_spec_t = (int * int * file)

type t = {
  mutable dot_file: string;
  mutable output: string;

  mutable files_spec : files_spec_t list;

  mutable _args : (string * Arg.spec * string) list;
  mutable _general_man : (string * string list) list; 

  mutable _others : string list;
  mutable _margin : int;
}

val parse : string array -> t
