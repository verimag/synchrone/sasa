let () = Random.self_init ();

type file = string
type files_spec_t = (int * int * file)
exception Invalid_file_spec of string*string

type t = {
  mutable dot_file: string;
  mutable output: string;

  mutable files_spec : files_spec_t list;

  mutable _args : (string * Arg.spec * string) list;
  mutable _general_man : (string * string list) list; 

  mutable _others : string list;
  mutable _margin : int;
}

let usage_msg tool = 
  ("usage: " ^ tool ^ " \"<decoration pattern>\" <input-file.dot> [<option>]*
decorates an <input-file.dot> using a <decoration pattern>. 
" )

let print_usage tool = Printf.printf "\n%s use -h for additional information.\n\n" (usage_msg tool); flush stdout; exit 1


let (make_args : unit -> t) = 
  fun () -> 
    {
      dot_file = "";
      output = "";

      files_spec = [];

      _args = [];
      _general_man = [];

      _others = [];
      _margin = 12;
    }

let first_line b = (
  try (
    let f = String.index b '\n' in
    String.sub b 0 f
  ) with Not_found -> b
)
let exist_file f = (
  if not (Sys.file_exists f) then (
    prerr_string ("File not found: \""^f^"\"");
    prerr_newline ();
    exit 1
  )
)
let unexpected s = (
  prerr_string ("unexpected argument \""^s^"\"");
  prerr_newline ();
  exit 1
)


let printSpec args outChannel (c, messageList) = (
  let (m1, oth) = match messageList with
   |  h::t -> (h,t)
   |  _ -> ("",[])
  in
  let t2 = String.make args._margin ' ' in
  let cl = String.length c in
  let t1 = if (cl < args._margin ) then
   String.make (args._margin - cl) ' '
  else
   "\n"^t2
  in
   Printf.fprintf outChannel "   %s%s%s" c t1 m1;
   List.iter (function x -> Printf.fprintf outChannel "\n%s%s" t2 x) oth ;
   Printf.fprintf outChannel "\n" ;
)

let help args tool = (
  Printf.printf "%s" (usage_msg tool);
  Printf.printf "
The <decoration pattern> specifies which algos should be attached to which nodes.
Its syntax (in EBNF) is:
   <decoration pattern> ::= <deco><more>
   <deco> ::= 
    | <int>:<string>
    | <int>-<int>:<string>
    | <int>-:<string>
   <more> ::= ([ \\t]*<deco>)*

  \"i:file\"   attaches <file> to node number <i> (the first node is numbered 0)
  \"i-j:file\" attaches <file> to nodes number <i>, <i>+1, ..., <j>
  \"i-:file\"  attaches <file> from node number <i> untill the end

decorations are handled from left to rigth (i.e., the rigth-most one wins).

Example: the <decoration pattern> \"0:root.ml 1-:p.ml\" assigns 
  - \"root.ml\" to the first node, and 
  - \"p.ml\" to the other ones.
 
<option> can be:
";
  List.iter (printSpec args stdout) (List.rev args._general_man);
  exit 0
)


let mkopt : t -> string list -> ?arg:string -> Arg.spec -> (string list -> unit) =
  fun opt ol ?(arg="") se ml ->
    let add_option o = opt._args <- (o, se, "")::opt._args in
     List.iter add_option ol ;
     let col1 = (String.concat ", " ol)^arg in
     opt._general_man <- (col1, ml)::opt._general_man

(*** User Options Tab **)
let (mkoptab : string array -> t -> unit) = 
  fun argv args -> (
    mkopt args ["--output";"-o"] ~arg:" <file>"
      (Arg.String (fun s -> args.output <- s))
      ["Redirect stdout into a <file>"];

    mkopt args ["--help";"-h"]
      (Arg.Unit (fun () -> help args argv.(0) ))
      ["Print this help\n"];
  )

(* all unrecognized options are accumulated *)
let (add_other : t -> string -> unit) =
  fun opt s -> 
    opt._others <- s::opt._others

let current = ref 0;;

let parse_file_spec : (string -> files_spec_t) = 
  fun patt ->
  try
    Scanf.sscanf patt "%[-0-9]:%s"
      (fun range file -> 
         if range = "" then
           raise (
             Invalid_file_spec (patt, "The first and last node's indexes are missing")) 
         else
           Scanf.sscanf range "%d%s"
             (fun a s -> 
                if (a < 0) then
                  raise (Invalid_file_spec
                           (patt,"The first node's index have to be positive or null"))
                else
                if (s = "") then (a,a,file) else
                if (s = "-") then (a,-1,file) else
                  Scanf.sscanf s "-%d"
                    (fun b -> 
                       if (b < a) then 
                         raise
                           (Invalid_file_spec
                              (patt,
                               "The last node's index have to be higher than the first node's index")) 
                       else 
                         (a,b,file)
                    )
             )
      )
  with 
  | Scanf.Scan_failure _ -> 
    raise (
      Invalid_file_spec (
        patt, 
        "The boundaries (first and last node's indexes) should be integers, but an non-numerical character has been found"))
    
let parse argv = (
  let save_current = !current in
  let args = make_args () in
  mkoptab argv args;
  try (
    Arg.parse_argv ~current:current argv args._args (add_other args)
      (usage_msg argv.(0));
    current := save_current;

    (*  Same  as List.rev,  but  also  check  if there's  no  option
        (starting by '-') in these arguments *)
    let others =
      List.fold_left
        (fun l o -> if String.get o 0 = '-' then unexpected o else o::l)
        [] args._others
    in
    (match others with
     | [deco_patt;ifile] -> (
         let deco_patt_list = Str.split (Str.regexp "[ \t]+") deco_patt in
         let fl = try List.map parse_file_spec (List.rev deco_patt_list)
           with Invalid_file_spec (fs,s) -> (
               Printf.fprintf stderr
                 "Error while parsing the file specification \"%s\" :\n" fs; 
               Printf.fprintf stderr "%s\n"s; 
               flush stderr; print_usage (argv.(0))
             )
         in
         args.files_spec <- fl;
         exist_file ifile;
         args.dot_file <- ifile
       )
     | _ -> 
       (Printf.fprintf stderr "Error: you need 2 arguments to use %s\n" (argv.(0));
        flush stderr;
        print_usage (argv.(0)))
    );
    args
  )
  with
  | Arg.Bad msg -> (
      Printf.fprintf stderr "*** Error when calling '%s': %s\n%s\n" (argv.(0))
        (first_line msg) (usage_msg argv.(0)); exit 2
    )
  | Arg.Help _ -> (
      help args argv.(0)
    )
)
