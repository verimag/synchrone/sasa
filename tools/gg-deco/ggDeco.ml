open GgDeco_Arg
open Sasacore
open Topology



(* Parse strings into a file_spec list.  Each file_spec element should
   be represented in the format  "a-b:file", with "a" being the start,
   "b" being the  end, and "file" being the file.   To create multiple
   file_spec  elements from  one string,  separate each  element by  a
   whitespace.  If the  syntax is not respected, an  exception will be
   raised.

Possible   exceptions    :   End_of_file,   Stdlib.Scanf.Scan_failure,
   int_of_string

Caution : Crossover exceptions are not raised in this function *)

let rec apply_pattern : (files_spec_t list -> int -> string) = 
  fun fl i -> 
  match fl with
  | [] -> "nopattern"
  | (a,b,s)::tl ->
    if i < a || (i > b && b <> -1) then
      (* ignore this pattern and try one in tl *)
      apply_pattern tl i
    else
      s

let deco : (Topology.t -> files_spec_t list -> Topology.t) = 
  fun g fl ->
  List.iter (fun (i,j,file) -> Printf.eprintf "deco pattern: %i-%i:%s\n%!" i j file) fl;
  let newNodes =
    List.mapi
      (fun i n -> 
         let s = apply_pattern fl i in
         { n with file= if s = "nopattern" then n.file else s  }
      )
      g.nodes
  in
  { g with nodes = newNodes }

    
(**********************************************************)
    
(* XXX duplicated from GraphGen !! *)
let to_dot_string  : (t -> string -> string) =
  fun g name ->
  let attr =
    let attrs =
      List.map (fun (an,av) -> Printf.sprintf "%s=%s" an av) g.Topology.attributes
    in 
    Printf.sprintf "graph [%s]" (String.concat "\n\t" attrs)
  in
  let node_to_node_string n =
    Printf.sprintf "  %s [algo=\"%s\"]\n" n.id n.file
  in
  let nodes = String.concat "" (List.map node_to_node_string g.Topology.nodes) in
  let node_to_link_string n =
    let pred = g.pred n.id in
    let link_kind = if g.directed then "->" else "--" in
    let links =
      List.map
        (fun (w,neighbour) ->
          (match w with
           | 1 ->
              if n.id < neighbour || g.directed then
                Printf.sprintf ("  %s %s %s") neighbour link_kind n.id
              else
                Printf.sprintf ("  %s %s %s") n.id link_kind neighbour
           | x ->
              Printf.sprintf ("  %s %s %s [weight=%d]") neighbour link_kind n.id x
          )
        )
        pred
    in
    links
  in
  let links = List.map node_to_link_string g.nodes in
  let links = List.sort_uniq compare (List.flatten links) in 
  let links = String.concat "\n" links in
  Printf.sprintf "%s %s {\n%s\n%s\n%s\n}\n" (if g.directed then "digraph" else "graph")
    name attr nodes links 

                  
let make_dot : (t -> string -> unit) =
  (*Create a dot file from a graph*)
  fun t file_name ->
  let name = ref "graph0" in
  let oc = if file_name = "" then stdout
          else (
            name := Filename.basename file_name;
            name:= Str.global_replace (Str.regexp "[-,]") "" !name ;
            (try ( (* remove all extensions. So if name = ref "tt.dot.dot" 
                      at the beginning, at the end name = ref "tt". *)
               while true do
                 name := Filename.chop_extension !name;
               done;
             ) with Invalid_argument _ -> ());
            open_out file_name
          )
  in
  let dot = to_dot_string t !name in
  Printf.fprintf oc "%s\n" dot
    (*ignore (Sys.command (Printf.sprintf "echo \"%s\" > \"%s.dot\"" dot file_name)); ()*)
(**********************************************************)

let () = (
    let args = parse Sys.argv in 
    let g = read args.dot_file in
      let new_g = deco g args.files_spec in 
      make_dot new_g args.output 
)
