#! /usr/bin/env Rscript

# usage distrib.r datafile int str str str 

args <- commandArgs(TRUE)
if(length(args) < 5)
{
  stop("Not enough arguments. Please supply 5 arguments: f.data mean str1 str2 str3")
}


datafilename <- args[1]    
mean <- as.integer(args[2])

mydata  <- data.frame(val=read.table(datafilename))

attach(mydata)

pdffilename = paste(datafilename,".pdf", sep ="")
pdf(pdffilename)

# require "apt-get install r-cran-ggplot2"
library(ggplot2)


ggplot(data=mydata, aes(x=V1)) +     
  geom_histogram(aes(y = ..density..), fill="lightblue") +
  geom_density(alpha = 0.1, fill = "red")+
  geom_vline(aes(xintercept=vals),
     xintercept=mean,
     linetype=2,
     size=1,
     color="red"
     ) +
    labs(x=paste(args[3],"Counts",sep=" "),y=paste(args[3],"Counts Frequency",sep=" ")) +
    ggtitle(paste("|sample|=",args[5],"   ",args[4],sep=""))
   


summary(mydata)


data2=table(mydata)
# data3=sort(data2)
print(data2)



