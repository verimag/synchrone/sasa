#use "parseLogUtils.ml";;
(*  [parse_log algo_labels  graphs  daemons] generates  as many  .data
   files as there are graphs  i.e., if graphs=["g1"; "g2"; etc.], then
   this function  generates "g1.data",  "g2.data", etc.  If  the .data
   files  do  not  exist,  there are  created.   Otherwise,  data  are
   appended at the end.  *)
let parse_log
    (algo_labels:(string * string) list) (* (algo label, algo directory) *)
    (graphs: string list) (* graph label (clique, ring, udg) *)
    (daemons:string list) = (* sasa daemon short option name *)
  let f l a g d =
    let log_mask = Printf.sprintf  "%s*%s-*%s.log" g d a in
    Printf.printf "parse_log \"%s\" \"%s\" \"%s\" \"%s\" mask='%s'\n%!" l a g d log_mask;
    data_from_log log_mask d l (g^".data")
  in
  List.iter (fun (l, a) -> List.iter (fun g -> List.iter (fun d ->
      f l a g d) daemons) graphs) algo_labels
;;
