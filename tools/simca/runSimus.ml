#use "topfind";;
#require "unix";;
#require "str";;

let (run0 : string -> (string -> string option) -> string list) =
 fun cmd filter ->
  let proc = Unix.open_process_in ("("^cmd^" ) 2>&1") in
  let list = ref [] in
  try
    while true do
      let line = input_line proc in
        match filter line with
          | None -> ()
          | Some str -> list := str::!list
    done;
    []
  with End_of_file ->
    ignore (Unix.close_process_in proc);
    List.rev !list

(* Executes a system call and get the printed result in a list *)
let run str = run0 str (fun s -> Some s)

let run_p str = print_string (String.concat " " (run str))
let sh = run_p

let time f x =
  let t = Sys.time() in
  let fx = f x in
  Printf.printf "Execution time: %fs\n" (Sys.time() -. t);
  fx


(**********************************************************************)
(* Parses a sasa call outputs to get the nb of moves, rounds, and steps. *)
let get_step str =
  (*    Printf.printf "get_step '%s'\n%!" str;  *)
  if
    Str.string_match
      (Str.regexp "#?This algo \\(.+\\) after \\(.+\\) moves?, \\(.+\\) steps?, \\(.+\\) rounds?.$") str 0
  then
    let moves  = int_of_string (Str.matched_group 2 str) in
    let step   = int_of_string (Str.matched_group 3 str) in
    let rounds = int_of_string (Str.matched_group 4 str) in
    Some (moves, step, rounds)
  else
    (
      Printf.printf "runSimu.ml get_step: Fail to parse '%s'\n%!" str;
      None
    )

(**********************************************************************)
let tf = float_of_int
let stddev m xl =
  let n = List.length xl in
  let sum =
    List.fold_left (fun acc xi -> acc +. (m -. (tf xi)) *. (m -. (tf xi))) 0.0 xl
  in
  sqrt (sum /. (tf n))

type t = {  all: int list;  sum: int;  mini: int;  maxi: int }
let init = { all = []; sum = 0; mini = max_int; maxi = 0 }

let update v x =  {
  all = v::x.all;
  sum = v+x.sum;
  mini = min v x.mini;
  maxi = max v x.maxi;
}

let gen_data_file  m il fn n label title =
  let oc =open_out fn in
  Printf.fprintf oc "# %s %s \n" label title;
  List.iter (Printf.fprintf oc "%i\n") il;
  flush oc;
  close_out oc;
  ignore(Sys.command (Printf.sprintf "../../tools/simca/distrib.r %s %f %s %s %i"
                        fn m label title n));
  ()

(* run_simus [cmd] several times [*], and generates 3 files:
   -  f.step.data
   -  f.move.data
   -  f.round.data
   where f is a string made of out of the [cmd] string, and
   where data files contain [n] step/move/round numbers (one per simu)

   Also call the R script distrib.r on each data file

   [*] at least 10, and at most 10000 times.  New  simulations are performed
   as long  as the size of the Confidence Interval with a confidence level of
   95% (CI_95) is higher  than threshold x m, where  is the estimation of one
   on the complexity measure number.

   Take threshold= 0.01 for 1% of uncertainty
*)
let run_simus regen_dot threshold max_simu_nb timeout cmd =
  let t = Unix.time() in
  let g, d =
    if Str.string_match (Str.regexp ".* \\(.*\\)\\.dot \\([^ ]*\\) ") cmd 0
    then Str.matched_group 1 cmd,Str.matched_group 2 cmd
    else "g",""
  in
  let basefn = Printf.sprintf "%s%s" g d in
  let step = ref init in
  let move = ref init in
  let round = ref init in
  let continue = ref true in
  let i = ref 0 in
  while !continue  do
    if regen_dot then (
      let gen_dot_cmd = Printf.sprintf "sh ./%s.dot.sh 2>&1" g in
      Printf.printf "Regenerating the dot graph: %s\n%!" gen_dot_cmd;
      assert (0 = Sys.command gen_dot_cmd)
    );
    let l = run cmd in
    if (l=[]) then (
      Printf.printf "The cmd '%s' returned nothing! \n%!" cmd;
      exit 2
    );
    List.iter (fun s -> Printf.printf "%s\n%!" s) l;
   let l = List.filter
       (fun str -> String.length str > 11 &&
                   ((String.sub str 0 10 = "This algo ")||(String.sub str 0 10 = "#This algo")))
                     l
   in
   assert (l<>[]);
   List.iter
      (fun x  ->
         match get_step x with
         | None -> Printf.printf "RunSimus.get_step fail. Maybe the graph is not connected? start again..=\n%!";incr i
         (* can occur if the generated graph is not connected on some algos *)
         | Some (m,s,r) ->
           incr i;
           step := update s !step;
           move := update m !move;
           round := update r !round;
           Printf.printf
             "\n%i: step = %i round = %i mean(step) = %d mean(round) = %d\n%!"
             !i  s r
             (!step.sum / !i)
             (!round.sum / !i) ;
      )
      l;
    let step_mean = (tf !step.sum /. (tf !i))
    and move_mean = (tf !move.sum /. (tf !i))
    and round_mean= (tf !round.sum /. (tf !i))
    in
    Printf.printf "step_mean=%f move_mean=%f round_mean=%f !i=%d \n%!" step_mean move_mean round_mean !i;
    let step_dev = stddev step_mean !step.all
    and move_dev = stddev move_mean !move.all
    and round_dev = stddev round_mean !round.all
    in
    let continue_f m rho =
      let x = (1.96 *. rho /. (threshold *. m)) ** 2.0 in
      let y = (1.96 *. rho *. 10.) ** 2.0 in
      Printf.printf "m=%f rho=%f : %d > %f or %f ? \n%!" m rho !i x y;
      !i < max_simu_nb && ( (* do at most max_simu_nb simulations *)
        (Unix.time() -. t < timeout) &&
        (!i < 10 (* and at least 10 *)
         ||
         (!i < (int_of_float x ) (* stop when |ci|< threshold x m *)
          && !i < (int_of_float y )) (* or stop when |ci|< 0.1 *)
        ))
    in
    continue := continue_f move_mean move_dev ||
                continue_f step_mean step_dev ||
                continue_f round_mean round_dev
  done;
  let step_mean = (tf !step.sum /. (tf !i))
  and move_mean = (tf !move.sum /. (tf !i))
  and round_mean= (tf !round.sum /. (tf !i))
  in
  let step_dev = stddev step_mean !step.all
  and move_dev = stddev move_mean !move.all
  and round_dev = stddev round_mean !round.all
  in
  let step_delta_ci = 2.0 *.  1.96 *. step_dev /. (sqrt (tf !i))
  and move_delta_ci = 2.0 *.  1.96 *. move_dev /. (sqrt (tf !i))
  and round_delta_ci = 2.0 *.  1.96 *. round_dev /. (sqrt (tf !i))
  in
  let step_str = Printf.sprintf "\"[%d ... %.2f~%.2f ... %d]   |CI_95|=%.2f n=%d\"%!"
      !step.mini step_mean step_dev !step.maxi step_delta_ci !i
  and move_str= Printf.sprintf "\"[%d ... %.2f~%.2f ... %d]   |CI_95|=%.2f n=%d\"%!"
      !move.mini move_mean move_dev !move.maxi move_delta_ci !i
  and round_str = Printf.sprintf "\"[%d ... %.2f~%.2f ... %d]   |CI_95|=%.2f n=%d\"%!"
      !round.mini  round_mean round_dev !round.maxi round_delta_ci !i
  in
  gen_data_file move_mean !move.all (basefn^".move.data") !i "Move" move_str;
  gen_data_file step_mean !step.all (basefn^".step.data") !i "Step" step_str;
  gen_data_file round_mean !round.all (basefn^".round.data") !i "Round" round_str;
  Printf.printf "Moves:%s\nSteps:%s\nRounds:%s\n" move_str step_str round_str ;
  if !i = max_simu_nb then
    Printf.printf "ZZZ Maximal number of simulations (%d) reached!\n" max_simu_nb;
  if Unix.time() -. t > timeout  then
    Printf.printf "ZZZ Timeout! (%f > %f)\n"  (Unix.time() -. t) timeout ;
  Printf.printf "Wall-clock Execution time for this file: %fs\n" (Unix.time() -. t);
  exit 0;;
