#use "genExpeMakefiles.ml";;
precision := 0.1;;
(* 0.1  means that we simulate until the  confidence Interval  size of
   the 3  complexity numbers under  estimation is smaller than  10% of
   their current estimation.

   Starting with  10% to  get a  rough idea of  the complexity  in the
   first place is probably a wise idea.  *)
max_simu_nb := 1000;; (* by default, it's more *)
timeout_in_sec := 60;; (* ditto *)
let algos = ["../../test/alea-coloring-alt";
             "../../test/alea-coloring-unif";
             "../../test/alea-coloring"]
let daemons = ["-sd";"-lcd";"-dd"]
let cliques = List.init 5 (fun n -> Clique (10*(n+1))) (* [10; 20; ...; 50] *)
let rings = List.init 5 (fun n -> Ring (100*(n+1))) (* [100; 200; ...; 500] *)
let er = List.init 5 (fun n -> ER (20*(n+1), 0.4)) (* [20; 40; ...; 100] *)
let networks = (cliques@rings@er)

let gen_make_rules () = gen_makefile "Makefile.expe-rules" daemons algos networks;;

#use "parseLog.ml";;
let gen_pdf () =
  let gl = ["clique"; "ring"; "er"] in
  List.iter (fun n -> sh ("rm -f "^n^".data")) gl; (* because parse_log appends data *)
  parse_log ["Uniform When Activated", "alea-coloring-unif"] gl daemons;
  parse_log ["Smallest When Activated","alea-coloring"] gl daemons;
  parse_log ["Always the Biggest", "alea-coloring-alt"] gl daemons;
  List.iter (fun n -> sh ("./gen_pdf.r "^n^".data nonreg")) gl
