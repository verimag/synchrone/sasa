#use "topfind";;
#require "unix";;
#require "str";;

(** Generate a dot file with gg *)
let gen_dot graph_kind dot dir =
  let gg_cmd = Printf.sprintf "echo \"gg %s %s -o %s/%s.dot\" > %s/%s.dot.sh"
      graph_kind (if !directed then "-dir" else "") dir dot dir dot in
  gg_cmd

(** Decorate gg generated  dot files with gg-deco *)
let deco_dot dir dot =
  let deco, _label = algo_to_deco dir in
  let dot = dir^"/"^dot in
  Printf.sprintf "echo \"gg-deco \\\"%s\\\" %s -o %s\" >> %s.sh " deco dot dot dot

let grid w h label dir =
  let name = Printf.sprintf "grid%dx%d_%s" w h label in 
  name, gen_dot (Printf.sprintf "grid -w %d -he %d" w h) name dir

let erdos_renyi n p label dir =
  let name = Printf.sprintf "er%i_%s" n label in
  (* The graph is a.c. connected if p> log(n)/n *) 
  name, gen_dot (Printf.sprintf "ER -n %d -p %f" n p) name dir

let clique n label dir =
  let name = Printf.sprintf "clique%i_%s" n label in 
  name, gen_dot (Printf.sprintf "clique -n %d" n) name dir

let ring n label dir =
  let name = Printf.sprintf "ring%i_%s" n label in 
  name, gen_dot (Printf.sprintf "ring -n %d" n) name dir

let udg n label dir =
  let name = Printf.sprintf "udg%i_%s" n label in
  name, gen_dot (Printf.sprintf "UDG -n %d" n) name dir

let qudg n label dir =
  let name = Printf.sprintf "qudg%i_%s" n label in 
  name, gen_dot (Printf.sprintf "QUDG -n %d" n) name dir

let gen_graph label dir = function
  | Udg i  -> udg i label  dir
  | Qudg i -> qudg i label  dir
  | ER  (i,p)  -> erdos_renyi i p label dir
  | Grid (i,j) -> grid i j label dir
  | Clique i -> clique i  label dir
  | Ring i -> ring i  label dir
  
let (gen_dot_rule : out_channel -> graph -> daemon list -> dir list ->
     string list * string list) =
  fun oc g dl dirl ->
  List.iter (fun dir -> 
      if not (Sys.file_exists dir) then failwith ("Error: "^dir^" does not exist\n");)
    dirl;
  let regen_dot = match g with
    | Udg _ | Qudg _ | ER  _  -> !regen_dot
    | Grid (_,_) | Clique _ | Ring _ -> false (* those are deterministic *)
  in
  let targets = 
    (List.fold_left
       (fun acc dir ->
          let _, label = algo_to_deco dir in
          let name, _ = gen_graph label dir g in
          let dot = Printf.sprintf "%s.dot" name in
          let bdir = Filename.basename dir in
          let log = Printf.sprintf "%s%s-%s.log" name (List.hd dl) bdir in
          let cwd = Sys.getcwd() in
          Printf.fprintf oc "\n%s: \n\t[ -f %s/%s.cmxs ] || \\
(echo \"\\n ===> do a 'make cmxs' before!\\n\\n\"; exit 1)\n\tcd %s \\\n" log dir name dir;
          List.iter (fun d -> 
              let log = Printf.sprintf "%s%s-%s.log" name d bdir in
              Printf.fprintf oc
                "\t && echo \"#use \\\"%s/runSimus.ml\\\";;\\n \\
         run_simus %b %f %d %d.0 \\\"sasa -l 20000 %s %s -nd \\\";;\\n\" \\
         | ocaml  > %s/%s \\
"
                cwd regen_dot !precision !max_simu_nb !timeout_in_sec dot d cwd log 
            )
          dl;
          Printf.fprintf oc "\t && echo \"%s done\"\n\n" log; 
          log::acc
       )
       [] dirl
    )
  in
  let targets_cmxs : string list =
    List.fold_left
      (fun acc dir ->
         let _deco, label = algo_to_deco dir in
         let name, gg_cmd = gen_graph label dir g in
         let dot = Printf.sprintf "%s.dot" name in
         let cmxs = Printf.sprintf "%s.cmxs" name in
         Printf.fprintf oc "%s/%s:\n	%s\n" dir dot gg_cmd;
         Printf.fprintf oc "	%s\n" (deco_dot dir dot);
         Printf.fprintf oc "	sh %s/%s.sh\n" dir dot;
         Printf.fprintf oc "\n\n%s/%s: %s/%s\n" dir cmxs dir dot;
         Printf.fprintf oc "	cd %s; make %s\n" dir cmxs;
         (dir^"/"^cmxs)::acc
      )
      []
      dirl
  in
  targets,targets_cmxs

let (gen_makefile0 : out_channel -> daemon list -> dir list ->
     string list * string list -> graph -> string list * string list) =
  fun oc dl dirl (targets_acc, targets_cmxs_acc) g ->
  let targets, targets_cmxs = gen_dot_rule oc g dl dirl in
  targets @ targets_acc, targets_cmxs @ targets_cmxs_acc
