#! /usr/bin/env Rscript
# Let's first give to CLI arguments some meaningfull names
args <- commandArgs(TRUE)
datafilename <- args[1]
outfilename <- args[2]
title <- args[3]
pdffilename = paste(datafilename,outfilename,"rounds.pdf", sep ="-")
pdffilename2 = paste(datafilename,outfilename,"moves.pdf", sep ="-")
pdffilename3 = paste(datafilename,outfilename,"steps.pdf", sep ="-")

# Read the data file 
data <- data.frame(val=read.table(datafilename))

# Give to columns a name
names(data) <- c("n", "r", "Daemon", "Algorithm", "complexity_kind")

# Generate a pdf visualisation of the Data with ggplot2
library(ggplot2)
pdf(pdffilename)
ggplot(subset(data,complexity_kind=="rounds"),aes(x=n,y=r,colour=Daemon,shape=Algorithm))+
    geom_line() + geom_point() +
#    facet_grid(complexity_kind ~ .)
    ylab("Rounds Number")+xlab("Nodes Number")+ggtitle(paste(title,"Rounds Numbers"," "))

# Ditto for moves
pdf(pdffilename2)
ggplot(subset(data,complexity_kind=="moves"),aes(x=n,y=r,colour=Daemon,shape=Algorithm))+
    geom_line() +  geom_point() +
    ylab("Moves Number")+xlab ("Nodes Number")+ggtitle(paste(title,"Moves Numbers"," "))

# Ditto for steps
pdf(pdffilename3)
ggplot(subset(data,complexity_kind=="steps"),aes(x=n,y=r,colour=Daemon,shape=Algorithm))+
    geom_line() + geom_point() +
    ylab("Steps Number")+xlab("Nodes Number")+ggtitle(paste(title,"Steps Numbers"," "))

