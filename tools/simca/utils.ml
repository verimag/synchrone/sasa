#use "topfind";;

#require "str";;

let (run0 : string -> (string -> string option) -> string list) =
 fun cmd filter -> 
  let proc = Unix.open_process_in ("("^cmd^" | sed -e 's/^/stdout: /' ) 2>&1") in
  let list = ref [] in
  try
    while true do
      let line = input_line proc in
      if String.length line >= 8 && String.sub line 0 8 = "stdout: " then 
        let str = String.sub line 8 (String.length line - 8) in
        match filter str with
          | None -> ()
          | Some str -> list := str::!list
    done;
    []
  with End_of_file ->
    ignore (Unix.close_process_in proc);
    List.rev !list

(* Executes a system call and get the printed result in a list *)
let run str = run0 str (fun s -> Some s)

let run_p str = print_string (String.concat " " (run str))
let sh = run_p

let time f x =
  let t = Sys.time() in
  let fx = f x in
  Printf.printf "Execution time: %fs\n" (Sys.time() -. t);
  fx 
