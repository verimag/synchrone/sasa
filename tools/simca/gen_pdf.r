#! /usr/bin/env Rscript
args <- commandArgs(TRUE)
if (length(args) == 0) {
    stop("At least one argument is necessary
usage:
  gen_pdf.r file.data [a_label [ordinate_label]]
where the optional arguments
  a_label is used to build output file names (use current dir by default)
  ordinate_label is used to label the ordinate axis (use 'Nodes' by default)

example:
  gen_pdf.r clique.data
  gen_pdf.r clique.data coloring
  gen_pdf.r clique.data coloring Diameter
", call.=FALSE)
} 

datafilename <- args[1]
campaign <- ifelse(is.na(args[2]), basename(getwd()), args[2])
abscissa  <- ifelse(is.na(args[3]),"Nodes",args[3])
graphname <- tools::file_path_sans_ext(args[1])

# Read the data file 
data <- data.frame(val=read.table(datafilename))

# Give to columns a name
names(data) <- c("n", "Daemons", "Algorithms", "complexity_kind", "min", "mean", "max")

# Ordering the grid manually
data$complexity_kind_f = factor(data$complexity_kind, levels=c('rounds','steps','moves'))

# Generate a pdf visualisation of the Data with ggplot2
library(ggplot2)
library(dplyr)

gen_pdf <- function(pdffilename, x1, x2){   
    pdf(pdffilename,onefile=TRUE)
    mplot <- ggplot(data, aes_string(x="n",y="mean",colour=x1))+ geom_line() +
        facet_grid(c(paste("complexity_kind_f"), paste(x2)) , scales='free') +
        ylab("Round/Step/Move Numbers")+xlab(paste(abscissa,"Number"))+
        ggtitle(paste("Compare", x1, "on various", x2, "Numbers on",
                      stringr::str_to_title(graphname), sep = " "))+
         theme(legend.position="bottom")
    mplot2 <- mplot+geom_ribbon(aes_string(x="n", ymax="max", ymin="min"), alpha=0.2) +
        ggtitle(paste("Compare", x1, "on various", x2, "Numbers on",
                      stringr::str_to_title(graphname), "(+ min/max ribbon)", sep = " "))+
         theme(legend.position="bottom")
    print(mplot)
    print(mplot2)
        
    for (cm in c("moves","steps","rounds")){
        datax <- filter(data, complexity_kind == cm)
        
        plot <- ggplot(datax,aes_string(x="n",y="mean",colour=x1))+ geom_line() +
            facet_grid(c(paste(x2)), scales='free') +
            ylab(paste(cm, "Number", sep=" ")) + xlab(paste(abscissa,"Number"))+
            ggtitle(paste(stringr::str_to_title(cm), "Numbers on",
                          stringr::str_to_title(graphname), sep = " "))+
         theme(legend.position="bottom")
        plot1 <- plot + geom_point() 
        plot2 <- plot+geom_ribbon(aes_string(x="n", ymax="max", ymin="min"), alpha=0.2)+
            ggtitle(paste(stringr::str_to_title(cm), "Numbers on",
                          stringr::str_to_title(graphname),
                          "(+ min/max ribbon)", sep = " "))+
         theme(legend.position="bottom")
        print(plot1)
        print(plot2)
        
        plot <- ggplot(datax,aes_string(x="n",y="mean",colour=x1,shape=x2))+ geom_line() +
            ylab(paste(cm, "Number", sep=" ")) + xlab(paste(abscissa,"Number"))+
            ggtitle(paste(stringr::str_to_title(cm), "Numbers on",
                          stringr::str_to_title(graphname), sep = " "))+
         theme(legend.position="bottom")
        plot1 <- plot + geom_point() 
        plot2 <- plot+geom_ribbon(aes_string(x="n", ymax="max", ymin="min"), alpha=0.2)+
            ggtitle(paste(stringr::str_to_title(cm), "Numbers on",
                          stringr::str_to_title(graphname),
                          "(+ min/max ribbon)", sep = " "))+
         theme(legend.position="bottom")
        print(plot1)
        print(plot2)
    }
    pngfilename = paste(pdffilename,".png", sep ="")
    png(pngfilename)
    print(mplot2)
    
}

pdffilename1 = paste(campaign, datafilename,"algos.pdf", sep ="-")
pdffilename2 = paste(campaign, datafilename,"demons.pdf", sep ="-")
gen_pdf(pdffilename1, "Algorithms", "Daemons")
gen_pdf(pdffilename2, "Daemons", "Algorithms")

