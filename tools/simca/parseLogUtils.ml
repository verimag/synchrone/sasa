#use "utils.ml";;

(***********************************************************************)
(* Append to datafile  information read in log files specified with log_mask.
log files are expected to contain something like
"
 Min.   : 906.0
 1st Qu.: 973.5
 Median :1007.5
 Mean   :1020.5
 3rd Qu.:1053.5
 Max.   :1164.0
"
3 times (for moves, steps, and then rounds)
*)
exception Pb
let data_from_log log_mask demon algolbl datafile  =
  let demon = match demon with
    | "-sd" -> "Synchronous"
    | "-dd" -> "Distributed"
    | "-lcd" -> "Locally Central"
    | "-cd" -> "Central"
    | s -> s
  in
  let oc = open_out_gen [Open_wronly; Open_creat; Open_append; Open_binary] 0o666
      datafile
  in
  let ocorg = open_out_gen [Open_wronly; Open_creat; Open_append; Open_binary] 0o666
      (datafile^".org")
  in
  let log_files = run ("ls -1 " ^ log_mask) in
  List.iter
    (fun log ->
       try
         let first_int = Str.search_forward (Str.regexp "[0-9]+") log 0 in
         let size =
           if String.sub log 0 first_int = "grid" then
             try
               let size1 = Str.matched_string log |> int_of_string in
               let _second_int = Str.search_forward (Str.regexp "[0-9]+") log first_int in
               let size2 = Str.matched_string log |> int_of_string in
               string_of_int (size1*size2)
             with _ -> assert false
           else
             Str.matched_string log
         in
         let min_moves, min_steps, min_rounds =
           match run ("grep -n \"Min.\" "^ log) with
           | str1::str2::str3::_ ->
             List.hd (List.rev(Str.split (Str.regexp ":") str1)),
             List.hd (List.rev(Str.split (Str.regexp ":") str2)),
             List.hd (List.rev(Str.split (Str.regexp ":") str3))
           | _  ->
             Printf.printf  "W: no Min found in %s\n%!" log;
             raise Pb
         in
         let max_moves, max_steps, max_rounds =
           match run ("grep -n \"Max.\" "^ log) with
           | str1::str2::str3::_ ->
             List.hd (List.rev(Str.split (Str.regexp ":") str1)),
             List.hd (List.rev(Str.split (Str.regexp ":") str2)),
             List.hd (List.rev(Str.split (Str.regexp ":") str3))
           | _  ->
             Printf.printf  "W: no Max found in %s\n%!" log;
             raise Pb
         in
         let mean_moves, mean_steps, mean_rounds =
           match run ("grep -n \"Mean\" "^ log) with
           | str1::str2::str3::_ ->
             List.hd (List.rev(Str.split (Str.regexp ":") str1)),
             List.hd (List.rev(Str.split (Str.regexp ":") str2)),
             List.hd (List.rev(Str.split (Str.regexp ":") str3))
           | _  ->
             Printf.printf  "W: no Mean found in %s\n%!" log;
             raise Pb
         in
         Printf.fprintf oc "%s \"%s\" \"%s\" moves %s %s %s\n%!"
           size demon algolbl min_moves mean_moves max_moves ;
         Printf.fprintf oc "%s \"%s\" \"%s\" steps %s %s %s\n%!"
           size demon algolbl min_steps mean_steps max_steps ;
         Printf.fprintf oc "%s \"%s\" \"%s\" rounds %s %s %s\n%!"
           size demon algolbl min_rounds mean_rounds max_rounds;
         Printf.fprintf ocorg "| %s | %s | %s | moves | %s | %s | %s| \n%!"
           size demon algolbl min_moves mean_moves max_moves ;
         Printf.fprintf ocorg "| %s | %s | %s | steps | %s | %s | %s| \n%!"
           size demon algolbl min_steps mean_steps max_steps ;
         Printf.fprintf ocorg "| %s | %s | %s | rounds | %s | %s | %s| \n%!"
           size demon algolbl min_rounds mean_rounds max_rounds
       with
         Pb -> ()
    )
    log_files;
  close_out oc;
  close_out ocorg;
  ()
