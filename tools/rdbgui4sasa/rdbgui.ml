
let quote str = if String.contains str ' ' then ("\""^str^"\"") else str

let rdbg_cmd =
  let cmd = List.tl (List.map quote (Array.to_list Sys.argv)) in
  let str = String.concat " " cmd in
  let salut_mode = not (Str.string_match (Str.regexp ".*sasa .*\\.dot") str 0) in
  let str = if salut_mode then str else
              Str.replace_first (Str.regexp "sasa ") "sasa -custd -replay " str
      (* I add -custd here, but it ought to be overridden by user option.
         The idea is that I don't want the default daemon to be -dd from the gui
         (as it is the case from the cli)
      *)
  in
  let sasa_opt = if salut_mode then "--salut" else "--sasa" in
  String.concat " " ("rdbg"::sasa_opt::str::[])

let gui =
  Printf.sprintf " --ocaml-cmd \"#use \\\"gtkgui.ml\\\";;\""


let welcome () =
  Printf.printf "rdbgui4sasa is a GUI wrapper around rdbg when used with sasa\n";
  Printf.printf "you can thus replace rdbg by rdbgui4sasa, but sasa *must be* ";
  Printf.printf "involved in the session\nExample: \n";
  Printf.printf "   rdbgui4sasa -sut \"sasa g.dot\"\n";
  Printf.printf "   rdbgui4sasa -h\n\n";
  flush stdout

let _ =
  let n = Array.length Sys.argv in
  welcome ();
  if n = 1 && Mypervasives.ls ".rdbg-session" "ml" = [] then exit 0;
  let cmd = rdbg_cmd ^ gui in
  Printf.printf "%s\n%!" cmd;
  Sys.command cmd
