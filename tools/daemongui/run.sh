#!/bin/bash

set -e
cd $(dirname "$0")
exe=$(readlink -f ./gui)

./build.sh
cd ../../test/coloring
"$exe" -sut "sasa -custd ring.dot" --missing-vars-last
#"$exe" -sut "sasa ring.dot"
