(* Time-stamp: <modified the 13/01/2025 (at 14:40) by Erwan Jahier> *)

#thread
#require "lablgtk3"

open GMain
open GdkKeysyms
open RdbgEvent
open Data


(** Extrait le nom et l'état des noeuds
@return liste de tuples (nom, etat, activable)
*)
let rdbg_nodes_info e: (string * string * bool) list =
  let enabled =
    List.filter (fun (n,v) -> String.length n > 5 && String.sub n 0 5 = "Enab_") e.data
  in
  let split_var (str, value) =
    let v = match value with B v -> v | _  -> assert false in
    let p, label =
      match args.salut_mode, String.split_on_char '_' str with
      | _, [] | _, [_] | _, [_;_] -> assert false (* should not occur (naming conv) *)
      | false, _::x::y -> x, String.concat "_" y
      | true,  _::x::y::z -> x^y, String.concat "_" z
    in
    p, label,v
  in
  List.map split_var enabled

(** Liste  qui dit pour chaque  noeud s'il est activable.   On suppose
   qu'ils sont groupés.

    XXX  On  suppose aussi  qu'au  plus  une  action par  process  est
   enabled.  Les  demons  dans  sasa,  font  mieux.  Mais  ici,  c'est
   raisonnable si on ne veut pas avoir nb_actions fois plus de boutons
   ! (alors que ca n'arrive jamais) *)
let rdbg_nodes_enabled e =
  match rdbg_nodes_info e with
  | [] -> []
  | (node, _action, enab)::l ->
    let last, res =
      List.fold_left
        (fun ((p_node, p_enab), res) (node, _action, enab) ->
           if p_node = node then
             (node, p_enab || enab), res
           else
             (node, enab), (p_node, p_enab)::res
        )
        ((node, enab), [])
        l
    in
    last::res

(**********************************************************************************)
type daemon_kind =
  Distributed | Synchronous | Central | LocCentral | ManualCentral | Manual
let daemon_kind = ref ManualCentral

type daemon_mode = Internal (* sasa with internal daemon *) | GuiAuto | GuiManual
(* Possible way of using rdbgui4sasa w.r.t.  daemon modes:
|-------+----------+---------+-----------|
|       | Internal | GuiAuto | GuiManual |
|-------+----------+---------+-----------|
| sasa  | yes      | yes     | yes       |
| salut |          | yes     | yes       |
|-------+----------+---------+-----------|
*)

let daemon_mode () =
  if not custom_mode then Internal else
  match !daemon_kind with
  | Distributed | Synchronous | Central | LocCentral -> GuiAuto
  | ManualCentral | Manual -> GuiManual

let gui_mode = custom_mode (* a better name? *)

let check_point_event e =
  (* Depending on the way rdbgui4sasa is used, some  kinds of events
     make more sense as checkpoint candidate. Moreover, it is better to have only
     one checkpoint per step.
  *)
  match daemon_mode () with
  | Internal -> e.kind = Ltop
  (*   | GuiAuto  -> e.kind = Ltop *)
  | GuiAuto  -> e.kind = Call && e.name = "mv_hook"
  | GuiManual -> e.kind = Call && e.name = "mv_hook"

let _ = check_ref := fun e -> e.nb = 1 || (check_point_event e) && round e;;

(*  modify the  one in .rdbg-cmd.ml to  inhibit it  when the  undo is
   impossible: in  manual modes,  it would require  to store  all the
   user  manual  inputs  to  replay  the  simulation  from  the  last
   checkpoint (because of the PRGs) *)
let store i = if daemon_mode() = GuiManual then () else store i

let refresh_fun_tbl = Hashtbl.create 1
let _ = Hashtbl.add refresh_fun_tbl "update dot" d
let refresh () =
  Hashtbl.iter (fun str f -> f()) refresh_fun_tbl

(** Met en place le hook  *)
let daemongui_activate : (string, bool) Hashtbl.t = Hashtbl.create 1
(* states whether a node should be activated *)

let (fake_val_of_type : Data.t -> Data.v) = function
  | Bool -> B false
  | Int -> I (Random.int 10)
  | Real -> F (Random.float 10.0)
  | Extern _ -> assert false
  | Enum _ -> assert false
  | Struct _ -> assert false
  | Array _ -> assert false
  | Alpha _ -> assert false
  | Alias _ -> assert false
  | String _ -> assert false


(**********************************************************************************)
(* Write with colors *)
let create_tags (buffer:GText.buffer) =
  let mktags n c =
    ignore (buffer#create_tag ~name:n [`FAMILY "monospace";`FOREGROUND c])
  in
  mktags "blue_foreground" "blue";
  mktags "black_foreground" "black";
  mktags "red_foreground" "red";
  mktags "green_foreground" "green";
  ignore (buffer#create_tag ~name:"red_background" [`BACKGROUND "red"]);
  ()

let tags_created = ref false
let oracle_button_ref = ref None

let write color b str =
  if not !tags_created then (create_tags b; tags_created := true);
  b#set_text "";
  b#insert ~tag_names:[color] str

let write_add color b str =
  if not !tags_created then (create_tags b; tags_created := true);
  b#insert ~tag_names:[color] str


let blue  = write "blue_foreground"
let black = write "black_foreground"
let red   = write "red_foreground"
let green = write "green_foreground"

let blue_add  = write_add "blue_foreground"
let black_add = write_add "black_foreground"
let red_add   = write_add "red_foreground"

let display_event b =
  blue_add b#buffer "----------------------------------------";
  blue_add b#buffer "----------------------------------------\n";
  blue_add b#buffer (str_of_sasa_event false !e)


(*  *)


let goto_hook_call e =
  if gui_mode then
    next_cond e (fun e -> e.name = "mv_hook" && e.kind = Call)
  else
    e

let goto_hook_exit e =
  if gui_mode then
    next_cond e (fun e -> e.name = "mv_hook" && e.kind = Exit)
  else
    e

let goto_top e = next_cond e (fun e -> e.kind = Ltop)

let init_rdbg_hook () =
  let guidaemon sl =
    if sl = [] then
      (* when  called first  (salut), its  activation output  will be
         ignored; nevertheless,  we need to provide  give fake values
         for Enab and state values that will be ignored *)
      let res = List.map (fun (n,t) ->
          let v = fake_val_of_type t in
          (* Printf.printf "tossing a value for %s -> %s \n%!" n  *)
          (*  (Data.val_to_string string_of_float v);  *)
          n, v) (snd !rdbg_mv)
      in
      Some res
    else
      let sl = List.filter
                 (fun (n,v) -> String.length n>5 && String.sub n 0 5 = "Enab_") sl
      in
      let res = List.map (fun (n,enabled) ->
          (* n est de la forme Enab_node_state, enabled est un Data.v *)
          let str = String.sub n 5 ((String.length n)-5) in
          let node_name = match args.salut_mode, String.split_on_char '_' str with
            | false, x::_ -> x
            | true, x::y::_ -> x^y
            | _, ([] | [_]) -> assert false
          in
          let to_activate = match Hashtbl.find_opt daemongui_activate node_name with
            | None -> false
            | Some x -> x
          in
          let activate = match enabled with
            | B true -> B to_activate
            | B false -> B false  (* activate only the Enabled actions! *)
            | _ -> assert false
          in
          (str, activate)
        ) sl
      in
      let fake_init_val = (* unused, but must be provided!*)
        List.map (fun (n,t) -> n, fake_val_of_type t) (snd !rdbg_mv)
      in
      let ok_var = fst (List.split res) in
      let res = List.fold_left
          (fun acc (n,v) -> if List.mem n ok_var then acc else (n,v)::acc)
          res fake_init_val
      in
      Some res
  in
  match !rdbg_mv_hook with
  | None -> rdbg_mv_hook := Some guidaemon
  | _ -> ()

let set_tooltip b = b#misc#set_tooltip_text

let start () =
  if gui_mode then init_rdbg_hook ();
  if args.salut_mode then
    (* In  this mode, the hook  plays first to provide  fake values to
       sasa but the hook does not need input at this first step *)
    e:=next_cond_gen !e (fun e -> e.name="mv_hook" && e.kind=Exit) (fun e -> e.next());
  if gui_mode then
    e:=next_cond_gen !e check_point_event (fun e -> e.next())
  else
    (* internal daemon mode *)
    e:=next_cond_gen !e (fun e -> e.kind=Ltop) (fun e -> e.next());
  redos := [!e.nb];
  ckpt_list := [!e];
  !e.save_state !e.nb

let start_with_seed p seed =
  Seed.set seed;
  p (Printf.sprintf "Restarting using the seed %d" seed);
  !e.RdbgEvent.reset(); (* should be done after the see reset! *)
  Round.reinit();
  e:=RdbgStdLib.run ~call_hooks:true ();
  start ();
  d()

let restart p _ =
  Seed.replay_seed := true;
  let seed = Seed.get dotfile in
  (* change_sasa_seed seed; *)
  start_with_seed p seed


let restart_new_seed p _ =
  Seed.replay_seed := false;
  Seed.reset();
  let seed = Seed.get "restart_new_seed" in
  change_sasa_seed seed;
  p (Printf.sprintf "Restarting using the seed %d" seed);
  start_with_seed p seed

let _restart_new_seed p _ =
  Seed.reset();
  let seed = Seed.get "restart_new_seed" in
  change_sasa_seed seed;
  Seed.set (seed);
  !e.RdbgEvent.reset();
  p (Printf.sprintf "Restarting using the seed %d" seed);

  e:=RdbgStdLib.run ~call_hooks:true ();
  Round.reinit();
  redos := [1];
  ckpt_list := [!e];

  if args.salut_mode then
    (* in this mode, the hook plays first to provide fake values to sasa
       but the hook does not need input at this first step
    *)
    e:=goto_hook_exit !e;
  e:=goto_hook_call !e;
  d()

let inject p _ =
  let seed = Seed.get dotfile in
  Seed.set seed;
  p (Printf.sprintf "A fault is injected.\nThe seed is set to %d\n" seed);
  Round.reinit_mask();
  !e.restore_state min_int;
  start ();
  d()


let counter_map = Hashtbl.create 0

let update_daemongui_tbl e =
  (* In rdbgui automatic modes (ie, not in Manual nor CentralManual
       nor internal sasa daemons):
     - looks  who is enabled,
     - chooses whom to activate, and
     - sets the tbl shared with the hook function

       Should therefore be used at events where the enabled process info is available

     nb: do side-effects via PRGs via Daemons
  *)
  assert (gui_mode);
  let rec get_higher_priority nl =
    let prio n =
      let counter = try Hashtbl.find counter_map n with Not_found -> assert false in
      counter#get
    in
    let rec aux p acc = function
      | [] -> acc
      | (n, false)::t -> aux p acc t
      | (n, true)::t ->
        let pn = prio n in
        if p > pn then aux p acc t else
        if p = pn then aux p (n::acc) t else
          aux pn [n] t
    in
    aux 0 [] nl
  in
  let nodes_enabled = rdbg_nodes_enabled e in
  let nodes = List.filter (fun (_,b) -> b) nodes_enabled in
  let nodes = get_higher_priority nodes in
  (* p ("==> gtkgui: CALL =" ^ (string_of_event e)); *)
  (match !daemon_kind with
   | Distributed -> (
       let nodes = List.map (fun x -> [x]) nodes in
       let to_activate = Daemon.distributed nodes in (* PRGs inside! *)
       Hashtbl.clear daemongui_activate;
       List.iter (fun n -> Hashtbl.replace daemongui_activate n true) to_activate
     )
   | Synchronous -> (
       Hashtbl.clear daemongui_activate;
       List.iter (fun n -> Hashtbl.replace daemongui_activate n true) nodes
     )
   | Central -> (
       let nodes = List.map (fun x -> [x]) nodes in
       let to_activate = Daemon.central nodes in
       Hashtbl.clear daemongui_activate;
       List.iter (fun n ->
           (* Printf.printf "Activating %s\n" n; *)
           Hashtbl.replace daemongui_activate n true) to_activate
     )
   | LocCentral -> (
       let get_neigbors x =
         let pred = snd (List.split (topology.pred x)) in
         let succ = topology.succ x in
         let res = List.fold_left
             (fun acc x -> if List.mem x acc then acc else x::acc) succ pred
         in
         (* p (Printf.sprintf "voisins(%s)=%s\n" x (String.concat "," res)); *)
         res
       in
       let nodes = List.map (fun x -> [x, get_neigbors x]) nodes in
       let to_activate = Daemon.locally_central nodes in
       Hashtbl.clear daemongui_activate;
       List.iter (fun n ->  Hashtbl.replace daemongui_activate n true) to_activate
     )
   | ManualCentral -> () (* SNO; the step is done in pushbox callbacks *)
   | Manual -> ()
  )

(* redraws the gui buttons a each move, to show only buttons related to enabled nodes *)
let draw_manual_daemons_buttons p gtext vbox step_button back_step_button round_button
    legitimate_button undo_button =
  let daemon_box = GPack.hbox ~packing:vbox#add ()  ~homogeneous:true  ~height:15 in
  let dk_cd = GButton.radio_button  ~active:(!daemon_kind=Central)
      ~label:"Central" ~packing:daemon_box#add ()
  in
  let make_but act lbl = GButton.radio_button ~active:act ~label:lbl
      ~group:dk_cd#group ~packing:daemon_box#add ()
  in
  let dk_lcd = make_but (!daemon_kind=LocCentral) "Locally Central" in
  let dk_dd = make_but  (!daemon_kind=Distributed) "Distributed" in
  let dk_sd = make_but  (!daemon_kind=Synchronous) "Synchronous" in
  let dk_manual = make_but (!daemon_kind=Manual) "Manual" in
  let dk_manual_central = make_but (!daemon_kind=ManualCentral) "Manual Central" in
  set_tooltip dk_dd (Printf.sprintf "Set the automatic distributed mode");
  set_tooltip dk_sd (Printf.sprintf "Set the automatic synchronous mode");
  set_tooltip dk_cd (Printf.sprintf "Set the automatic central mode");
  set_tooltip dk_lcd (Printf.sprintf "Set the automatic locally central mode");
  set_tooltip dk_manual (Printf.sprintf "Set the manual mode");
  set_tooltip dk_manual_central (Printf.sprintf "Set the manual central mode");

  blue_add gtext#buffer (str_of_sasa_event false !e);
  d();
  let nodes_enabled = rdbg_nodes_enabled !e in
  let nodes = fst (List.split nodes_enabled) in
  let update_rdbg_hook node activate =
    (match !daemon_kind with
     | Distributed | Synchronous | Central | LocCentral ->
       assert false (* SNO *)
     | ManualCentral -> (
         let txt = Printf.sprintf "ManualCentral step: %s\n\n%s"
             node (str_of_sasa_event false !e) in
         (* gtext#buffer#set_text txt;  *)
         blue gtext#buffer txt;
         List.iter (fun n -> Hashtbl.add daemongui_activate n (n = node)) nodes;
         (* Hashtbl.filter_map_inplace (fun n _prev_status -> Some (n = node)) daemongui_activate; *)
       )
     | Manual ->
       let txt = Printf.sprintf "Set %s to %b\n" node activate in
       blue_add gtext#buffer txt;
       Hashtbl.replace daemongui_activate node activate
    );
  in
  (* 1 case par noeud : activer/pas activer *)
  (* NB : lablgtk3 ne propose pas le FlowBox (pourtant dispo dans GTK >= 3.12) *)
  let n = List.length nodes_enabled in
  let m = int_of_float (sqrt (float_of_int n)) in

  (* Des checkbox pour le mode Manuel *)
  (* build manually a m x m grid *)
  let i = ref 0 in
  let checkbox_grid = GPack.vbox ~packing:vbox#add () in
  let checkbox_scrolled_grid = GBin.scrolled_window ~border_width:10 ~hpolicy:`AUTOMATIC
      ~vpolicy:`AUTOMATIC
      ~height:300
      ~shadow_type:`OUT ~packing:checkbox_grid#add ()
  in
  let checkbox_scrolled_grid_box = GPack.vbox ~packing:checkbox_scrolled_grid#add () in
  let checkbox_line = GPack.hbox ~packing:checkbox_scrolled_grid_box#add () in
  let checkbox_line_ref = ref checkbox_line in
  let checkbox_map = Hashtbl.create n in
  List.iter (fun (name, enabled) ->
      incr i;
      if !i > m then (
        i := 1;
        let new_checkbox_line = GPack.hbox ~packing:checkbox_scrolled_grid_box#add () in
        checkbox_line_ref := new_checkbox_line;
      );
      (* cf. classe toggle_button de lablgtk3 *)
      let checkbox = GButton.check_button ~label:name ~packing:!checkbox_line_ref#add () in
      (* Quand on coche/décoche, met à jour le rdbg_mv_hook *)
      set_tooltip checkbox (Printf.sprintf "check to activate %s at the next step" name);
      ignore(checkbox#connect#toggled
               ~callback: (fun () -> update_rdbg_hook name checkbox#active));
      checkbox#set_sensitive enabled; (* désactive la box si le noeud n'est pas activable *)
      checkbox#set_active false; (* décoche la case *)
      Hashtbl.add checkbox_map name checkbox;
    )
    nodes_enabled;

  (* Des boutons pour le mode Manuel Central *)
  let pushbox_grid = GPack.vbox ~packing:vbox#add () ~homogeneous:true in
  let pushbox_scrolled_grid = GBin.scrolled_window ~border_width:10 ~hpolicy:`AUTOMATIC
      ~vpolicy:`AUTOMATIC
      ~height:300
      ~shadow_type:`OUT ~packing:pushbox_grid#add ()
  in
  let pushbox_scrolled_grid_box =
    GPack.vbox ~homogeneous:true ~packing:pushbox_scrolled_grid#add ()
  in
  let pushbox_line = GPack.hbox ~packing:pushbox_scrolled_grid_box#add () in
  let pushbox_line_ref = ref pushbox_line in
  let pushbox_map = Hashtbl.create n in
  i := 0;
  List.iter (fun (name, enabled) ->
      incr i;
      if !i > m then (
        i := 1;
        let new_pushbox_line = GPack.hbox ~packing:pushbox_scrolled_grid_box#add () in
        pushbox_line_ref := new_pushbox_line
      );
      (* cf. classe toggle_button de lablgtk3 *)
      let pushbox = GButton.button ~label:name ~packing:!pushbox_line_ref#add () in
      set_tooltip pushbox (Printf.sprintf "Press to activate %s" name);
      (* Quand on appuie, met à jour le rdbg_mv_hook *)
      ignore(pushbox#event#connect#button_press
               ~callback: (fun _ ->
                   update_rdbg_hook name true;
                   e := goto_hook_exit !e;
                   e := goto_hook_call !e;
                   display_event gtext;
                   store !e.nb;
                   refresh ();
                   false));
      Hashtbl.add pushbox_map name pushbox
    )
    nodes_enabled;

  (* Des compteurs pour les modes automatiques *)
  let counter_grid = GPack.vbox ~packing:vbox#add () in
  let counter_scrolled_grid = GBin.scrolled_window ~border_width:10 ~hpolicy:`AUTOMATIC
      ~vpolicy:`AUTOMATIC ~height:400
      ~shadow_type:`OUT ~packing:counter_grid#add ()
  in
  let counter_scrolled_grid_box = GPack.vbox ~packing:counter_scrolled_grid#add () in
  let counter_line = GPack.hbox ~packing:counter_scrolled_grid_box#add () in
  let counter_line_ref = ref counter_line in
  i := 0;
  List.iter (fun (name, enabled) ->
      incr i;
      if !i > m then (
        i := 1;
        let new_counter_line = GPack.hbox ~packing:counter_scrolled_grid_box#add () in
        counter_line_ref := new_counter_line
      );

      let counter_container_frame = GBin.frame ~label:name ~packing:!counter_line_ref#add () in
      let counter_container =
        GPack.hbox ~homogeneous:true ~border_width: 2 ~spacing:0
          ~packing:counter_container_frame#add ()
      in
      let incr_container = GPack.vbox  ~packing:counter_container#add () in

      let counter = new GUtil.variable 0 in
      let decB = GButton.button ~label:"-" ~packing:incr_container#add () in
      let counter_lbl = GMisc.label  ~packing:counter_container#pack () in
      let incB = GButton.button ~label:"+" ~packing:incr_container#add () in
      let adj = GData.adjustment ~lower:0. ~upper:100. ~step_incr:1. ~page_incr:10. () in
      ignore (decB#connect#clicked
                ~callback:(fun () -> adj#set_value (float(counter#get-1))));
      ignore (incB#connect#clicked
                ~callback:(fun () -> adj#set_value (float(counter#get+1))));
      ignore (adj#connect#value_changed
                ~callback:(fun () -> counter#set (truncate adj#value)));
      ignore (counter#connect#changed
                ~callback:(fun n -> counter_lbl#set_text (string_of_int n)));
      counter#set 1;
      set_tooltip counter_container (Printf.sprintf "Set the priority of %s" name);
      Hashtbl.add counter_map name counter
    )
    nodes_enabled;
  let hide b = b#misc#hide() in
  let show b = b#misc#show() in
  let update_checkbox node enabled =
    match !daemon_kind with
    | Manual ->
      show step_button; show checkbox_grid;
      hide back_step_button;
      hide legitimate_button;
      hide undo_button;
      (match !oracle_button_ref with Some b -> hide b | None -> ());
      hide round_button; hide pushbox_grid;   hide counter_grid;
      let checkbox = try Hashtbl.find checkbox_map node with Not_found -> assert false in
      if enabled then
        show checkbox
      else (
        checkbox#set_active false; (* on decoche *)
        hide checkbox
      );
      checkbox#set_sensitive enabled
    | ManualCentral ->
      hide undo_button;
      hide legitimate_button;
      (match !oracle_button_ref with Some b -> hide b | None -> ());
      hide back_step_button; (* requires to store all the inputs from the last ckpt! *)
      hide step_button;
      hide round_button; hide checkbox_grid; hide counter_grid;
      show pushbox_grid;
      let pushbox = try Hashtbl.find pushbox_map node with Not_found -> assert false in
      if enabled then show pushbox else hide pushbox;
      pushbox#set_sensitive enabled

    | Synchronous | Distributed | Central | LocCentral ->
      show undo_button;
      if !daemon_kind <> Synchronous then show counter_grid;
      (match !oracle_button_ref with Some b -> show b | None -> ());
      show back_step_button; show step_button; show round_button;
      if not args.salut_mode then show legitimate_button; (* for the time being *)
      hide checkbox_grid; hide pushbox_grid;
  in
  let update_all_checkboxes () =
    (* only display the buttons of enabled nodes (for the manual daemon) *)
    let nodes_enabled = rdbg_nodes_enabled !e in
    List.iter (fun (name, enabled) -> update_checkbox name enabled) nodes_enabled
  in
  Hashtbl.add refresh_fun_tbl "" update_all_checkboxes;
  let set_dd_mode () =
    black gtext#buffer"==> Switch to a distributed daemon\n";
    daemon_kind := Distributed; refresh () in
  let set_sd_mode () =
    black gtext#buffer"==> Switch to a synchronous daemon\n";
    daemon_kind := Synchronous; refresh () in
  let set_cd_mode () =
    black_add gtext#buffer"==> Switch to a central daemon\n";
    daemon_kind := Central; refresh () in
  let set_lcd_mode () =
    black gtext#buffer"==> Switch to a locally central daemon\n";
    daemon_kind := LocCentral; refresh () in
  let set_manual_mode () =
    black gtext#buffer"==> Switch to a manual daemon\n";
    daemon_kind := Manual; refresh ()  in
  let set_manual_central_mode () =
    black gtext#buffer"==> Switch to a manual central daemon\n";
    daemon_kind := ManualCentral; refresh ()  in
  ignore(dk_dd#connect#clicked ~callback:set_dd_mode);
  ignore(dk_sd#connect#clicked ~callback:set_sd_mode);
  ignore(dk_cd#connect#clicked ~callback:set_cd_mode);
  ignore(dk_lcd#connect#clicked ~callback:set_lcd_mode);
  ignore(dk_manual#connect#clicked ~callback:set_manual_mode);
  ignore(dk_manual_central#connect#clicked ~callback:set_manual_central_mode);
  (* Affichage d'informations *)
  (*   gtext#buffer#set_text !gtext_content; *)
  (*   update_daemongui_tbl *)
  ()
(* End of draw_manual_daemons_buttons *)

let prefix =
  try
    let opam_dir = Unix.getenv "OPAM_SWITCH_PREFIX" in
    opam_dir
   with Not_found -> "$HOME/sasa/"

let lib_prefix = prefix ^ "/lib/sasa"
let libui_prefix = prefix ^ "/lib/rdbgui4sasa"

let oc_stdin = stdout
let ic_stdout = stdin


open GButton
  (* GTK3 *)
let main () =
  let _locale = GtkMain.Main.init () in
  let _thread = GtkThread.start () in
  let window = GWindow.window
      (*       ~width:320 ~height:240 *)
      ~title:"A rdbg GUI for sasa"
      ~show:true ()
  in
  let w = GPack.vbox ~packing:window#add () ~homogeneous:false in
  let box0 = GPack.vbox ~packing: w#add () in
  let box = GPack.vbox ~packing: box0#add () in
  let gbox = GPack.hbox ~packing: box#add () in
  let gbox2 = GPack.hbox ~packing: box#add () in
  let sw2 = GBin.scrolled_window ~border_width:10 ~shadow_type:`OUT ~height:250
      ~packing:box#add ()
  in
  set_tooltip sw2 "This window displays commands outputs";
  let text_out = GText.view ~wrap_mode:`CHAR ~height:250 ~editable:false
      ~packing: sw2#add () ~cursor_visible:true
  in
  let p str = black text_out#buffer str in
  (* It should be better to rely on the gtk event handler *)

  restart p ();

  let bbox0 = GPack.hbox ~packing: box0#add () in
  let bbox = GPack.hbox ~packing: box#add () in

  let change_label button str =
    let icon = button#image in
    button#set_label str;
    button#set_image icon;
    refresh ()
  in
  let button_cb display_event_flag store_flag cmd () =
    blue text_out#buffer "From ";
    let txt = Printf.sprintf "\n%s%!" (str_of_sasa_event false !e) in
    (*     text_out#buffer#set_text txt; *)
    blue_add text_out#buffer txt;
    cmd ();
    if store_flag then store !e.nb;
    if display_event_flag then display_event text_out;
    refresh ()
  in
  let button_cb_string cmd () =
    let txt = Printf.sprintf "\n%s" (cmd ()) in
    (*     text_out#buffer#set_text txt; *)
    blue text_out#buffer txt;
    let txt = Printf.sprintf "\n%s%!" (str_of_sasa_event false !e) in
    red_add text_out#buffer txt;
    refresh ()
  in

  let make_button0 stock lbl msg cmd =
    let butt = button ~use_mnemonic:true ~stock:stock ~packing:bbox0#add ~label:lbl () in
    set_tooltip butt msg;
    change_label butt lbl;
    ignore (butt#connect#clicked ~callback:cmd);
    butt
  in
  let make_button stock lbl msg cmd =
    let butt = button ~use_mnemonic:true ~stock:stock ~packing:bbox#add ~label:lbl () in
    set_tooltip butt msg;
    change_label butt lbl;
    ignore (butt#connect#clicked ~callback:cmd);
    butt
  in
  (*   Going  backward   requires   special  care   here  to   remain
       deterministic, because  of all the PRGs!  It concerns back_step,
       back_round, and undo. *)
  let goto_gui i =
    if not gui_mode then ( u(); d() )
    else
      let lnext e =
        if args.salut_mode then (
          update_daemongui_tbl e;
          let e = goto_hook_call e in
          e
        )
        else (
          update_daemongui_tbl e;
          if e.kind = Ltop then e.restore_state e.nb;
          let e = goto_hook_call e in
          e
        )
      in
      let ne =
        if i < !e.nb then
          rev_cond_gen !e (fun ne -> ne.nb = i) lnext (fun _ -> ())
        else if i > !e.nb then
          next_cond_gen !e (fun ne -> ne.nb = i) lnext
        else
          !e
      in
      e:=ne;
      ignore (round !e);
      d()
  in
  let undo_gui () =
    let i =
      match !redos with
      | _::i::t -> redos := i::t; i
      | i::[] -> i
      | [] -> 1
    in
    goto_gui i
  in
  let back_round_button =
    button ~packing:bbox0#add ~stock:`MEDIA_PREVIOUS ~use_mnemonic:true
      ~label:"back round" ()
  in
  let back_step_button = button ~use_mnemonic:true ~stock:`GO_BACK ~packing:bbox0#add () in
  set_tooltip back_step_button "Move BACKWARD to the previous STEP";
  change_label back_step_button "Ste_p";

  let undo_button  = make_button0 `UNDO "_Undo" "Undo the last move"
      (button_cb true false undo_gui)
  in
  let step_button = button ~use_mnemonic:true ~packing:bbox0#add ~stock:`GO_FORWARD ()  in
  let round_button =
    button  ~use_mnemonic:true ~stock:`MEDIA_FORWARD ~packing:bbox0#add ~label:"round" ()
  in
  let legitimate_button =
    button ~use_mnemonic:true ~stock:`GOTO_LAST ~packing:bbox0#add ()
  in
  set_tooltip legitimate_button
    "Move FORWARD until a legitimate configuration is reached (silence by default)";
  (*   let image = GMisc.image ~file:(libui_prefix^"/chut_small.svg") () in *)
  (*   legitimate_button#set_image image#coerce;  *)

  if gui_mode then draw_manual_daemons_buttons p text_out w step_button
      back_step_button round_button legitimate_button undo_button;
  let update_daemongui_tbl_and_redraw =
    if gui_mode then update_daemongui_tbl
    else
      (fun _ -> ())
  in
  let a_gui_step e =
    (* set the daemongui_tbl and step to the next event where the user
       is asked to choose whom to activate *)
    if gui_mode then (
      update_daemongui_tbl_and_redraw e;
      let e = goto_hook_exit e in
      let e = goto_hook_call e in
      e
    )
    else (* internal daemon mode *)
      let e = sasa_step e in
      store e.nb;
      print_event e;
      e
  in
  let postambule () = (* necessary to get the round nb rigth! *)
    if not args.salut_mode && not (check_point_event !e) then
      e := next_cond !e check_point_event
  in
  let next_step_gui () =
    if not (is_silent ~dflt:false !e)
    (* ??? || (not args.salut_mode && !e.kind <> Ltop) *)
    then (
      e := a_gui_step !e;
      d()
    )
  in
  let next_round_gui () =
    if gui_mode then (
      let rec next_round_gui_loop rn =
        if is_silent ~dflt:false !e then (postambule ())
        else e := a_gui_step !e;
        if (get_round !e) || is_silent ~dflt:false !e then () else (
          next_round_gui_loop rn
        );
      in
      next_round_gui_loop (get_round_nb !e);
      if
        args.salut_mode && !e.name<>"mv_hook"
        && !e.kind<>Call && not (is_silent ~dflt:false !e)
      then
        e:= goto_hook_call !e
    )
    else ( (* internal daemon mode *)
      e := next_cond_gen !e
          (fun e -> e.kind = Ltop && (round e || is_legitimate e))
          sasa_next
    )
  in
  let back_step_gui () =
    if not gui_mode then ( (* internal daemon mode *)
      e := back_step !e;
      pe()
    )
    else (
      if args.salut_mode then
        let lnext e =
          update_daemongui_tbl e; (* necessary to get the PRGs rigth during backward moves *)
          let e = goto_hook_call e in
          e
        in
        let ne =
          rev_cond_gen !e
            (fun ne ->
               if ne.step = !e.step-1 && ne.kind = !e.kind then true else (
                 Printf.printf "%d: ne.step=%d (!e.step-1)=%d\n%!" ne.nb ne.step (!e.step-1);
                 false
               )
            )
            lnext (fun _ -> ())
        in
        e:=ne
      else (* custom sasa mode *)
        let lnext e =
          update_daemongui_tbl e;
          if e.kind = Ltop then
            (* Necessary for  reproductibility because update_daemongui_tbl
               calls Random.int (via Daemon) which changes the PRGS! *)
            e.restore_state e.nb;
          let e = goto_hook_call e in
          e
        in
        let ne = rev_cond_gen !e
            (fun ne -> ne.step = !e.step-1 && check_point_event ne)
            lnext  (fun _ -> ())
        in
        e:=ne
    )
  in
  let back_round_gui () =
    if not gui_mode then ( (* internal daemon mode *)
      e:=goto_last_ckpt !e.nb
    )
    else
      (
        let ne1 = goto_last_ckpt !e.nb in
        let ne2 = if round !e then ne1 else goto_last_ckpt ne1.nb in
        if ne1.nb = !e.nb then
          (* already at the first event. Do nothing *)
          ()
        else if ne1.nb = ne2.nb then (
          (* Still in the first round. Go at the beginning *)
          e:=ne1
        )
        else (
          (* From round n>1, go to round n-1 *)
          e:=ne2;
          if !e.kind <> Call && not args.salut_mode then
            (* only the first event is already a call*)
            e:=goto_hook_call !e
            (* else if args.salut_mode then e:=??? *)
        )
      );
    refresh ()
  in
  ignore (back_step_button#connect#clicked
            ~callback:(button_cb true true back_step_gui));

  let rec legitimate_gui () =
    if gui_mode then (
      if is_silent ~dflt:false !e then () else e := a_gui_step !e;
      if is_legitimate !e || is_silent ~dflt:false !e then (
        postambule (); d()
      )
      else legitimate_gui ();
    )
    else (* internal daemon mode *)
      e:= next_cond !e (fun le -> is_legitimate le && le.kind=Ltop)
  in
  change_label legitimate_button "_Legit";
  ignore (legitimate_button#connect#clicked ~callback:
            (button_cb true true legitimate_gui)
         );
  if daemon_mode()=GuiManual then legitimate_button#misc#hide();

  set_tooltip step_button "Move FORWARD to the next STEP";
  change_label step_button "_Step";
  ignore (step_button#connect#clicked ~callback:(button_cb true true next_step_gui));
  set_tooltip round_button "Move FORWARD to the next ROUND";
  change_label round_button "_Round";
  ignore (round_button#connect#clicked ~callback:(button_cb true true next_round_gui));
  set_tooltip back_round_button "Move BACKWARD to the previous ROUND";
  change_label back_round_button "Roun_d";
  ignore (back_round_button#connect#clicked
            ~callback:(button_cb true true back_round_gui));

  if args.oracles <> [] then (
    let oracle_button =
      make_button `OK "_Oracle" "Move FORWARD until an oracle is violated"
        (button_cb_string
           (fun () -> let str = viol_string () in
             e:=goto_hook_call !e; d(); store !e.nb; str))
    in
    oracle_button#misc#hide(); (* indeed, in the defaut mode (manual central), it should be hided *)
    oracle_button_ref := Some oracle_button
  );
  if not args.salut_mode then (
    let _ =
      make_button `ADD "Inject _Fault" "Inject a fault (the Algo.fault_function should be set)"
        (button_cb true true (inject p))
    in
    ());
  let _ = make_button `GOTO_FIRST "Restar_t" "Restart from the beginning"
      (button_cb true true (restart p))
  in
  let _ = make_button `REFRESH "_New Seed" "Restart from the beginning using a New Seed"
      (button_cb true true (restart_new_seed p))
  in
  let graph () =
    let graph_button = button  ~use_mnemonic:true  ~packing:bbox#add () in
    set_tooltip graph_button "Visualize the Topology states: Green=Enabled ; Gold=Active";
    let image = GMisc.image ~file:(libui_prefix^"/graph_small.png") () in
    graph_button#set_image image#coerce;
    ignore (graph_button#connect#clicked ~callback:(button_cb false false graph_view));

  in
  graph ();
  let _ = make_button `MEDIA_PLAY "Sim2_chro" "Launch sim2chro on the generated data (so far)"
      (button_cb false false sim2chro)
  in
  let _ = make_button `MEDIA_PLAY "_Gnuplot" "Launch gnuplot-rif on the generated data (so far)"
      (button_cb false false gnuplot)
  in
  let _ = make_button `INFO "_Info" "Get information about the current session"
      (button_cb_string info_string)
  in

  let _ = make_button `QUIT "_Quit" "Quit RDBGUI" (fun() -> p "bye"; Stdlib.exit 0) in

  let sw1 = GBin.scrolled_window ~border_width:10 ~shadow_type:`IN  ~height:130 ~width:50
      ~packing:w#add ()
  in
  sw1#misc#set_tooltip_text "This window displays the rdbg.log file";
  let text_in = GText.view ~wrap_mode:`CHAR ~height:250 ~editable:true ~width:50
      ~packing: sw1#add () ~cursor_visible:true
  in
  let rdbg_log = open_in "rdbg.log" in
  create_tags text_in#buffer;
  let rec read_rdbglog () =
    try
      let str = input_line rdbg_log in
      text_in#buffer#insert  ~tag_names:["black_foreground"] (str^"\n");
      let end_mark = text_in#buffer#create_mark text_in#buffer#end_iter in
      text_in#scroll_to_mark (`MARK end_mark);
      read_rdbglog ()
    with End_of_file ->
      Unix.sleepf 1.0;
      read_rdbglog ()
  in
  let _ = Thread.create read_rdbglog () in


  let dot_button = radio_button ~packing:gbox#add ~label:"dot" () in
  let make_but active lbl = radio_button ~packing:gbox#add
      ~active:active ~group:dot_button#group ~label:lbl ()
  in
  let fd_button = make_but false "fdp" in
  let sf_button = make_but false "sfdp" in
  let ne_button = make_but true "neato" in
  let tw_button = make_but false "twopi" in
  let ci_button = make_but false "circo" in
  let pa_button = make_but false "patchwork" in
  let os_button = make_but false "osage" in

  let connect button str cmd =
    ignore (button#connect#clicked
              ~callback:(fun () -> p ((button#misc#tooltip_text)^"\n"^(help_string str));
                          dot_view := cmd; !dot_view()))
  in
  let have_parent () = (* is there a parent field in the state ? *)
    List.exists (fun (v,_) -> Str.string_match (Str.regexp ".*_par.*") v 0) !e.data
  in
  if have_parent () then (
    let make_but lbl = GButton.radio_button ~packing:gbox2#add
        ~group:dot_button#group ~label:lbl ()
    in
    let par_dot_button = make_but "dot*" in
    let par_fd_button = make_but "fdp*" in
    let par_sf_button = make_but "sfdp*" in
    let par_ne_button = make_but "neato*" in
    let par_tw_button = make_but "twopi*" in
    let par_ci_button = make_but "circo*" in
    let par_pa_button = make_but "patchwork*" in
    let par_os_button = make_but "osage*" in
    set_tooltip par_dot_button "Use dot, but show only links to the parent";
    set_tooltip par_fd_button  "Use fdp, but show only links to the parent";
    set_tooltip par_sf_button  "Use sfdp, but show only links to the parent";
    set_tooltip par_ne_button  "Use neato, but show only links to the parent";
    set_tooltip par_tw_button  "Use twopi, but show only links to the parent";
    set_tooltip par_ci_button  "Use circo, but show only links to the parent";
    set_tooltip par_pa_button  "Use patchwork, but show only links to the parent";
    set_tooltip par_os_button  "Use osage, but show only links to the parent";
    connect par_dot_button "d_par" d_par;
    connect par_fd_button "fd_par" fd_par;
    connect par_sf_button "sf_par" sf_par;
    connect par_ne_button "ne_par" ne_par;
    connect par_tw_button "tw_par" tw_par;
    connect par_ci_button "ci_par" ci_par;
    connect par_pa_button "pa_par" pa_par;
    connect par_os_button "os_par" os_par;
  );
  set_tooltip dot_button "Use the dot engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip fd_button "Use the fdp engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip sf_button "Use the sfdp engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip ne_button "Use the neato engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip tw_button "Use the twopi engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip ci_button "Use the circo engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip pa_button "Use the patchwork engine to display the graph (cf https://graphviz.org/docs/layouts/)";
  set_tooltip os_button "Use the osage engine to display the graph (cf https://graphviz.org/docs/layouts/)";

  connect dot_button "d" dot;
  connect fd_button "fd" fd;
  connect sf_button "sf" sf;
  connect ne_button "ne" ne;
  connect tw_button "tw" tw;
  connect ci_button "ci" ci;
  connect pa_button "pa" pa;
  connect os_button "os" os;

  ignore (window#connect#destroy ~callback: (
      fun () ->
        quit (); (* quit rdbg, this will stop the readloop below *)
        Main.quit () (* terminate gtk *)
    ));
  Seed.replay_seed := true;
  ignore(Seed.get dotfile);
  refresh ()

let gui = main
(* todo
- les oracles sont violés en silence (lancer un pop-up ?)
- couper les grosses fonctions en morceaux
- cacher les messages issus du #use
- lire les commandes dans text_in (comment ? c'est rdbgtop qui lance gtk maintenant...)
- reglage de la taille des boites
- utiliser les GEdit.spin_button ?
    cf lablgtk/examples/spin.ml
    https://lazka.github.io/pgi-docs/Gtk-3.0/classes/SpinButton.html#Gtk.SpinButton
 *)
;;

gui();;
