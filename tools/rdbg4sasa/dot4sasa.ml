(** Generating dot files dynamically from rdbg *)

open Graph
open Graph.Dot_ast
open Data
open RdbgEvent
open Sasacore
open Topology

type process = {
  name: string;
  actions: (string * bool * bool) list; (* (action name, enabled, active) *)
  vars: (string * Data.v) list (* pid local vars*)
}

let (is_parent: string -> string -> int -> RdbgEvent.t -> bool) =
  fun par_var a i e ->
    (* XXX marche ssi une variable s'appelle par!
       je devrais au moins generaliser avec l'existence
       d'une variable de type parent (et encore)
    *)
      match  List.assoc_opt (a^"_"^par_var) e.data with
      | None -> false
      | Some (I j) -> j > -1 && i = j
      | _ -> false

let (get_processes : RdbgEvent.t -> process list) =
  fun e ->
  (*    if e.kind <> Ltop then (
        print_string "dot should be called from Ltop event\n";
        failwith "exit dot"
        );*)
  let l = List.map (fun (x,v) -> Str.split (Str.regexp "_") x, v) e.data in
  let rec sortv (enab, other) (x,v) =
    match args.salut_mode,x with
    | false,"Enab"::pid::tail -> (pid, String.concat "_" tail,v)::enab, other
    | false,pid::tail -> enab, (pid,(String.concat "_" tail),v)::other
    | _,[] -> assert false
    | true, ([_] | [_; _]) -> enab, other
    | true, "Enab"::p::id::tail -> (p^id, String.concat "_" tail,v)::enab, other
    | true, p::id::id2::tail ->
      try
        let _i = int_of_string id in
        enab, (p^id,(String.concat "_" (id2::tail)),v)::other
      with _ ->
      try
        let _i = int_of_string id2 in
        enab, (p^id2,(String.concat "_" (id::tail)),v)::other
      with _ -> assert false
  in
  let enab, other = List.fold_left sortv ([],[]) l in
  let rec (build_pidl: process list -> (string * string * Data.v) list ->
           (string * string * Data.v) list -> process list) =
    fun pidl enab other ->
      match enab with
      | [] -> pidl
      | (pid, _, _)::_ ->
        let enab_pid_list, enab =
          List.partition (fun (pid0,_,_) -> pid=pid0) enab
        in
        let other_pid, other =
          List.partition (fun (pid0,_,_) -> pid=pid0) other
        in
        let acti_pid, vars_pid =
          List.partition
            (fun (_,n,_) -> List.exists (fun (_,n2,_) -> n2=n) enab_pid_list)
            other_pid
        in
        let get_actions (_, n, enabv) =
          match List.find_opt (fun (_,n0, _) -> n=n0) acti_pid with
          | Some (_,_,activ) ->
            (n, enabv = Data.B true, activ = B true)
          | None ->
            (n, enabv = Data.B true, false)
        in
        let pid = {
          name = pid;
          actions = List.map get_actions enab_pid_list;
          vars = List.map (fun (_,n,v) -> n,v) vars_pid;
        }
        in
        build_pidl (pid::pidl) enab other
  in
  let pidl = build_pidl [] enab other in
  List.rev pidl

(* If nodes have a var that is of type neighbor, we suppose that it is
   used to compute a spanning tree, a draws edges accordingly in the
   dot output. If no var of type neighbor exists, we return the emty
   string here and no edges will be displayed. if several vars of type
   neighbor exist, we take the first one.
let parent_var_name = ref None (* memoize it! *)
let (get_parent_var_name: nodes list -> string) =
  fun nl ->
    match !parent_var_name with
    | Some x -> x
    | None ->
      let rec search = function
        | [] -> ""
        | n::tail -> (
            let ml_file = (Filename.chop_extension file) ^ .ml in

            match List.find_opt (fun (vn,vt) -> vt = Algo.Nt) n with
            | None -> search tail
            | Some (vn,_) -> vn
          )
      in
      let vn = search pl in
      parent_var_name := Some vn;
      vn
      *)


let val_to_string_trunc v =
  let res = Data.val_to_string string_of_float v in
  if String.length res > 50 (* XXX should be a sasarg *) then
    (String.sub res 0 50^ "...")
  else
    res
(* Compute  a dot from the  content of e.data. if  [only_parent], only
   display the arcs of the parent, where the parent is an integer held
   in a variable named "par". if no such variable exist in the current
   algo, no edges are drawn.  *)
let to_pdf engine par_var only_parent rn g f e =
  let nodes = g.nodes in
  let (pidl : process list) = get_processes e in
  let n = List.length pidl in
  let ln = Topology.get_nb_link g in
  let oc = open_out ("sasa-"^f) in
  let nodes_decl =
    String.concat "\n"
      (List.map
         (fun pid ->
            let color =
              if List.exists (fun (_,_,a) -> a) pid.actions then
                "fillcolor=gold,style=filled,"
              else
              if List.exists (fun (_,e,_) -> e) pid.actions then
                "fillcolor=green,style=filled,"
              else ""
            in
            let enabled = String.concat ","
                (List.map
                   (fun (n,_,_) -> n)
                   (List.filter (fun (_,e,_) -> e) pid.actions))
            in
            let enabled = if enabled = "" then "" else (enabled^"|") in
            let loc = String.concat "|"
                (List.map (fun (n,v) ->
                     Printf.sprintf "%s=%s" n
                       (val_to_string_trunc v))
                    pid.vars
                )
            in
            if (n>200 || ln > 5000) && enabled <> "" then
              Printf.sprintf " %s [shape=point]"  pid.name
            else
              Printf.sprintf " %s [%slabel=\"%s|{%s%s}\"] "
              pid.name color pid.name enabled loc
         )
         pidl
      )
  in
  let trans =
    List.flatten
      (List.map
         (fun n ->
            let l = g.succ n.id in
            List.mapi (fun i t ->
                if g.directed then
                  Printf.sprintf "%s -> %s" t n.id
                else if is_parent "par" n.id i e  then
                  Printf.sprintf "%s -> %s" n.id t
                else if n.id < t then (* to avoid duplication in undir graphs *)
                  Printf.sprintf "%s -- %s" n.id t
                else
                  Printf.sprintf "%s -- %s" t n.id
              )
              l
         )
         nodes
      )
  in
  let trans = List.sort_uniq compare trans in
  let is_directed str =
    try
      ignore (Str.search_forward (Str.regexp "->") str  0);
      true
    with Not_found -> false
  in
  let trans_dir,trans_undir = List.partition is_directed trans in
  let trans_dir_str = String.concat "\n" trans_dir in
  let trans_undir_str = String.concat "\n" trans_undir in
  let trans_undir_str =
    Str.global_replace (Str.regexp "--") "->" trans_undir_str in
  let trans_str =
    (*     if trans_dir_str = "" then trans_undir_str else *)
    if only_parent then
        Printf.sprintf "subgraph dir {\n\t%s} " trans_dir_str
      else
      Printf.sprintf "subgraph dir {\n\t%s}
subgraph undir {\n\t edge [dir=none]\n%s} " trans_dir_str trans_undir_str
  in
  let pot =  match List.assoc_opt "potential" e.data with
  | Some F f -> Printf.sprintf " potential=%.1f" f
  | _ -> ""
  in
  Printf.fprintf oc
    "digraph %s {\nlabel=\"%s \nRound %d / Step %d\n%s\"\nnode [shape=record];\n%s\n%s\n}\n"
    "g" f rn e.step pot
    nodes_decl trans_str;
  flush oc;
  close_out oc;
  if Sys.command (Printf.sprintf "%s -Tpdf sasa-%s -o sasa-%s.pdf&" engine f f) > 0
  then (

    flush stdout
  )

;;

let dot = to_pdf "dot" "_par" ;;
let neato = to_pdf "neato"  "_par" ;;
let twopi = to_pdf "twopi"  "_par" ;;
let circo = to_pdf "circo"  "_par" ;;
let fdp = to_pdf "fdp"  "_par" ;;
let sfdp = to_pdf "sfdp"  "_par" ;;
let patchwork = to_pdf "patchwork"  "_par";;
let osage = to_pdf "osage"  "_par" ;;
(*
  From the dot man:
       dot - filter for drawing directed graphs
       neato - filter for drawing undirected graphs
       twopi - filter for radial layouts of graphs
       circo - filter for circular layout of graphs
       fdp - filter for drawing undirected graphs
       sfdp - filter for drawing large undirected graphs
       patchwork - filter for squarified tree maps
       osage - filter for array-based layouts

let _ =
  add_doc_entry
    "dot" "unit -> unit" "Generate a pdf (out of the current) topology with dot"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "neato" "unit -> unit" "Generate a pdf (out of the current) topology with neato"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "twopi" "unit -> unit" "Generate a pdf (out of the current) topology with twopi"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "circo" "unit -> unit" "Generate a pdf (out of the current) topology with circo"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "fdp" "unit -> unit" "Generate a pdf (out of the current) topology with fdp"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "sfdp" "unit -> unit" "Generate a pdf (out of the current) topology with sfdp"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "patchwork" "unit -> unit" "Generate a pdf (out of the current) topology with patchwork"
    "sasa" "rdbg-utils/dot.ml";
  add_doc_entry
    "osage" "unit -> unit" "Generate a pdf (out of the current) topology with osage"
    "sasa" "rdbg-utils/dot.ml";;
  *)


(***********************************************************************)

let get_removable pl =
  let pl = List.filter
      (fun p ->
         (List.exists (fun (_,_,acti) -> acti) p.actions) ||
         (List.for_all (fun (_,enab,_) -> (not enab)) p.actions)
      )
      pl
  in
  List.map (fun p -> p.name) pl
(***********************************************************************)
