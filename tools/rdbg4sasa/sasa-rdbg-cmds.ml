(* This file defines sasa specific commands.

It is meant  to be loaded after the (rdbg-generated) ".rdbg-cmds.ml" file.
*)
open RdbgMain
#require "sasacore";;

open Sasacore.Topology;;
#use "dot4sasa.ml";;

let verbose = ref false
let p =
  try Topology.read dotfile
  with _ -> failwith "This is not a sasa rdbg session!";;

let is_silent ?(dflt=true) e =
  match List.assoc_opt "silent" e.data with
  | Some B b -> b
  | _ ->
    if !verbose then
      Printf.printf "The silent value is not available at event %d\n%!" e.nb;
     dflt

let is_legitimate e =
  match List.assoc_opt "legitimate" e.data with
  | Some B b -> b
  | _ -> failwith ("legitimate not available at this event: "^(string_of_event e))

(**********************************************************************)
let round_cpt = ref 0 (* make the round nb available at ell events *)
let is_round = ref false (* ditto *)

let get_round_nb e =
  match List.assoc_opt "round_nb" e.data with
  | Some I i -> round_cpt:=i; i
  | _ -> !round_cpt

let get_round e =
  match List.assoc_opt "round" e.data with
  | Some B b -> is_round := b; b
  | _ -> !is_round

let enabled pl = (* returns the enabled processes *)
  let el = List.filter
    (fun p -> List.exists (fun (_,enab,_) -> enab) p.actions)
    pl
  in
  List.map (fun p -> p.name) el

(* called at each event via the time-travel hook *)
let (round : RdbgEvent.t -> bool) =
  fun e ->
  let get_it =
    if args.salut_mode then
      e.kind=Call && e.name = "mv_hook"
    else
      true
      (* e.kind=Exit && e.name = "sasa" *)
  in
  if get_it then get_round e else false

(**********************************************************************)
let d_par () = dot true (get_round_nb !e) p dotfile !e;;
let dot_par () = dot true (get_round_nb !e) p dotfile !e;;
let ne_par () = neato true (get_round_nb !e) p dotfile !e;;
let tw_par () = twopi true (get_round_nb !e) p dotfile !e;;
let ci_par () = circo true (get_round_nb !e) p dotfile !e;;
let fd_par () = fdp true (get_round_nb !e) p dotfile !e;;
let sf_par () = sfdp true (get_round_nb !e) p dotfile !e;;
let pa_par () = patchwork true (get_round_nb !e) p dotfile !e;;
let os_par () = osage true (get_round_nb !e) p dotfile !e;;

let dot () = dot false (get_round_nb !e) p dotfile !e;;
let ne () = neato false (get_round_nb !e) p dotfile !e;;
let tw () = twopi false (get_round_nb !e) p dotfile !e;;
let ci () = circo false (get_round_nb !e) p dotfile !e;;
let fd () = fdp false (get_round_nb !e) p dotfile !e;;
let sf () = sfdp false (get_round_nb !e) p dotfile !e;;
let pa () = patchwork false (get_round_nb !e) p dotfile !e;;
let os () = osage false (get_round_nb !e) p dotfile !e;;

(* To change the default dot/graph viewer:
   dot_view := ci;;
 *)
let dot_view : (unit -> unit) ref =
  ref ( (* Make our best to chose the most sensible default viewer *)
      if String.length dotfile > 4 && String.sub dotfile 0 4 = "ring" then ci else
        if Algo.is_directed () then dot else ne)

let d () = !dot_view ()

(**********************************************************************)
let sasa_next e =
  if e.step = args.step_nb then d ();
  let ne = e.next () in
  List.iter
    (fun str -> if str<>"print_event" then (RdbgStdLib.get_hook str) ne)
    (RdbgStdLib.list_hooks ());
  ne

let next_cond e =
  if e.step = args.step_nb then d ();
  next_cond e

let next_cond_gen e p n =
  if e.step = args.step_nb then d ();
  next_cond_gen e p n

let next_round e =
  next_cond_gen e (fun e -> round e || is_legitimate e) sasa_next


let back_step e =
  let e = rev_cond_gen e (fun ne -> ne.kind = e.kind && ne.name = e.name)
    sasa_next (fun _ -> ())
  in
  store e.nb;
  e

(**********************************************************************)
(* redefine (more meaningful) step and back-step for sasa *)
let sasa_step e = next_cond e (fun ne -> ne.kind = e.kind && ne.name = e.name)
let s () = e:=sasa_step !e ; emacs_udate !e; store !e.nb;pe()
let b () =
  let ne = back_step !e in
  e:=ne ;
  emacs_udate !e; store !e.nb;pe()

let sd () = s();!dot_view();;
let bd()= e:=prev !e ; emacs_udate !e; pe();!dot_view();;


let nr () =
  let ne = next_round !e in
  e := ne;
  store !e.nb; !dot_view ();;

let pr () =
  e:=goto_last_ckpt !e.nb;
  !dot_view ();
  store !e.nb

(* I need to overrides those *)


(**********************************************************************)
(* change the sasa seed in the rdbgplugin

I've suposed that all Ocaml plugins called from sasa have a --seed option. Is it true?
It is the case for sasa and lutin at least.
 *)

let (string_to_string_list : string -> string list) =
  fun str ->
  Str.split (Str.regexp "[ \t]+") str

let (change_ocaml_plugin_seed: int -> RdbgPlugin.t -> RdbgPlugin.t) =
  fun seed rdbgplugin ->
  if args.salut_mode then ((* nothing to do here *) rdbgplugin) else
    let new_sasa_call = Printf.sprintf "%s --seed %d" rdbgplugin.id seed in
    Printf.printf "%s\n%!" new_sasa_call;
    Sasa.SasaRun.make (Array.of_list (string_to_string_list new_sasa_call))


let (change_plugin_seed : int -> RdbgArg.reactive_program -> RdbgArg.reactive_program) =
  fun seed plugin ->
  match plugin with
  | Ocaml rdbgplugin -> Ocaml (change_ocaml_plugin_seed seed rdbgplugin)
  | o -> o

let change_sasa_seed seed =
  args.suts <- List.map (change_plugin_seed seed) args.suts;
  args.envs <- List.map (change_plugin_seed seed) args.envs

(**********************************************************************)

(* won't work in semi-auto modes, but the buttons are hided *)
let u () = undo (); ignore (round !e);;
let r () =
  let seed = Seed.get dotfile in
  Seed.set seed;
  Printf.printf "Restarting using the seed %d\n" seed;
  !e.RdbgEvent.reset();
  Round.reinit();
  e:=RdbgStdLib.run ~call_hooks:true ();
  redos := [1];
  (* ignore (round !e); *)
  (* if the first event is not a round, add it as a check_point *)
  (* if !ckpt_list = [] then *)
  ckpt_list := [!e];;

(**********************************************************************)
(* Fault injection  *)

let inject_a_fault () =
  let seed = Seed.get dotfile in
  Seed.set seed;
  Printf.printf "Restarting using the seed %d\n" seed;
  Round.reinit_mask();
  !e.restore_state min_int;
  redos := [1];
  ckpt_list := [!e];;


let iaf = inject_a_fault

(**********************************************************************)
(* Move forward until silence *)

let is_silent_or_end e = is_silent ~dflt:false e || e.step = args.step_nb
let goto_silence e = next_cond e is_silent_or_end
let silence () = e:=goto_silence !e; !dot_view () ;;

let _ = add_doc_entry
          "silence" "unit -> unit" "Move forward until is_silent returns true"
          "sasa" "sasa-rdbg-cmds.ml";
        add_doc_entry
          "is_silent" "RdbgEvent.t -> bool"
          "does the event correspond to a silent configuration? (i.e., no enable node)"
          "sasa" "sasa-rdbg-cmds.ml";;


let is_legitimate_or_end e = is_legitimate e || e.step = args.step_nb
let goto_legitimate e = next_cond e is_legitimate_or_end
let legitimate () =  e:=goto_legitimate !e; !dot_view ();;
let _ =
  add_doc_entry
    "legitimate" "unit -> unit"
    "  Move forward until a legitimate configuration is reached (uses 'silence' by default)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry
    "is_legitimate" "RdbgEvent.t -> bool"
    "does the event correspond to a legitimate configuration?"
    "sasa" "sasa-rdbg-cmds.ml";;

(**********************************************************************)
(* print_event tuning *)

(* split  the vars into returns  (the enab vars, the  activated vars,
   the other vars) nb: in the "Enab" prefix is removed from enab vars
   names; ie we leave only the pid and the action name *)
type s = (string * string * Data.v)
let split_data (l:Data.subst list) : s list * s list * s list =
  let l = List.map (fun (x,v) -> Str.split (Str.regexp "_") x, v) l in
  let rec sortv (enab, other) (x,v) =
    match x with
    | "Enab"::pid::tail -> (pid, String.concat "_" tail,v)::enab, other
    | pid::tail -> enab, (pid,(String.concat "_" tail),v)::other
    | [] -> assert false
  in
  let enab, other = List.fold_left sortv ([],[]) l in
  let acti_names = List.map (fun (pid, n, v) -> pid, n) enab in
  let act, vars =
    List.partition (fun (pid,n, _) -> List.mem (pid,n) acti_names) other
  in
  enab, act, vars


let only_true l = List.filter (fun (_,_, v)  -> v = B true) l
(* Only print the active process values *)
let str_of_sasa_event short e =
  let enab, act, vars = split_data e.data in
  let enab = only_true enab in
  let act = only_true act in
  let act_pid = List.map (fun (pid,_,_) -> pid) act in
  let vars = List.filter (fun (pid, _,_) -> List.mem pid act_pid) vars in
  let _to_string (pid, n, _) = Printf.sprintf "%s_%s" pid n in
  let to_string_var (pid, n, v) =
    Printf.sprintf "%s_%s=%s" pid n (Data.val_to_string string_of_float v)
  in
  let pot = match List.assoc_opt "potential" e.data with
    | Some F f -> Printf.sprintf " => potential %.1f" f
    | _ -> ""
  in
  let leg = match List.assoc_opt "legitimate" e.data with
    | Some B true -> Printf.sprintf "The current configuration is legitimate\n"
    | Some B false -> ""
    | _ -> ""
  in
  let silent = if enab = [] then "The current configuration is silent\n"  else "" in
  let vars = List.rev vars in
  (* restore_round_nb e; *)
  (if short then
     Printf.sprintf "[round %i, step %i%s] %s %s\n" (get_round_nb e) e.step pot
       (if e.step <> e.nb then (":" ^ (string_of_int e.nb)) else "")
       (String.concat " " (List.map to_string_var vars))
   else
     Printf.sprintf "Round %i - Step %i%s%s\n%s%s\n%s%s" (get_round_nb e) e.step
       (if e.step <> e.nb then (" - Event " ^ (string_of_int e.nb)) else "") pot
       (RdbgStdLib.string_of_event e)
       (if vars = [] then "" else
          ("Active node states: "^(String.concat " " (List.map to_string_var vars))))
       leg silent
  )

let print_sasa_event short e =
  if e.kind <> Ltop then print_event e else
    (
      print_string (str_of_sasa_event short e);
      flush stdout
    )

let _ =
  del_hook "print_event";
  add_hook "print_event" (print_sasa_event true)

(* shortcuts to the default and sasa event printers *)
let pe () = print_event !e;;
let spe () = print_sasa_event true !e;;
let spel () = print_sasa_event false !e;;

(**********************************************************************)
(* Goto to the next Lustre oracle violation *)
let goto_next_false_oracle e =
  next_cond e (fun e -> e.kind = Exit && e.lang = "lustre" &&
                          List.mem ("ok", Bool) e.outputs  &&
                            not (vb "ok" e))

let viol_string () =
  if args.oracles <> [] then (
    e:=goto_next_false_oracle !e; !dot_view (); "An oracle has been violated. Cf the .rif file"
  ) else (
    "No oracle is set."
  )
;;
let viol () = Printf.printf "%s\n%!" (viol_string ())

let _ = add_doc_entry
          "viol" "unit -> unit" "Move forward until the oracle is violated"
          "sasa" "sasa-rdbg-cmds.ml"


(**********************************************************************)
(* Potential  *)

let potential_opt () = List.assoc_opt "potential" !e.data

let potential () =
  match potential_opt () with
  | Some F f -> Some f
  | _ -> None

let _ = add_doc_entry
          "potential" "unit -> float option" "returns the current potential if available"
          "sasa" "sasa-rdbg-cmds.ml"

(**********************************************************************)
(* overide the default checkpointing at rounds

nb : this is overridden in gtkgui.ml
*)
let _ = check_ref := fun e -> e.nb = 1 || (e.kind=Exit && e.name="sasa" && round e);;

(**********************************************************************)
let _ =
  add_doc_entry "d"
    "unit -> unit" "update the current network with dot" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "ne"
    "unit -> unit" "update the current network with neato" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "tw"
    "unit -> unit" "update the current network with twopi" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "ci"
    "unit -> unit" "update the current network with circo" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "fd"
    "unit -> unit" "update the current network with fdp" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "sf"
    "unit -> unit" "update the current network with sfdp" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "pa"
    "unit -> unit" "update the current network with patchwork" "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "os"
    "unit -> unit" "update the current network with osage" "sasa" "sasa-rdbg-cmds.ml";

  add_doc_entry "sd" "unit -> unit"
    "go to the next step and update the network with one of the GraphViz tools" "sasa"
    "sasa-rdbg-cmds.ml";
  add_doc_entry "bd" "unit -> unit" "go to the previous step and update the network"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "nr" "unit -> unit" "go to the next round (or legit) and update the network"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "pr" "unit -> unit" "go to the previous round  (or legit) and update the network"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "d_par" "unit -> unit" "cf d (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "ne_par" "unit -> unit" "cf ne (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "tw_par" "unit -> unit" "cf tw (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "ci_par" "unit -> unit" "cf ci (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "fd_par" "unit -> unit" "cf fd (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "sf_par" "unit -> unit" "cf sf (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "pa_par" "unit -> unit" "cf pa (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  add_doc_entry "os_par" "unit -> unit" "cf os (for topology with a parent field)"
    "sasa" "sasa-rdbg-cmds.ml";
  ()

let l () =
  l();
  print_string (
      "
       - sasa commands
       + Display network with GraphViz tools
       d: "^(RdbgMain.doc_msg "d")^"
       ne: "^(RdbgMain.doc_msg "ne")^"
       tw: "^(RdbgMain.doc_msg "tw")^"
       ci: "^(RdbgMain.doc_msg "ci")^"
       fd: "^(RdbgMain.doc_msg "fd")^"
       sf: "^(RdbgMain.doc_msg "sf")^"
       pa: "^(RdbgMain.doc_msg "pa")^"
       os: "^(RdbgMain.doc_msg "os")^"

       nb: for algorithms that have a field named 'par', you can try
           one of the following (which only draw the parent arcs)
       d_par: "^(RdbgMain.doc_msg "d")^" (parent arcs only)
       ne_par: "^(RdbgMain.doc_msg "ne")^" (parent arcs only)
       tw_par: "^(RdbgMain.doc_msg "tw")^" (parent arcs only)
       ci_par: "^(RdbgMain.doc_msg "ci")^" (parent arcs only)
       fd_par: "^(RdbgMain.doc_msg "fd")^" (parent arcs only)
       sf_par: "^(RdbgMain.doc_msg "sf")^" (parent arcs only)
       pa_par: "^(RdbgMain.doc_msg "pa")^" (parent arcs only)
       os_par: "^(RdbgMain.doc_msg "os")^" (parent arcs only)

       + Moving commands [*]
         sd: "^(RdbgMain.doc_msg "sd")^"
         nd: "^(RdbgMain.doc_msg "nd")^"
         bd: "^(RdbgMain.doc_msg "bd")^"
         nr: "^(RdbgMain.doc_msg "nr")^"
         pr: "^(RdbgMain.doc_msg "pr")^"
      [*] in order to change the current graph drawing engine, you can use
          the dot_view reference as follows:
           dot_view := ci
");;


(**********************************************************************)
(* ok, let's start the debugging session! *)

let pdf_viewer = (* try hard to find a working pdf viewer! *)
  try
    let res = Unix.getenv "PDF_VIEWER" in (* may raise Not_found *)
    if Sys.command (Printf.sprintf "which %s" res) = 0 then res else raise Not_found
  with Not_found ->
    if Sys.command "which see" = 0 then "see" else
    if Sys.command "which zathura" = 0 then "zathura" else
    if Sys.command "which okular" = 0 then "okular" else
    if Sys.command "which xpdf" = 0 then "xpdf" else
    if Sys.command "which evince" = 0 then "evince" else
    if Sys.command "which acroread" = 0 then "acroread" else (
      Printf.printf "Warning: no pdf viewer found to visualize %s\n%!" dotfile;
      "ls"
    )

let graph_view () =
  !dot_view ();
  let cmd = Printf.sprintf "%s sasa-%s.pdf&" pdf_viewer dotfile in
  Printf.printf "%s\n!" cmd;
  ignore(Sys.command cmd)

;;
(* let _ = graph_view () *)
let gv = graph_view

let _ =
  add_doc_entry "gv" "unit -> unit"
    "launch a pdf viewer of the current network

     in order to change the current graph drawing engine, you can use
     the dot_view reference as follows:
     (rdbg)  dot_view := ci;;
     or
     (rdbg)  dot_view := fd;;

     In order to get the list of  graph drawing engine:
     (rdbg)   h sasa
     "
    "sasa" "sasa-rdbg-cmds.ml";
