open Sasacore
open Topology
type probability = float (*between 0 and 1*)
type prob_udg = (float -> probability) 

type node_udg =  node_id*float*float
type plan_udg = node_udg list

let prob_from_list : (float list -> float -> prob_udg) =
  fun l r d ->  (List.nth l (int_of_float (((float_of_int (List.length l))/.r)*.d)))

let linear_prob : (float -> prob_udg) =
  fun r -> (fun d -> d/.r)

let prob_from_constant : (float -> prob_udg) =
  fun  x _ -> x

let recommend_radius : (int -> float -> float -> float -> float) =
  fun  nb_node mean_deg h w ->
  if(mean_deg > float_of_int nb_node) then
    failwith "Error : the number of node should be greater or equal than mean degree"
  else
   sqrt ((h*.w)*.mean_deg/.(Float.pi*.(float_of_int nb_node)))

let recommend_nb_node : (float -> float -> float -> float -> int) =
  fun radius mean_deg h w ->
  (int_of_float ((mean_deg*.h*.w)/.((radius**2.)*.Float.pi)))+1

let compute_mean_degree : (int -> float -> float -> float -> float) =
  fun nb_node radius h w ->
  ((radius**2.)*.Float.pi*.(float_of_int nb_node))/.(h*.w)

(******************************************************************************)

let rec make_nodes_dot_udg : (node_udg list -> float -> float -> string) =
  (*Create a string in the dot syntax from a node list*)
  fun nudg r0 r1 ->
  match nudg with
  | [] -> ""
  | head::tail ->
     let (node,x,y) = head in
     (Printf.sprintf "%s [pos=\"%f,%f!\"]\n" node x y )^
       let draw_rad = (if (r0 > 0.) then
                         (Printf.sprintf "%srad [pos=\"%f,%f!\",width=%f, length=%f,shape = circle,label=\"\",color=\"red\"]\n" 
                            node x y (2.*.r0) (2.*.r0) ) else "")^
                        if(r1 > r0) then
                          (Printf.sprintf "%srad2 [pos=\"%f,%f!\",width=%f, length=%f,shape = circle,label=\"\",color=\"lightblue\"]\n" 
                             node x y (2.*.r1) (2.*.r1) ) else "" in
       draw_rad^(make_nodes_dot_udg tail r0 r1)

(* XXX duplicates GraphGen.to_dot_string *)
let make_links_dot g =
  let node_to_link_string n =
    let pred = g.pred n.id in
    let links =
      List.map
        (fun (w,neighbour) ->
          (match w with
           | 1 ->
              if n.id < neighbour then
                Printf.sprintf ("  %s -- %s") neighbour n.id
              else
                Printf.sprintf ("  %s -- %s") n.id neighbour
           | x ->
              Printf.sprintf ("  %s -- %s [weight=%d]") neighbour n.id x
          )
        )
        pred
    in
    links
  in
  let links = List.map node_to_link_string g.nodes in
  let links = List.sort_uniq compare (List.flatten links) in 
  let links = String.concat "\n" links in
  links

       
let make_dot_udg_qudg : (Topology.t -> plan_udg -> (float*float) ->
                         ?r0:float  -> ?r1:float  -> string -> unit) =
  (*Create a dot file from a graph*)
  fun t plan dim ?(r0 = 0.) ?(r1 = 0.) file_name ->
  let name = ref "graph0" in (* default name *)
  let f = (if file_name = "" then stdout else
             (
               name := file_name;
               (try ( (* remove all extensions. So if name = ref "tt.dot.dot" 
at the beginning, at the end name = ref "tt". *)
                  while true do
                    name := Filename.chop_extension !name;
                  done;
                ) with Invalid_argument _ -> ());
               open_out file_name
             )
          ) in
  let (w,l) = dim in
  let mpos = if(r0 > 0. || r1 > 0.) then 
               (Printf.sprintf "size = \"%f,%f!\"\ntopLeft [pos=\"%f,%f!\",style=invis]\nlowRight [pos=\"0,0!\",style = invis]\nnode [fixedsize=false,shape=circle]\n" w l w l) 
             else "" in
  let dot = (Printf.sprintf "graph %s {\n\n"!name )^mpos
            ^(make_nodes_dot_udg plan r0 r1) ^ "\n" ^ (make_links_dot t) ^ "\n}\n" in
  Printf.fprintf f "%s" dot;
  flush f;
  close_out f
