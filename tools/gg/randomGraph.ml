open Sasacore
open Topology
open Ggcore
open List

type node_succ_t = (node_id, node_id) Hashtbl.t
type node_pred_t = (node_id, int * node_id) Hashtbl.t
type probability = float (*between 0 and 1*)

(* Add symmetric edges when not directed *)
let update_tbl directed succ pred =
  if not directed then (
    Hashtbl.iter
      (fun n1 n2 ->
         if not (List.mem n1 (Hashtbl.find_all succ n2)) then
           Hashtbl.add succ n2 n1)
      (Hashtbl.copy succ);
    Hashtbl.iter
      (fun n1 (_,n2) ->
         if not (List.mem (1,n1) (Hashtbl.find_all pred n2)) then
         Hashtbl.add pred n2 (1,n1))
      (Hashtbl.copy pred);
  )


let gen_ER : (bool -> int -> probability -> Topology.t) =
  fun directed nb p ->
  let (node_succ:node_succ_t) = Hashtbl.create nb in
  let (node_pred:node_pred_t) = Hashtbl.create nb in
  let nodes = create_nodes "p" (0,nb) in
  let succ n = Hashtbl.find_all node_succ n in
  let pred n = Hashtbl.find_all node_pred n in
  let add_succ n m =
    Hashtbl.add node_succ n m;
    Hashtbl.add node_pred m (1,n)
  in
  iteri (fun i n ->
      iteri (fun j m ->
          if not directed then (
            if i < j && (Random.float 1.) < p then add_succ n m)
          else (
            if i <> j && (Random.float 1.) < p then add_succ n m)
        )
        nodes
    )
    nodes;
  let nl = id_to_empty_nodes nodes in
  update_tbl directed node_succ node_pred;
 {
    nodes = nl;
    succ = succ;
    pred = pred;
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }

(* split the list, the 1st one having the first m nodes *)
let get_m_nodes m nodes =
  let rec f i acc nodes =
  match nodes with
  | [] -> assert false
  | (node::tail) ->
    if i > 0 then
      f (i-1) (node::acc) tail
    else
      List.rev acc, nodes
  in
  f m [] nodes

let (neighbours_BA : node_id list -> int -> node_succ_t -> node_pred_t ->
     ((node_id -> node_id list) * (node_id -> (int * node_id) list))) =
  fun nodes m node_succ node_pred ->
  let d_tot = 2 * m in
  let m_nodes, nodes = get_m_nodes m nodes in
  List.iter (fun n ->
      Hashtbl.remove node_succ n;
      Hashtbl.remove node_pred n;
    )
    m_nodes;
  match nodes with
  | [] -> assert false
  | head::nodes ->
    List.iter (fun n ->
        Hashtbl.add node_succ n head;
        Hashtbl.add node_succ head n;
        Hashtbl.add node_pred head (1,n);
        Hashtbl.add node_pred n (1,head)
      )
      m_nodes;
    (*init terminée. On a un graph connexe pour les m premiers points,
      nodes ne contient que les points non ajoutés*)
    ignore (
      fold_left
        (fun deg_tot node ->
           let succ = ref [] in (* hold the m new links for node *)
           let deg_ref = ref deg_tot in
           for _ = 0 to m-1 do (*for each edge to create*)
             let ran = Random.int !deg_ref in
             let visited = ref [] in
             ignore (
               Hashtbl.fold
                 (fun n_id _ r ->
                    let skip = List.mem n_id !visited in
                    if not skip then visited := n_id::!visited;
                    (* skip is used to make sure n_id is considered once *)
                    if (r >= 0 && not skip) then (
                      let n_succ = Hashtbl.find_all node_succ n_id in
                      let d_n_id  = length n_succ in
                      let r = r - d_n_id in
                      if r < 0 && not skip then (
                        succ := (1,n_id)::!succ;
                        deg_ref := !deg_ref - d_n_id
                      );
                      r
                    )
                    else r
                 )
                 node_succ
                 ran);
           done;
           assert (length !succ = m);
           List.iter (fun (w,s) ->
               Hashtbl.add node_succ node s;
               Hashtbl.add node_succ s node;
               
               Hashtbl.add node_pred s (w,node);
               Hashtbl.add node_pred node (w,s)
             )
             !succ;
           (deg_tot + (2 * m))
        )
        d_tot
        nodes
    );
    (fun n -> Hashtbl.find_all node_succ n),
    (fun n -> Hashtbl.find_all node_pred n)

let gen_BA : (bool -> int -> int -> Topology.t) =
  fun directed nb m ->
  if directed then (
    Printf.eprintf "A Barabasi–Albert graph cannot be directed\n%!";
    exit 2
  );
  let (node_succ:node_succ_t) = Hashtbl.create nb in
  let (node_pred:node_pred_t) = Hashtbl.create nb in
  let nodes = create_nodes "p" (0,nb) in
    if nb < m + 1 then
     (Printf.eprintf
        "Error: with -m %d, the node number needs to be at least %d (it is %d).\n%!"
        m (m+1) nb;
      exit 2
    );
    let nl = id_to_empty_nodes nodes in
    let succ, pred = neighbours_BA nodes m node_succ node_pred in
    {
      nodes = nl;
      succ = succ;
      pred = pred;
      of_id = get_of_id nl;
      directed = directed;
      attributes = []
    }

let (pre_rand_tree : bool -> GraphGen_arg.tree_edge -> node_succ_t ->
     node_pred_t -> node_id list ->
     ((node_id -> node_id list) * (node_id -> (int * node_id) list))) =
  fun dir tree_edge node_succ node_pred ->
  function
  | [] -> failwith "Tree Error : You need at least one nodes in your tree"
  | h::t ->
    ignore (List.fold_left (fun acc elem ->
        (* choose a random node *)
        let no = (List.nth acc (Random.int (List.length acc))) in
        (* add an option to control whether to add
           - down edges
           - up edges
           - both
        *)
        if tree_edge <> GraphGen_arg.OutTree then (
          (* add elem as a successor of node *)
          Hashtbl.add node_succ no elem;
          (* add node as a predecessor of elem *)
          Hashtbl.add node_pred elem (1,no)
        );
        if tree_edge <> GraphGen_arg.InTree then (
          (* add node as a successor of elem *)
          Hashtbl.add node_succ elem no;
          (* add elem as a predecessor of node *)
          Hashtbl.add node_pred no (1,elem)
        );
        (elem::acc)
    ) [h] (t));
    update_tbl dir node_succ node_pred;
    (fun n -> Hashtbl.find_all node_succ n),
    (fun n -> Hashtbl.find_all node_pred n)

let (rand_tree: GraphGen_arg.tree_edge -> bool -> bool -> int -> Topology.t) =
  fun tree_edge _rooted directed nb ->
  let (node_succ:node_succ_t) = Hashtbl.create nb in
  let (node_pred:node_pred_t) = Hashtbl.create nb in
  let node_base_name =  "p" in
  let nodes = "Root"::(create_nodes node_base_name (1,nb)) in
  let nl = id_to_empty_nodes nodes in
  let succ, pred = pre_rand_tree directed tree_edge node_succ node_pred nodes in
  {
    nodes = nl;
    succ = succ;
    pred = pred;
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }


type node_udg =  node_id*float*float
type plan_udg = node_udg list

let (make_plan_udg: node_id list -> float -> float -> plan_udg) =
  fun nodes x y ->
  List.map (fun elem -> (elem,(Random.float x),(Random.float y))) (nodes)

let (dist_udg: node_udg -> node_udg -> float) =
  fun n1 n2 ->
  let (_,x1,y1) = n1 and (_,x2,y2) = n2 in
  sqrt (((x1-.x2)**2.) +. ((y1 -. y2)**2.))

let gen_qudg : (bool -> int -> float -> float -> float -> float -> float ->
                (Topology.t * plan_udg)) =
  fun directed nb x y r0 r1 p ->
  let (node_succ:node_succ_t) = Hashtbl.create nb in
  let (node_pred:node_pred_t) = Hashtbl.create nb in
  let nodes = create_nodes "p" (0,nb) in
  let pl = (make_plan_udg nodes x y) in
  List.iter (fun n_udg ->
      let (node, _, _) = n_udg in
      List.iter (fun elem ->
          let (n,_,_) = elem and dist = dist_udg n_udg elem in
          if node <> n &&
             (dist <= r0 || (dist <= r1 && Random.float 1. <= p))
             (* e.g. if the node is : (within the radius r0)
                or : (within the radius r1, with a probability of p) *)
          then (
            Hashtbl.add node_succ node n;
            Hashtbl.add node_pred n (1,node);
          )
        ) pl
    ) pl;
  let nl = id_to_empty_nodes nodes in
  update_tbl directed node_succ node_pred;
  {
    nodes = nl;
    succ = (fun n -> Hashtbl.find_all node_succ n);
    pred = (fun n -> Hashtbl.find_all node_pred n);
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  },pl

let (gen_udg : bool -> int -> float -> float -> float ->
     (Topology.t * plan_udg)) =
  fun directed nb x y r -> (gen_qudg directed nb x y r 0. 0.)
