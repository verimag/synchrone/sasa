open Sasacore
open Topology
open Ggcore
open List

type node_succ_t = (string, string) Hashtbl.t
type node_pred_t = (string, int * string) Hashtbl.t

(* Add symmetric edges when not directed *)
let update_tbl directed succ pred =
  if not directed then (
    Hashtbl.iter
      (fun n1 n2 ->
         if not (List.mem n1 (Hashtbl.find_all succ n2)) then
           Hashtbl.add succ n2 n1)
      (Hashtbl.copy succ);
    Hashtbl.iter
      (fun n1 (_, n2) ->
         if not (List.mem (1,n1) (Hashtbl.find_all pred n2)) then
         Hashtbl.add pred n2 (1,n1))
      (Hashtbl.copy pred);
  )

let nid_list_remove : (node_id list -> node_id -> node_id list) =
  fun l e ->
  (* remove e from l, and add the weight 1 to elements of l *)
  rev (fold_left (fun acc elem ->
      if(elem <> e) then elem::acc else acc ) [] l)

let (gen_clique: bool -> int -> Topology.t) =
  fun directed nb ->
  let (node_succ:node_succ_t) = Hashtbl.create nb
  and (node_pred:node_pred_t) = Hashtbl.create nb
  and nodes = create_nodes "p" (0,nb)
  in
  List.iter
    (fun node_id ->
       List.iter
         (fun x ->
            if x < node_id then (
              Hashtbl.add node_succ node_id x;
              Hashtbl.add node_pred x (1,node_id)
            )
         )
         (nid_list_remove nodes node_id))
    nodes;
  let nl = id_to_empty_nodes nodes in
  update_tbl directed node_succ node_pred;
  {
    nodes = nl;
    succ = (fun n -> Hashtbl.find_all node_succ n);
    pred = (fun n -> Hashtbl.find_all node_pred n);
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }

let (gen_star: bool -> int -> Topology.t) =
  fun directed nb ->
  let (node_succ:node_succ_t) = Hashtbl.create nb in
  let (node_pred:node_pred_t) = Hashtbl.create nb in
  let nodes = create_nodes "p" (1,nb) in

  List.iter    (fun n -> Hashtbl.add node_succ "root" n) nodes;
  List.iter (fun n -> Hashtbl.add node_pred n (1,"root")) nodes;
  update_tbl directed node_succ node_pred;
  let nl = id_to_empty_nodes ("root"::nodes) in
  {
    nodes = nl;
    succ = (fun n -> Hashtbl.find_all node_succ n);
    pred = (fun n -> Hashtbl.find_all node_pred n);
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }

let neighbours_ring_chain dir chain nodes =
  let nb = length nodes in
  let node_succ:node_succ_t = Hashtbl.create nb in
  let (node_pred:node_pred_t) = Hashtbl.create nb in
  let nodes2 = match nodes with
    | n1::t -> t@[n1] (* circular permutation *)
    | _ ->  assert false
  in
  let nodes  = if chain then List.tl nodes  else nodes
  and nodes2 = if chain then List.tl nodes2 else nodes2 in (* cut the ring to get a chain *)
    List.iter2 (fun n1 n2 ->
        Hashtbl.add node_succ n1 n2;
        Hashtbl.add node_pred n2 (1,n1)
    )
      nodes nodes2 ;
    update_tbl dir node_succ node_pred;
    (fun n -> Hashtbl.find_all node_succ n),
    (fun n -> Hashtbl.find_all node_pred n)


let (gen_ring: bool -> int -> Topology.t) =
  fun directed nb ->
  let nodes = (create_nodes "p" (0,nb)) in
  let nl = id_to_empty_nodes nodes in
  let succ, pred = neighbours_ring_chain directed false nodes in
  {
    nodes = nl;
    succ = succ;
    pred = pred;
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }


let (gen_chain: bool -> int -> Topology.t) =
  fun directed nb ->
  let nodes = (create_nodes "p" (0,nb)) in
  let nl = id_to_empty_nodes nodes in
  let succ, pred = neighbours_ring_chain directed true nodes in
  {
    nodes = nl;
    succ = succ;
    pred = pred;
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }


let (gen_grid: bool -> int -> int -> Topology.t) =
  fun directed length width ->
  Printf.eprintf "Computing a %ix%i grid...\n" length width;
  flush stderr;
  let nb = length*width in
  let nodes = (create_nodes "p" (0,nb))
  and succ_t = Hashtbl.create nb
  and pred_t = Hashtbl.create nb
  in
  for i=0 to length-1 do
    for j=0 to width-1 do
      let n_id = (List.nth nodes (j*length + i)) in
      let bl = if(i=0) then 0 else -1 in
      let br = if(i=(length-1)) then 0 else 1 in
      let bup = if(j=0) then 0 else -1 in
      let bdown = if(j=(width-1)) then 0 else 1 in
      for ip=bl to br do
        for jp=bup to bdown do
          let n_id2 = List.nth nodes ((j+jp)*length + i+ip) in
          if not ((ip=0 && jp=0) || (ip=jp) || (ip = -jp)) && (n_id<n_id2)
          then (
            Hashtbl.add succ_t n_id n_id2;
            Hashtbl.add pred_t n_id2 (1, n_id)
          )
        done;
      done;
    done;
  done;
  let nl = id_to_empty_nodes nodes in
  update_tbl directed succ_t pred_t;
  Printf.eprintf "Computing a %ix%i grid: Done!\n" length width;flush stderr;
  {
    nodes = nl;
    succ = (fun nid -> Hashtbl.find_all succ_t nid);
    pred = (fun nid -> Hashtbl.find_all pred_t nid);
    of_id = get_of_id nl;
    directed = directed;
    attributes = []
  }

let rec link_hypercube_nodes :
  (node_id array -> node_succ_t -> node_pred_t -> unit) =
  fun na n_s n_p ->
  let len = Array.length na in
  let mid = len / 2 in
  if len > 1 then
    let n1 = (Array.sub na 0 mid)
    and n2 = (Array.sub na mid mid) in
    link_hypercube_nodes n1 n_s n_p;
    link_hypercube_nodes n2 n_s n_p;
    Array.iter2 (fun node1 node2 ->
        if node1 < node2 then (
          Hashtbl.add n_s node1 node2
        )
      ) n1 n2

let (neighbours_hyper_cube : bool -> node_id list ->
     (node_id -> node_id list) * (node_id -> (int * node_id) list)) =
  fun dir nl ->
    let na = Array.of_list nl in
    let (node_succ:node_succ_t) = Hashtbl.create (Array.length na) in
    let (node_pred:node_pred_t) = Hashtbl.create (Array.length na) in
    link_hypercube_nodes na node_succ node_pred;
    update_tbl dir node_succ node_pred;
    (fun n -> Hashtbl.find_all node_succ n),
    (fun n -> Hashtbl.find_all node_pred n)

let gen_hyper_cube : (bool -> int -> Topology.t) =
  fun directed dim ->
    let nb = int_of_float (2. ** (float_of_int dim)) in
    let nodes = (create_nodes "p" (0,nb)) in
    let nl = id_to_empty_nodes nodes in
    let succ, pred = neighbours_hyper_cube directed nodes in
    {
      nodes = nl;
      succ = succ;
      pred = pred;
      of_id = get_of_id nl;
      directed = directed;
      attributes = []
  }
