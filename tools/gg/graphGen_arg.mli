type action = string

type grid_arg = {
  mutable width: int;
  mutable height: int;
}

type qudg_arg = {
  mutable width: float;
  mutable height: float;
  mutable radius: float;
  mutable r1: float;
  mutable p: float;
}

type er_prob = float (*between 0 and 1*)
type ba_m = int (*positive*)
type tree_edge = InTree | OutTree | InOutTree

type t = {
  mutable outputFile: string;
  mutable dotUDG: string;
  mutable dotUDGrad: string;
  mutable action: action;

  mutable n : int;
  mutable tree_edge : tree_edge;
  mutable grid : grid_arg;
  mutable er : er_prob;
  mutable ba : ba_m;
  mutable qudg : qudg_arg;

  mutable seed : int option;
  mutable silent : bool;
  mutable connected : bool;
  mutable directed : bool;
  mutable diameter : bool;
  mutable rooted : bool;

  mutable _args : (string * Arg.spec * string) list;
  mutable _man : (string * (string list * action) list) list;

  mutable _others : string list;
  mutable _margin : int;
}

val parse : (string array -> t)
