open Sasacore

(** Generate  a clique of  n nodes; the bool  states whether the
   graph is directed *)
val gen_clique : bool -> int -> Topology.t

(** Generate a star of n nodes *)
val gen_star : bool -> int -> Topology.t

(** Generate a ring of n nodes *)
val gen_ring : bool -> int -> Topology.t

(** Generate a chain of n nodes *)
val gen_chain : bool -> int -> Topology.t

(** Take  the two dimensions  i,j of the grid  and return a  grid graph
   with these dimensions *)
val gen_grid : bool -> int -> int -> Topology.t

(** Take a dimension and generate hyper cube graph of this dimension *)
val gen_hyper_cube : bool -> int -> Topology.t
