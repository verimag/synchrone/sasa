open Sasacore.Topology

type node_ofId_t = (string, node) Hashtbl.t

(* TODO: should be tail-recursive+tmp is a poor name *)
let rec create_nodes : (string -> int*int -> node_id list) =
  (* Create names from a generic name *)
  fun name (start,finish) ->
  if start >= finish then [] else
  let tmp : node_id = name ^ (string_of_int (start)) in 
  tmp::(create_nodes name (start+1, finish))

let id_to_empty_nodes : (node_id list -> node list) = 
    List.map (fun n_id -> {id = n_id; file = ""; init = ""})

let get_of_id : (node list->(node_id-> node)) =
  fun nl ->
  let (of_id_hash:node_ofId_t) = Hashtbl.create (List.length nl) in
  List.iter (fun node -> Hashtbl.replace of_id_hash node.id node) nl;
  (fun n -> try Hashtbl.find of_id_hash n with Not_found ->
        failwith (n^ " unknown node id"))
