open Sasacore
open Topology
type probability = float (*between 0 and 1*)

type node_udg =  node_id*float*float
type plan_udg = node_udg list

(** [gen_ER n p] generate a graph using Erdos Renyi model, 
    of n nodes and of probability p for each possible edge to appear. *)
val gen_ER : bool -> int -> probability -> Topology.t

(** [gen_BA  n m] generate a  graph using Barabasi–Albert model,  of n
   nodes and with m edges added for  each new node.  m has to be lower
   than n.

    The initialization is  a star of m+1 nodes, with  the (m+1)th node
   being the  root.  Barabasi–Albert model  is used for  the remaining
   nodes *)
val gen_BA : bool -> int -> int -> Topology.t


(** [rand_tree rooted directed n] generate a random tree of n nodes *)
val rand_tree: GraphGen_arg.tree_edge -> bool -> bool -> int -> Topology.t

(** [gen_udg  nb x  y r] generate  a graph using  the Unit  Disc Graph
   model, of  n nodes.  w and  h are the  width and the height  of the
   area in which the nodes are  randomly disposed, and r is the radius
   around each node,  in which all the other nodes  will be neighbors.
   *)
val gen_udg : bool -> int -> float -> float -> float -> (Topology.t * plan_udg)

(** [gen_qudg nb  x y r0 r1  p] generate a graph using  the Quasi Unit
   Disc Graph model, of n nodes.  w and h are the width and the height
   of the area in which the nodes are randomly disposed.  r0, r1 and p
   are three values to determine if two nodes, at a distance d of each
   other, are neighbors. If d <= r0, they are neighbors. Otherwise, if
   d <= r1, they have a probability of p of being neighbors.  *)
val gen_qudg: bool -> int -> float -> float -> float -> float -> float ->
  (Topology.t * plan_udg)
