open Sasacore
open Topology
type probability = float (*between 0 and 1*)
type prob_udg = (float -> probability) 

type node_udg =  node_id*float*float
type plan_udg = node_udg list


(** create a probability function for UDG that always return the same probability *)
val prob_from_constant: (float -> prob_udg)

(** [proba_from_list fl r] create a probability function for UDG that changes the probability according to the distance.
    It cuts r into (length fl), and attribute the first element of fl to the first slice (closest to the center), and so on.
    For example, [proba_from_list [1.;0.5;0.3;0.1] 10 d] will return 1. if 0 <= d < 2.5, 0.5 if 2.5 <= d < 5, and so on.
    Note that r must be equal to the radius of the Unit Disc *)
val prob_from_list: (float list -> float -> prob_udg)

(** [linear_proba r] gives a function that, for an input d (distance) outputs d/r. 
    If r is the Unit Disc radius and d the distance between two points, 
    it outputs a probability that is higher for a low d and lower for a high d. *)
val linear_prob: (float -> prob_udg)

(** [recommend_radius n mean_deg h w] returns the recommended radius to give to UDG 
    in order to get a mean degree approximately equal to mean_deg, 
    knowing there's n nodes and the UDG is applied in an area of height h and width w. *)
val recommend_radius : (int -> float -> float -> float -> float)

(** [reccomend_nb_node r mean_deg h w] returns the recommended number of nodes 
    to give to UDG in order to get a mean degree approximately equal to mean_deg, 
    knowing the radius is r and the UDG is applied in an area of height h and width w. *)
val recommend_nb_node : (float -> float -> float -> float -> int)

(** [compute_mean_degree n r h w] computes and return the approximative mean degree of 
    an UDG graph with the same arguments. *)
val compute_mean_degree : (int -> float -> float -> float -> float)

(** [make_dot_udg g p (h,w) ~r=r f] Creates a DOT file to represent the UDG area, 
    r being the radius, g being the graph, p being the UDG area, 
    and (h,w) being the dimensions of the plan.
    If no radius is given, they won't appear on the pdf.
    If you have Grahviz, we advice using 'twopi -Tpdf f' to obtain a pdf. *)
val make_dot_udg_qudg : (Topology.t -> plan_udg -> (float*float) -> ?r0:float -> ?r1:float -> string -> unit)
