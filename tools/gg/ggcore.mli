open Sasacore.Topology

(** Create a name (i.d. node ID) list from a generic name *)
val create_nodes : (string -> int*int -> node_id list)

(** creates a list of nodes, each  having an ID from the list given in
   argument, and no file or init value. *)
val id_to_empty_nodes : (node_id list -> node list)

(** create a function to get a node from it's ID *)
val get_of_id : (node list -> (node_id-> node))
